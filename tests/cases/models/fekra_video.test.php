<?php
/* FekraVideo Test cases generated on: 2014-01-31 19:04:49 : 1391187889*/
App::import('Model', 'FekraVideo');

class FekraVideoTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_video', 'app.fekra', 'app.category', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->FekraVideo =& ClassRegistry::init('FekraVideo');
	}

	function endTest() {
		unset($this->FekraVideo);
		ClassRegistry::flush();
	}

}
