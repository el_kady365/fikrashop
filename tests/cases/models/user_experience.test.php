<?php
/* UserExperience Test cases generated on: 2014-01-09 00:48:13 : 1389221293*/
App::import('Model', 'UserExperience');

class UserExperienceTestCase extends CakeTestCase {
	var $fixtures = array('app.user_experience', 'app.user', 'app.country', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->UserExperience =& ClassRegistry::init('UserExperience');
	}

	function endTest() {
		unset($this->UserExperience);
		ClassRegistry::flush();
	}

}
