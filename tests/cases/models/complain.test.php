<?php
/* Complain Test cases generated on: 2014-06-30 01:35:19 : 1404081319*/
App::import('Model', 'Complain');

class ComplainTestCase extends CakeTestCase {
	var $fixtures = array('app.complain', 'app.transaction', 'app.fekra_phase', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_video', 'app.fekra_comment', 'app.fekra_job');

	function startTest() {
		$this->Complain =& ClassRegistry::init('Complain');
	}

	function endTest() {
		unset($this->Complain);
		ClassRegistry::flush();
	}

}
