<?php
/* FekraImage Test cases generated on: 2014-01-31 17:05:36 : 1391180736*/
App::import('Model', 'FekraImage');

class FekraImageTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_image', 'app.fekra', 'app.category', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->FekraImage =& ClassRegistry::init('FekraImage');
	}

	function endTest() {
		unset($this->FekraImage);
		ClassRegistry::flush();
	}

}
