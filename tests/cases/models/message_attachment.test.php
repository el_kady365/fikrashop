<?php
/* MessageAttachment Test cases generated on: 2014-07-23 02:06:06 : 1406070366*/
App::import('Model', 'MessageAttachment');

class MessageAttachmentTestCase extends CakeTestCase {
	var $fixtures = array('app.message_attachment', 'app.message', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_user', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->MessageAttachment =& ClassRegistry::init('MessageAttachment');
	}

	function endTest() {
		unset($this->MessageAttachment);
		ClassRegistry::flush();
	}

}
