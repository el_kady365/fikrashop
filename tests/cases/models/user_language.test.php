<?php
/* UserLanguage Test cases generated on: 2014-01-09 00:09:50 : 1389218990*/
App::import('Model', 'UserLanguage');

class UserLanguageTestCase extends CakeTestCase {
	var $fixtures = array('app.user_language', 'app.user', 'app.country', 'app.user_skill');

	function startTest() {
		$this->UserLanguage =& ClassRegistry::init('UserLanguage');
	}

	function endTest() {
		unset($this->UserLanguage);
		ClassRegistry::flush();
	}

}
