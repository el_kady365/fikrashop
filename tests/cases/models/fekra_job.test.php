<?php
/* FekraJob Test cases generated on: 2014-06-14 21:43:42 : 1402771422*/
App::import('Model', 'FekraJob');

class FekraJobTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_job', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->FekraJob =& ClassRegistry::init('FekraJob');
	}

	function endTest() {
		unset($this->FekraJob);
		ClassRegistry::flush();
	}

}
