<?php
/* FekraPhase Test cases generated on: 2014-02-04 02:29:32 : 1391473772*/
App::import('Model', 'FekraPhase');

class FekraPhaseTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_phase', 'app.fekra', 'app.category', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_image', 'app.fekra_video');

	function startTest() {
		$this->FekraPhase =& ClassRegistry::init('FekraPhase');
	}

	function endTest() {
		unset($this->FekraPhase);
		ClassRegistry::flush();
	}

}
