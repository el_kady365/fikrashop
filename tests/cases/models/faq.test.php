<?php
/* Faq Test cases generated on: 2013-11-25 23:33:03 : 1385415183*/
App::import('Model', 'Faq');

class FaqTestCase extends CakeTestCase {
	var $fixtures = array('app.faq');

	function startTest() {
		$this->Faq =& ClassRegistry::init('Faq');
	}

	function endTest() {
		unset($this->Faq);
		ClassRegistry::flush();
	}

}
