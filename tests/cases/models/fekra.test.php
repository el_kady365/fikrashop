<?php
/* Fekra Test cases generated on: 2014-01-27 23:29:12 : 1390858152*/
App::import('Model', 'Fekra');

class FekraTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra', 'app.category', 'app.fekra_user');

	function startTest() {
		$this->Fekra =& ClassRegistry::init('Fekra');
	}

	function endTest() {
		unset($this->Fekra);
		ClassRegistry::flush();
	}

}
