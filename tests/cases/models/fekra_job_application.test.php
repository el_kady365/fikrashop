<?php
/* FekraJobApplication Test cases generated on: 2014-06-15 17:10:05 : 1402841405*/
App::import('Model', 'FekraJobApplication');

class FekraJobApplicationTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_job_application', 'app.fekra_job', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->FekraJobApplication =& ClassRegistry::init('FekraJobApplication');
	}

	function endTest() {
		unset($this->FekraJobApplication);
		ClassRegistry::flush();
	}

}
