<?php
/* UserSkill Test cases generated on: 2013-12-26 07:14:26 : 1388034866*/
App::import('Model', 'UserSkill');

class UserSkillTestCase extends CakeTestCase {
	var $fixtures = array('app.user_skill', 'app.user', 'app.country');

	function startTest() {
		$this->UserSkill =& ClassRegistry::init('UserSkill');
	}

	function endTest() {
		unset($this->UserSkill);
		ClassRegistry::flush();
	}

}
