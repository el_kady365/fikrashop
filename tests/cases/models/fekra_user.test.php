<?php
/* FekraUser Test cases generated on: 2014-01-29 22:49:32 : 1391028572*/
App::import('Model', 'FekraUser');

class FekraUserTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_user', 'app.fekra', 'app.category', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->FekraUser =& ClassRegistry::init('FekraUser');
	}

	function endTest() {
		unset($this->FekraUser);
		ClassRegistry::flush();
	}

}
