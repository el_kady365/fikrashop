<?php
/* FekraComment Test cases generated on: 2014-03-06 01:35:04 : 1394062504*/
App::import('Model', 'FekraComment');

class FekraCommentTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_comment', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video');

	function startTest() {
		$this->FekraComment =& ClassRegistry::init('FekraComment');
	}

	function endTest() {
		unset($this->FekraComment);
		ClassRegistry::flush();
	}

}
