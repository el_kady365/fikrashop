<?php
/* BoardRating Test cases generated on: 2014-03-20 00:16:04 : 1395267364*/
App::import('Model', 'BoardRating');

class BoardRatingTestCase extends CakeTestCase {
	var $fixtures = array('app.board_rating', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->BoardRating =& ClassRegistry::init('BoardRating');
	}

	function endTest() {
		unset($this->BoardRating);
		ClassRegistry::flush();
	}

}
