<?php
/* UserEducation Test cases generated on: 2014-01-17 23:27:00 : 1389994020*/
App::import('Model', 'UserEducation');

class UserEducationTestCase extends CakeTestCase {
	var $fixtures = array('app.user_education', 'app.user', 'app.country', 'app.user_experience', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->UserEducation =& ClassRegistry::init('UserEducation');
	}

	function endTest() {
		unset($this->UserEducation);
		ClassRegistry::flush();
	}

}
