<?php
/* Sponsor Test cases generated on: 2014-05-24 23:54:52 : 1400964892*/
App::import('Model', 'Sponsor');

class SponsorTestCase extends CakeTestCase {
	var $fixtures = array('app.sponsor');

	function startTest() {
		$this->Sponsor =& ClassRegistry::init('Sponsor');
	}

	function endTest() {
		unset($this->Sponsor);
		ClassRegistry::flush();
	}

}
