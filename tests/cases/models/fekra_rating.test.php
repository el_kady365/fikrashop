<?php
/* FekraRating Test cases generated on: 2014-03-20 00:15:53 : 1395267353*/
App::import('Model', 'FekraRating');

class FekraRatingTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_rating', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->FekraRating =& ClassRegistry::init('FekraRating');
	}

	function endTest() {
		unset($this->FekraRating);
		ClassRegistry::flush();
	}

}
