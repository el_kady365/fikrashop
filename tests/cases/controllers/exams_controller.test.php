<?php
/* Exams Test cases generated on: 2013-05-12 02:11:13 : 1368313873*/
App::import('Controller', 'Exams');

class TestExamsController extends ExamsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ExamsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.exam', 'app.teacher', 'app.group', 'app.student', 'app.student_exam');

	function startTest() {
		$this->Exams =& new TestExamsController();
		$this->Exams->constructClasses();
	}

	function endTest() {
		unset($this->Exams);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
