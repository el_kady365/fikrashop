<?php
/* UserSkills Test cases generated on: 2013-12-26 07:14:31 : 1388034871*/
App::import('Controller', 'UserSkills');

class TestUserSkillsController extends UserSkillsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class UserSkillsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.user_skill', 'app.user', 'app.country');

	function startTest() {
		$this->UserSkills =& new TestUserSkillsController();
		$this->UserSkills->constructClasses();
	}

	function endTest() {
		unset($this->UserSkills);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
