<?php
/* Sponsors Test cases generated on: 2014-05-24 23:55:12 : 1400964912*/
App::import('Controller', 'Sponsors');

class TestSponsorsController extends SponsorsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class SponsorsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.sponsor');

	function startTest() {
		$this->Sponsors =& new TestSponsorsController();
		$this->Sponsors->constructClasses();
	}

	function endTest() {
		unset($this->Sponsors);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
