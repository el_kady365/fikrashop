<?php
/* FekraComments Test cases generated on: 2014-03-06 01:35:18 : 1394062518*/
App::import('Controller', 'FekraComments');

class TestFekraCommentsController extends FekraCommentsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class FekraCommentsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_comment', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video');

	function startTest() {
		$this->FekraComments =& new TestFekraCommentsController();
		$this->FekraComments->constructClasses();
	}

	function endTest() {
		unset($this->FekraComments);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
