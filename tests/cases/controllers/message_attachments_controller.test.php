<?php
/* MessageAttachments Test cases generated on: 2014-09-13 23:57:26 : 1410645446*/
App::import('Controller', 'MessageAttachments');

class TestMessageAttachmentsController extends MessageAttachmentsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class MessageAttachmentsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.message_attachment', 'app.message', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_user', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->MessageAttachments =& new TestMessageAttachmentsController();
		$this->MessageAttachments->constructClasses();
	}

	function endTest() {
		unset($this->MessageAttachments);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
