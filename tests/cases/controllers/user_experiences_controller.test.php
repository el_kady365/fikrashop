<?php
/* UserExperiences Test cases generated on: 2014-01-09 00:48:33 : 1389221313*/
App::import('Controller', 'UserExperiences');

class TestUserExperiencesController extends UserExperiencesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class UserExperiencesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.user_experience', 'app.user', 'app.country', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->UserExperiences =& new TestUserExperiencesController();
		$this->UserExperiences->constructClasses();
	}

	function endTest() {
		unset($this->UserExperiences);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
