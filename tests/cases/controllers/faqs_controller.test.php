<?php
/* Faqs Test cases generated on: 2013-11-25 23:33:16 : 1385415196*/
App::import('Controller', 'Faqs');

class TestFaqsController extends FaqsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class FaqsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.faq');

	function startTest() {
		$this->Faqs =& new TestFaqsController();
		$this->Faqs->constructClasses();
	}

	function endTest() {
		unset($this->Faqs);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
