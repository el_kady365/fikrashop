<?php
/* Snippets Test cases generated on: 2013-11-11 22:08:03 : 1384200483*/
App::import('Controller', 'Snippets');

class TestSnippetsController extends SnippetsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class SnippetsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.snippet');

	function startTest() {
		$this->Snippets =& new TestSnippetsController();
		$this->Snippets->constructClasses();
	}

	function endTest() {
		unset($this->Snippets);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
