<?php
/* BoardRatings Test cases generated on: 2014-03-20 00:16:25 : 1395267385*/
App::import('Controller', 'BoardRatings');

class TestBoardRatingsController extends BoardRatingsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class BoardRatingsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.board_rating', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->BoardRatings =& new TestBoardRatingsController();
		$this->BoardRatings->constructClasses();
	}

	function endTest() {
		unset($this->BoardRatings);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
