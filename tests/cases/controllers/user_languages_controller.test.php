<?php
/* UserLanguages Test cases generated on: 2014-01-09 00:10:24 : 1389219024*/
App::import('Controller', 'UserLanguages');

class TestUserLanguagesController extends UserLanguagesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class UserLanguagesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.user_language', 'app.user', 'app.country', 'app.user_skill');

	function startTest() {
		$this->UserLanguages =& new TestUserLanguagesController();
		$this->UserLanguages->constructClasses();
	}

	function endTest() {
		unset($this->UserLanguages);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
