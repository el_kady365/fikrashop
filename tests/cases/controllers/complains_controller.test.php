<?php
/* Complains Test cases generated on: 2014-06-30 01:35:40 : 1404081340*/
App::import('Controller', 'Complains');

class TestComplainsController extends ComplainsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ComplainsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.complain', 'app.transaction', 'app.fekra_phase', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_video', 'app.fekra_comment', 'app.fekra_job');

	function startTest() {
		$this->Complains =& new TestComplainsController();
		$this->Complains->constructClasses();
	}

	function endTest() {
		unset($this->Complains);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
