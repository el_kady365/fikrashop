<?php
/* FekraJobs Test cases generated on: 2014-06-14 21:44:15 : 1402771455*/
App::import('Controller', 'FekraJobs');

class TestFekraJobsController extends FekraJobsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class FekraJobsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_job', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->FekraJobs =& new TestFekraJobsController();
		$this->FekraJobs->constructClasses();
	}

	function endTest() {
		unset($this->FekraJobs);
		ClassRegistry::flush();
	}

}
