<?php
/* FekraJobAttachments Test cases generated on: 2014-09-13 23:42:44 : 1410644564*/
App::import('Controller', 'FekraJobAttachments');

class TestFekraJobAttachmentsController extends FekraJobAttachmentsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class FekraJobAttachmentsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra_job_attachment', 'app.fekra_job', 'app.fekra', 'app.category', 'app.poll', 'app.poll_question', 'app.poll_question_option', 'app.poll_answer', 'app.fekra_user', 'app.user', 'app.country', 'app.user_education', 'app.user_experience', 'app.user_skill', 'app.user_language', 'app.fekra_phase', 'app.fekra_video', 'app.fekra_comment');

	function startTest() {
		$this->FekraJobAttachments =& new TestFekraJobAttachmentsController();
		$this->FekraJobAttachments->constructClasses();
	}

	function endTest() {
		unset($this->FekraJobAttachments);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
