<?php
/* Fekras Test cases generated on: 2014-01-27 23:29:26 : 1390858166*/
App::import('Controller', 'Fekras');

class TestFekrasController extends FekrasController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class FekrasControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.fekra', 'app.category', 'app.fekra_user');

	function startTest() {
		$this->Fekras =& new TestFekrasController();
		$this->Fekras->constructClasses();
	}

	function endTest() {
		unset($this->Fekras);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
