<?php
/* UserEducations Test cases generated on: 2014-01-17 23:28:02 : 1389994082*/
App::import('Controller', 'UserEducations');

class TestUserEducationsController extends UserEducationsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class UserEducationsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.user_education', 'app.user', 'app.country', 'app.user_experience', 'app.user_skill', 'app.user_language');

	function startTest() {
		$this->UserEducations =& new TestUserEducationsController();
		$this->UserEducations->constructClasses();
	}

	function endTest() {
		unset($this->UserEducations);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
