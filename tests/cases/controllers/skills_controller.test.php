<?php
/* Skills Test cases generated on: 2014-06-22 05:50:06 : 1403405406*/
App::import('Controller', 'Skills');

class TestSkillsController extends SkillsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class SkillsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.skill');

	function startTest() {
		$this->Skills =& new TestSkillsController();
		$this->Skills->constructClasses();
	}

	function endTest() {
		unset($this->Skills);
		ClassRegistry::flush();
	}

	function testAdminIndex() {

	}

	function testAdminView() {

	}

	function testAdminAdd() {

	}

	function testAdminEdit() {

	}

	function testAdminDelete() {

	}

	function testAdminDoOperation() {

	}

}
