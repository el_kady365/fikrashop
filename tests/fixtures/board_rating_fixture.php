<?php
/* BoardRating Fixture generated on: 2014-03-20 00:16:04 : 1395267364 */
class BoardRatingFixture extends CakeTestFixture {
var $name = 'BoardRating';

    var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'fekra_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'rating' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

    var $records = array(
		array(
			'id' => 1,
			'fekra_id' => 1,
			'user_id' => 1,
			'rating' => 1
		),
	);
}
