<?php
/* FekraUser Fixture generated on: 2014-01-29 22:49:32 : 1391028572 */
class FekraUserFixture extends CakeTestFixture {
var $name = 'FekraUser';

    var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'fekra_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'type' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 1),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

    var $records = array(
		array(
			'id' => 1,
			'fekra_id' => 1,
			'user_id' => 1,
			'type' => 1
		),
	);
}
