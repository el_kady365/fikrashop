<?php
/* MessageAttachment Fixture generated on: 2014-07-23 02:06:06 : 1406070366 */
class MessageAttachmentFixture extends CakeTestFixture {
var $name = 'MessageAttachment';

    var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'message_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'file' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

    var $records = array(
		array(
			'id' => 1,
			'message_id' => 1,
			'file' => 'Lorem ipsum dolor sit amet'
		),
	);
}
