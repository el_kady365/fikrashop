<?php

class Authenticate {

    public $config = array(
        'facebook' => array(
            'appId' => '571424699595865',
            'secret' => '50df4962784b931debb1ef20563310a3',
        ),
        'linkedin' => array(
            'api_key' => '77f0zppk3wklrh',
            'api_secret' => 'C0akfg7IEVnx0RW9',
            'redirect_uri' => "http://elkady.pxdev.com/users/register/linkedin",
            'scope' => 'r_fullprofile r_emailaddress r_contactinfo'
        )
    );
    public $user = array();

    function __construct($config = array()) {
        $this->config = array_merge($this->config, $config);
    }

    public function getUserProfile($provider) {
        if ($provider == 'facebook') {
            require 'facebook/facebook.php';

            $facebook = new Facebook($this->config[$provider]);

            $user = $facebook->getUser();

            if ($user) {
                $data = $facebook->api('/me', 'GET');

                $this->user['identifier'] = (array_key_exists('id', $data)) ? $data['id'] : "";
                $this->user['displayName'] = (array_key_exists('name', $data)) ? $data['name'] : "";
                $this->user['firstName'] = (array_key_exists('first_name', $data)) ? $data['first_name'] : "";
                $this->user['lastName'] = (array_key_exists('last_name', $data)) ? $data['last_name'] : "";
                $this->user['photoURL'] = "https://graph.facebook.com/" . $this->user['identifier'] . "/picture?type=large";
                $this->user['profileURL'] = (array_key_exists('link', $data)) ? $data['link'] : "";
                $this->user['webSiteURL'] = (array_key_exists('website', $data)) ? $data['website'] : "";
                $this->user['gender'] = (array_key_exists('gender', $data)) ? $data['gender'] : "";
                $this->user['description'] = (array_key_exists('bio', $data)) ? $data['bio'] : "";
                $this->user['email'] = (array_key_exists('email', $data)) ? $data['email'] : "";
                $this->user['region'] = (array_key_exists("location", $data) && array_key_exists("name", $data['location'])) ? $data['location']["name"] : "";

                if (array_key_exists('birthday', $data)) {
                    list($birthday_month, $birthday_day, $birthday_year) = explode("/", $data['birthday']);
                    $this->user['dateOfBirth'] = (int) $birthday_year . '-' . (int) $birthday_month . '-' . (int) $birthday_day;
                }


                return $this->user;
            } else {
                $statusUrl = $facebook->getLoginStatusUrl();
                $loginUrl = $facebook->getLoginUrl();
                header("Location: $loginUrl");
                exit;
            }
        } elseif ($provider == 'linkedin') {
            require 'linkedin/linkedin.php';

            $linkedin = new LinkedIn($this->config[$provider]);
           
// OAuth 2 Control Flow
            if (isset($_GET['error'])) {
// LinkedIn returned an error
                return false;
            } elseif (isset($_GET['code'])) {
// User authorized your application
// Get token so you can make API calls
                if ($linkedin->getAccessToken($_GET['code'])) {
                    $user_data = $linkedin->fetch('GET', '/v1/people/~:(id,first-name,last-name,picture-urls::(original),public-profile-url,picture-url,email-address,location:(country:(code)),date-of-birth,phone-numbers,summary,skills,positions,educations,languages)');

                    $this->user['identifier'] = strval($user_data->{'id'});
                    $this->user['firstName'] = (string) $user_data->{'firstName'};
                    $this->user['lastName'] = (string) $user_data->{'lastName'};
                    $this->user['displayName'] = trim($this->user['firstName'] . " " . $this->user['lastName']);
                    $this->user['profileURL'] = $user_data->{'publicProfileUrl'};
                    $this->user['email'] = (string) $user_data->{'emailAddress'};
                    $this->user['country_code'] = (string) $user_data->{'location'}->country->code;
                    $this->user['positions'] = $user_data->{'positions'};
                    $this->user['educations'] = $user_data->{'educations'};
                    $this->user['skills'] = $user_data->{'skills'};
                    $this->user['languages'] = $user_data->{'languages'};


                    $this->user['photoURL'] = (string) $user_data->{'pictureUrls'}->{'values'}[0];

                    $this->user['description'] = (string) $user_data->{'summary'};

                    if ($user_data->{'phoneNumbers'} && $user_data->{'phoneNumbers'}->{'values'}) {
                        foreach ($user_data->{'phoneNumbers'}->{'values'} as $key => $number) {
                            $this->user['phone'][$number->phoneType] = $number->phoneNumber;
                        }
                    } else {
                        $this->user['phone'] = null;
                    }

                    if ($user_data->{'dateOfBirth'}) {
                        $this->user['dateOfBirth'] = (string) $user_data->{'dateOfBirth'}->year . '-' . (string) $user_data->{'dateOfBirth'}->month . '-' . (string) $user_data->{'dateOfBirth'}->day;
                    }
                    return $this->user;


// CSRF attack? Or did you mix up your states?
                } else {
                    return false;
                }
            } else {

                if (empty($linkedin->data['access_token'])) {
// Start authorization process
                    $linkedin->getAuthorizationCode();
                }
            }
        }
    }

}

?>
