<?php

// You'll probably use a database


class Linkedin {

    var $config;
    var $data;

    function __construct($config) {
        $this->config = $config;
    }

    function getAuthorizationCode() {
        $params = array('response_type' => 'code',
            'client_id' => $this->config['api_key'],
            'scope' => $this->config['scope'],
            'state' => uniqid('', true), // unique long string
            'redirect_uri' => $this->config['redirect_uri'],
        );

        // Authentication request
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);

        // Needed to identify request when it returns to us
        $this->data['state'] = $params['state'];

        // Redirect user to authenticate
        header("Location: $url");
        exit;
    }

    function getAccessToken($code) {
        $params = array(
            'grant_type' => 'authorization_code',
            'client_id' => $this->config['api_key'],
            'client_secret' => $this->config['api_secret'],
            'code' => $code,
            'redirect_uri' => $this->config['redirect_uri'],
        );


        // Access Token request
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);

        // Tell streams to make a POST request
        $context = stream_context_create(
                array('http' =>
                    array('method' => 'GET',
                    )
                )
        );

        // Retrieve access token information

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

//        $response = file_get_contents($url, false, $context);

        // Native PHP object, please
        $token = json_decode($response);
        if (isset($token->access_token)) {
            // Store access token and expiration time
            $this->data['access_token'] = $token->access_token; // guard this!
            $this->data['expires_in'] = $token->expires_in; // relative time (in seconds)
            $this->data['expires_at'] = time() + $this->data['expires_in']; // absolute time
            return true;
        } else {
            $this->getAuthorizationCode();
        }
        return false;
    }

    function fetch($method, $resource, $body = '') {
        $params = array('oauth2_access_token' => $this->data['access_token'],
            'format' => 'json',
        );

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
        // Tell streams to make a (GET, POST, PUT, or DELETE) request
        $context = stream_context_create(
                array('http' =>
                    array('method' => $method,
                    )
                )
        );


        // Hocus Pocus
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
//        $response = file_get_contents($url, false, $context);

        // Native PHP object, please
        return json_decode($response);
    }

}

?>
