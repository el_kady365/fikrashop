<?php

class Newsletter extends AppModel {

    var $name = 'Newsletter';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
//            'name' => array('rule' => 'notEmpty', 'message' => 'Required'),
            'email' => array(
                array('rule' => 'email', 'message' => 'Enter valid email'),
                array('rule' => 'isUnique', 'message' => __('This email is already used', true), 'on' => 'create'),
                )
//            'security_code' => array('rule' => 'checkCaptcha', 'message' => __('Invalid security code.', true))
        );
    }

    function checkCaptcha($data) {
        if (!$_SESSION['security_code']) {
            return false;
        }
        return low($data['security_code']) == low($_SESSION['security_code']);
    }

}

?>