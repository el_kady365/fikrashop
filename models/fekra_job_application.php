<?php

class FekraJobApplication extends AppModel {

    var $name = 'FekraJobApplication';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'price' =>
            array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'numeric', 'message' => __('Enter valid number', true))
            ),
            'total' =>
            array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'numeric', 'message' => __('Enter valid number', true))
            ),
            'description' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'FekraJob' => array(
            'className' => 'FekraJob',
            'foreignKey' => 'fekra_job_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function checkvalid($data, $limit) {
       
        if ($data['charged'] > $limit) {
            return false;
        }
        return true;
    }

}
