<?php

class Contact extends AppModel {

    var $name = 'Contact';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
            'subject' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
            'message' => array('rule' => 'notEmpty', 'message' => __('Required', true)),
            'email' => array(
                array('rule' => 'email', 'message' => __('Enter a valid email', true), 'allowEmpty' => false),
                array('rule' => 'notEmpty', 'message' => __('Required', true), 'allowEmpty' => false),
            ),
//            'security_code' => array('rule' => 'checkCaptcha', 'message' => __('Invalid security code', true))
        );
    }

    function checkCaptcha($data) {
        if (!$_SESSION['security_code']) {
            return false;
        }
        return low($data['security_code']) == low($_SESSION['security_code']);
    }

}

?>