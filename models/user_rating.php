<?php

class UserRating extends AppModel {

    var $name = 'UserRating';
    public static $ratings = array();

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'creator_id' => array(
                'numeric' => array(
                    'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
                ),
            ),
            'rating' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => __('Required', true),
                    'allowEmpty' => false,
                    'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
                ),
            ),
        );
        self::$ratings = array(1 => __('Bad', true), 2 => __('Poor', true), __('Good', true), __('Very Good', true), __('Excellent', true));
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Creator' => array(
            'className' => 'User',
            'foreignKey' => 'creator_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public static function get_rating($id) {
        $Rating = ClassRegistry::init('UserRating');
        $ratings = $Rating->find('all', array('conditions' => array('UserRating.user_id' => $id), 'fields' => 'UserRating.rating'));
        $total = 0;
        if (!empty($ratings)) {
            foreach ($ratings as $rating) {
                $total+=$rating['UserRating']['rating'];
            }
            return $total / count($ratings);
        }
        return 0;
    }

}
