<?php

class Transaction extends AppModel {

    var $name = 'Transaction';
    var $statuses = array();

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
        $this->statuses = array(
            0 => __('Started', true),
            1 => __('Pending', true),
            2 => __('Completed', true),
            3 => __('Refunded', true),
            5 => __('Can be refunded', true),
            6 => __('Paid', true),
            7 => __('Complained by sender', true),
            8 => __('Complained by receiver', true),
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'FekraPhase' => array(
            'className' => 'FekraPhase',
            'foreignKey' => 'fekra_phase_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'FekraJob' => array(
            'className' => 'FekraJob',
            'foreignKey' => 'fekra_job_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Sender' => array(
            'className' => 'User',
            'foreignKey' => 'sender_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Receiver' => array(
            'className' => 'User',
            'foreignKey' => 'receiver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        $Fekra = ClassRegistry::init('Fekra');
        foreach ($results as &$result) {
            if (!empty($result['FekraPhase']['id'])) {
                $fekra = $Fekra->find('first', array('conditions' => array('Fekra.id' => $result['FekraPhase']['fekra_id'])));
                $result['Fekra'] = $fekra['Fekra'];
            } elseif (!empty($result['FekraJob']['id'])) {
                $fekra = $Fekra->find('first', array('conditions' => array('Fekra.id' => $result['FekraJob']['fekra_id'])));
                $result['Fekra'] = $fekra['Fekra'];
            }
        }
        return $results;
    }

}
