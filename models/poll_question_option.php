<?php

class PollQuestionOption extends AppModel {

    var $name = 'PollQuestionOption';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'PollQuestion' => array(
            'className' => 'PollQuestion',
            'foreignKey' => 'poll_question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Poll' => array(
            'className' => 'Poll',
            'foreignKey' => 'poll_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
