<?php

class Sponsor extends AppModel {

    var $name = 'Sponsor';
    public $actsAs = array(
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
                    'resize' => array('width' => 170, 'height' => 60),
                )
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
    }

}
