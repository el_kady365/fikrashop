<?php

class Poll extends AppModel {

    var $name = 'Poll';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed


    var $hasMany = array(
        'PollQuestion' => array('className' => 'PollQuestion', 'foreignKey' => 'poll_id', 'dependent' => true),
        'PollAnswer' => array('className' => 'PollAnswer', 'foreignKey' => 'poll_id', 'dependent' => true),
    );

}
