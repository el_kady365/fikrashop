<?php

class News extends AppModel {

    var $name = 'News';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
    }

    public $actsAs = array(
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
//                    'resize' => array('width' => 395, 'height' => 296),
                    'versions' => array(
                        array('prefix' => 'thumb1', 'width' => 78, 'height' => 78, 'allow_enlarge' => false),
//                        array('prefix' => 'thumb2', 'width' => 186, 'height' => 139, 'allow_enlarge' => false)
                    )
                )
            )
        )
    );
//The Associations below have been created with all possible keys, those that are not needed can be removed

    

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = slug($this->data[$this->name]['title'], '-');
        }
        return true;
    }

   
}
