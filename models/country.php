<?php

class country extends AppModel {

    var $name = 'Country';
    var $primaryKey = 'code';

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

}

?>