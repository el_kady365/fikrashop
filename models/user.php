<?php

define('MIN_PASSWORD', 6);

class User extends AppModel {

    var $name = 'User';
    var $certificationTypes = array();
    var $levels = array();
    public $actsAs = array(
        'Containable',
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
                    'resize' => array('width' => 180, 'height' => 180),
//                    'versions' => array(
////                        array('prefix' => 'thumb1', 'width' => 78, 'height' => 78, 'allow_enlarge' => false),
////                        array('prefix' => 'thumb2', 'width' => 186, 'height' => 139, 'allow_enlarge' => false)
//                    )
                )
            )
        )
    );
    var $belongsTo = array(
        'Country' => array('className' => 'Country', 'foreignKey' => 'country_code')
    );
    var $hasMany = array(
        'UserEducation' => array('className' => 'UserEducation', 'foreignKey' => 'user_id', 'dependent' => true, 'order' => 'UserEducation.graduation_date desc'),
        'UserExperience' => array('className' => 'UserExperience', 'foreignKey' => 'user_id', 'dependent' => true, 'order' => 'UserExperience.start_date desc'),
        'UserSkill' => array('className' => 'UserSkill', 'foreignKey' => 'user_id', 'dependent' => true, 'order' => 'UserSkill.id asc'),
        'UserLanguage' => array('className' => 'UserLanguage', 'foreignKey' => 'user_id', 'dependent' => true),
        'FekraUser' => array('className' => 'FekraUser', 'foreignKey' => 'user_id', 'dependent' => true)
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'password' => array('rule' => 'notempty', 'message' => __('Required', true)),
//            'first_name' => array('rule' => 'notempty', 'message' => __('Required', true)),
//            'last_name' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'email' => array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'email', 'message' => __('Enter a valid email', true)),
                array('rule' => 'isUnique', 'message' => __('This email is already used', true), 'on' => 'create'),
            ),
////            'security_code' => array('rule' => 'checkCaptcha', 'message' => __('Invalid security code', true))
//            'country_code' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
        $this->certifications = array('1' => __('BSc', true), '2' => __('License', true), '3' => __('Master', true), 4 => __('Phd', true));
    }

    function CSV_titles($imported_csv_titles) {
//        debug($imported_csv_titles);debug($this->schema);
        if (empty($imported_csv_titles)) {
            return false;
        }
//        debug($imported_csv_titles);
        foreach ($imported_csv_titles as $i => $imported_csv_title) {
            if (low(trim($imported_csv_title)) != low(trim($this->schema[$i]))) {

//                debug($i);
//                debug(var_dump(low(trim(utf8_encode($imported_csv_title)))));
//                debug(var_dump(low(trim(utf8_encode($this->schema[$i])))));
                return false;
            }
        }
        return true;
    }

    function validate_imported_user($user) {
        $result = array();
//        debug($user);
//        exit;
//        if (empty($user)) {
//            $result['errors'][] = "The record is empty";
//            return $result;
//        }
        if (empty($user[0])) {
            $result['errors'][] = __("Name is empty", true);
        }
        if (empty($user[1])) {
            $result['errors'][] = __('Username is empty', true);
        }
        if (empty($user[2])) {
            $result['errors'][] = __('Email is empty', true);
        }
        if (empty($user[3])) {
            $result['errors'][] = __("Password is empty", true);
        }


        if (empty($result['errors'])) {
            $user_ret = array();

            $user_ret['name'] = trim($user[0]);
            $user_ret['username'] = trim($user[1]);
            $user_ret['email'] = $user[2];
//            $user_ret['password'] = hashPassword($user[3]);
            $user_ret['telephone'] = $user[4];
            $user_ret['mobile'] = $user[5];
            $user_ret['address'] = $user[6];
            if (!empty($user[7])) {
                $user_ret['birth_date'] = date('Y-m-d', strtotime($user[7]));
            }

            $user_ret['password'] = $user_ret['passwd'] = hashPassword($user[3]);
            $user_ret['approved'] = 1;





            $result['data']['User'] = $user_ret;
//            $result['data']['Subscription'] = $user_sub;
        }


        return $result;
    }

    function checkCaptcha($data) {
        if (!$_SESSION['security_code']) {
            return false;
        }
        return low($data['security_code']) == low($_SESSION['security_code']);
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed


    function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        if (!empty($this->data[$this->name]['password'])) {
            $this->validate['passwd'] = array('rule' => 'checkPasswd', 'message' => __('Passwords not matched', true));
        } elseif (empty($this->data[$this->name]['password']) && !empty($this->data[$this->name]['id'])) {
            unset($this->data[$this->name]['password'], $this->data[$this->name]['passwd'], $this->validate['password'], $this->validate['passwd']);
        }

        return true;
    }

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data[$this->name]['password'])) {
            $this->data[$this->name]['password'] = hashPassword($this->data[$this->name]['password']);
        }
        if (!empty($this->data[$this->name]['categories'])) {
            $this->data[$this->name]['categories'] = implode(',', $this->data[$this->name]['categories']);
        }

        return true;
    }

    function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);

        $Transaction = ClassRegistry::init('Transaction');
        foreach ($results as &$result) {
            if ($primary && !empty($result[$this->name]['id'])) {
                $pending = $Transaction->find('all', array('conditions' => array('Transaction.receiver_id' => $result[$this->name]['id'], 'Transaction.status' => 1)));
                $completed = $Transaction->find('all', array('conditions' => array('Transaction.receiver_id' => $result[$this->name]['id'], 'Transaction.status' => 2)));

                $withdaw = $Transaction->find('all', array('conditions' => array('Transaction.receiver_id' => $result[$this->name]['id'], 'Transaction.status' => 4)));
                $total_pending = 0;
                $total_completed = 0;
                $total_withdraw = 0;
                if (!empty($pending)) {
                    foreach ($pending as $trans) {
                        $total_pending+=$trans['Transaction']['actual'];
                    }
                }
                if (!empty($completed)) {
                    foreach ($completed as $transa) {
                        $total_completed+=$transa['Transaction']['actual'];
                    }
                }
                if (!empty($withdaw)) {
                    foreach ($withdaw as $transw) {
                        $total_withdraw+=$transw['Transaction']['actual'];
                    }
                }
                $result[$this->name]['pending_payments'] = $total_pending;
//                debug(array($total_completed));
                $result[$this->name]['completed_payments'] = $total_completed;

                if (!empty($result[$this->name]['categories'])) {
                    $result[$this->name]['categories'] = explode(',', $result[$this->name]['categories']);
                }
            }
        }
        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
        }
        return $results;
    }

    public static function getFekraCount($user_id) {
        $mdl = new self;
        return $mdl->FekraUser->find('count', array('conditions' => array('FekraUser.type' => 1, 'FekraUser.user_id' => $user_id)));
    }

    public static function getMessageCount($user_id) {
        $Message = ClassRegistry::init('Message');
        return $Message->find('count', array('conditions' => array('Message.receiver_id' => $user_id, 'Message.read !=' => 1)));
    }

    public static function getJobsCount($user_id) {
        $Job = ClassRegistry::init('FekraJob');
        $mdl = new self;
        $fekras = $mdl->FekraUser->find('list', array('conditions' => array('FekraUser.type' => 1, 'FekraUser.user_id' => $user_id), 'fields' => array('FekraUser.fekra_id')));
        return $Job->find('count', array('conditions' => array('FekraJob.fekra_id' => array_values($fekras))));
    }

    public static function get_country($country_code, $lang) {
        $Country = ClassRegistry::init('Country');
        $country = $Country->findByCode($country_code);
        if (!empty($country)) {
            return $country['Country'][$lang . '_name'];
        }
        return '';
    }

    function checkPasswd($data) {
        if (!empty($this->data[$this->name]['password'])) {
            return $this->data[$this->name]['password'] == $data['passwd'];
        }
        return true;
    }

    function Change_password($data, $id) {
        $this->validate = array();

        $user = $this->read(null, $id);


        if (hashPassword($data[$this->name]['old_password']) != $user['User']['password']) {
            $this->validationErrors['old_password'] = __('Current Password incorrect', true);
            return false;
        }

        if (strlen($data[$this->name]['password']) < MIN_PASSWORD) {
            $this->validationErrors['password'] = String::insert(__('Minimum :len characters allowed', true), array('len' => MIN_PASSWORD));
            return false;
        }

        if ($data[$this->name]['password'] != $data[$this->name]['passwd'] || empty($data[$this->name]['password']) || empty($data[$this->name]['passwd'])) {
            $this->validationErrors['passwd'] = __('Not match', true);
            return false;
        }

        $newpassword = $data[$this->name]['password'];

        $this->create();
        $this->id = $id;
        if ($this->saveField('password', $newpassword)) {

            $this->contain();
            $neuser = $this->read(null, $id);

            return $neuser;
        }

        return false;
    }

    function Forget($data) {

        unset($this->validate);
        $this->validate['email'] = array('rule' => 'email', 'message' => 'Please enter a valid email', 'allowEmpty' => false, 'required' => true);

        $this->set($data);
        if ($this->validates()) {

            $user = $this->find('first', array('conditions' => array('User.email' => $data[$this->name]['email'])));

            if (!empty($user)) {
                return $user;
            } else {
                return false;
            }
        }
        return false;
    }

}
