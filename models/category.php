<?php

class Category extends AppModel {

    var $name = 'Category';
    var $belongsTo = array('Poll');
    public $actsAs = array('Containable',
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
                    'resize' => array('width' => 190, 'height' => 160),
                )
            )
    ));

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notempty', 'message' => __('Required', true))
        );
    }

    function beforeSave($options = array()) {
        parent::beforeSave($options);

        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = slug($this->data[$this->name]['title'], '-');
        }

        return true;
    }

}
