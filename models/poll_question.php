<?php

class PollQuestion extends AppModel {

    var $name = 'PollQuestion';
    var $types = array();

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'name' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'type' => array('rule' => 'notempty', 'message' => __('Required', true))
        );
        $this->types = array(1 => __('Single Choice', true), __('Multi choice', true), __('Essay text', true));
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Poll' => array(
            'className' => 'Poll',
            'foreignKey' => 'poll_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'PollQuestionOption' => array(
            'className' => 'PollQuestionOption',
            'foreignKey' => 'poll_question_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function addOptions($data) {
        $PollQuestionOption = ClassRegistry::init('PollQuestionOption');
        if (!empty($data['Course']['id'])) {
            if (!empty($data['PollQuestionOption'])) {

                $PollQuestionOption->recursive = -1;
                $_product_images = $PollQuestionOption->find('all', array('conditions' => array('PollQuestionOption.poll_question_id' => $data['Course']['id'])));

                $product_images_ids = array();
                foreach ($_product_images as $pag) {
                    $product_images_ids[] = $pag['PollQuestionOption']['id'];
                }
                $delete_pages_array = array();
                $posted_ids = array();
                foreach ($data['PollQuestionOption'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }
                $delete_pages_array = array_diff($product_images_ids, $posted_ids);
                $PollQuestionOption->deleteAll(array('PollQuestionOption.id' => $delete_pages_array), true, true);
            } else {
                $PollQuestionOption->deleteAll(array('PollQuestionOption.poll_question_id' => $data['PollQuestion']['id']), true, true);
            }
        }

        $id = $this->id;

        $Errors = '';
        if (!empty($data['PollQuestionOption'])) {
            foreach ($data['PollQuestionOption'] as $i => $item) {
//                debug($data['PollQuestionOption'][$i]);
                if ($data['PollQuestionOption'][$i]['name'] != '') {
                    $data['PollQuestionOption'][$i]['poll_question_id'] = $id;
                    $data['PollQuestionOption'][$i]['poll_id'] = $data['PollQuestion']['poll_id'];

                    $data2['PollQuestionOption'] = $data['PollQuestionOption'][$i];
                    $PollQuestionOption->create();
                    if (!$PollQuestionOption->save($data2['PollQuestionOption'], false)) {
                        return false;
                    }
                } else {
                    unset($data['PollQuestionOption'][$i]);
                }
            }
//            exit;
        }
    }

}
