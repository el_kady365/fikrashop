<?php

class FekraUser extends AppModel {

    var $name = 'FekraUser';
    var $roles = array();

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
        $this->roles = array(1 => __('Creator', true),__('Developer', true), __('Investor',true));
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Fekra' => array(
            'className' => 'Fekra',
            'foreignKey' => 'fekra_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
