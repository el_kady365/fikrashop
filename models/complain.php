<?php

class Complain extends AppModel {

    var $name = 'Complain';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
        $this->statuses = array(
            1 => __('Completed', true),
            2 => __('Refunded', true),
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'transaction_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
