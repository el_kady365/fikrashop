<?php

class Banner extends AppModel {

    var $name = 'Banner';
    public $actsAs = array(
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
                    'resize' => array('width' => 940, 'height' => 425),
                )
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

}

?>
