<?php

class Admin extends AppModel {

    var $name = 'Admin';

    function __construct($id = false, $table = null, $ds =null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
    }

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (empty($this->data[$this->name]['password']) && !empty($this->data[$this->name]['id'])) {
            unset($this->data[$this->name]['password']);
        } elseif (!empty($this->data[$this->name]['password'])) {
            $this->data[$this->name]['password'] = hashPassword($this->data[$this->name]['password']);
        }
        return true;
    }

}

?>