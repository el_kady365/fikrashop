<?php

class UserSkill extends AppModel {

    var $name = 'UserSkill';
    var $levels = array();

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array();
        $this->levels = array('1' => __('Beginner', true), '2' => __('Intermediate', true), '3' => __('Expert', true));
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
