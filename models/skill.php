<?php

class Skill extends AppModel {

    var $name = 'Skill';
    var $displayField = 'title';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notempty', 'message' => __('Required', true))
        );
    }

    public static function get_skill($id) {
        $modl = new self;
        $skill = $modl->field('title', array('Skill.id' => $id));
        return $skill;
    }

}
