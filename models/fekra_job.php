<?php

class FekraJob extends AppModel {

    var $name = 'FekraJob';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'description' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'fekra_id' => array('rule' => 'notempty', 'message' => __('Required', true)),
//            'price' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'delivery_date' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Fekra' => array(
            'className' => 'Fekra',
            'foreignKey' => 'fekra_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'FekraJobAttachment' => array(
            'className' => 'FekraJobAttachment',
            'foreignKey' => 'fekra_job_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
    ));

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data[$this->name]['skills'])) {
            $this->data[$this->name]['skills'] = implode(',', $this->data[$this->name]['skills']);
        }
        return true;
    }

    function afterFind($results, $primary = false) {
        parent::afterFind($results, $primary);
        foreach ($results as &$result) {
            if (!empty($result[$this->name]['skills'])) {
                $result[$this->name]['skills'] = explode(',', $result[$this->name]['skills']);
            }
        }
        return $results;
    }

    function addJobAttachments($data) {

        if (!empty($data['FekraJob']['id'])) {
            if (!empty($data['FekraJobAttachment'])) {

                $this->FekraJobAttachment->recursive = -1;
                $_product_images = $this->FekraJobAttachment->find('all', array('conditions' => array('FekraJobAttachment.fekra_job_id' => $data['FekraJob']['id'])));

                $product_images_ids = array();
                foreach ($_product_images as $pag) {
                    $product_images_ids[] = $pag['FekraJobAttachment']['id'];
                }
                $delete_pages_array = array();
                $posted_ids = array();
                foreach ($data['FekraJobAttachment'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }
                $delete_pages_array = array_diff($product_images_ids, $posted_ids);
                $this->FekraJobAttachment->deleteAll(array('FekraJobAttachment.id' => $delete_pages_array), true, true);
            } else {
                $this->FekraJobAttachment->deleteAll(array('FekraJobAttachment.fekra_job_id' => $data['FekraJob']['id']), true, true);
            }
        }

        $id = $this->id;

        $Errors = '';
//        debug($data['FekraPhase']);exit;

        if (!empty($data['FekraJobAttachment'])) {
            foreach ($data['FekraJobAttachment'] as $i => $item) {

                if ((isset($data['FekraJobAttachment'][$i]['file']) && $data['FekraJobAttachment'][$i]['file']['name'] != '')) {
                    $data['FekraJobAttachment'][$i]['fekra_job_id'] = $id;
                    $data2['FekraJobAttachment'] = $data['FekraJobAttachment'][$i];
                    $this->FekraJobAttachment->create();
                    if (!$this->FekraJobAttachment->save($data2['FekraJobAttachment'], false)) {
                        return false;
                    }
                } else {
                    if (isset($item['id'])) {
                        $this->FekraJobAttachment->delete($item['id']);
                    }
                    unset($data['FekraJobAttachment'][$i]);
                }
            }
//            exit;
        }
    }

    static function getTotalPayments($job_id, $receiver_id) {
        $totalp = 0;
        $trans = ClassRegistry::init('Transaction');
        $totals = $trans->find('all', array('conditions' => array('Transaction.fekra_job_id' => $job_id, 'Transaction.receiver_id' => $receiver_id, 'Transaction.status !=' => 0)));
        if (!empty($totals)) {
            foreach ($totals as $total) {
                $totalp+=$total['Transaction']['actual'];
            }
        }
        return $totalp;
    }

}
