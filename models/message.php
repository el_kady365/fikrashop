<?php

class Message extends AppModel {

    var $name = 'Message';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'subject' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'body' => array('rule' => 'notempty', 'message' => __('Required', true))
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Sender' => array(
            'className' => 'User',
            'foreignKey' => 'sender_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Receiver' => array(
            'className' => 'User',
            'foreignKey' => 'receiver_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ParentMessage' => array(
            'className' => 'Message',
            'foreignKey' => 'message_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'MessageAttachment' => array(
            'className' => 'MessageAttachment',
            'foreignKey' => 'message_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
    ));

    function addMessageAttachments($data) {

        if (!empty($data['Message']['id'])) {
            if (!empty($data['MessageAttachment'])) {

                $this->MessageAttachment->recursive = -1;
                $_product_images = $this->MessageAttachment->find('all', array('conditions' => array('MessageAttachment.message_id' => $data['Message']['id'])));

                $product_images_ids = array();
                foreach ($_product_images as $pag) {
                    $product_images_ids[] = $pag['MessageAttachment']['id'];
                }
                $delete_pages_array = array();
                $posted_ids = array();
                foreach ($data['MessageAttachment'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }
                $delete_pages_array = array_diff($product_images_ids, $posted_ids);
                $this->MessageAttachment->deleteAll(array('MessageAttachment.id' => $delete_pages_array), true, true);
            } else {
                $this->MessageAttachment->deleteAll(array('MessageAttachment.message_id' => $data['Message']['id']), true, true);
            }
        }

        $id = $this->id;

        $Errors = '';
//        debug($data['FekraPhase']);exit;

        if (!empty($data['MessageAttachment'])) {
            foreach ($data['MessageAttachment'] as $i => $item) {

                if ((isset($data['MessageAttachment'][$i]['file']) && $data['MessageAttachment'][$i]['file']['name'] != '')) {
                    $data['MessageAttachment'][$i]['message_id'] = $id;
                    $data2['MessageAttachment'] = $data['MessageAttachment'][$i];
                    $this->MessageAttachment->create();
                    if (!$this->MessageAttachment->save($data2['MessageAttachment'], false)) {
                        return false;
                    }
                } else {
                    if(isset($item['id'])) {
                        $this->FekraJobAttachment->delete($item['id']);
                    }
                    unset($data['MessageAttachment'][$i]);
                }
            }
//            exit;
        }
    }

    function get_attachments($id) {
        $attachmets = array();
        $files = $this->MessageAttachment->find('all', array('conditions' => array('MessageAttachment.message_id' => $id)));
        if (!empty($files)) {
            foreach ($files as $file) {
                $attachmets[] = WWW_ROOT . $file['MessageAttachment']['file_info']['full_path'];
            }
        }
        return $attachmets;
    }

}
