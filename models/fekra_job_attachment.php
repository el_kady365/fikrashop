<?php

class FekraJobAttachment extends AppModel {

    var $name = 'FekraJobAttachment';
    public $actsAs = array(
        'file' => array('extensions' => array(
                array('doc', 'pdf', 'docx', 'xls', 'xlsx', 'txt', 'jpg', 'gif', 'png')
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'fekra_job_id' => array(
                'numeric' => array(
                    'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
                ),
            ),
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'FekraJob' => array(
            'className' => 'FekraJob',
            'foreignKey' => 'fekra_job_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    function afterFind($results, $primary = false) {
        $results = parent::afterFind($results, $primary);

        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
        }
        return $results;
    }

}
