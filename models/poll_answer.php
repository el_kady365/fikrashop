<?php

class PollAnswer extends AppModel {

    var $name = 'PollAnswer';

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'FekraUser' => array(
            'className' => 'FekraUser',
            'foreignKey' => 'fekra_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Poll' => array(
            'className' => 'Poll',
            'foreignKey' => 'poll_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PollQuestion' => array(
            'className' => 'PollQuestion',
            'foreignKey' => 'poll_question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
