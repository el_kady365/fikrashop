<?php

class Fekra extends AppModel {

    var $name = 'Fekra';
    public $actsAs = array(
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
                    'versions' => array(
                        array('prefix' => 'thumb1', 'width' => 190, 'height' => 160, 'allow_enlarge' => false),
                        array('prefix' => 'thumb2', 'height' => 330, 'allow_enlarge' => true)
                    )
                )
            )
        ),
        'file' => array('extensions' => array(
                array('doc', 'pdf', 'docx', 'xls', 'xlsx')
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'category_id' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'title' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'description' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'price' => array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'numeric', 'message' => __('Enter numeric value', true)),
            ),
            'end_date' => array('rule' => 'date', 'message' => __('Required', true)),
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed
    function afterFind($results, $primary = false) {
        $results = parent::afterFind($results, $primary);

        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
            foreach ($results as $key => &$result) {
                if (!empty($result['Fekra']['category_id'])) {
                    $cat = $this->Category->read(null, $result['Fekra']['category_id']);
                    $results[$key]['Fekra']['Category'] = $cat['Category'];
                }
               
               
            }
        }
        return $results;
    }

    public static function get_funds($id) {
        $Transaction = ClassRegistry::init('Transaction');
        $mdl = new self;
        $total = 0;
        $phases = $mdl->FekraPhase->find('list', array('conditions' => array('FekraPhase.fekra_id' => $id)));
        $all_paid_phases = $Transaction->find('all', array('conditions' => array('Transaction.fekra_phase_id' => array_keys($phases), 'Transaction.status' => 2)));
        if (!empty($all_paid_phases)) {
            foreach ($all_paid_phases as $phase) {
                $total+=$phase['Transaction']['actual'];
            }
        }
        return $total;
    }

   public static function get_rating($id) {
        $Rating = ClassRegistry::init('FekraRating');
        $ratings = $Rating->find('all', array('conditions' => array('FekraRating.fekra_id' => $id), 'fields' => 'FekraRating.rating'));
        $total = 0;
        if (!empty($ratings)) {
            foreach ($ratings as $rating) {
                $total+=$rating['FekraRating']['rating'];
            }
            return $total / count($ratings);
        }
        return 0;
    }

    var $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'FekraUser' => array(
            'className' => 'FekraUser',
            'foreignKey' => 'fekra_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'FekraPhase' => array(
            'className' => 'FekraPhase',
            'foreignKey' => 'fekra_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'FekraVideo' => array(
            'className' => 'FekraVideo',
            'foreignKey' => 'fekra_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'FekraComment' => array(
            'className' => 'FekraComment',
            'foreignKey' => 'fekra_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function addIdeaAssets($data) {

        if (!empty($data['Fekra']['id'])) {
            if (!empty($data['FekraPhase'])) {

                $this->FekraPhase->recursive = -1;
                $_product_images = $this->FekraPhase->find('all', array('conditions' => array('FekraPhase.fekra_id' => $data['Fekra']['id'])));

                $product_images_ids = array();
                foreach ($_product_images as $pag) {
                    $product_images_ids[] = $pag['FekraPhase']['id'];
                }
                $delete_pages_array = array();
                $posted_ids = array();
                
                foreach ($data['FekraPhase'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }
                $delete_pages_array = array_diff($product_images_ids, $posted_ids);
                $this->FekraPhase->deleteAll(array('FekraPhase.id' => $delete_pages_array), true, true);
            } else {
                $this->FekraPhase->deleteAll(array('FekraPhase.fekra_id' => $data['Fekra']['id']), true, true);
            }




            if (!empty($data['FekraVideo'])) {

                $this->FekraVideo->recursive = -1;
                $_product_images = $this->FekraVideo->find('all', array('conditions' => array('FekraVideo.fekra_id' => $data['Fekra']['id'])));

                $product_images_ids = array();
                foreach ($_product_images as $pag) {
                    $product_images_ids[] = $pag['FekraVideo']['id'];
                }
                $delete_pages_array = array();
                $posted_ids = array();
                foreach ($data['FekraVideo'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }
                $delete_pages_array = array_diff($product_images_ids, $posted_ids);
                $this->FekraVideo->deleteAll(array('FekraVideo.id' => $delete_pages_array), true, true);
            } else {
                $this->FekraVideo->deleteAll(array('FekraVideo.fekra_id' => $data['Fekra']['id']), true, true);
            }
        }

        $id = $this->id;

        $Errors = '';
//        debug($data['FekraPhase']);exit;
        if (!empty($data['FekraPhase'])) {
            foreach ($data['FekraPhase'] as $i => $item) {
//                debug($data['FekraImage'][$i]);
                if ($data['FekraPhase'][$i]['title'] != '') {
                    $data['FekraPhase'][$i]['fekra_id'] = $id;
                    $data2['FekraPhase'] = $data['FekraPhase'][$i];
                    $this->FekraPhase->create();
                    if (!$this->FekraPhase->save($data2['FekraPhase'], false)) {
                        return false;
                    }
                } else {
                    unset($data['FekraPhase'][$i]);
                }
            }
//            exit;
        }
        if (!empty($data['FekraVideo'])) {
            foreach ($data['FekraVideo'] as $i => $item) {

                if ((isset($data['FekraVideo'][$i]['file']) && $data['FekraVideo'][$i]['file']['name'] != '') || ($data['FekraVideo'][$i]['video'] != '')) {
                    $data['FekraVideo'][$i]['fekra_id'] = $id;
                    $data2['FekraVideo'] = $data['FekraVideo'][$i];
                    $this->FekraVideo->create();
                    if (!$this->FekraVideo->save($data2['FekraVideo'], false)) {
                        return false;
                    }
                } else {
                    unset($data['FekraVideo'][$i]);
                }
            }
//            exit;
        }
    }

    function addPollAnswers($data, $fekra_user) {
        $PollAnswer = ClassRegistry::init('PollAnswer');
        $PollQuestion = ClassRegistry::init('PollQuestion');
        //$PollAnswer->deleteAll(array('PollAnswer.fekra_user_id' => $fekra_user));
        $quess = $PollQuestion->find('list', array('conditions' => array('PollQuestion.poll_id' => $data['Poll']['poll_id'])));
        foreach ($quess as $key => $que) {
            if (!isset($data['PollQuestion']['question_' . $key])) {
                $PollAnswer->deleteAll(array('PollAnswer.fekra_user_id' => $fekra_user, 'PollAnswer.poll_question_id' => $key));
            } else {
                foreach ($data['PollQuestion'] as $key => $question) {

                    $params = explode('_', $key);
                    $answer_data['poll_id'] = $data['Poll']['poll_id'];
                    $answer_data['fekra_user_id'] = $fekra_user;
                    $answer_data['poll_question_id'] = $params[1];
                    if (is_array($question)) {
                        $answer_data['answer'] = implode(',', $question);
                    } else {
                        $answer_data['answer'] = $question;
                    }

                    $answer = $PollAnswer->find('first', array('conditions' => array('PollAnswer.poll_id' => $answer_data['poll_id'], 'PollAnswer.fekra_user_id' => $fekra_user, 'PollAnswer.poll_question_id' => $params[1])));

                    if (!empty($answer) && !empty($answer_data['answer'])) {
                        $answer_data['id'] = $answer['PollAnswer']['id'];
                    } elseif (empty($answer) && !empty($answer_data['answer'])) {
                        $PollAnswer->create();
                    } elseif (!empty($answer) && empty($answer_data['answer'])) {
                        $PollAnswer->delete($answer['PollAnswer']['id']);
                    }

                    $question_answer_save['PollAnswer'] = $answer_data;
                    if (!empty($answer_data['answer'])) {

                        $PollAnswer->save($question_answer_save, false);
                    }
                }
            }
        }
    }

}
