<?php

class Page extends AppModel {

    var $name = 'Page';
    var $types;
    public $actsAs = array(
        'Image' => array(
            'fields' => array(
                'image' => array(
                    'thumbnail' => array('create' => false),
                    'versions' => array(
                        array('prefix' => 'thumb1', 'width' => 186, 'height' => 139, 'allow_enlarge' => false),
                        array('prefix' => 'thumb2', 'width' => 162, 'height' => 122, 'allow_enlarge' => false),
                    )
                )
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
            'title' => array('rule' => 'notempty', 'message' => __('Required', true)),
            'type' => array('rule' => 'notempty', 'message' => __('Required', true)),
        );
        $this->types = array(1 => __('Content', true), 2 => __('Link', true));
    }

    function beforeSave($options = array()) {
        parent::beforeSave($options);

        if (empty($this->data[$this->name]['permalink']) && !empty($this->data[$this->name]['title'])) {
            $this->data[$this->name]['permalink'] = slug($this->data[$this->name]['title'], '-');
        }
        if (!empty($this->data[$this->name]['video']) && $this->data[$this->name]['content_type'] == 2) {
            $url = $this->data[$this->name]['video'];
            $this->data[$this->name]['player'] = $this->parse_video_url($url, 'embed', 600, 450);

            if (!empty($this->data[$this->name]['video'])) {
                $image = file_get_contents($this->parse_video_url($url, 'hqthumb'));
                $img_name = 'image_' . $this->parse_video_url($url, '') . '.jpg';
                file_put_contents(str_replace('DS', '/', $this->getFullFolder('image')) . $img_name, $image);
                $this->data[$this->name]['image'] = $img_name;
            }
        }
        return true;
    }

    function parse_video_url($url, $return = 'embed', $width = '', $height = '', $rel = 0) {

        if (strstr($url, 'vimeo')) {
            preg_match('/[0-9]+/', $url, $match);
            $video_id = $match[0];
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_id.php"));
            if ($return == 'thumb') {
                return $hash[0]['thumbnail_small'];
            } elseif ($return == 'embed') {
                return '<iframe src="http://player.vimeo.com/video/' . $video_id . '?title=0&amp;byline=0&amp;portrait=0" width="' . $width . '" height="' . $height . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            } else {
                return $video_id;
            }
        } else {

            $urls = parse_url($url);

            //url is http://youtu.be/xxxx
            if ($urls['host'] == 'youtu.be') {
                $id = ltrim($urls['path'], '/');
            }
            //url is http://www.youtube.com/embed/xxxx
            else if (strpos($urls['path'], 'embed') == 1) {
                $id = end(explode('/', $urls['path']));
            }
            //url is xxxx only
            else if (strpos($url, '/') === false) {
                $id = $url;
            }

            //http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
            //url is http://www.youtube.com/watch?v=xxxx
            else {
                parse_str($urls['query']);
                $id = $v;
                if (!empty($feature)) {

                    $id = end(explode('v=', $urls['query']));
                    if (strpos($id, '&') !== false) {
                        $id = substr($id, 0, strpos($id, '&'));
                    }
                }
            }
            //return embed iframe
            if ($return == 'embed') {
                return '<iframe width="' . ($width ? $width : 560) . '" height="' . ($height ? $height : 349) . '" src="http://www.youtube.com/embed/' . $id . '?rel=' . $rel . '&wmode=opaque" frameborder="0" allowfullscreen></iframe>';
            }
            //return normal thumb
            else if ($return == 'thumb') {
                return 'http://i1.ytimg.com/vi/' . $id . '/default.jpg';
            }
            //return hqthumb
            else if ($return == 'hqthumb') {
                return 'http://i1.ytimg.com/vi/' . $id . '/hqdefault.jpg';
            }
            // else return id
            else {
                //debug($id);exit;
                return $id;
            }
        }
    }

}

?>