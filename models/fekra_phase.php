<?php

class FekraPhase extends AppModel {

    var $name = 'FekraPhase';
    public $actsAs = array(
        'file' => array('extensions' => array(
                array('doc', 'pdf', 'docx', 'xls', 'xlsx', 'txt')
            )
        )
    );

    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->validate = array(
        );
    }

//The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Fekra' => array(
            'className' => 'Fekra',
            'foreignKey' => 'fekra_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function afterFind($results, $primary = false) {
        $results = parent::afterFind($results, $primary);
        if (!$primary) {
            $results = $this->afterFindAssociated($results, true);
        }
        return $results;
    }

}
