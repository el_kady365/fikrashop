<?php

/* SVN FILE: $Id$ */
/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
//$countries = get_countries();
//$countries = implode('|', $countries);

Router::parseExtensions('rss', 'csv', 'html');
App::import('Lib', 'routes/I18nRoute');
Router::connect('/:language', array('controller' => 'pages', 'action' => 'home'), array('routeClass' => 'I18nRoute'));
Router::connect('/:language/board', array('controller' => 'users', 'action' => 'board'), array('routeClass' => 'I18nRoute'));
Router::connect('/:language/content/*', array('controller' => 'pages', 'action' => 'view'), array('routeClass' => 'I18nRoute'));
Router::connect('/:language/contact', array('controller' => 'contacts', 'action' => 'contactus'), array('routeClass' => 'I18nRoute'));
Router::connect('/:language/:controller', array(), array('routeClass' => 'I18nRoute'));
Router::connect('/:language/:controller/:action/*', array(), array('routeClass' => 'I18nRoute'));

//Router::connect('/:language/:prefix/:controller/:action/*', array(), array('routeClass' => 'I18nRoute'));



Router::connect('/content/*', array('controller' => 'pages', 'action' => 'view'));
Router::connect('/admin', array('controller' => 'admins', 'action' => 'login'));
Router::connect('/', array('controller' => 'pages', 'action' => 'home'));



/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
?>