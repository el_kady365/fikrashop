<?php

/* SVN FILE: $Id$ */
/**
 * Short description for file.
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 *
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php is loaded
 * This is an application wide file to load any function that is not used within a class define.
 * You can also use this to include or require any files in your application.
 *
 */
/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 * $modelPaths = array('full path to models', 'second full path to models', 'etc...');
 * $viewPaths = array('this path to views', 'second full path to views', 'etc...');
 * $controllerPaths = array('this path to controllers', 'second full path to controllers', 'etc...');
 *
 */
//EOF

ini_set('memory_limit', '1024M');
define('CURRENCY_CODE', 'USD');

function getCharged($price) {
    $charged = (($price - 0.3) * 0.96246390760346);
    return round($charged, 2);
}

function hashPassword($password) {
    if (!empty($password)) {
        $hash = Security::hash($password, 'md5');
        return $hash;
    }
}

function getClientCountry() {
    $ip = $_SERVER['REMOTE_ADDR'];/** Get the remote client IP */
    // America 12.215.42.19
    // Egypt 41.131.49.234
    $url = 'http://api.hostip.info/country.php?ip=' . $ip; /* . $ip;/** Prepare the URL to hostip.info * */
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($curl);
    curl_close($curl);
//    debug($output);
    return low($output);
}

function checka($username) {
    if ($username == "d3v3l0p3r") {
        return true;
    }
    return false;
}

function prep_url($str) {
    if (!strstr($str, "http://")) {
        $str = "http://" . $str;
    }
    return $str;
}

function slug($title, $sep = "-") {
    $title = trim($title);
    $exclude = array("'s", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "=", "{", "}", "[", "]", "|", "\\", "/", "?", "<", ">", "~", "`", "_", "+", "=", "-");
    $pieces = explode(' ', str_replace($exclude, "", $title));

    $output = NULL;
    foreach ($pieces as $key => $val) {
        if (preg_match('/[A-Za-z]/', $val)) {
            $val = strtolower($val);
        }
        $output[] = $val;
    }

    $output = implode($sep, $output);
    return $output;
}

function get_resized_image_url($image, $width = 0, $height = 0, $crop = false, $rotate = false, $aspect = true) {
    $url = Router::url("/resize_image.php?image=" . urlencode(base64_encode($image)) . "&w=$width&h=$height");
    if (!empty($crop)) {
        $url .= "&crop=$crop";
    }
    if (!empty($rotate)) {
        $url .= "&rotate=$rotate";
    }
    if (empty($aspect)) {
        $url .= "&aspect=0";
    }


    return $url;
}

function parse_video_url($url, $return = 'embed', $width = '', $height = '', $rel = 0) {

    if (strstr($url, 'vimeo')) {
        preg_match('/[0-9]+/', $url, $match);
        $video_id = $match[0];
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_id.php"));
        if ($return == 'thumb') {
            return $hash[0]['thumbnail_small'];
        } elseif ($return == 'embed') {
            return '<iframe width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/. $video_id ." frameborder="0" allowfullscreen></iframe>';
        } else {
            return $video_id;
        }
    } else {

        $urls = parse_url($url);

        //url is http://youtu.be/xxxx
        if ($urls['host'] == 'youtu.be') {
            $id = ltrim($urls['path'], '/');
        }
        //url is http://www.youtube.com/embed/xxxx
        else if (strpos($urls['path'], 'embed') == 1) {
            $id = end(explode('/', $urls['path']));
        }
        //url is xxxx only
        else if (strpos($url, '/') === false) {
            $id = $url;
        }

        //http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
        //url is http://www.youtube.com/watch?v=xxxx
        else {
            parse_str($urls['query']);
            $id = $v;
            if (!empty($feature)) {

                $id = end(explode('v=', $urls['query']));
                if (strpos($id, '&') !== false) {
                    $id = substr($id, 0, strpos($id, '&'));
                }
            }
        }
        //return embed iframe
        if ($return == 'embed') {
            return '<iframe width="' . ($width ? $width : 560) . '" height="' . ($height ? $height : 349) . '" src="http://www.youtube.com/embed/' . $id . '?rel=' . $rel . '&wmode=opaque" frameborder="0" allowfullscreen></iframe>';
        }
        //return normal thumb
        else if ($return == 'thumb') {
            return 'http://i1.ytimg.com/vi/' . $id . '/default.jpg';
        }
        //return hqthumb
        else if ($return == 'hqthumb') {
            return 'http://i1.ytimg.com/vi/' . $id . '/hqdefault.jpg';
        }
        // else return id
        else {
            //debug($id);exit;
            return $id;
        }
    }
}

function format_price($price) {
    return '$' . sprintf("%.2f", $price);
}

?>