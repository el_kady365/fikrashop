<?php

uses('L10n');

class AppController extends Controller {

    var $helpers = array('Html', 'Form', 'Javascript', 'Fck', 'Text', 'Session');
    var $components = array('Session', 'Cookie', 'Email');
    var $config = '';
    var $pageTitle;

    function beforeFilter() {
        $this->disableCache();
        $this->Session->write('Config.language', 'en');
        Configure::write('Config.language', 'en');
        $lang = $this->_setLanguage();
        $config = $this->__load_config();
        $this->set('config', $config);
        $this->lang = $lang;
        $prefix = (!isset($this->params['prefix'])) ? '' : $this->params['prefix'];

        if (!empty($prefix) && $prefix == 'admin') {
            $this->layout = 'admin';
            $this->L10n = new L10n();
            $this->L10n->get('en');

            $admin = $this->is_admin();
            $this->set('admin', $admin);
        } else {
            $user = $this->is_user();
            $this->set('user', $user);
        }

        if (empty($this->titleAlias)) {
            $this->titleAlias = __(Inflector::humanize(Inflector::underscore($this->name)), true);
        }
        $this->set('titleAlias', $this->titleAlias);
    }

    function beforeRender() {
        parent::beforeRender();
        $this->__change_title();
        $prefix = (!isset($this->params['prefix'])) ? '' : $this->params['prefix'];

        if ($prefix != 'admin') {

            $this->loadModel('Page');
            $pages = $this->Page->find('all', array('conditions' => array('Page.active' => 1, 'Page.add_to_mainmenu' => 1, 'Page.lang' => $this->lang), 'order' => array('Page.display_order' => 'asc')));
            $this->set('top_menus', $pages);
            $this->set('footer_snippet', $this->get_snippets('footer-snippet', $this->lang));
            if (!$this->is_user()) {
                $this->get_countries();
            }
        }
    }

    function setFlash($message, $class = 'fail alert alert-error', $key = 'flash') {
        $this->Session->setFlash(__($message, true), 'default', array('class' => $class), $key);
    }

    function is_admin() {
        if ($this->Session->check('admin')) {
            return $this->Session->read('admin');
        } else {
            $this->Session->write('admin_redirect', $_SERVER["REQUEST_URI"]);
            $this->redirect('/admin/');
            die();
        }
    }

    function get_countries($code = false) {
        $this->loadModel('Country');
        $countries = $this->Country->find('list', array('fields' => array('code', $this->lang . '_name'), 'recursive' => -1));
        $this->set('countries', $countries);
        if ($code) {
            return $countries[$code];
        }
    }

    function download_file() {
        if (!empty($_GET['path']) && isset($_GET['path'])) {
            $file_name = urldecode($_GET['path']);
            $file = substr(strrchr($file_name, '/'), 1);
            $file = explode('_', $file);
            array_shift($file);
            $file = implode('_', $file);
            $file_name = WWW_ROOT . str_replace('/', DS, $file_name);
        } else {
            echo 'not found';
        }
        header("Content-Disposition: attachment; filename=" . urlencode($file));
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file_name));
        flush(); // this doesn't really matter.

        $fp = fopen($file_name, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);

        die();
    }

    function _setLanguage($lang = 'en') {

        if (isset($this->params['language'])) {
            $lang = $this->params['language'];
            $this->Session->write('Config.language', $this->params['language']);
        } elseif ($this->Session->check('Config.language')) {
            $lang = $this->Session->read('Config.language');
        }


        if ($lang != 'ar') {
            $dir = 'ltr';
        } else {
            $dir = 'rtl';
        }
        $this->L10n = new L10n();
        $this->L10n->get($lang);
        $this->Session->write('Config.language', $lang);
        Configure::write('Config.language', $lang);
        $this->set('lang', $lang);
        $this->set('dir', $dir);
        return $lang;
    }

    function _setCountry() {
        $countries = get_countries();
        $country = getClientCountry();
        $countries_exists = array_keys($countries, $country);


        if (isset($this->params['country'])) {
            $country_se = array_keys($countries, $this->params['country']);
            $country = !empty($country_se) ? $country_se[0] : 'eg';
        } elseif ($this->Session->check('country')) {
            $country_se = array_keys($countries, $this->Session->read('country'));
            $country = !empty($country_se) ? $country_se[0] : 'eg';
        } elseif (!empty($countries_exists)) {
            $country = $countries_exists[0];
        } elseif (empty($countries_exists) && empty($this->params['country']) && !$this->Session->check('country')) {
            $country = 'eg';
        }
        $country_name = isset($countries[$country]) ? $countries[$country] : '';

        $this->Session->write('country', $country);
        $this->Cookie->write('country', $country, false, '20 days');
        $this->set('country_appr', $country);
        $this->set('country_name', $country_name);

        $this->country = $country;
//        debug($country);
        return $country;
    }

    function admin_delete_field($id, $field) {
        $model = Inflector::classify($this->name);
        $basename = $this->$model->find('first', array('conditions' => array("$model.id" => $id), 'callbacks' => false));

        if (!empty($basename)) {
            $return = $this->$model->deleteFile($field, $id, $basename[$model][$field]);
            if (!$return) {
                $this->$model->Behaviors->disable('Image');
                $this->$model->deleteFile($field, $id, $basename[$model][$field]);
                $this->$model->Behaviors->enable('Image');
            }
        }
//$this->$model->create();
        $data = array($model => array('id' => $id, $field => ''));

        if ($this->$model->save($data, array('callbacks' => false))) {
//            debug($data);exit;
            $this->setFlash("The {$field} has been deleted successfully", 'alert alert-success');
            $this->redirect(array('action' => 'edit', $id));
        }
    }

    function delete_field($id, $field) {
        $model = Inflector::classify($this->name);
        $this->$model->id = $id;
        $basename = $this->$model->find('first', array('callbacks' => false));

        if (!empty($basename)) {
            $return = $this->$model->deleteFile($field, $id, $basename[$model][$field]);
            if (!$return) {
                $this->$model->Behaviors->disable('Image');
                $this->$model->deleteFile($field, $id, $basename[$model][$field]);
                $this->$model->Behaviors->enable('Image');
            }
        }
//$this->$model->create();
        $data = array($model => array('id' => $id, $field => ''));

        if ($this->$model->save($data, array('callbacks' => false))) {
//            debug($data);exit;
            if ($model == 'User') {
                $user = $this->User->find('first', array('conditions' => array('User.id' => $id), 'contain' => false));
                $this->Session->write('user', $user['User']);
            }

            $this->setFlash("The {$field} has been deleted successfully", 'success alert alert-success');
            $this->redirect($this->referer());
        }
    }

    function __change_title($title = false) {

        $prefix = empty($this->params['prefix']) ? '' : $this->params['prefix'];
        if (strtolower($prefix) == 'admin') {

            $prefix = empty($this->params['prefix']) ? false : $this->params['prefix'];
            $action = $this->params['action'];
            if ($prefix) {
                $titleArr[] = Inflector::humanize($this->params['prefix']);
                $action = substr($action, strlen("{$prefix}_"));
            }
            $titleArr[] = $this->titleAlias;
            if (strtolower($action) != 'index') {
                $titleArr[] = Inflector::humanize($action);
            }
        } else {
            $titleArr = array($this->config['site_name']);
            $titleArr[] = $this->pageTitle;
        }



        $this->set('title_for_layout', implode(' - ', $titleArr));
        $this->set('og_title', implode(' - ', $titleArr));
        if ($this->params['action'] == 'home') {
            $this->set('title_for_layout', $this->config['site_name']);
        }
    }

    function __load_config() {
        $this->loadModel('Configuration');
        $configurations = $this->Configuration->read(null, 1);
        $this->config = $configurations['Configuration'];
//        debug($this->config);
        return $this->config;
    }

    function get_languages() {
        $languages = Configure::read('Config.languages');
        $this->set(compact('languages'));
    }

    function get_snippets($key = null, $lang = null) {
        $this->loadModel('Snippet');
        if (!$lang) {
            $lang = $this->lang;
        }
        $snippet = $this->Snippet->find('first', array('conditions' => array('Snippet.key' => $key, 'Snippet.lang' => $lang)));
        return $snippet['Snippet']['content'];
    }

    function is_user() {
        $data = false;
        if (!$this->Session->check('user')) {
            return false;
        }
        return $this->Session->read('user');
    }

    function authenticate() {
        $user = $this->is_user();
        if (!$user) {
            $this->Session->write('LOGINREDIRECT', $_SERVER["REQUEST_URI"]);
            $this->setFlash(__('You must login first', true), 'fail');
            $this->redirect('/#login');
        }
        return $user;
    }

    function set_previous_page() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->Session->write('LOGINREDIRECT', $_POST['url']);
        echo $_POST['url'];
    }

    function _send_message($to, $from, $subject, $template, $attachments = array(), $debug = 0) {
        $this->Email->to = $to;

        $this->Email->sendAs = 'html';
        $this->Email->layout = 'contact';
        $this->Email->template = $template;
        $this->Email->from = $from;
        $this->Email->subject = $subject;
        if ($debug) {
            $this->Email->delivery = 'debug';
        }
        if (!empty($attachments)) {
            $this->Email->attachments = $attachments;
        }
        return $this->Email->send();
    }

}

?>