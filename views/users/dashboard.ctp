<div class="content-box">
    <div class="dashboard">
        <div class="profile-header clearfix">
            <div class="profile-image">
                <?php if (!empty($data['User']['image']['path'])) { ?>
                    <img alt="" src="<?php echo Router::url($data['User']['image']['path']) ?>">
                <?php } else { ?>
                    <img src="<?php echo Router::url('/css/img/anonymous.jpg') ?>"  alt="" title="" />
                <?php } ?>
            </div>
            <div class="profile-info font-a">
                <h3><?php echo $data['User']['first_name'] . ' ' . $data['User']['last_name'] ?></h3>
                <h6><?php echo!empty($data['User']['city']) ? $data['User']['city'] . ',' : '' ?> <?php echo isset($data['Country']['code']) ? $data['Country'][$lang . '_name'] : ''; ?></h6>
                <ul class="profile-social clearfix">
                    <?php if (!isset($edit)) { ?>
                        <li> <a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'send_message', base64_encode($data['User']['id']))) ?>" class="btn btn-default mail-bg"><span class="fa fa-envelope"></span></a></li>
                    <?php } ?>
                    <?php if (!empty($data['User']['facebook_url'])) { ?>
                        <li> <a href="<?php echo $data['User']['facebook_url'] ?>" class="btn btn-default fb-bg"><span class="fa fa-facebook"></span></a></li>
                    <?php }if (!empty($data['User']['linkedin_url'])) { ?>
                        <li> <a href="<?php echo $data['User']['linkedin_url'] ?>" class="btn btn-default in-bg"><span class="fa fa-linkedin"></span></a></li>
                    <?php } ?>
                </ul>
            </div>

            <div class="user-stats">
                <ul class="font-a">
                    <?php if (isset($edit)) { ?>
                        <li><span class="fa fa-credit-card" aria-hidden="true"></span> <?php __('Balance') ?> <a href="<?php echo Router::url(array('controller' => 'transactions')) ?>" class="header-link-color"><?php
                                echo format_price($data['User']['completed_payments']);
                                !empty($data['User']['pending_payments']) ? print(' (' . __('Pending', true) . ':' . format_price($data['User']['pending_payments']) . ')')  : '';
                                ?></a></li>
                    <?php } ?>

                    <li><span class="fa fa-lightbulb-o" aria-hidden="true"></span> <?php __('Ideas') ?>
                        <?php if (isset($edit)) { ?>
                            <a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'user_ideas')) ?>" class="header-link-color"><?php echo User::getFekraCount($data['User']['id']) ?></a>                 
                        <?php } else { ?>
                            <a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'user_ideas', $data['User']['id'])) ?>" class="header-link-color"><?php echo User::getFekraCount($data['User']['id']); ?></a>    
                        <?php } ?>
                    </li>
                    <li><span class="fa fa-briefcase" aria-hidden="true"></span> <?php __('Jobs') ?>
                        <?php if (isset($edit)) { ?>
                            <a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'user_jobs')) ?>" class="header-link-color"><?php echo User::getJobsCount($data['User']['id']) ?></a>
                        <?php } else { ?>
                            <a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'user_jobs', $data['User']['id'])) ?>" class="header-link-color"><?php echo User::getJobsCount($data['User']['id']); ?></a>    
                        <?php } ?>
                    </li>



                    <?php if (isset($edit)) { ?>
                        <li><span class="fa fa-envelope-o" aria-hidden="true"></span> <?php __('Messages') ?> <a  href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'inbox')) ?>" class="header-link-color"><?php echo User::getMessageCount($data['User']['id']) ?></a></li>
                    <?php } ?>
                </ul>
                <span id="member-since" class="tiny-text shape"><?php __('Member since') ?>: <span class="join-date"><?php echo date('M d, Y', strtotime($user['created'])) ?></span></span> </div>

        </div>
        <!-- profile header -->

        <?php if (isset($edit)) { ?>
            <div class="btns-bar clearfix"><a href="<?php echo Router::url('/users/get_linkedin_data') ?>" class="btn btn-success right"><span class="fa fa-lg fa-linkedin-square"></span> <?php __('Import LinkedIn profile') ?></a></div>
        <?php } ?>

        <div class="clear"></div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#About" data-toggle="tab"><i class="fa fa-user"></i> <?php __('Summary') ?></a></li>
            <li><a href="#Positions" data-toggle="tab"><i class="fa fa-briefcase"></i> <?php __('Positions') ?></a></li>
            <li><a href="#Languages" data-toggle="tab"><i class="fa fa-puzzle-piece"></i> <?php __('Languages') ?></a></li>
            <li><a href="#Skills" data-toggle="tab"><i class="fa fa-list-ul"></i> <?php __('Skills') ?></a></li>
            <li><a href="#Educations" data-toggle="tab"><i class="fa fa-trophy"></i> <?php __('Educations') ?></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="About">
                <div class="section-details">

                    <?php
                    if (!empty($data['User']['summary'])) {
                        echo '<p>' . nl2br($data['User']['summary']) . '</p>';
                    } else {
                        if (isset($edit)) {
                            echo '<p class="text-muted"><em>' . __('Write small description about yourself', true) . '</em></p>';
                        }
                    }
                    ?>    

                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url('/users/summary/') ?>" class="skill btn btn-default btn-small"><i class="fa fa-pencil"></i> <?php __('Edit your summary') ?>  </a>
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane" id="Positions">
                <div class="section-details">
                    <ul>
                        <?php foreach ($data['UserExperience'] as $exp) { ?>
                            <li class="list-wp">
                                <?php if (isset($edit)) { ?>
                                    <div class="quick-actions right">
                                        <a href="<?php echo Router::url('/user_experiences/edit/' . $exp['id']) ?>" class="btn btn-default btn-small skill"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                                        <a href="<?php echo Router::url('/user_experiences/delete/' . $exp['id']) ?>" class="btn btn-danger btn-small delete"><span class="fa fa-trash-o" aria-hidden="true"></span></a>
                                    </div>
                                <?php } ?>
                                <h4><?php echo $exp['name'] ?>  @  <?php echo $exp['company_name'] ?>  </h4>
                                <p> <span class="fa fa-clock-o" aria-hidden="true"></span> <?php __('From') ?>: <?php echo date('Y-m', strtotime($exp['start_date'])) ?> <?php __('To') ?>:<?php echo!empty($exp['till_now']) ? __('Present', true) : date('Y-m', strtotime($exp['end_date'])) ?></p>


                            </li>
                        <?php } ?>
                    </ul>
                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url('/user_experiences/add/') ?>" class="skill btn btn-default btn-small"><i class="fa fa-plus"></i> <?php __('Add New Position') ?>  </a>
                    <?php } ?>
                </div>
            </div>

            <div class="tab-pane" id="Languages">
                <div class="section-details">
                    <ul>
                        <?php foreach ($data['UserLanguage'] as $ulang) { ?>
                            <li class="list-wp">
                                <?php if (isset($edit)) { ?>
                                    <div class="quick-actions right">
                                        <span><a href="<?php echo Router::url('/user_languages/edit/' . $ulang['id']) ?>" class="btn btn-default btn-small skill"><span class="fa fa-pencil" aria-hidden="true"></span></a></span>
                                        <span><a href="<?php echo Router::url('/user_languages/delete/' . $ulang['id']) ?>" class="btn btn-danger btn-small delete"><span class="fa fa-trash-o" aria-hidden="true"></span></a></span>
                                    </div>
                                <?php } ?>

                                <h5><span aria-hidden="true" class="fa fa-angle-right"></span> <?php echo $ulang['name'] ?></h5>
                                <?php if ($ulang['level']) echo '- ' . $levels[$ulang['level']] ?>

                            </li>
                        <?php } ?>
                    </ul>
                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url(array('controller' => 'user_languages', 'action' => 'add')) ?>" class="btn btn-default btn-small skill"><i class="fa fa-plus"></i> <?php __('Add New Language') ?> </a>
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane" id="Skills">
                <div class="section-details">
                    <ul>
                        <?php foreach ($data['UserSkill'] as $skill) { ?>
                            <li class="list-wp">
                                <?php if (isset($edit)) { ?>
                                    <div class="qucik-actions right">
                                        <span><a href="<?php echo Router::url('/user_skills/edit/' . $skill['id']) ?>" class="btn btn-default btn-small skill"><span class="fa fa-pencil" aria-hidden="true"></span></a></span>
                                        <span><a href="<?php echo Router::url('/user_skills/delete/' . $skill['id']) ?>" class="btn btn-danger btn-small delete"><span class="fa fa-trash-o" aria-hidden="true"></span></a></span>
                                    </div>
                                <?php } ?>
                                <h5><span aria-hidden="true" class="fa fa-angle-right"></span> <?php echo $skill['name'] ?></h5>
                                <?php if ($skill['level']) echo ' - ' . $levels[$skill['level']] ?>

                            </li>
                        <?php } ?>
                    </ul>

                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url(array('controller' => 'user_skills', 'action' => 'add')) ?>" class="btn btn-default btn-small skill"><i class="fa fa-plus"></i> <?php __('Add New Skill') ?> </a> 
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane" id="Educations">
                <div class="section-details">
                    <ul>
                        <?php foreach ($data['UserEducation'] as $edu) { ?>
                            <li class="list-wp">
                                <?php if (isset($edit)) { ?>
                                    <div class="quick-actions right">
                                        <a href="<?php echo Router::url('/user_educations/edit/' . $edu['id']) ?>" class="btn btn-default btn-small skill"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                                        <a href="<?php echo Router::url('/user_educations/delete/' . $edu['id']) ?>" class="btn btn-danger btn-small delete"><span class="fa fa-trash-o" aria-hidden="true"></span></a>
                                    </div>
                                <?php } ?>
                                <h5><span class="fa fa-angle-right" aria-hidden="true"></span> <?php echo $edu['organization'] ?></h5>  
                                <?php if (!empty($edu['certification'])) { ?>-
                                    <?php
                                    echo $edu['certification'];
                                } if (!empty($edu['major'])) {
                                    ?> 
                                    - <?php
                                    echo $edu['major'];
                                } if (!empty($edu['grade'])) {
                                    ?> - <?php
                                    echo $edu['grade'];
                                }
                                ?>
                                <br/>   
                                <p> <span class="fa fa-calendar" aria-hidden="true"></span> <?php __('Graduation year') ?>: <?php echo date('Y', strtotime($edu['graduation_date'])) ?> </p>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url(array('controller' => 'user_educations', 'action' => 'add')) ?>" class="btn btn-default btn-small skill"><i class="fa fa-plus"></i> <?php __('Add New Education') ?> </a>
                    <?php } ?>
                </div>
            </div>
        </div>        


        <div class="clear"></div>


    </div>
    <!-- dashboard --> 
</div>
<?php echo $this->element('user/user_leftmenu'); ?>

<!-- main --> 
<?php
echo $html->css('jquery-impromptu.min');
echo $javascript->link('jquery-impromptu.min');
?>
<?php echo $html->scriptStart(array('inline' => false)); ?> //<script>
    $(document).ready(function() {
        $('.collapse').click(function() {
            $(this).closest('.sections').find('.section-details ul').slideToggle();
            $(this).closest('.sections').find('.section-details p').slideToggle();
            $(this).toggleClass('collapsed');
            if ($(this).hasClass('collapsed')) {
                $(this).find('> span').removeClass('icon-arrow-up-2').addClass('icon-arrow-down-2');
            } else {
                $(this).find('> span').removeClass('icon-arrow-down-2').addClass('icon-arrow-up-2');
            }
            return false;
        });


        $('.hide').hide();
        $('.skill').click(function() {
            $('#skillform').find('.ajax-load').show();
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                dataType: 'html',
                success: function(data) {
                    $('#skillform').find('.ajax-load').hide();
                    $('#skillform').find('.form').html(data);
                }
            })
            $('#skillform').reveal();
            $('#skillform').bind('reveal:close', function() {
                $('#skillform').find('.form').html('');
            })

            return false;
        });
        $('.delete').click(function() {
            hrf = $(this).get(0).href;
            $this = $(this);
            var txt = '<?php __('Are you sure you want to remove this entry?') ?>';

            $.prompt(txt, {
                buttons: {'<?php __('Delete') ?>': true, '<?php __('Cancel') ?>': false},
                close: function(e, v, m, f) {

                    if (v) {

                        //Here is where you would do an ajax post to remove the user
                        //also you might want to print out true/false from your .php
                        //file and verify it has been removed before removing from the 
                        //html.  if false dont remove, $promt() the error.
                        $.ajax({
                            url: hrf,
                            type: 'POST',
                            dataType: 'json',
                            success: function(data) {
                                if (data.status == '1') {
                                    $this.closest('li').hide('slow', function() {
                                        $this.closest('li').remove();
                                        $.prompt(data.message);
                                    });
                                } else {
                                    $.prompt(data.message);
                                }
                            }
                        })

                    }
                    else {
                    }

                }
            });
            return false;
        });


    });

    <?php echo $html->scriptEnd(); ?>

