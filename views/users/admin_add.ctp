<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['User']['id'])) {

                    __('Edit User');
                } else {

                    __('Add User');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('User', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('first_name');
            echo $form->input('last_name');
            echo $form->input('email');
            echo $form->input('username');
            if (empty($this->data['User']['id'])) {
                echo $form->input('password');
                echo $form->input('passwd', array('label' => __('Re-type password', true)));
            }   
            echo $form->input('gender');
            echo $form->input('address');
            echo $form->input('city');
            echo $form->input('country_id',array('empty'=>__('Select country',true)));
            echo $form->input('telephone');
            echo $form->input('mobile');
            echo $form->input('image', array('type' => 'file',
                'between' => $this->element('image_element', array('info' => !empty($this->data['User']['image']) ? $this->data['User']['image'] : '', 'field' => 'image'))));
            
            echo $form->input('evaluation_board');
            echo $form->input('balance');
            echo $form->input('confirmed');
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>

