<h3 class="font-a">
    <strong> <?php
        __('Edit your summary');
        ?>
    </strong>
</h3>
<div class="message"></div>
<?php echo $form->create('User', array('class' => 'fonta', 'id' => 'skillForm')) ?>

<div class="input textarea">     
    <?php echo $form->input('summary', array('div' => false, 'label' => false, 'id' => 'Summary', 'placeholder' => __('Summary', true))); ?>
</div>


<div class="input submit">
    <button type="submit"><span class="btn btn-default"><?php __('Save') ?></span></button>
</div>                   
<?php echo $form->end(); ?>


<?php
echo $html->script(array('jquery.validate'));
?>
<script type="text/javascript">
    $(function() {
        var v = jQuery("#skillForm").validate({
            submitHandler: function(form) {
                $('div.message').html('').hide();
                $.ajax({
                    url: $(form).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: $(form).serialize(),
                    success: function(data) {

                        if (data.status == '1') {
                            $('div.message').html('<div id="flashMessage" class="success">' + data.message + '</div>').fadeIn();
                            setTimeout(function() {
                                location.reload()
                            }, '300');
                        } else {
                            $('div#flashMessage').addClass('fail');
                            $('div.message').html('<div id="flashMessage" class="fail">' + data.message + '</div>').fadeIn();
                        }
                    }
                })
            }
        });

    });
</script>






