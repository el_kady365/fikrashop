<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Account Settings') ?></strong></h3>

    <?php
    echo $form->create('User', array('type' => 'file', 'url' => array('action' => 'profile'), 'class' => 'form default-forms font-a'));
    ?>

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-user"></i> <?php __('Personal Information') ?></div>
        <div class="panel-body">

            <?php
            echo $form->input('id');
            echo $form->input('first_name');
            echo $form->input('last_name');
            echo $form->input('email');
//    echo $form->input('password', array('autocomplete' => 'off'));
//    echo $form->input('passwd', array('label' => __('Re-type Password', true)));
            echo $form->input('gender', array('options' => array(1 => __('Male', true), 2 => __('Female', true)), 'empty' => __('Select Gender', true)));

            echo $form->input('city');
            echo $form->input('country_code', array('options' => $countries, 'empty' => __('Select Country', true), 'label' => __('Country', true)));

            echo $form->input('image', array('type' => 'file',
                'between' => $this->element('front_image_element', array('info' => !empty($this->data['User']['image']) ? $this->data['User']['image'] : '', 'field' => 'image'))));
            ?>
        </div>
    </div>



    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-thumb-tack"></i> <?php __('Contact Information') ?></div>
        <div class="panel-body">
            <?php
            echo $form->input('address');
            echo $form->input('telephone');
            echo $form->input('mobile');
//    echo $form->input('summary');
            ?>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-facebook-square"></i> <?php __('Social links') ?></div>
        <div class="panel-body">
            <?php
            echo $form->input('facebook_url', array('label' => __('Facebook', true)));
            echo $form->input('linkedin_url', array('label' => __('Linkedin', true)));

//    echo $form->input('summary');
            ?>

        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-envelope"></i> <?php __('Subscriptions') ?></div>
        <div class="panel-body">
            <div class="checkbox">
                <label>
                    <?php echo $form->input('send_jobs', array('label' => false, 'div' => false)); ?>  <?php echo __('Send jobs that match your skills') ?>
                </label>
            </div>
            <div class="input">
                <label><?php __('Choose from below categories to recieve daily message of ideas of chosen categories') ?></label>
                <div class="checkgroup">
                    <?php echo $form->input('categories', array('multiple' => 'checkbox', 'div' => false, 'label' => false, 'value' => isset($this->data['User']['categories']) ? $this->data['User']['categories'] : '', 'options' => $categories, 'style' => 'width:186px')); ?>
                </div>
            </div>
            <?php
//    echo $form->input('summary');
            ?>

        </div>
    </div>


    <div class="input submit">
        <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-check-circle"></i> <?php __('Save') ?> </button>
    </div>
    <?php echo $form->end(); ?>
</div>

<?php echo $this->element('user/user_leftmenu'); ?>




