<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Change Password') ?></strong></h3>


    <?php echo $form->create('User', array('action' => 'change_password', 'class' => 'form default-forms font-a')); ?>
    <div class="input text">
        <label><?php __('Current Password')?><span class="star">*</span></label>
        <?php echo $form->input('old_password', array('type' => 'password', 'class' => 'INPUT required', 'div' => false, 'label' => false)); ?> 
    </div>	
    <div class="input text">
        <label><?php __('New Password')?><span class="star">*</span></label>
        <?php echo $form->input('password', array('class' => 'INPUT required', 'div' => false, 'label' => false, 'type' => 'password')); ?> 
    </div>	
    <div class="input text">
        <label><?php __('Confirm Password')?><span class="star">*</span></label>
        <?php echo $form->input('passwd', array('class' => 'INPUT required', 'div' => false, 'label' => false, 'type' => 'password')); ?> 
    </div>	
    <div class="input submit">
        <button type="submit"><span class="btn btn-default"><?php __('Save')?></span></button>
    </div>
  <?php echo $form->end(); ?>



</div>
<?php echo $this->element('user/user_leftmenu'); ?>