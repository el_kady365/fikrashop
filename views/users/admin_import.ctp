<?php $title = __('Users Import', true); ?>
<div class="breadcrumbwidget">
    <ul class="breadcrumb">
        <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'dashboard')) ?>"><?php __('Home') ?></a></li>
        <span class="divider"> / </span>
        <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'index')) ?>"><?php __('Users') ?></a></li>
        <span class="divider"> / </span>
        <li class="active"><?php echo $title ?></li>
    </ul>
</div>
<h1>
    <?php echo $title ?>
</h1>
<div class="alert alert-block"> To import users use this schema <br />
name, username, email, password, telephone (Not required), mobile (Not required), address(Not required), birth date (Not required)   
</div>
<?php echo $form->create('User', array('type' => 'file')); ?>
<?php echo $form->input('file', array('type' => 'file')); ?>
<div class="form-actions">
    <?php
    echo $form->submit('Import', array('class' => 'btn btn-primary'));
    ?>
</div>
<?php if (isset($processed) && $processed): ?>
   

    <h3><?php __('Results');?></h3>
    
    <?php if ($count): ?>
        <div class="alert alert-success"><strong><?php echo $count ?></strong> Users imported successfully.</div>
    <?php endif; ?>
        
    <div class="log-result">
        <ul>
            <?php if ($failed): ?>
                <li class="red">
                    <div class="alert alert-error"><?php __('Failed to import');?> <strong><?php echo $failed ?></strong> user.</div>
                    <ul>
                        <?php foreach ($failedCode as $code): ?>
                            <li><strong>Record no.<?php echo $code['record_num'] ?></strong>
                                <?php foreach ($code['errors'] as $error): ?>
                                    <span><?php echo $error ?></span>
                                <?php endforeach; ?>	
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </div>
<?php endif; ?>
<?php echo $form->end(); ?>