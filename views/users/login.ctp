
<div class="module">
    <h2><span><?= __("Login") ?></span></h2>
    <?php echo $session->flash();?>
    <div class="module-body">

        <?php echo $form->create('User', array('action' => 'login')); ?>
        <?php echo $form->input('username'); ?>
        <?php echo $form->input('password'); ?>
        <?php echo $form->submit('Login') ?>
        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'register')) ?>"><?php __('Don\'t you have an account?') ?></a>
        <br />
        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'forget_password')) ?>"><?php __('Forget Password?') ?></a>
    </div>
</div>
