<h3 class="font-a">
    <strong> <?php
        __('Select ideas for get rating');
        ?>
    </strong>
</h3>
<div class="message"></div>
<?php echo $form->create('User', array('class' => 'fonta', 'id' => 'rateForm', 'url' => array('action' => 'get_ideas_for_rating', $board_id))) ?>
<?php echo $form->input('id'); ?>

<div class="input checkgroup">
    <h5><?php __('Ideas') ?></h5>
    <?php echo $form->input('ideas', array('multiple' => 'checkbox', 'div' => false, 'label' => false, 'options' => $fekras, 'style' => 'width:186px')); ?>
</div>

<div class="input submit">
    <button type="submit"><span class="btn btn-default"><?php __('Send request') ?></span></button>
</div>                   
<?php echo $form->end(); ?>

<script>
    $(function() {
        $('#rateForm').submit(function() {
            $('div.message').html('').hide();
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $(this).serialize(),
                success: function(data) {

                    if (data.status == '1') {
                        $('div.message').html('<div id="flashMessage" class="success alert alert-success">' + data.message + '</div>').fadeIn();
                        setTimeout(function() {
                            location.reload()
                        }, '300');
                    } else {
                        $('div#flashMessage').addClass('fail');
                        $('div.message').html('<div id="flashMessage" class="fail alert alert-error">' + data.message + '</div>').fadeIn();
                    }
                }
            })
            return false;
        });
    });

</script>