<h3 class="font-a sub-heading"><strong><?php __('Board Team') ?></strong> <span class="shape">(<?php echo count($boards) ?>)</span></h3>

<?php
//debug($_SESSION);
if (!empty($boards)) {
    ?>
    <ul class="sp-list">
        <?php foreach ($boards as $board) {
            ?>
            <li>
                <div class="sp-img">
                    <a href="#">
                        <?php
                        if (!empty($board['User']['image'])) {
                            $image = $board['User']['image']['path'];
                        } else {
                            $image = '/css/img/anonymous.jpg';
                        }
                        ?>
                        <img title="" alt="" src="<?php echo Router::url($image) ?>">
                    </a>
                </div>
                <h5 class="font-a"><strong><?php echo $board['User']['first_name'] . ' ' . $board['User']['last_name'] ?></strong></h5>
                <h6 class="font-a"><?php
                    if (isset($board['UserExperience'][0])) {
                        echo $board['UserExperience'][0]['name'] . ' @ ' . $board['UserExperience'][0]['company_name'];
                    }
                    ?>
                </h6>
                        <!-- <h6 class="font-a"><strong>$117,000</strong> of <strong class="highlight-b">$150,000</strong></h6> --> 
                        <!-- <h6 class="font-a">Funded on <strong>23</strong> Project</h6> --> 

                <a class="ask_for_rating btn btn-success" id="" href="<?php echo Router::url(array('action' => 'get_ideas_for_rating', $board['User']['id'])) ?>"><? __('Ask for rating idea') ?></a> 
            </li>
        <?php } ?>
        <div class="clear"></div>
    </ul>
<?php } ?>
<div class="clear"></div>
<script>
    $(function() {
        $id = '<?php echo $user['id'] ?>';
        $('.ask_for_rating').click(function() {
            if ($id == '') {
                $.ajax({
                    url: '<?php echo Router::url(array('action' => 'set_previous_page')) ?>',
                    type: 'POST',
                    dataType: 'html',
                    data: "url=<?php echo Router::url(array('action' => 'board')) ?>",
                    success: function(data) {
//                        console.log(data);
                    }
                })
                $('#login').reveal();
            }
            else {
                $('#skillform').find('.ajax-load').show();
                $.ajax({
                    url: $(this).get(0).href,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $('#skillform').find('.ajax-load').hide();
                        $('#skillform').find('.form').html(data);
                    }
                })
                $('#skillform').reveal();
                $('#skillform').bind('reveal:close', function() {
                    $('#skillform').find('.form').html('');
                })
            }
            return false;
        })

    })
</script>
