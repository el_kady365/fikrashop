
<div class="module"> 
    <h2><span><?php __('Register') ?></span></h2>
    <?php echo $session->flash();?>
    <?php echo $form->create('User', array('type' => 'file')); ?>
    <div class="module-body">
        
        <?php
        echo $form->input('id');
        echo $form->input('username');
        echo $form->input('password');
        echo $form->input('passwd', array('label' => __('Re-enter your password', true)));
        echo $form->input('email');
        echo $form->input('mobile', array('placeholder' => '00123456789'));
        echo $form->submit('Submit', array('class' => 'submit-green'));
        ?>
        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>"><?php __('Login') ?></a>
    </div>
    <?php echo $form->end(); ?>

</div>
