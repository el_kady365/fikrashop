<div class="module">
    <h2><span><?php __('Send SMS'); ?></span></h2>
    <?php echo $session->flash();?>
    <div class="note"><?php echo __('Allowed messages you can sent', true) . ' ' . $allow_messages_count; ?></div>
    <div class="module-body">
        <?php
        
        echo $form->create('User', array('type' => 'file'));


        if ($allow_messages_count <= 0) {
            $allow_messages_count = 0;
        }
        ?>

        
        <?php
        echo $form->hidden('to');
        echo $form->input('from', array('label'=>'الرقم الذى سيستخدم فى الإرسال منه','placeholder' => '002012345678','after' => $html->tag('p', __('سيظهر هذا الرقم فى اسم المرسل إذا تركته فارغ فسيرسل برقم الموبايل المحفوظ فى بياناتك', true), array('style' => 'color:#F00;'))))
        ?>
        <div id="Number" class="hide">
            <?php echo $form->input('mobile', array('placeholder' => '002012345678')) ?>
        </div>
        <div id="Groupss" class="hide">
            <?php
            echo $form->input('groups', array('multiple' => true, 'type' => 'select', 'data-placeholder' => __("Choose group", true), 'style' => "width:415px;", 'class' => "chzn-select chzn-rtl", 'options' => $groups));
//            echo $form->input('messages_count', array('after' => $html->tag('p', __('leave it blank if you want to send message to all users in selected groups', true), array('style' => 'color:#F00;'))));
            ?>

        </div>

        <?php
        echo $form->input('message', array('type' => 'textarea'));


        echo $form->submit('Send', array('class' => ''));
        echo $form->end();
        ?>
    </div>
</div>
<?php
echo $javascript->link(array('chosen.jquery'));
echo $html->css(array('chosen'));
?>
<script type="text/javascript">
    $('#UserGroups').chosen({no_results_text: "<?php __("No results matched") ?>", width: '415px'});
    $(document).ready(function() {
        change_type($('input[name="data[User][to]"]').val());
        $('input[name="data[User][to]"]').click(function() {
            change_type($('input[name="data[User][to]"]').val());
        });
    });

    function change_type(va) {
        if (va == '2') {
            $('#Groupss').show();
            $('#Number').hide();
        } else {
            $('#Groupss').hide();
            $('#Number').show();
        }

    }
</script>