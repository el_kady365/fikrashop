<div class="module">
    <h2><span><?php __("Forget Password?") ?></span></h2>
    <?php echo $session->flash(); ?>
    <div class="module-body">

        <?php echo $form->create('User', array('action' => 'forget_password')); ?>
        <?php echo $form->input('email'); ?>


        <?php echo $form->submit('Submit') ?>

        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>"><?php __('Login') ?></a>
    </div>
</div>
