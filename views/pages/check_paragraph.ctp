<h2><span><?php __('Errors'); ?></span></h2>
<div class="module-table-body">

    <?php
    if (!empty($errorsww)) {
        ?>
        <table >
            <tr>

                <th>Error</th>
                <th>Type</th>
                <th>Suggestion</th>
            </tr>

            <?
            foreach ($errorsww as $key => $error) {
                ?>
                <tr>

                    <td><?php echo $error['string'] ?></td>
                    <td><?php echo $error['type'] ?></td>
                    <td><?php
        if (is_array($error['suggestions']['option']))
            echo implode(',', $error['suggestions']['option']);
        else
            echo $error['suggestions']['option'];
                ?></td>

                </tr>
                <?
            }
            ?>
        </table>
        <?
    } else {
        echo $session->flash();
    }
    ?> 
</div>
