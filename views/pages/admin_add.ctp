<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Page']['id'])) {

                    __('Edit Page');
                } else {
                    __('Add Page');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('Page', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('title');
            echo $form->input('sub_heading');
            echo $form->input('type', array('empty' => __('Select one', true)));
            ?>
            <div id="Content" class="hide">
                <?php
                if (!empty($this->data['Page']['id'])) {
                    echo $form->input('permalink');
                }
                

                echo $fck->load('Page', 'description');
                echo $form->input('image', array('type' => 'file',
                'between' => $this->element('image_element', array('info' => !empty($this->data['Page']['image']) ? $this->data['Page']['image'] : '', 'field' => 'image'))));
                echo $form->input('keywords');
                ?>
            </div>
            <div id="URL" class="hide">
                <?php
                echo $form->input('url');
                ?>
            </div>
            <?php
            echo $form->input('lang', array('label' => __('Language', true), 'options' => $languages));
            echo $form->input('add_to_mainmenu');
            echo $form->input('display_order');
            echo $form->input('active');




            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        change_type($('#PageType').val());
        $('#PageType').change(function() {
            change_type($('#PageType').val());
        });

    });
    function change_type(va) {
        $('.hide').hide();
        //$('.hide input').attr('disabled','disabled');
        if (va == 1) {
            $('#Content').show();
            $('.uploader').show();
            //  $('#ItemFile').removeAttr('disabled');
        }
        if (va == 2) {
            $('#URL').show();
            //$('#URL input').removeAttr('disabled');
        }
    }
</script>
