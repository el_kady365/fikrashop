<div class="home-filters">
    <div class="wrap">
        <div class="find-box font-a left">
            <img src="<?php echo Router::url('/css/img/icons/idea-ico.png') ?>" alt="" title="" class="find-box-img" />
            <h3>
                <span><?php __('Create') ?></span>
            </h3>
            <a href="<?php echo Router::url(array('controller'=>'fekras','action'=>'add'))?>" class="btn btn-lg btn-default"><i class="fa fa-lg fa-plus"></i> <?php __('Create your idea')?></a>   
        </div>
        
        <div class="find-box font-a left active">
            <img src="<?php echo Router::url('/css/img/icons/idea.png') ?>" alt="" title="" class="find-box-img" />
            <h3>
                <span><?php __('Invest') ?></span>
            </h3>
            <a href="<?php echo Router::url(array('controller'=>'fekras','action'=>'index'))?>" class="btn btn-lg btn-default"><i class="fa fa-lg fa-search"></i> <?php __('Search for ideas')?></a>   
        </div> 
        
        <div class="find-box font-a left">
            <img src="<?php echo Router::url('/css/img/icons/job.png') ?>" alt="" title="" class="find-box-img" />
            <h3>
                <span><?php __('Develop') ?></span>
            </h3>
            <a href="<?php echo Router::url(array('controller'=>'fekra_jobs','action'=>'index'))?>" class="btn btn-lg btn-default"><i class="fa fa-lg fa-search"></i> <?php __('Search for jobs')?></a>   
        </div>
        
        <div class="clear"></div>
    </div>
</div>
<!-- home-filters -->


<div class="welcome-box gray-box">
    <div class="wrap">
        <div class="text-center">
            <h2 class="font-a"><?php __('How does it <strong>Work?</strong>')?></h2>
            <?php
            //echo $this->element('video_player', array('width' => '540', 'file' => Router::url('/css/img/fekra-intro.mp4', true)))
            echo parse_video_url('http://youtu.be/OVIZUGxckZQ', 'embed', 540);
            ?>
        </div>
    </div>
</div>


<div class="welcome-box ">
    <div class="wrap">
        <?php echo $snippet; ?>
    </div>
</div>
<!-- /welcome-box -->
<?php if (!empty($featured_ideas)) { ?>
    <div class="featured-idea gray-box">
        <div class="wrap">
            <h2 class="font-a"><?php __('Featured <strong>Ideas</strong>') ?> </h2>

            <div class="featured-idea-wrap"> <a class="prev" href="#">1</a> <a class="next" href="#">2</a>
                <div class="featured-carousel">
                    <ul>
                        <?php
                        foreach ($featured_ideas as $idea) {
                            if (!empty($idea['Fekra']['image'])) {
                                $img = Router::url($idea['Fekra']['image']['thumb1']);
                            } else {
                                $img = Router::url('/css/img/fekra-thumb.png');
                            }
                            ?>
                            <li>
                                <a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $idea['Fekra']['id'], slug($idea['Fekra']['title']))); ?>">
                                    <img src="<?php echo $img ?>" alt="" />
                                </a>
                                <span class="font-a"><?php echo $idea['Fekra']['title'] ?></span>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- / featured-idea -->
<?php } ?>
<!-- / featured-idea -->

<?php if (!empty($sponsors)) { ?>
    <div class="featured-partners ">
        <div class="wrap">
            <h2 class="font-a"><strong><?php __('Sponsors') ?></strong></h2>
            <div class="featured-idea-wrap"> <a class="sprev" href="#">1</a> <a class="snext" href="#">2</a>
                <div class="partners-slider">
                    <ul>
                        <?php foreach ($sponsors as $sponsor) { ?>
                            <li> <a href="<?php echo $sponsor['Sponsor']['url'] ?>"><img src="<?php echo Router::url($sponsor['Sponsor']['image']['path']) ?>" alt="<?php echo $sponsor['Sponsor']['title'] ?>" title="<?php echo $sponsor['Sponsor']['title'] ?>" /></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- / featured-partners -->

<?php echo $html->script(array('caroufredsel')); ?>
<script>
    $(function() {
        $(".featured-carousel").jCarouselLite({
            btnNext: ".next",
            btnPrev: ".prev",
            visible: 4
        });


        $(".partners-slider").jCarouselLite({
            btnNext: ".snext",
            btnPrev: ".sprev",
            visible: 4
        });

    });
</script>
