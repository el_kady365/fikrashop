
<div class="module">
    <h2><span><?php  __('User Language');?></span></h2>
    <div class="module-body">
        <dl><?php $i = 0; $class = ' class="altrow"';?>
            		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userLanguage['UserLanguage']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userLanguage['UserLanguage']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Level'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userLanguage['UserLanguage']['level']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Years Of Exp'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userLanguage['UserLanguage']['years_of_exp']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($userLanguage['User']['id'], array('controller' => 'users', 'action' => 'view', $userLanguage['User']['id'])); ?>
			&nbsp;
		</dd>
        </dl>


        <div class="actions">
            <h3><?php __('Actions'); ?></h3>
            <ul>
                		<li><?php echo $this->Html->link(__('Edit User Language', true), array('action' => 'edit', $userLanguage['UserLanguage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete User Language', true), array('action' => 'delete', $userLanguage['UserLanguage']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $userLanguage['UserLanguage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List User Languages', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Language', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
            </ul>
        </div>
            </div>
</div>