<style type="text/css">
    .url-div{
        border: 1px dotted #999999;
        padding-right: 10px;
        margin:8px;
        position: relative;
    }
    #Options{
        border: 1px dotted #999999;

        margin:4px 8px;
    }
    .delete-section2{
        position: absolute;
        top:0;
        right:0;
        width:16px;
    }
    .inline-labeles label
    {
        display: inline-block;
    }
</style>
<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['PollQuestion']['id'])) {

                    __('Edit Poll Question');
                } else {

                    __('Add Poll Question');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('PollQuestion', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->hidden('poll_id');
            echo $form->input('name');
            echo $form->input('type');
            ?>
            <div id="options-div">
                <div id="Options">
                    <?php
                    $section_count = 0;
//                debug($this->data['AssignmentFile']);
                    if (!empty($this->data['PollQuestionOption'])) {
                        foreach ($this->data['PollQuestionOption'] as $j => $option) {
                            //debug($level);
                            ?>

                            <div id='row<?php echo $j ?>' class="url-div">
                                <?php
                                echo $form->input('PollQuestionOption.' . $j . '.id', array('value' => $option['id']));
                                echo $form->input('PollQuestionOption.' . $j . '.name', array('value' => $option['name']));
                                echo $form->input('PollQuestionOption.' . $j . '.display_order', array('value' => $option['display_order']));
                                ?>
                                &nbsp;
                                &nbsp
                                <a href = '#' class = "delete-section2 btn btn-small" onClick = 'removeFormField("#row<?php echo $j ?>");
                                                return false;'>
                                    <i class="icon-remove"></i>
                                </a>
                            </div>
                            <?php
                            $section_count++;
                        }
                    } else {
                        ?>
                        <div id='row0' class="url-div">
                            <?php
//                echo $form->input('Author.0.id');
                            echo $form->input('PollQuestionOption.' . 0 . '.name', array());
                            echo $form->input('PollQuestionOption.' . 0 . '.display_order', array());
                            ?>
                        </div>
                    <?php } ?>

                </div>
                <a href="#" class="add-author btn">
                    <i class="icon-plus"></i> <?php echo __('Add Option') ?>
                </a>
            </div>
            <?php
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>
<script type="text/javascript">
                                    $(document).ready(function() {
                                        change_type($('#PollQuestionType').val());
                                        $('#PollQuestionType').change(function() {
                                            change_type($('#PollQuestionType').val());
                                        });

                                    });
                                    function change_type(va) {
                                        $('.hide input').prop('disabled', 'disabled');
                                        $('.hide').hide();
                                        selected_submenu = [1, 2];
                                        if (selected_submenu.indexOf(parseInt(va)) != -1) {
                                            $('#options-div').show();
                                            $('#Options input').removeAttr('disabled');

                                        } else {
                                            $('#options-div').hide();
                                            $('#Options input').prop('disabled', 'disabled');

                                        }


                                    }

                                    var section_count = '<?php echo $section_count ?>';
                                    $('.add-author').live('click', function() {

                                        section_count++;

                                        x = '<?php echo $javascript->escapeString($form->input('PollQuestionOption.' . 0 . '.name', array())) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('PollQuestionOption.' . 0 . '.display_order', array())) ?>';
                                        x += '<a href="#" onClick=\'removeFormField("#row0"); return false;\' class="delete-section2 btn btn-small"><i class="icon-remove"></i></a>';




                                        y = x.replace(/PollQuestionOption0/g, 'PollQuestionOption' + section_count);
                                        y = y.replace(/row0/g, 'row' + section_count);
                                        y = y.replace(/\[PollQuestionOption]\[0\]/g, '[PollQuestionOption][' + section_count + ']');

                                        $('#Options').append('<div class="url-div" id="row' + section_count + '">' + y + '</div>');
                                        $('#Options').find('.url-div:last').find('input').val('');
                                        $('#Options').find('.url-div:last').find('textarea').val('');
                                        $('#Options').find('.url-div:last').find('input').attr('checked', false);


                                        return false;

                                    });

                                    function removeFormField(id) {
                                        $(id).remove();
                                    }
</script>
