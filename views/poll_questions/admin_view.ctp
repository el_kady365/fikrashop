
<div class="module">
    <h2><span><?php  __('Poll Question');?></span></h2>
    <div class="module-body">
        <dl><?php $i = 0; $class = ' class="altrow"';?>
            		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $pollQuestion['PollQuestion']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $pollQuestion['PollQuestion']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $pollQuestion['PollQuestion']['type']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Poll'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($pollQuestion['Poll']['name'], array('controller' => 'polls', 'action' => 'view', $pollQuestion['Poll']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $pollQuestion['PollQuestion']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $pollQuestion['PollQuestion']['updated']; ?>
			&nbsp;
		</dd>
        </dl>


        <div class="actions">
            <h3><?php __('Actions'); ?></h3>
            <ul>
                		<li><?php echo $this->Html->link(__('Edit Poll Question', true), array('action' => 'edit', $pollQuestion['PollQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Poll Question', true), array('action' => 'delete', $pollQuestion['PollQuestion']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $pollQuestion['PollQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Poll Questions', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Poll Question', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Polls', true), array('controller' => 'polls', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Poll', true), array('controller' => 'polls', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Poll Question Options', true), array('controller' => 'poll_question_options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Poll Question Option', true), array('controller' => 'poll_question_options', 'action' => 'add')); ?> </li>
            </ul>
        </div>
                    <div class="related">
                <h3><?php __('Related Poll Question Options');?></h3>
                <?php if (!empty($pollQuestion['PollQuestionOption'])):?>
                <table cellpadding = "0" cellspacing = "0">
                    <tr>
                        		<th><?php __('Id'); ?></th>
		<th><?php __('Poll Question Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Poll Id'); ?></th>
		<th><?php __('Display Order'); ?></th>
                        <th class="actions"><?php __('Actions');?></th>
                    </tr>
                    	<?php
		$i = 0;
		foreach ($pollQuestion['PollQuestionOption'] as $pollQuestionOption):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $pollQuestionOption['id'];?></td>
			<td><?php echo $pollQuestionOption['poll_question_id'];?></td>
			<td><?php echo $pollQuestionOption['name'];?></td>
			<td><?php echo $pollQuestionOption['poll_id'];?></td>
			<td><?php echo $pollQuestionOption['display_order'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'poll_question_options', 'action' => 'view', $pollQuestionOption['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'poll_question_options', 'action' => 'edit', $pollQuestionOption['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'poll_question_options', 'action' => 'delete', $pollQuestionOption['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $pollQuestionOption['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
                </table>
                <?php endif; ?>

                <div class="actions">
                    <ul>
                        <li><?php echo $this->Html->link(__('New Poll Question Option', true), array('controller' => 'poll_question_options', 'action' => 'add'));?> </li>
                    </ul>
                </div>
            </div>
            </div>
</div>