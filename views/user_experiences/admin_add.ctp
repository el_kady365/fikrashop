<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php

            	if(!empty($this->data['UserExperience']['id'])){

            		__('Edit User Experience');

                	}else{

                		__('Add User Experience');

                	}
            ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('UserExperience',array('type'=>'file'));?>
        <div class="block-fluid">
            	<?php
		echo $form->input('id');
		echo $form->input('company_name');
		echo $form->input('name');
		echo $form->input('start_date');
		echo $form->input('end_date');
		echo $form->input('till_now');
		echo $form->input('user_id');
		echo $form->input('job_description');
echo 	$form->submit('Submit', array('class' => 'submit-green'));
	?>
        </div>
        <?php echo $form->end();?>
    </div>
</div>

