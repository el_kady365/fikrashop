<h3 class="font-a">
    <strong> <?php
        if (!empty($this->data['UserExperience']['id'])) {
            __('Edit Experience');
        } else {
            __('Add New Experience');
        }
        ?>
    </strong></h3>
<div class="message"></div>

<?php echo $form->create('UserExperience', array('class' => 'fonta', 'id' => 'skillForm')) ?>
<?php echo $form->input('id'); ?>
<div class="input text">     
    <?php echo $form->input('name', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'SkillName', 'placeholder' => __('Position', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('company_name', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Company name', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('start_date', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'required hasDate', 'placeholder' => __('Start Date', true))); ?>
</div>
<div class="input checkbox">
    <?php echo $form->input('till_now', array('div' => false, 'label' => false)); ?>
    <label for="UserExperienceTillNow"><?php __('Till Now') ?></label>
</div>
<div class="input text end_date">
    <?php echo $form->input('end_date', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'required hasDate', 'placeholder' => __('End Date', true))); ?>
</div>
<div class="input textarea">
    <?php echo $form->input('job_description', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Job Description', true))); ?>
</div>

<div class="input submit">
    <button type="submit"><span class="btn btn-default"><?php __('Save') ?></span></button>
</div>                   
<?php echo $form->end(); ?>


<?php
echo $html->script(array('jquery.validate'));
?>
<?php
echo $html->css('jquery-ui-1.9.2.custom.min');
echo $javascript->link('jquery-ui-1.9.2.custom.min');
?>
<script type="text/javascript">
    $(document).ready(function() {
        display_enddate($('#UserExperienceTillNow').is(':checked'))
        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        $('#UserExperienceTillNow').change(function() {
            display_enddate($(this).is(':checked'));
        });
    });
    function display_enddate(val) {
        if (val) {
            $('.end_date').hide();
            $('.end_date :input').attr('disabled', 'disabled');
        } else {
            $('.end_date').show();
            $('.end_date :input').removeAttr('disabled');
        }
    }
    $('.hasDate').on('focus', function() {
        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
            changeYear: true});
    })

</script>
<script type="text/javascript">
    $(function() {
        jQuery.validator.addMethod("greaterThan",
                function(value, element, params) {

                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) > Number($(params).val()));
                });
        var v = jQuery("#skillForm").validate({
            rules: {
                'data[UserExperience][end_date]': {greaterThan: "#UserExperienceStartDate"},
            },
            messages: {
                'data[UserExperience][name]': {
                    required: '<?php __('Required') ?>',
                },
                'data[UserExperience][company_name]': {
                    required: '<?php __('Required') ?>'
                },
                'data[UserExperience][start_date]': {
                    required: '<?php __('Required') ?>'
                },
                'data[UserExperience][end_date]': {
                    greaterThan: '<?php __('Invalid date range') ?>'
                }
            },
            errorClass: "error-message",
            errorElement: "div",
            errorPlacement: function(error, element) {
                if (element.get(0).type != "checkbox") {
                    error.insertAfter(element);
                } else {
                    element.parent().append(error);
                }
            },
            submitHandler: function(form) {
                $('div.message').html('').hide();
                $.ajax({
                    url: $(form).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: $(form).serialize(),
                    success: function(data) {

                        if (data.status == '1') {
                            $('div.message').html('<div id="flashMessage" class="success">' + data.message + '</div>').fadeIn();
                            setTimeout(function() {
                                location.reload()
                            }, '300');
                        } else {
                            $('div#flashMessage').addClass('fail');
                            $('div.message').html('<div id="flashMessage" class="fail">' + data.message + '</div>').fadeIn();
                        }
                    }
                })
            }
        });
    });
</script>






