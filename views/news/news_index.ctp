<div class="box news_sections countries">
    <div class="head">
        <h2><a href="<?php echo Router::url(array('controller' => 'news', 'action' => 'countries_index', $country['Country']['code'])) ?>" title="<?php echo$country['Country'][$lang . '_name'] ?>"><?php echo $country['Country'][$lang . '_name'] ?></a></h2>	
    </div>
    <div class="content">
        <?php if (!empty($news)) { ?>
            <div class="arena">
                <ul class="teasers_mod01">
                    <?php foreach ($news as $news) { ?>
                        <li>
                            <a title="<?php echo $news['News'][$lang . '_title'] ?>" href="<?php echo Router::url(array('controller' => 'news', 'action' => 'view', $news['News']['permalink'])) ?>">
                                <img src="<?php echo get_resized_image_url($news['News']['image']['basename'], 186, 193, false, false, false)?>"  alt="<?php echo $news['News'][$lang . '_title'] ?>" />
                                <span><?php echo $news['News'][$lang . '_title'] ?></span>

                            </a>
                        </li>
                    <? } ?>  
                </ul>
                <!--<a class="more" title="<?php echo $category['Category'][$lang . '_title'] ?>" href="<?php echo Router::url(array('controller' => 'news', 'action' => 'countries_index', $country['Country']['permalink'])) ?>"><?php __('Read more') ?></a>-->
            </div>
        <?php } else { ?>
            <div class="note"><?php __('There are not any news in this category'); ?> </div>   
        <? } ?>
    </div>
    <div class="foot"></div>
</div>

