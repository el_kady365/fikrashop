<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['News']['id'])) {

                    __('Edit News');
                } else {

                    __('Add News');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('News', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('title');
            if (!empty($this->data['News']['id'])) {
                echo $form->input('permalink');
            }
            echo $form->input('permalink');
            echo $form->input('brief',array('class'=>'input-xxlarge'));
            echo $fck->load('News', 'description');
            echo $form->input('image', array('type' => 'file',
                'between' => $this->element('image_element', array('info' => !empty($this->data['News']['image']) ? $this->data['News']['image'] : '', 'field' => 'image'))));
            echo $form->input('creation_date', array('type' => 'text', 'class' => 'hasDate'));
            echo $form->input('lang', array('label' => __('Language', true), 'options' => $languages));
            echo $form->input('active');
            echo $form->input('display_order');
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>
<?php
echo $javascript->link(array('jquery-ui-1.8.24.custom.min', 'jquery-ui-sliderAccess', 'jquery-ui-timepicker-addon'));
echo $html->css(array('jquery-ui-1.8.24.custom', 'datetimepicker'));
?>
<script type="text/javascript">

    $(document).ready(function() {
        $('.hasDate').datepicker({
            "dateFormat": 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
//            isRTL: true
        });
    });

</script>
