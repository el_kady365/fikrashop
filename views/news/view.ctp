<div class="news-box">
    <h3 class="font-a"><a href="<?php echo Router::url(array('action' => 'view', $news['News']['permalink'])) ?>"><?php echo $news['News']['title'] ?></a></h3>
    <h5 class="meta"><span class="icon-calendar"></span> <?php echo date('d M Y', strtotime($news['News']['creation_date'])) ?> </h5>
    <?php echo $news['News']['description']; ?>
</div> 
