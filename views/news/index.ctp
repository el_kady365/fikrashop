<?php
if (!empty($news)) {
    foreach ($news as $new) {
        ?>
        <div class="news-box">
            <h3 class="font-a"><a href="<?php echo Router::url(array('action' => 'view', $new['News']['permalink'])) ?>"><?php echo $new['News']['title'] ?></a></h3>
            <h5 class="meta"><span class="icon-calendar"></span> <?php echo date('d M Y', strtotime($new['News']['creation_date'])) ?> </h5>
            <p><?php echo nl2br($new['News']['brief']); ?></p>
            <a href="<?php echo Router::url(array('action' => 'view', $new['News']['permalink'])) ?>" class="btn btn-default"><span class="icon-plus-sign"></span> Read More</a>
        </div>
        <?php
    }
}
?>
