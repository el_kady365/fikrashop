<?php
//debug($categories);
if (!empty($countries)) {
    foreach ($countries as $key => $country) {
        ?>
        <div class="box news_sections countries">
            <div class="head">
                <h2><a href="<?php echo Router::url(array('controller' => 'news', 'action' => 'news_index', $country['Country']['code'])) ?>" title="<?php echo $country['Country'][$lang . '_name'] ?>"><?php echo $country['Country'][$lang . '_name'] ?></a></h2>	
            </div>
            <div class="content">
                <?php if (!empty($country['news'])) { ?>
                    <div class="arena">
                        <ul class="teasers_mod01">
                            <?php foreach ($country['news'] as $news) { ?>
                                <li>
                                    <a title="<?php echo $news['News'][$lang . '_title'] ?>" href="<?php echo Router::url(array('controller'=>'news','action'=>'view', $news['News']['permalink']))?>">
                                        <img src="<?php echo Router::url($news['News']['image']['thumb2']) ?>"  alt="<?php echo $news['News'][$lang . '_title'] ?>" />
                                        <span><?php echo $news['News'][$lang . '_title'] ?></span>

                                    </a>
                                </li>
                            <? } ?>  
                        </ul>
                        <a class="more" title="<?php echo $country['Country'][$lang . '_name'] ?>" href="<?php echo Router::url(array('controller' => 'news', 'action' => 'news_index', $country['Country']['code'])) ?>"><?php __('Read more') ?></a>
                    </div>
                <?php } else { ?>
                    <div class="note"><?php __('There are not any news in this category'); ?> </div>   
                <? } ?>
            </div>
            <div class="foot"></div>
        </div>
        <?php
    }
}
?>
