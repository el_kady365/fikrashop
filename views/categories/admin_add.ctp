<div class="row-fluid">
    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Category']['id'])) {

                    __('Edit Category');
                } else {

                    __('Add Category');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>

        <?php echo $form->create('Category', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('ar_title');
            echo $form->input('en_title');
            echo $form->input('permalink');
             echo $form->input('image', array('type' => 'file',
            'between' => $this->element('image_element', array('info' => !empty($this->data['Category']['image']) ? $this->data['Category']['image'] : $info, 'field' => 'image'))));
            echo $form->input('poll_id', array('label' => __('Questionnairs', true), 'options' => $polls, 'empty' => __('Choose Questionnaire', true)));
            echo $form->input('display_order');
            echo $form->input('active');
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>

