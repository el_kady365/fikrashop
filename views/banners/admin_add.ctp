<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Banner']['id'])) {

                    __('Edit Banner');
                } else {

                    __('Add Banner');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



<?php echo $form->create('Banner', array('type' => 'file')); ?>
        <div class="block-fluid">
        <?php
        echo $form->input('id');
        echo $form->input('name');
        echo $form->input('url');
        echo $form->input('image', array('type' => 'file',
            'between' => $this->element('image_element', array('info' => !empty($this->data['Banner']['image']) ? $this->data['Banner']['image'] : $info, 'field' => 'image'))));
        echo $form->input('active');
        echo $form->submit('Submit', array('class' => 'submit-green'));
        ?>
        </div>
            <?php echo $form->end(); ?>
    </div>
</div>

