<?php echo $this->element('user/user_leftmenu'); ?>
<div class="content-box">
    <div class="fekra-head">
        <!--<a class="btn btn-success right" href="<?php echo Router::url(array('action' => 'get_paid')) ?>"><i class="fa fa-plus-square"></i> <?php __('Get paid') ?></a>-->
        <h3 class="font-a sub-heading"><strong><?php echo $sub_title ?></strong></h3>
        <h5 class="font-a sub-heading"><?php __('Your Balance') ?>: 
            <?php
            echo format_price($find_user['User']['completed_payments']);
            !empty($find_user['User']['pending_payments']) ? print(' (' . __('Pending', true) . ':' . format_price($find_user['User']['pending_payments']) . ')') : '';
            ?>
        </h5>
        <div class="clear"></div>
    </div>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#Payments" data-toggle="tab"><i class="fa fa-download"></i> <?php __('Sent') ?></a></li>
        <li><a href="#Expences" data-toggle="tab"><i class="fa fa-location-arrow"></i> <?php __('Received') ?></a></li>
        <li><a href="#withdrwals" data-toggle="tab"><i class="fa fa-money"></i> <?php __('Withdrawals') ?></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="Payments">
            <table class="table">
                <tr>

                    <th><?php echo __('To', true) ?></th>
                    <th><?php echo __('Amount', true) ?></th>
                    <th><?php echo __('About', true) ?></th>
                    <th><?php echo __('Fekra', true) ?></th>
                    <th><?php echo __('Status', true) ?></th>
                    <th><?php echo __('Date', true) ?></th>

                    <th><?php echo __('Actions', true) ?></th>
                </tr>

                <?php
                if (!empty($sent)) {
                    foreach ($sent as $trans) {
                        ?>
                        <tr>

                            <td><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($trans['Transaction']['receiver_id']))) ?>"><?php echo $trans['Receiver']['first_name'] . ' ' . $trans['Receiver']['last_name'] ?></a></td>
                            <td><?php echo format_price($trans['Transaction']['total']) ?></td>
                            <td><?php echo!empty($trans['FekraPhase']['id']) ? __('Phase') . ': ' . $trans['FekraPhase']['title'] : (!empty($trans['FekraJob']['id']) ? __('Job') . ': ' . $trans['FekraJob']['title'] : ''); ?></td>
                            <td><a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $trans['Fekra']['id'], slug($trans['Fekra']['title']))) ?>"><?php echo $trans['Fekra']['title'] ?></a></td>
                            <td><?php echo $statuses[$trans['Transaction']['status']] ?></td>
                            <td><?php echo date('Y/m/d H:i:s', strtotime($trans['Transaction']['created'])) ?></td>
                            <td>
                                <?php if ($trans['Transaction']['status'] == 5) { ?> 
                                    <a href = "<?php echo Router::url(array('action' => 'refund', $trans['Transaction']['id'])); ?>" class = "btn btn-default btn-sm"> 
                                        <?php __('Refund'); ?>
                                    </a>
                                
                                    <a href = "<?php echo Router::url(array('action' => 'set_as_complete', $trans['Transaction']['id'])); ?>" class = "btn btn-default btn-sm"> 
                                        <?php __('Complete'); ?>
                                    </a>

                                    <?php
                                }
                                if ($trans['Transaction']['status'] == 1 || $trans['Transaction']['status'] == 8) {
                                    ?>
                                    <a href = "<?php echo Router::url(array('action' => 'set_as_complete', $trans['Transaction']['id'])); ?>" class = "btn btn-default btn-sm"> 
                                        <?php __('Complete'); ?>
                                    </a>

                                    <a href = "<?php echo Router::url(array('action' => 'complain', $trans['Transaction']['id'], 1)); ?>" class = "btn btn-default btn-sm"> 
                                        <?php __('Complain'); ?>
                                    </a>
                                    <?php
                                }
                                if ($trans['Transaction']['status'] == 0) {
                                    ?>
                                    <a href = "<?php echo Router::url(array('action' => 'complete', $trans['Transaction']['id'])); ?>" class = "btn btn-default btn-sm"> 
                                        <?php __('Pay'); ?>
                                    </a>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="7">
                            <div id="flashMessage" class="info"><?php __("There aren't any sent transactions") ?></div>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php
            if ($paginator->numbers()) {
                ?>
                <div class="paging">
                    <ul >
                        <?php
                        if ($paginator->hasNext()) {
                            echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                        if ($paginator->hasPrev()) {
                            echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        ?>

                    </ul>
                </div>
            <?php } ?>
        </div>
        <div class = "tab-pane" id = "Expences">
            <table class="table">
                <tr>

                    <th><?php echo __('From', true) ?></th>
                    <th><?php echo __('Amount', true) ?></th>
                    <th><?php echo __('About', true) ?></th>
                    <th><?php echo __('Fekra', true) ?></th>
                    <th><?php echo __('Status', true) ?></th>
                    <th><?php echo __('Date', true) ?></th>
                    <th><?php echo __('Actions', true) ?></th>
                </tr>

                <?php
                if (!empty($received)) {
                    foreach ($received as $trans) {
                        ?>
                        <tr>

                            <td><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($trans['Transaction']['sender_id']))) ?>"><?php echo $trans['Sender']['first_name'] . ' ' . $trans['Sender']['last_name'] ?></a></td>
                            <td><?php echo format_price($trans['Transaction']['actual']) ?></td>
                            <td><?php echo!empty($trans['FekraPhase']['id']) ? __('Phase') . ': ' . $trans['FekraPhase']['title'] : (!empty($trans['FekraJob']['id']) ? __('Job') . ': ' . $trans['FekraJob']['title'] : ''); ?></td>
                            <td><a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $trans['Fekra']['id'], slug($trans['Fekra']['title']))) ?>"><?php echo $trans['Fekra']['title'] ?></a></td>
                            <td><?php echo $statuses[$trans['Transaction']['status']] ?></td>
                            <td><?php echo date('Y/m/d H:i:s', strtotime($trans['Transaction']['created'])) ?></td>
                            <td>
                                <?php
                                if ($trans['Transaction']['status'] == 1) {
                                    ?> <a href = "<?php echo Router::url(array('action' => 'complain', $trans['Transaction']['id'], 2)); ?>" class = "btn btn-default btn-sm">
                                    <?php __('Complain'); ?>
                                    </a>   
                                    <?php
                                }
                                if ($trans['Transaction']['status'] == 2) {
                                    ?>
                                    <a  href = "<?php echo Router::url(array('action' => 'get_paid', $trans['Transaction']['id'])); ?>" class = "btn getpaid btn-default btn-sm">
                                        <?php __('Get Paid'); ?>
                                    </a>
                                <?php } ?>
                            </td>



                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="7">
                            <div id="flashMessage" class="info"><?php __('There aren\'t any received transactions') ?></div>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php
            if ($paginator->numbers()) {
                ?>
                <div class="paging">
                    <ul >
                        <?php
                        if ($paginator->hasNext()) {
                            echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                        if ($paginator->hasPrev()) {
                            echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        ?>

                    </ul>
                </div>
            <?php } ?>

        </div>
        <div class = "tab-pane" id = "withdrwals">
            <table class="table">
                <tr>

                    <th><?php echo __('Amount', true) ?></th>
                    <th><?php echo __('About', true) ?></th>
                    <th><?php echo __('Fekra', true) ?></th>
                    <th><?php echo __('Date', true) ?></th>
                </tr>

                <?php
                if (!empty($withdrawals)) {
                    foreach ($withdrawals as $trans) {
                        ?>
                        <tr>

                            <td><?php echo format_price($trans['Transaction']['actual']) ?></td>
                            <td><?php echo!empty($trans['FekraPhase']['id']) ? __('Phase') . ': ' . $trans['FekraPhase']['title'] : (!empty($trans['FekraJob']['id']) ? __('Job') . ': ' . $trans['FekraJob']['title'] : ''); ?></td>
                            <td><a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $trans['Fekra']['id'], slug($trans['Fekra']['title']))) ?>"><?php echo $trans['Fekra']['title'] ?></a></td>
                            <td><?php echo date('Y/m/d H:i:s', strtotime($trans['Transaction']['created'])) ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="5">
                            <div id="flashMessage" class="info"><?php __("There aren't any withdrawals") ?></div>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php
            if ($paginator->numbers()) {
                ?>
                <div class="paging">
                    <ul >
                        <?php
                        if ($paginator->hasNext()) {
                            echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                        if ($paginator->hasPrev()) {
                            echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        ?>

                    </ul>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php
echo $html->css('jquery-impromptu.min');
echo $javascript->link('jquery-impromptu.min');
?>
<script>
    $('.getpaid').click(function () {
        hrf = $(this).get(0).href;
        $this = $(this);
        var txt = '<?php
echo __('Are you sure you create an paypal account with your email?', true);
echo '\n';
echo $user['email'];
?>';

        $.prompt(txt, {
            buttons: {'<?php __('Get Paid') ?>': true, '<?php __('Cancel') ?>': false},
            close: function (e, v, m, f) {
                if (v) {

                    //Here is where you would do an ajax post to remove the user
                    //also you might want to print out true/false from your .php
                    //file and verify it has been removed before removing from the 
                    //html.  if false dont remove, $promt() the error.
                    self.location = hrf;

                }
                else {
                }

            }
        });
        return false;
    });
</script>
