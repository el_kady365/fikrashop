<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Complain') ?></strong></h3>

    <?php
    echo $form->create('Complain', array('type' => 'file', 'url' => array('controller' => 'transactions', 'action' => 'complain', $this->data['Complain']['transaction_id'], $this->data['Complain']['type']), 'class' => 'form default-forms font-a'));
    ?>

    <div class="panel panel-default">

        <div class="panel-body">
            <?php
            echo $form->hidden('transaction_id');
            echo $form->hidden('type');
            echo $form->input('message', array('type' => 'textarea', 'label' => __('Type your message to FikraShop board to complain this transaction', true)));
            ?>
        </div>
    </div>

    <div class="input submit">
        <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-mail-forward"></i> <?php __('Send') ?> </button>
    </div>
    <?php echo $form->end(); ?>
</div>
<?php echo $this->element('user/user_leftmenu'); ?>