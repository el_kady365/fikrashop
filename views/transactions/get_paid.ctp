<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('GetPaid') ?></strong></h3>
    <h5 class="font-a sub-heading">
        <?php __('Your Balance') ?>: 
        <?php
        echo format_price($find_user['User']['completed_payments']);
        !empty($find_user['User']['pending_payments']) ? print(' (' . __('Pending', true) . ':' . format_price($find_user['User']['pending_payments']) . ')')  : '';
        ?>
    </h5>
    <div id="flashMessage" class="info alert alert-warning"><?php __('Make sure that your paypal account') ?></div>
    <?php
    echo $form->create('Transaction', array('type' => 'file', 'url' => array('controller' => 'transactions', 'action' => 'get_paid'), 'class' => 'form default-forms font-a'));
    ?>

    <div class="panel panel-default">

        <div class="panel-body">
            <div class="input text">
                <label><?php __('Email')?></label>
                <div>
                    <p class="form-control-static"><?php echo $find_user['User']['email'] ?></p>
                </div>
            </div>
            <?php
            echo $form->input('total');
            ?>
        </div>
    </div>

    <div class="input submit">
        <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-check-circle"></i> <?php __('Withdraw') ?> </button>
    </div>
    <?php echo $form->end(); ?>
</div>
<?php echo $this->element('user/user_leftmenu'); ?>