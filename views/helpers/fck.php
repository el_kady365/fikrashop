<?php

class FckHelper extends Helper {
    /*
     * @var $javascript JavascriptHelper;  
     */

    var $helpers = Array('Html', 'Form', 'Javascript');

    function load($model, $id, $css = '', $toolbar = 'Full') {
        echo $this->Html->script('ckeditor/ckeditor', array('inline' => false));
        $did = '';
        $css_files = "'" . str_replace(",", "','", $css) . "'";
        $image_url = ($toolbar == 'Full') ? Router::url('/js/ckfinder/ckfinder.html?type=Images') : '';
        echo $this->Form->input($id);
        $did = '';
        $did = str_replace(' ', '', ucfirst($model) . Inflector::humanize($id));
        $code = "
            $(document).ready(function(){
            CKEDITOR.dtd.\$removeEmpty.span = 0;
            CKEDITOR.replace( '" . $did . "',{
                toolbar : '" . $toolbar . "',
        baseHref:'" . Router::url('/', true) . "',
	filebrowserBrowseUrl : '" . Router::url('/js/ckfinder/ckfinder.html') . "',
	filebrowserImageBrowseUrl : '" . $image_url . "',
	filebrowserFlashBrowseUrl : '" . Router::url('/js/ckfinder/ckfinder.html?type=Flash') . "',
	filebrowserUploadUrl : '" . Router::url('/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') . "',
	filebrowserImageUploadUrl : '" . Router::url('/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') . "',
	filebrowserFlashUploadUrl : '" . Router::url('/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') . "',
        contentsCss : [" . $css_files . "]   
}

);
});";
        echo $this->Javascript->codeBlock($code, array('inline' => false));
    }

}

?>
