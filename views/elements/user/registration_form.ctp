<div id="register" class="reveal-modal">
    <a class="close-reveal-modal">&#215;</a>
    <div class="form left">
        <h3 class="font-a"><strong><?php __('Register') ?></strong></h3>
        <div class="message"></div>
        <?php echo $form->create('User', array('class' => 'fonta', 'id' => 'registrationForm', 'url' => array('controller' => 'users', 'action' => 'register'))) ?>
        <div class="input text">

            <?php echo $form->input('full_name', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Full name', true))); ?>
        </div>
        <div class="input text">

            <?php echo $form->input('email', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Email', true))); ?>
        </div>
        <div class="input text">

            <?php echo $form->input('password', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Password', true))); ?>
        </div>
        <div class="input text">

            <?php echo $form->input('passwd', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Re-type Password', true))); ?>
        </div>

        <div class="input select">
            <?php echo $form->input('country_code', array('options' => $countries, 'empty' => __('Select Country', true), 'div' => false, 'label' => false, 'class' => 'required')); ?>
        </div>
        <div class="input checkbox">
            <input type="checkbox" name="data[User][agree]" />
            <label><?php __('I have read and agreed') ?> <a target="_blank" href="<?php echo Router::url(array('controller' => 'pages', 'action' => 'view', 'terms-conditions')) ?>"><u><?php __('terms and conditions') ?></u></a></label>
        </div>
        <div class="input submit">
            <button type="submit"><span class="btn btn-default"><?php __('Create your account') ?></span></button>
        </div>                   
        <?php echo $form->end(); ?>
    </div>
    <div class="extra-actions right">
        <h6 class="font-a"><strong><?php __('Register with social media') ?></strong></h6>
        <p><?php __("Do you have a facebook account or Linkedin account? dont waste your time and signup with your info. it's easy and quick") ?></p>

        <div class="social-login">
            <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'register', 'facebook')) ?>" class="social-btn fb-btn font-a"><span class="fb-btn-ico"></span><?php __('Register by Facebook') ?> </a>
            <a class="social-btn in-btn font-a" href="<?php echo Router::url('/users/register/linkedin') ?>"><span class="in-btn-ico"></span> <?php __('Register by Linkedin') ?></a>
        </div>

    </div>
    <div class="clear"></div>
    <?php
    echo $html->script(array('jquery.validate', 'jquery.nimble.loader'));
    ?>
    <script type="text/javascript">
        $(function() {
            var v = jQuery("#registrationForm").validate({
                rules: {
                    'data[User][email]': {
                        required: true,
                        email: true,
                        remote: {
                            url: '<?php echo Router::url(array('controller' => 'users', 'action' => 'check_input')) ?>'
                        }
                    },
                    'data[User][passwd]': {
                        equalTo: '#UserPassword'
                    },
                    'data[User][agree]': {
                        required: true
                    }
                }, messages: {
                    'data[User][email]': {
                        required: '<?php __('Required') ?>',
                        email: '<?php __('Please enter a valid email') ?>',
                        remote: '<?php __('This email is already used') ?>'
                    },
                    'data[User][full_name]': {
                        required: '<?php __('Required') ?>'
                    },
                    'data[User][country_id]': {
                        required: '<?php __('Required') ?>'
                    },
                    'data[User][password]': {
                        required: '<?php __('Required') ?>'
                    },
                    'data[User][passwd]': {
                        required: '<?php __('Required') ?>',
                        equalTo: '<?php __('Passwords do not match') ?>'
                    },
                },
                errorClass: "error-message",
                errorElement: "div",
                errorPlacement: function(error, element) {
                    if (element.get(0).type != "checkbox") {
                        error.insertAfter(element);
                    } else {
                        element.parent().append(error);
                    }
                },
                submitHandler: function(form) {
                    $('div.message').html('').hide();
                    $("#register").nimbleLoader("show", {
                        loaderClass: "loading_bar_1",
                        debug: true,
                        speed: 700,
                        hasBackground: true,
                        zIndex: 999,
                        backgroundColor: "#fff",
                        backgroundOpacity: 0.9
                    });
                    $.ajax({
                        url: $(form).attr('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: $(form).serialize(),
                        success: function(data) {
                            
//                            console.log(data);
                            $("#register").nimbleLoader("hide");
                            if (data.status == '1') {
                                $('div.message').html('<div id="flashMessage" class="success">' + data.message + '</div>').fadeIn();
                                setTimeout(function() {
                                    self.location = data.url;
                                }, '300')
                            } else {
                                $('div#flashMessage').addClass('fail');
                                $('div.message').html('<div id="flashMessage" class="fail">' + data.message + '</div>').fadeIn();
                            }
                        }
                    })
                }
            });

        });
    </script>
</div> 