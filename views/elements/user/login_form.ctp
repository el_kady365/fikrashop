<div id="login"  class="reveal-modal">
    <a class="close-reveal-modal">&#215;</a>

    <div class="form left">
        <h3 class="font-a"><strong><?php __('Login') ?></strong></h3>
        <div class="loginmessage"></div>
        <?php echo $form->create('User', array('class' => 'fonta loginForm', 'id' => 'loginForm', 'url' => array('controller' => 'users', 'action' => 'login'))) ?>

        <div class="input text">

            <?php echo $form->input('email', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'LoginEmailAddress', 'placeholder' => __('Email', true))); ?>
        </div>
        <div class="input text">
            <?php echo $form->input('password', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'LoginPassword', 'placeholder' => __('Password', true))); ?>
        </div>

        <div class="input submit">
            <button type="submit"><span class="btn btn-default"><?php __('Login') ?></span></button>
            <?php __('or'); ?>
            <a href="#" id="reveal-reg"><?php __('Register') ?></a>
        </div>                   
        <div class="input submit">
            <a href="#" id="reveal-forget"><?php __('Forget your password?') ?></a>
        </div>                   

        <?php echo $form->end(); ?>
    </div>
    <div class="extra-actions right">
        <h6 class="font-a"><strong><?php __('Login with social media') ?></strong></h6>


        <div class = "social-login">
            <a href = "<?php echo Router::url(array('controller' => 'users', 'action' => 'register', 'facebook')) ?>" class = "social-btn fb-btn font-a"><span class = "fb-btn-ico"></span><?php __('Login by Facebook') ?></a>
            <a class = "social-btn in-btn font-a" href = "<?php echo Router::url('/users/register/linkedin') ?>"><span class = "in-btn-ico"></span><?php __('Login by Linkedin') ?></a>
        </div>

    </div>
    <div class = "clear"></div>
    <?php
    echo $html->script(array('jquery.validate', 'jquery.nimble.loader'));
    ?>
    <script type="text/javascript">
        $(function() {
            $('#reveal-reg').click(function() {
                $('#login').trigger('reveal:close');
                $('#register').reveal();
                return false;
            });
            $('#reveal-forget').click(function() {
                $('#login').trigger('reveal:close');
                $('#forget').reveal();
                return false;
            });

            var v = jQuery("#loginForm").validate({
                rules: {
                    'data[User][email]': {
                        required: true,
                        email: true
                    }

                }, messages: {
                    'data[User][email]': {
                        required: '<?php __('Required') ?>',
                        email: '<?php __('Please enter a valid email') ?>',
                    },
                    'data[User][password]': {
                        required: '<?php __('Required') ?>'
                    }

                },
                errorClass: "error-message",
                errorElement: "div",
                errorPlacement: function(error, element) {
                    if (element.get(0).type != "checkbox") {
                        error.insertAfter(element);
                    } else {
                        element.parent().append(error);
                    }
                },
                submitHandler: function(form) {
                    $('div.loginmessage').html('').hide();
                    $("#login").nimbleLoader("show", {
                        loaderClass: "loading_bar_1",
                        debug: true,
                        speed: 1500,
                        hasBackground: true,
                        zIndex: 999,
                        backgroundColor: "#fff",
                        backgroundOpacity: 0.9
                    });
                    $.ajax({
                        url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>",
                        type: 'POST',
                        dataType: 'json',
                        data: $(form).serialize(),
                        success: function(data) {
//console.log(data);

                            $("#login").nimbleLoader("hide");

                            //                            console.log(data);
                            if (data.status == '1') {
                                $('div.loginmessage').html('<div id="flashMessage" class="success alert alert-success">' + data.message + '</div>').fadeIn();
                                setTimeout(function() {
                                    self.location = data.url;
                                }, '300')
                            } else {

                                $('div.loginmessage').html('<div id="flashMessage" class="fail alert alert-error">' + data.message + '</div>').fadeIn();
                            }
                        }
                    })
                }
            });
        });
    </script>
</div> 