<div class="account-box">
    <div class="members-nav widget font-a">
        <div class="widget-head"><h3><?php __('My Account') ?></h3></div>
        <ul>
            <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'dashboard', 'prefix' => false, 'user' => false)) ?>"><i class="fa fa-desktop fa-lg"></i> <?php __('Dashboard') ?></a></li>
            <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>"><i class="fa fa-gear fa-lg"></i> <?php __('My account') ?></a></li>
            <li><a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'user_ideas')) ?>"><i class="fa fa-lightbulb-o fa-lg"></i> <?php __("My IDeas") ?></a></li>
            <li><a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'add')) ?>"><i class="fa fa-plus-circle fa-lg"></i> <?php __("Add new idea") ?></a></li>
            <li><a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'user_jobs')) ?>"><i class="fa fa-briefcase fa-lg"></i> <?php __("My Jobs") ?></a></li>
            <li><a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'job_apps')) ?>"><i class="fa fa-check-square-o fa-lg"></i> <?php __("Job Applications") ?></a></li>
            <li><a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'add')) ?>"><i class="fa fa-plus-square fa-lg"></i> <?php __("Add new Job") ?></a></li>
            <li>
                <a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'inbox')) ?>"><i class="fa fa-envelope-o fa-lg"></i> <?php __('Messages') ?></a>  
            </li>
            <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'change_password')) ?>"><i class="fa fa-lock fa-lg"></i> <?php __('Change password') ?></a></li>
            <li><a href="<?php echo Router::url(array('controller'=>'transactions'))?>"><i class="fa fa-dollar  fa-lg"></i><?php __('Transactions')?></a></li>
        </ul>
    </div>
</div>