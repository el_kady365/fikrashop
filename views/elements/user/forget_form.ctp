<div id="forget" class="reveal-modal">
    <a class="close-reveal-modal">&#215;</a>
    <div class="form">
        <h3 class="font-a"><strong><?php __('Forget your password') ?></strong></h3>
        <div class="forgetmessage"></div>
        <?php echo $form->create('User', array('class' => 'fonta', 'id' => 'forgetForm', 'url' => array('controller' => 'users', 'action' => 'forget_password'))) ?>

        <div class="input text">

            <?php echo $form->input('email', array('div' => false, 'label' => false, 'class' => 'required', 'placeholder' => __('Email', true))); ?>
        </div>

        <div class="input submit">
            <button type="submit"><span class="btn btn-default"><?php __('Send') ?></span></button>
        </div>                   
        <?php echo $form->end(); ?>
    </div>
    <?php
    echo $html->script(array('jquery.validate', 'jquery.nimble.loader'));
    ?>
    <script type="text/javascript">
        $(function() {
            var v = jQuery("#forgetForm").validate({
                rules: {
                    'data[User][email]': {
                        required: true,
                        email: true,
                    }
                }, messages: {
                    'data[User][email]': {
                        required: '<?php __('Required') ?>',
                        email: '<?php __('Please enter a valid email') ?>'
                    }
                },
                errorClass: "error-message",
                errorElement: "div",
                errorPlacement: function(error, element) {
                    if (element.get(0).type != "checkbox") {
                        error.insertAfter(element);
                    } else {
                        element.parent().append(error);
                    }
                },
                submitHandler: function(form) {
                    $('div.forgetmessage').html('').hide();
                    $("#forget").nimbleLoader("show", {
                        loaderClass: "loading_bar_1",
                        debug: true,
                        speed: 700,
                        hasBackground: true,
                        zIndex: 999,
                        backgroundColor: "#fff",
                        backgroundOpacity: 0.9
                    });
                    $.ajax({
                        url: $(form).attr('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: $(form).serialize(),
                        success: function(data) {

//                            console.log(data);
                            $("#forget").nimbleLoader("hide");
                            if (data.status == '1') {
                                $('div.forgetmessage').html('<div id="flashMessage" class="success alert alert-success">' + data.message + '</div>').fadeIn();

                            } else {
                                $('div.forgetmessage').html('<div id="flashMessage" class="fail alert alert-error">' + data.message + '</div>').fadeIn();
                            }
                        }
                    })
                }
            });
        });
    </script>
</div> 