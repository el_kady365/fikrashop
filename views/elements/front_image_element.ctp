<?php if (isset($info['basename']) && isset($info['id'])) { ?>
    <div id="img-<?php echo $info['id'] ?>" >
        <?php if (!empty($info['dimensions'])) { ?>
            <p class="hint image_desc">Image size <?php echo $info['dimensions'] ?>   </p>
        <?php } ?>
        <span class="image_base_name"><?php echo $info['basename'] ?></span>
        <a href="<?php echo Router::url($info['path']); ?>" target="_blank" class="btn btn-sm btn-primary" ><span class="fa fa-search"></span> <?php __("Preview") ?></a>
        <a href="<?php echo Router::url(array('controller' => $info['controller'], 'action' => 'delete_field', $info['id'], 'image')) ?>"  class="btn btn-sm  btn-primary"><span class="fa fa-trash-o"></span> <?php __("Delete") ?></a>
        <div class="clear"></div>
    </div>
    <?php
}
?>