<div class="side-box">
    <div class="filters-form form font-a">
        <form id="SearchForm" action="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'ideas')) ?>" method="get">
            <?php echo $form->input('category_id', array('label' => false, 'placeholder' => __('Category', true), 'name' => 'category_id', 'options' => $listCategories, 'empty' => __('Choose category', true), 'selected' => isset($filters['category_id']) ? $filters['category_id']['val'] : '')); ?>
            <?php echo $form->input('title', array('label' => false, 'value' => isset($filters['keyword']) ? $filters['keyword']['val'] : '', 'placeholder' => __('Keyword', true), 'name' => 'keyword')); ?>
            <?php echo $form->input('price', array('label' => false, 'value' => isset($filters['price']) ? $filters['price']['val'] : '', /* 'options' => $priceRanges, */ 'empty' => __('Price', true), 'placeholder' => __('Price', true), 'name' => 'price')); ?>



            <?php //echo $form->input('country', array('label' => false, 'name' => 'country', 'options' => $countries, 'empty' => __('Select Country', true), 'selected' => isset($filters['country']) ? $filters['country']['val'] : '')); ?>
            <div class="input submit">
                <button type="submit"><span class="btn btn-default"><?php __('Show Results') ?></span></button>
            </div>
        </form>
    </div>
</div>
<div class="clear"></div>
