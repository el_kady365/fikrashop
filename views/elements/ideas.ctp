<div class="fekra-list">
    <?php
    if (!empty($fekras)) {
        foreach ($fekras as $fekra) {
//                debug($fekra);
            ?>
            <div class="fekra-box font-a"> 

                <a href="<?php echo Router::url(array('action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']))) ?>" class="fekra-thumb">
                    <?php if (!empty($fekra['Fekra']['image'])) { ?>
                        <img src="<?php echo Router::url($fekra['Fekra']['image']['thumb1']) ?>" alt="" title="" />
                    <?php } else { ?>
                        <img src="<?php echo Router::url('/css/img/fekra-thumb.png') ?>" alt="" title="" />
                    <?php } ?>
                </a>
                <div class="fekra-box-content">
                    <h3><a href="<?php echo Router::url(array('action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']))) ?>"><?php echo $fekra['Fekra']['title'] ?></a></h3>
                    <h6 class="shape"><?php echo date('d M Y', strtotime($fekra['Fekra']['created'])) ?>  -  <?php echo $fekra['Fekra']['Category'][$lang . '_title'] ?> </h6>
                    <p><?php echo $text->truncate(strip_tags($fekra['Fekra']['description'], 'br'), 250); ?></p>
                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url(array('action' => 'edit', $fekra['Fekra']['id'])); ?>" class="btn btn-default btn-sm"><span aria-hidden="true" class="st-ico icon-pencil "></span><?php __('Edit') ?></a>
                        <a href="<?php echo Router::url(array('action' => 'delete', $fekra['Fekra']['id'])); ?>" class="btn btn-danger btn-sm"><span aria-hidden="true" class="st-ico icon-remove-2 "></span><?php __('Delete') ?></a>
                    <?php } else { ?>
                        <a class="btn btn-default btn-sm" href="<?php echo Router::url(array('action' => 'view', $fekra['Fekra']['id'])); ?>"><?php __('More details') ?></a>   
                    <?php } ?>
                </div>
            </div>
            <?php
        }

        if ($paginator->numbers()) {
            ?>
            <div class="paging">
                <ul >
                    <?php
                    if ($paginator->hasNext()) {
                        echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                    }
                    echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                    if ($paginator->hasPrev()) {
                        echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                    }
                    ?>

                </ul>
            </div>
            <?php
        }
    } else {
        ?>
        <div id="flashMessage" class="info"><?php __('There aren\'t any ideas') ?></div>
    <?php } ?>
</div>