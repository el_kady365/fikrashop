<?php
if (!empty($messages)) {
    ?>
    <table class="table">

        <?php
        foreach ($messages as $message) {
            $class = "";

            if (isset($inbox_i) && $message['Message']['read'] != 1) {
                $class = "active";
            }
            ?>
            <tr class="<?php echo $class ?>">
                <td><span aria-hidden="true" class="fa fa-envelope-o fa-2x"></span></td> 
                <td>
                    <div>
                        <small>
                            <a href="<?php echo Router::url(array('action' => 'read', $message['Message']['id'])) ?>" title="<?php echo $message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'] ?>"><?php echo $message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'] ?></a>
                        </small>
                    </div>
                    <p class="sent-date"><i class="fa fa-lg fa-clock-o"></i> <?php echo date('d/m/Y', strtotime($message['Message']['created'])) ?></p>
                </td> 
                <td>
                    <div><small><a href="<?php echo Router::url(array('action' => 'read', $message['Message']['id'])) ?>"><strong><?php echo $message['Message']['subject'] ?></strong></a></small></div>
                    <small class="text-muted"><?php echo $text->truncate($message['Message']['body'], 50) ?></small>
                </td>
                <td>
                    <a href="<?php echo Router::url(array('action' => 'delete', $message['Message']['id'])); ?>" class="btn btn-danger btn-sm delete"><span aria-hidden="true" class="fa fa-trash-o"></span> <?php __('Delete') ?></a>
                </td>
            </tr>
        <?php } ?>

    </table>
    <?php
    if ($paginator->numbers()) {
        ?>
        <div class="paging">
            <ul >
                <?php
                if ($paginator->hasNext()) {
                    echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                }
                echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                if ($paginator->hasPrev()) {
                    echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                }
                ?>

            </ul>
        </div>
        <?php
    }
} else {
    ?>
    <div class="Notemessage" id="flashMessage"><?php __('There are not any messages right now') ?></div>
<?php } ?>
