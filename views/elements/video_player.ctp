<?php
$width = empty($width) ? 550 : $width;
$height = empty($height) ? 310 : $height;
$autoPlayer = empty($autoPlayer) ? false : true;
$thumb_image = empty($video['image']) ? '' : get_resized_image_url($video['image'], $width, $height);
$style = "width:{$width}px;height:{$height}px;";
$play_btn = '';
if (empty($autoPlayer) && !empty($thumb_image)) {
    $style.="background-image: url('$thumb_image');text-align: center;";
    $margin_top = ($height / 2) - 20;
    $play_btn = "<img src='" . $play_img . "' style='cursor: pointer;margin-top: {$margin_top}px;'/>";
}
?>
<?php
$rand_number = time() . "_" . rand(1, 1000);
$pos = strrpos($file, '.');
if ($pos !== false) {
    $ext = low(substr($file, $pos + 1));
}
?>

<!-- 1. skin -->
<link rel="stylesheet" href="//releases.flowplayer.org/5.4.6/skin/minimalist.css">



<!-- 3. flowplayer -->
<script src="//releases.flowplayer.org/5.4.6/flowplayer.min.js"></script>



<div class="player" preload="none" data-ratio="0.4167" style="<?php echo $style; ?>">
    <video>
        <source type="video/<?php echo $ext ?>" src="<?php echo $file ?>">
    </video>
</div>


<script type="text/javascript">
    $(function() {

        // install flowplayer to an element with CSS class "player"
        $(".player").flowplayer({swf: "/swf/5.4.6/flowplayer.swf"});

    });


</script>