<?php __('Hi')?> <?php echo $data['Receiver']['first_name']?>,
<br /><br />
<font face="Arial" style="font-size:18px;font-weight: bold;color:#ccc;" >
<?php __('You received a new message from')?> <?php echo $data['Sender']['first_name']?>
</font>
<br /><br />
<hr />
<p>
    <?php echo nl2br($data['Message']['body']); ?>
</p>
<br />
<?php __('To reply to this message please follow this link to')?>
<a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'read', $data['Message']['id']), true) ?>">
    <?php echo Router::url(array('controller' => 'messages', 'action' => 'read', $data['Message']['id']), true) ?>
</a>
