<font face="Arial" style="font-size:18px;" ><?php __('You have received a new message to contact the sender') ?></font><br /><br />

<ul style="font-family:Arial, Helvetica, sans-serif; font-size:12px; ">    
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', __('Name', true)) . ': ' . $data['name']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', __('Email', true)) . ': ' . $data['email']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', __('Message', true)) . ': ' . $data['message']); ?>
    </font>

    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $html->tag('li', $html->tag('strong', __('Date on', true)) . ': ' . date('d / m / Y')); ?>
    </font>

</ul>
