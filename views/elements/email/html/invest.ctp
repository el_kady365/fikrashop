<p>
    <?php __('Hi') ?> <?php echo $transaction['Receiver']['first_name'] ?>,
</p>
<br />
<br />
<font face="Arial" style="font-size:18px;" > <?php __('Congratulations! you have received fund for your idea') ?> <?php echo $transaction['Fekra']['title'] ?></font><br /><br />
<br />
<br />
<?php
echo $html->tag('strong', __('Fund details as below', true));
?>
<ul style = "font-family:Arial, Helvetica, sans-serif; font-size:12px; ">
    <?php
    echo $html->tag('li', $html->tag('strong', 'Name') . ': ' . $transaction['Sender']['first_name'] . ' ' . $transaction['Sender']['last_name']);
    echo $html->tag('li', $html->tag('strong', 'Amount') . ': ' . format_price($transaction['Transaction']['actual']));
    echo $html->tag('li', $html->tag('strong', 'Phase') . ': ' . $transaction['Transaction']['FekraPhase']['title']);
    ?>

</ul>

