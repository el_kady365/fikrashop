<p>
    Hi <?php echo $data['Sender']['first_name'] ?>,
</p>
<br /><br />
<p>
    The following transactions need your action:
</p>
<br /><br />
<p>
    Please change the status of this transaction to 'Complete' by following this url <br />
    <a href="<?php echo Router::url(array('controller' => 'transactions', 'action' => 'set_as_complete', $data['Transaction']['id']), true) ?>"><?php echo Router::url(array('controller' => 'transactions', 'action' => 'set_as_complete', $data['Transaction']['id']), true) ?></a>
</p>
<br /><br />
<p>
    If you are not satisfied with the deliverable you can send a complain to Fikra Shop by following this url <br />
    <a href="<?php echo Router::url(array('controller' => 'transactions', 'action' => 'complain', $data['Transaction']['id']), true) ?>"><?php echo Router::url(array('controller' => 'transactions', 'action' => 'complain', $data['Transaction']['id']), true) ?></a>
</p>
