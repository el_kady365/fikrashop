<?php __('Hi') ?> <?php echo $transaction['Receiver']['first_name'] ?>,
<br />
<br />
<p><?php
    echo __('There is a payment');?> <?php echo format_price($transaction['Transaction']['actual']) ?>  <?php echo __('USD',  true) . __('received against the job', true) ?> .<br /> 
    <?php __("The payment can be released when the payer will change the status of the transaction to 'Complete'.") ?>
</p>