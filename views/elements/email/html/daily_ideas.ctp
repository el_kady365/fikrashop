<font face="Arial" style="font-size:18px;" > <?php echo __('Ideas that match your chosen categories', true) ?></font>
<br />
<hr />

<br />

<p>
    <?php __('Hi') ?> <?php echo $user['User']['first_name'] ?>,
</p>

<?php foreach ($fekras as $fekra) { ?>
    <h3>
        <a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title'])), true) ?>" > <?php echo $fekra['Fekra']['title'] ?></a>
    </h3>
    <p>
        <?php echo $text->truncate($fekra['Fekra']['description'], 250); ?>
    </p>
    <br />
    <hr />
    <?php
}
?>