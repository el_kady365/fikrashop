<?php __('Hi') ?> <?php echo $transaction['Receiver']['first_name'] ?>,
<br />
<br />
<p>

    <?php __("Congratulation! The payer has changed the status of the transaction to 'Complete'.") ?><br>
    <?php __("To receive your payment to your account, please go to the link") ?>:
    <br>
    <a href="<?php echo Router::url(array('controller' => 'transactions', 'action' => 'index'), true) ?>">
        <?php echo Router::url(array('controller' => 'transactions', 'action' => 'index'), true) ?>
    </a> <?php __("and click 'Get Paid'.") ?>

</p>

