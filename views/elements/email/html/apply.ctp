<p>
    <?php __('Hi') ?> <?php echo $user['User']['first_name'] ?>,
</p>
<br />
<br />
<?php
echo __('You have received a bid on the job', true) . $jobapp['FekraJob']['title'] . __('from', true) . $job_app['User']['first_name'] . __('and the proposed amount is', true) . format_price($job_app['FekraJobApplication']['price']);
echo $html->tag('strong ', __('Please find the bid details as below', true)) . ',';
?>
<br />

<?php
nl2br($job_app['FekraJobApplication']['description']);
?>

<p>
    <?php __('For further details regarding this bid please click on this link') ?><br />
    <a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'view_app', $job_app['FekraJobApplication']['id']), true) ?>"><?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'view_app', $job_app['FekraJobApplication']['id']), true) ?></a>
</p>
