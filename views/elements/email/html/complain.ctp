<?php
if ($data['Complain']['type'] == 1) {
    $type = 'refund';
    $title = 'There is a complain for refund this transaction';
    $suser = $data['Transaction']['Sender']['first_name'] . ' ' . $data['Transaction']['Sender']['last_name'];
} else {
    $title = 'There is a complain for release this transaction';
    $suser = $data['Transaction']['Receiver']['first_name'] . ' ' . $data['Transaction']['Receiver']['last_name'];
    $type = 'release';
}
?>
<p>
    Hi Admin,
</p>
<br /><br />
<hr />
<p>
    The Member <?php echo $suser; ?> has sent a complain message for <?php echo $type; ?> his transaction as below,
</p>
<br /><br />
<p>
    <?php echo nl2br($data['Complain']['message']) ?>
</p>
<br /><br />
<p>
    For further details, please click on this Link <br />
    <a href="<?php echo Router::url(array('controller' => 'complains', 'action' => 'view', $data['Complain']['id'], 'admin' => true, 'prefix' => 'admin'), true) ?>">
        <?php echo Router::url(array('controller' => 'complains', 'action' => 'view', $data['Complain']['id'], 'admin' => true, 'prefix' => 'admin'), true) ?>
    </a>
</p>
