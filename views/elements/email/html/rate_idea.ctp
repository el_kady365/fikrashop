<p>
    <?php __('Hi') ?> <?php echo $board_user['User']['first_name'] ?>,
</p>
<br /><br />
<p>
    <?php __('You have received a request from') ?>
    <?php echo $sender['first_name'] . ' ' . $sender['last_name'] ?>
    <?php __('to rate the following idea(s)') ?>,
</p>
<br />
<?php
if ($selected_fekras) {
    foreach ($selected_fekras as $id => $title) {
        ?>
        <p> <?php echo $title ?> : <a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $id, slug($title), '#' => 'BordRating'), true) ?>"><?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $id, slug($title), '#' => 'BordRating'), true) ?></a></p>
        <br />
        <?
    }
}
?>
<br />
<?php __('Your feedback is important to support the creator for getting fund on the above ideas') ?>.