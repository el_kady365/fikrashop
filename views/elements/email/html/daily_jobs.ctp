<font face="Arial" style="font-size:18px;" > <?php echo __('Jobs that match your skills', true) ?></font>
<br />
<hr />

<br />

<p>
    <?php __('Hi') ?> <?php echo $user['User']['first_name'] ?>,
</p>

<?php foreach ($return_jobs as $job) { ?>

    <h3><a href="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'view', $job['id'], slug($job['title'])), true) ?>">
            <?php echo $job['title'] ?></a>
    </h3>

    <p>
        <?php echo $text->truncate($job['description'], 250); ?>
    </p>
    <br />
    <hr />
    <?php
}
?>