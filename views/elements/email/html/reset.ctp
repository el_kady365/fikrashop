<?php __('Hi') ?> <?php echo $user_data['User']['first_name'] ?>,
<br /><br />
<?php __('To reset your password please follow this link to') ?>
<br />
<a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'reset_password', $hash), true) ?>">
    <?php echo Router::url(array('controller' => 'users', 'action' => 'reset_password', $hash), true) ?>
</a>