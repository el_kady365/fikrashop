<p>
    <?php __('Hi') ?> <?php echo $fekra['User']['first_name'] ?>,
</p>
<br />
<br />
<?php
echo $html->tag('strong', __('Comment', true) . ':');
?>
<br />
<?php echo nl2br($comment['FekraComment']['content']) ?>
<br />
<br />
<p>
    <?php __('To view this comment please click on this url') ?><br />
    <a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']), '#' => 'Comments'), true) ?>"><?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']), '#' => 'Comments'), true) ?></a>
</p>
<br />
<p>
    <?php __('To approved this comment please click on this url') ?><br />
    <a href="<?php echo Router::url(array('controller' => 'fekra_comments', 'action' => 'approve', $comment['FekraComment']['id']), true) ?>"><?php echo Router::url(array('controller' => 'fekra_comments', 'action' => 'approve', $comment['FekraComment']['id']), true) ?></a>
</p>
<br />
<p>
    <?php __('To delete this comment please click on this url') ?><br />
    <a href="<?php echo Router::url(array('controller' => 'fekra_comments', 'action' => 'delete', $comment['FekraComment']['id']), true) ?>"><?php echo Router::url(array('controller' => 'fekra_comments', 'action' => 'delete', $comment['FekraComment']['id']), true) ?></a>
</p>