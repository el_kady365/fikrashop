<?php
if (isset($info['basename']) && isset($info['id'])) {
    ?>
    <div id="img-<?php echo $info['id'] ?>" >
        <p class="hint image_desc">File formats (<?php echo implode(',', $info['extensions']) ?>)</p>
        <span class="image_base_name"><?= $info['basename'] ?></span>
        <a href="<?php echo Router::url($info['full_path']); ?>" target="_blank" class="btn btn-small" ><i class="isw-zoom"></i><?php __("Preview") ?></a>
        <a href="<?php echo Router::url(array('controller' => $info['controller'], 'action' => 'delete_field', $info['id'], $info['field'])) ?>"  class="btn btn-small"><i class="isw-delete"></i><?php __("Delete") ?></a>
        <div class="clear"></div>
    </div>
    <?php
} else {
    if (isset($info) && is_array($info)) {
        ?>
        <p class="hint image_desc">File formats (<?php echo implode(',', $info['extensions'][$field]) ?>)</p> 
    <?php }
}
?>
