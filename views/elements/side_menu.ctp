
<?php
$admin_menu = array(
    'Categories' => array('class' => 'openable', 'icon' => '<i class="isw-folder"></i>',
        'subs' => array(
            __('List Categories', true) => array('url' => array('controller' => 'categories', 'action' => 'index')),
            __('Add category', true) => array('url' => array('controller' => 'categories', 'action' => 'add')),
        )
    ),
    'Ideas' => array('class' => 'openable', 'icon' => '<i class="isw-folder"></i>',
        'subs' => array(
            __('List Ideas', true) => array('url' => array('controller' => 'fekras', 'action' => 'index')),
            
        )
    ),
    'Polls' => array('class' => 'openable', 'icon' => '<i class="isw-target"></i>',
        'subs' => array(
            __('List Polls', true) => array('url' => array('controller' => 'polls', 'action' => 'index')),
            __('Add Poll', true) => array('url' => array('controller' => 'polls', 'action' => 'add')),
        )
    ),
    'Pages' => array('class' => 'openable', 'icon' => '<span class="isw-grid"></span>',
        'subs' => array(
            __('List Pages', true) => array('url' => array('controller' => 'pages', 'action' => 'index')),
            __('List Mainmenu items', true) => array('url' => array('controller' => 'pages', 'action' => 'index', '?' => array('main' => 1))),
            __('Add Page', true) => array('url' => array('controller' => 'pages', 'action' => 'add')),
        )
    ),
    'News' => array('class' => 'openable', 'icon' => '<i class="isw-time"></i>',
        'subs' => array(
            __('List News', true) => array('url' => array('controller' => 'news', 'action' => 'index')),
            __('Add News', true) => array('url' => array('controller' => 'news', 'action' => 'add')),
        )
    ),
    'Banners' => array('class' => 'openable', 'icon' => '<i class="isw-picture"></i>',
        'subs' => array(
            __('List Banners', true) => array('url' => array('controller' => 'banners', 'action' => 'index')),
            __('Add Banner', true) => array('url' => array('controller' => 'banners', 'action' => 'add')),
        )
    ),
    'FAQs' => array('class' => 'openable', 'icon' => '<i class="isw-chats"></i>',
        'subs' => array(
            __('List FAQs', true) => array('url' => array('controller' => 'faqs', 'action' => 'index')),
            __('Add FAQ', true) => array('url' => array('controller' => 'faqs', 'action' => 'add')),
        )
    ),
    'Contacts' => array('class' => 'openable', 'icon' => '<i class="isw-mail"></i>',
        'subs' => array(
            __('List Contact Us messages', true) => array('url' => array('controller' => 'contacts', 'action' => 'index')),
        )
    ),
    'Snippets' => array('class' => 'openable', 'icon' => '<i class="isw-edit"></i>',
        'subs' => array(
            __('List Snippets', true) => array('url' => array('controller' => 'snippets', 'action' => 'index')),
            __('Add Snippet', true) => array('url' => array('controller' => 'snippets', 'action' => 'add')),
        )
    ),
    'Users' => array('class' => 'openable', 'icon' => '<i class="isw-user"></i>',
        'subs' => array(
            __('List Users', true) => array('url' => array('controller' => 'users', 'action' => 'index')),
            __('Add Users', true) => array('url' => array('controller' => 'users', 'action' => 'add')),
        )
    ),
    'Newsletters' => array('class' => 'openable', 'icon' => '<i class="isw-mail"></i>',
        'subs' => array(
            __('List Newsletters', true) => array('url' => array('controller' => 'newsletters', 'action' => 'index')),
            __('Export', true) => array('url' => array('controller' => 'newsletters', 'action' => 'export', 'ext' => 'csv')),
        )
    ),
    'Sponsors' => array('class' => 'openable', 'icon' => '<i class="isw-users"></i>',
        'subs' => array(
            __('List Sponsors', true) => array('url' => array('controller' => 'sponsors', 'action' => 'index')),
            __('Add Sponsor', true) => array('url' => array('controller' => 'sponsors', 'action' => 'add')),
        )
    ),
    'Skills' => array('class' => 'openable', 'icon' => '<i class=" isw-download"></i>',
        'subs' => array(
            __('List Skills', true) => array('url' => array('controller' => 'skills', 'action' => 'index')),
            __('Add Skill', true) => array('url' => array('controller' => 'skills', 'action' => 'add')),
        )
    ),
    'Complains' => array('class' => 'openable', 'icon' => '<i class=" isw-folder"></i>',
        'subs' => array(
            __('List Complains', true) => array('url' => array('controller' => 'complains', 'action' => 'index')),
            
        )
    ),
    'Configurations' => array('class' => 'openable', 'icon' => '<i class="isw-settings"></i>',
        'subs' => array(
            __('Edit Configurations', true) => array('url' => array('controller' => 'configurations', 'action' => 'edit')),
        )
    ),
    'Admins' => array('class' => 'openable', 'icon' => '<i class="isw-power"></i>',
        'subs' => array(
            __('List Admins', true) => array('url' => array('controller' => 'admins', 'action' => 'index')),
            __('Add Admin', true) => array('url' => array('controller' => 'admins', 'action' => 'add')),
        )
    )
);

$publisher_menu = array(
    __('Control panel', true) => array('url' => '/admin/',
        'subs' => array(
            'Items' => array('class' => 'openable',
                'subs' => array(
                    'List Items' => array('url' => array('controller' => 'items', 'action' => 'index')),
                    'Add Item' => array('url' => array('controller' => 'items', 'action' => 'add')),
                )
            ),
        )
    )
);
?>
<div class="menu">                

    <div class="breadLine">            
        <div class="arrow"></div>
        <div class="adminControl active">
            Hi, <?php echo $admin['Admin']['username'] ?>
        </div>
    </div>

    <div class="admin">

        <ul class="control">                
            <li><span class="icon-comment"></span> <a href="<?php echo Router::url(array('controller' => 'contacts', 'action' => 'index')); ?>">Messages</a> </li>
            <li><span class="icon-cog"></span> <a href="<?php echo Router::url(array('controller' => 'configurations', 'action' => 'edit')); ?>">Settings</a></li>
            <li><span class="icon-share-alt"></span> <a href="<?php echo Router::url(array('controller' => 'admins', 'action' => 'logout')); ?>">Logout</a></li>
        </ul>
        <!--        <div class="info">
                    <span>Welcom back! Your last visit: 24.10.2012 in 19:55</span>
                </div>-->
    </div>

    <ul class="navigation">       

        <?php
        if ($admin['Admin']['publisher'] == 1) {
            $menu = $publisher_menu;
        } else {
            $menu = $admin_menu;
        }
        side_menu($menu);
        ?>

    </ul>

</div>


<script type="text/javascript">
    $(document).ready(function() {
//        $("#browser").treeview({});
        var $id = '<?php echo isset($menuId) ? $menuId : Inflector::camelize($this->params['controller']); ?>';
        $('#' + $id).addClass('active');
//        $('#' + $id).find('> div').removeClass('closed-hitarea').removeClass('expandable-hitarea').addClass('opened-hitarea').addClass('collapsable-hitarea');
//        $('#' + $id).find('> div').removeClass('lastExpandable-hitarea').addClass('lastCollapsable-hitarea');
//        $('#' + $id).find(' > ul').show();

    });
</script>

<?php

function side_menu($menu = array()) {
    foreach ($menu as $key => $item) {
        $class = (!empty($item['subs'])) ? 'folder' : 'file';
        $class2 = (!empty($item['class'])) ? $item['class'] : '';
        ?>
        <li class="<?php echo $class2; ?>" id="<?php echo (isset($item['id'])) ? Inflector::camelize($item['id']) : Inflector::camelize($key); ?>">
            <a href="<?php echo (!empty($item['url'])) ? Router::url($item['url']) : "#" ?>">
                <?php if (empty($item['subs'])) { ?>
                    <span class="icon-chevron-right"></span>
                <?php } ?>

                <?php
                if (!empty($item['icon'])) {
                    echo $item['icon'];
                }
                ?>
                <span class="text">  <?php echo __($key, true); ?></span>
            </a>


            <?php
            if ($class == 'folder') {
                echo '<ul>';
                side_menu($item['subs']);
                echo '</ul>';
            }
            ?>
        </li>

        <?php
    }
}
?>






