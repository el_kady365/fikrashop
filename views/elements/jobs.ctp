<div class="fekra-list">
    <?php
    if (!empty($jobs)) {
        foreach ($jobs as $job) {
//                debug($job);
            ?>
            <div class="fekra-box font-a"> 
                <div class="fekra-box-content">
                    <h3><a href="<?php echo Router::url(array('action' => 'view', $job['FekraJob']['id'], slug($job['FekraJob']['title']))) ?>"><?php echo $job['FekraJob']['title'] ?></a></h3>
                    <h6 class="shape"><?php echo __('Price', true) . ': ' . format_price($job['FekraJob']['price']) ?>  </h6>
                    <h6 class="shape"><?php __('Date posted') ?>: <?php echo $time->relativeTime($job['FekraJob']['created']) ?> - <?php echo $job['Fekra']['title'] ?> </h6>
                    <p><?php echo $text->truncate(strip_tags($job['FekraJob']['description'], 'br'), 250); ?></p>
                    <?php if (isset($edit)) { ?>
                        <a href="<?php echo Router::url(array('action' => 'edit', $job['FekraJob']['id'])); ?>" class="btn btn-default btn-sm"><span aria-hidden="true" class="fa fa-pencil "></span><?php __('Edit') ?></a>
                        <a href="<?php echo Router::url(array('action' => 'delete', $job['FekraJob']['id'])); ?>" class="btn btn-danger btn-sm"><span aria-hidden="true" class="fa fa-trash-o "></span><?php __('Delete') ?></a>
                    <?php } else { ?>
                        <a class="btn btn-default btn-sm" href="<?php echo Router::url(array('action' => 'view', $job['FekraJob']['id'])); ?>"><?php __('More details') ?></a>   
                    <?php } ?>
                </div>
            </div>
            <?php
        }

        if ($paginator->numbers()) {
            ?>
            <div class="paging">
                <ul >
                    <?php
                    if ($paginator->hasNext()) {
                        echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                    }
                    echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                    if ($paginator->hasPrev()) {
                        echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                    }
                    ?>

                </ul>
            </div>
            <?php
        }
    } else {
        ?>
        <div id="flashMessage" class="info"><?php __("There aren't any jobs") ?></div>
    <?php } ?>
</div>