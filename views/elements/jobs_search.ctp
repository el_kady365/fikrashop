<div class="side-box">
    <div class="filters-form form font-a">
        <form id="SearchForm" action="<?php echo Router::url(array('controller' => 'fekra_jobs', 'action' => 'jobs')) ?>" method="get">
            <?php echo $form->input('category_id', array('label' => false, 'placeholder' => __('Category', true), 'name' => 'category_id', 'options' => $listCategories, 'empty' => __('Choose category', true), 'selected' => isset($filters['category_id']) ? $filters['category_id']['val'] : '')); ?>
            <?php echo $form->input('title', array('label' => false, 'value' => isset($filters['keyword']) ? $filters['keyword']['val'] : '', 'placeholder' => __('Keyword', true), 'name' => 'keyword')); ?>
            <?php echo $form->input('price', array('label' => false, 'value' => isset($filters['price']) ? $filters['price']['val'] : '', /* 'options' => $priceRanges, */ 'empty' => __('Price', true), 'placeholder' => __('Price', true), 'name' => 'price')); ?>
            <div class="input checkgroup">
                <h5><?php __('Skills')?></h5>
                <?php echo $form->input('skills', array('multiple' => 'checkbox', 'div' => false, 'label' => false, 'value' => isset($filters['skills']) ? $filters['skills']['val'] : '', 'options' => $skills, 'name' => 'skills', 'style' => 'width:186px')); ?>
            </div>


            <?php //echo $form->input('country', array('label' => false, 'name' => 'country', 'options' => $countries, 'empty' => __('Select Country', true), 'selected' => isset($filters['country']) ? $filters['country']['val'] : '')); ?>
            <div class="input submit">
                <button type="submit"><span class="btn btn-default"><?php __('Show Results') ?></span></button>
            </div>
        </form>
    </div>
</div>
<div class="clear"></div>
<?php
echo $html->css(array('jquery-ui-1.9.2.custom.min', 'select2'));
echo $javascript->link(array('jquery-ui-1.9.2.custom.min', 'select2'));
?>
<script>
    $(document).ready(function() {

//        $('#skills').select2({
//            placeholder: "<?php echo __('Select Skill', true) ?>",
//        });

    });

</script>