<?php if (isset($info['basename']) && isset($info['id'])) { ?>
    <div id="img-<?php echo $info['id'] ?>" >
        <?php if (!empty($info['dimensions'])) { ?>
            <p class="hint image_desc">Image size <?= $info['dimensions'] ?>   </p>
        <?php } ?>
        <span class="image_base_name"><?= $info['basename'] ?></span>
        <a href="<?php echo Router::url($info['path']); ?>" target="_blank" class="btn btn-small" ><i class="isw-zoom"></i><?php __("Preview") ?></a>
        <a href="<?php echo Router::url(array('controller' => $info['controller'], 'action' => 'delete_field', $info['id'], 'image')) ?>"  class="btn btn-small"><i class="isw-delete"></i><?php __("Delete") ?></a>
        <div class="clear"></div>
    </div>
    <?php
} else {
    if (!empty($info[$field]['resize'])) {
        ?>
        <div>
            <p class="hint image_desc">Image size <?= $info[$field]['resize']['width'] . 'px X ' . $info[$field]['resize']['height'] . 'px' ?>   </p> 
        </div>

        <?php
    }
}
?>