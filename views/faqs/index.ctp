<?php
if (!empty($faqs)) {
    foreach ($faqs as $faq) {
        ?>
        <div class="faq-box">
            <h3 class="font-a faq-question"><a href="#"><span class="faq-icon icon-plus-sign"></span> <?php echo $faq['Faq']['question'] ?></a></h3>
            <div class="faq-answer">
                <?php echo $faq['Faq']['answer'] ?>
            </div>
        </div>
        <?php
    }
}
?>
<script type="text/javascript">
    $(function() {
        $('.faq-answer:first').show();
        $('.faq-icon:first').removeClass('icon-plus-sign').addClass('icon-minus-sign');
        $('.faq-question a').click(function() {
            $('.faq-answer').slideUp();
            $('.faq-icon').removeClass('icon-minus-sign').addClass('icon-plus-sign');
            if ($(this).closest('.faq-box').find('.faq-answer').is(':hidden')) {
                $(this).find('.faq-icon').removeClass('icon-plus-sign').addClass('icon-minus-sign');
                $(this).closest('.faq-box').find('.faq-answer').slideDown();
//                return false;
            }
//            $(this).find('.faq-icon').removeClass('icon-plus-sign').addClass('icon-minus-sign');
            return false;
        });
    });
</script>