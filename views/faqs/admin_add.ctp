<div class="row-fluid">
    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Faq']['id'])) {
                    __('Edit Faq');
                } else {
                    __('Add Faq');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>
        <?php echo $form->create('Faq', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('question');
            echo $fck->load('Faq', 'answer');
            echo $form->input('lang', array('label' => __('Language', true), 'options' => $languages));
            echo $form->input('active');
            echo $form->input('display_order');
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>

