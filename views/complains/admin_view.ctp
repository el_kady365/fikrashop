<?php
//debug($complain);
if ($complain['Complain']['type'] == 1) {
    $type = 'Refund';
    $amount = $complain['Transaction']['total'];
    $title = 'View complain for refund this transaction';
    $suser = $complain['Transaction']['Sender']['first_name'] . ' ' . $complain['Transaction']['Sender']['last_name'];
    $url = Router::url(array('action' => 'refund', $complain['Complain']['id']));
} else {
    $title = 'View complain for release this transaction';
    $suser = $complain['Transaction']['Receiver']['first_name'] . ' ' . $complain['Transaction']['Receiver']['last_name'];
    $amount = $complain['Transaction']['actual'];
    $type = 'Release';
    $url = Router::url(array('action' => 'complete', $complain['Complain']['id']));
}
?>
<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                echo $title;
                ?>
            </h1>
            <div class="clear"></div>
        </div>
        <div class="block">
            <div class="row-form">
                <div class="span4">User:</div>
                <div class="span8"><?php echo $suser ?></div>
                <div class="clear"></div>
            </div>
            <div class="row-form">
                <div class="span4">Transaction amount:</div>
                <div class="span8"><?php echo format_price($amount) ?></div>
                <div class="clear"></div>
            </div>
            <div class="row-form">
                <div class="span4">Message:</div>
                <div class="span8"><p><?php echo nl2br($complain['Complain']['message']) ?></p></div>
                <div class="clear"></div>
            </div>
            <?php if(empty($complain['Complain']['status'])){?>
            <div class="form-actions">
                <a href="<?php echo $url ?>" class="btn btn-default"><?php echo $type ?> </a>
            </div>
            <?php }?>

        </div>


    </div>
</div>
