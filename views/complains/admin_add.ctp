<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Complain']['id'])) {

                    __('Edit Complain');
                } else {

                    __('Add Complain');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('Complain', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('type');
            echo $form->input('transaction_id');
            echo $form->input('message');
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>

