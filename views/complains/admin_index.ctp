
<div class="row-fluid">
    <div class="span12"> 
        <div class="head">
            <div class="isw-grid"></div>
            <h1><?php __('Complains'); ?></span></h1>

            <ul class="buttons">
                <li><a href="<?php echo Router::url(array('action' => 'add')); ?>" class="isw-plus"></a></li>                                                        
                <li>
                    <a href="#" class="isw-settings"></a>
                    <ul class="dd-list">
                        <li><a href="#" class="delete"><span class="isw-delete"></span>Delete</a></li>
                    </ul>
                </li>
            </ul>  

            <div class="clear"></div>
        </div>
        <div class="block-fluid table-sorting">
            <form action="<?php echo Router::url(array("action" => "do_operation")) ?>" id="forn" method="post">
                <table class="table" id="tSortable"  cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th  width="5%"><input type="checkbox" name="checkall" /></th>
                            <th ><?php __('id'); ?></th>
                            <th ><?php __('Ask for ?'); ?></th>
                            <th ><?php __('From'); ?></th>
                            <th class="actions"><?php __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($complains as $complain):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                $class = ' class="altrow"';
                            }
                            if ($complain['Complain']['type'] == 1) {
                                $type = 'Refund';
                                $amount = $complain['Transaction']['total'];
                                $title = 'View complain for refund this transaction';
                                $suser = $complain['Transaction']['Sender']['first_name'] . ' ' . $complain['Transaction']['Sender']['last_name'];
                                $suser_id = base64_encode($complain['Transaction']['Sender']['id']);
                                $url = array('action' => 'refund', $complain['Complain']['id']);
                            } else {
                                $title = 'View complain for release this transaction';
                                $suser = $complain['Transaction']['Receiver']['first_name'] . ' ' . $complain['Transaction']['Receiver']['last_name'];
                                $suser_id = base64_encode($complain['Transaction']['Receiver']['id']);
                                $amount = $complain['Transaction']['actual'];
                                $type = 'Release';
                                $url = array('action' => 'complete', $complain['Complain']['id']);
                            }
                            ?>
                            <tr<?php echo $class; ?>>
                                <td>
                                    <input  type="checkbox" name="chk[]" value="<?php echo $complain['Complain']['id']; ?>" />
                                </td>
                                <td>
                                    <?php echo $complain['Complain']['id']; ?>
                                </td>

                                <td>
                                    <?php echo $type ?>
                                </td>
                                <td>
                                    <a target="_blank" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $suser_id, 'prefix' => false, 'admin' => false)) ?>"> <?php echo $suser ?></a>
                                </td>
                                <td class="actions">
                                    <?php
                                    if (empty($complain['Complain']['status'])) {
                                        echo $html->link('<i class="isw-settings"></i> ' . $type, $url, array('class' => 'btn btn-small', 'escape' => false));
                                    }
                                    ?>
                                    <?php echo $html->link('<i class="isw-zoom"></i> ' . __('View', true), array('action' => 'view', $complain['Complain']['id']), array('class' => 'btn btn-small', 'escape' => false)); ?>
                                    <?php echo $html->link('<i class="isw-delete"></i> ' . __('Delete', true), array('action' => 'delete', $complain['Complain']['id']), array('class' => 'btn btn-small', 'escape' => false), sprintf(__('Are you sure you want to delete # %s?', true), $complain['Complain']['id'])); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </form>
            <div class="clear"></div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".dd-list li a").click(function() {
                    action = $(this).attr('class');
                    form = $(this).parents().find('form');
                    if (action != "")
                    {
                        if ($('input[name="chk[]"]:checked').length == 0)
                        {
                            alert("You must choose one element at least");
                        } else {
                            del = confirm("Are you sure you want to perform this operation?");
                            if (del)
                            {
                                form_action = form.attr('action');
                                form.attr('action', form_action + '?action=' + action);
                                form.submit();
                            } else {
                                $(this).val('');
                            }
                        }

                    }
                });

                if ($("#tSortable").length > 0)
                {
                    $("#tSortable").dataTable({"iDisplayLength": 10, "aLengthMenu": [5, 10, 25, 50, 100], "sPaginationType": "full_numbers", "aoColumns": [{"bSortable": false}, null, null, null, {"bSortable": false}]});
                }

            });
        </script>

    </div> 
</div>

