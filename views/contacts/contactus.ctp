


<div class="contact-page">
    <div class="contact-form form left font-a">
        <h3 class="font-a"><strong><?php __('Contact us') ?></strong></h3>
        <?php echo $form->create('Contact', array('url' => array('action' => 'contactus'))) ?>
        <div class="input text">

            <?php echo $form->input('name', array('label' => false, 'div' => false, 'placeholder' => __('Name', true))); ?>
        </div>
        <div class="input text">
            
            <?php echo $form->input('email', array('label' => false, 'div' => false, 'placeholder' => __('Email', true))); ?>
        </div>
        <div class="input text">
            
            <?php echo $form->input('subject', array('label' => false, 'div' => false, 'placeholder' => __('Subject', true))); ?>

        </div>
        <div class="input textarea">
            
            <?php echo $form->input('message', array('label' => false, 'div' => false, 'placeholder' => __('Message', true))); ?>

        </div>
        <div class="input submit">
            <button type="submit"><span class="btn btn-default"><?php __('Send message')?></span></button>
        </div>
        <?php echo $form->end(); ?>
    </div>
    <div class="contact-map right"><div class="map" id="map_canvas" style="width: 362px;height:363px"></div></div>
    <div class="clear"></div> 
</div>


<?php
$map_details = explode(',', $coordinates);
$latlng = $map_details[0] . ',' . $map_details[1];
$zoom = $map_details[2];
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var map;
    var markersArray = [];

    function initMap()
    {

        var latlng = new google.maps.LatLng(<?php echo $latlng; ?>);
        var myOptions = {
            zoom: <?php echo $zoom ?>,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map
        });


        // add a click event handler to the map object
    }

    window.onload = initMap;
   
</script>