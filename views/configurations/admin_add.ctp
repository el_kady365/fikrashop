<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1><?php __('Edit Configurations'); ?></h1>
            <div class="clear"></div>
        </div>
        <?php echo $form->create('Configuration', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id', array('value' => '1'));
            echo $form->input('site_name');
            echo $form->input('admin_email');
            echo $form->input('admin_send_mail_from');
//    echo $form->input('facebook_url');
//    echo $form->input('twitter_url');


            echo $form->input('home_keywords');
            echo $form->input('home_description');
            echo $form->input('facebook');
            echo $form->input('twitter');
            echo $form->input('google');
            echo $form->input('linkedin');
            echo $form->input('coordinates');
            ?>
            <div id="map_canvas" style="height:300px;"></div>
            <?
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?php


if (empty($this->data['Configuration']['coordinates'])) {
//    $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$Addrress}&sensor=false";
//
//    $ch = curl_init($url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    $geocode = curl_exec($ch);
//    $output = json_decode($geocode);
//    if ($output->status == "OK") {
//        $formated_address = $output->results[0]->formatted_address;
//        $lat = $output->results[0]->geometry->location->lat;
//        $lng = $output->results[0]->geometry->location->lng;
        $latlng = "0, 0";
        $zoom = 1;
//    }
} else {
    $map_details = explode(',', $this->data['Configuration']['coordinates']);
    $latlng = $map_details[0] . ',' . $map_details[1];
    $zoom = $map_details[2];
}
?>
<script type="text/javascript">
    var map;
    var markersArray = [];

    function initMap()
    {

        var latlng = new google.maps.LatLng(<?php echo $latlng; ?>);
        var myOptions = {
            zoom: <?php echo $zoom ?>,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map
        });
        markersArray.push(marker);
        document.getElementById("ConfigurationCoordinates").value = map.getCenter().lat() + "," + map.getCenter().lng() + "," + map.getZoom();
        // add a click event handler to the map object
        google.maps.event.addListener(map, "click", function(event)
        {
            // place a marker
            placeMarker(event.latLng);

            // display the lat/lng in your form's lat/lng fields
            //            document.getElementById("latFld").value = event.latLng.lat();
            //            document.getElementById("lngFld").value = event.latLng.lng();
            document.getElementById("ConfigurationCoordinates").value = event.latLng.lat() + "," + event.latLng.lng() + "," + map.getZoom();

        });


    }
    function placeMarker(location) {
        // first remove all markers if there are any
        deleteOverlays();

        var marker = new google.maps.Marker({
            position: location,
            map: map
        });

        // add marker in markers array
        markersArray.push(marker);

        //map.setCenter(location);
    }

    // Deletes all markers in the array by removing references to them
    function deleteOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }
    window.onload = initMap;

</script>


