
<?php
if (!empty($this->data['Poll']['id'])) {
    $url = array('action' => 'edit', $this->data['Poll']['id']);
    $title = __('Edit Poll', true);
} else {
    $url = array('action' => 'add', $course_id, '?' => array('level' => $_GET['level']));
    $title = __('Add Poll', true);
}
?>
<div class="breadcrumbwidget">
    <ul class="breadcrumb">
        <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'dashboard')) ?>"><?php __('Home') ?></a></li>
        <span class="divider"> / </span>
        <li><a href="<?php echo Router::url(array('controller' => 'courses', 'action' => 'view', $ccourse['Course']['id'])) ?>"> <?php echo $ccourse['Course']['name'] ?></a></li>
        <span class="divider"> / </span>
        <li><a href="<?php echo Router::url(array('controller' => 'polls', 'action' => 'index', $ccourse['Course']['id'], '?' => array('level' => isset($_GET['level']) ? $_GET['level'] : $this->data['Poll']['level_id']))) ?>"> <?php __('Polls') ?></a></li>
        <span class="divider"> / </span>
        <li class="active"><?php echo $title ?></li>
    </ul>
</div>
<h1><?php echo $title ?></h1>
<hr />
<?php echo $form->create('Poll', array('type' => 'file', 'url' => $url)); ?>

<?php
echo $form->input('id');
echo $form->input('name', array('div' => array('class' => 'control-group')));
echo $form->input('course_id', array('type' => 'hidden', 'value' => !empty($this->data['Poll']['course_id']) ? $this->data['Poll']['course_id'] : $course_id));
echo $form->input('group_mode', array('options' => array(1 => __('No groups', true), 2 => __('Choose group', true)), 'div' => array('class' => 'control-group')));
?>
<div class="hide" id="Groups">
    <?php
    echo $form->input('sections', array('multiple' => true, 'type' => 'select', 'data-placeholder' => __("Choose a section", true), 'style' => "width:415px;", 'class' => "chzn-select chzn-rtl"));
    echo $form->input('students', array('multiple' => true, 'type' => 'select', 'data-placeholder' => __("Choose a student", true), 'style' => "width:415px;", 'class' => "chzn-select chzn-rtl"));
    ?>
</div>
<?php
echo $form->input('publish_date', array('type' => 'text', 'class' => 'hasDate', 'div' => array('class' => 'control-group')));
echo $form->input('cutoff_date', array('type' => 'text', 'class' => 'hasDate', 'div' => array('class' => 'control-group')));
?>
<div class="form-actions">
    <?php
    echo $form->submit('Submit', array('class' => 'btn btn-primary'));
    ?>
</div>

<?php echo $form->end(); ?>
<?php
echo $javascript->link(array('jquery-ui-1.8.24.custom.min', 'jquery-ui-sliderAccess', 'jquery-ui-timepicker-addon'));
echo $html->css(array('jquery-ui-1.8.24.custom', 'datetimepicker'));
echo $javascript->link(array('chosen.jquery'));
echo $html->css(array('chosen'));
?>
<script type="text/javascript">
    $('#PollSections').chosen({no_results_text: "<?php __("No results matched") ?>", width: '415px'});
    $('#PollStudents').chosen({no_results_text: "<?php __("No results matched") ?>", width: '415px'});

    $(document).ready(function() {
        change_type($('#PollGroupMode').val());
        $('#PollGroupMode').change(function() {
            change_type($('#PollGroupMode').val());
        });

    });
    function change_type(va) {
        if (va == 2) {
            $('#Groups').show();

        } else {
            $('#Groups').hide();

        }

    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.hasDate').datetimepicker({
            "dateFormat": 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            isRTL: true
        });

        change_sections($('#PollCourseId').val());
        var $sections = '<?php !empty($this->data['Poll']['sections']) ? print(implode(',', $this->data['Poll']['sections']))  : '' ?>';

        change_students($('#PollCourseId').val(), $sections);

        if (!$('#PollCourseId').val()) {
            document.getElementById('PollStudents').disabled = true;
            document.getElementById('PollSections').disabled = true;
            //            $('#CourseLevelId').html('<option value="">'+'<?php __('Select one') ?>'+'</option>');
        }

        $('#PollCourseId').change(function() {

            change_students($(this).val());
            change_sections($(this).val());
        });

        $('#PollSections').change(function() {
            var $sections = $(this).val();
            change_students($('#PollCourseId').val(), $sections);
        });
    });
    function change_sections(course_id)
    {
        if (!course_id) {
            return false;
        }
        $.ajax({
            async: true,
            type: "GET",
            url: "<?php echo Router::url(array('controller' => 'sections', 'action' => 'get_sections_by_course', 'admin' => false)) ?>/" + course_id + "/",
            dataType: "json",
            success: function(data) {
                $('#PollSections').html('');
                if (data.submenus.length != 0) {
                    for (var i = 0; i < data.submenus.length; i++)
                    {
                        var submenu = data.submenus[i];
                        var is_Selected = "";

                        selected_submenu = [<?php !empty($this->data['Poll']['sections']) ? print(implode(',', $this->data['Poll']['sections']))  : ''; ?>];
                        if (selected_submenu.indexOf(parseInt(submenu.Section.id)) != -1)
                        {
                            is_Selected = "selected=selected";
                        }
                        $('#PollSections').append('<option value="' + submenu.Section.id + '"' + is_Selected + '>' + submenu.Section.name + '</option>');
                    }
                    document.getElementById('PollSections').disabled = false;
                    //                    $('#PollSections').chosen({
                    //                        no_results_text: "<?php __("No results matched") ?>",
                    //                        width: "415px"
                    //                        
                    //                    });


                    $("#PollSections").trigger("liszt:updated");

                }
                else {
                    document.getElementById('PollSections').disabled = false;

                    //                    $('#PollSections').chosen({no_results_text: "<?php __("No results matched") ?>"});
                    $("#PollSections").trigger("liszt:updated");
                }

            }

        });
        return false;
    }


    function change_students(course_id, sections)
    {
        if (!course_id) {
            return false;
        }
        if (sections) {
            url = "<?php echo Router::url(array('controller' => 'users', 'action' => 'get_all_students_by_course', 'admin' => false)) ?>/" + course_id + "/" + sections;
        } else {

            url = "<?php echo Router::url(array('controller' => 'users', 'action' => 'get_all_students_by_course', 'admin' => false)) ?>/" + course_id + "/";
        }
        $.ajax({
            async: true,
            type: "GET",
            url: url,
            dataType: "json",
            success: function(data) {
                $('#PollStudents').html('');
                if (data.submenus.length != 0) {

                    for (var i = 0; i < data.submenus.length; i++)
                    {

                        var submenu = data.submenus[i];

                        var is_Selected = "";
                        selected_submenu = [<?php !empty($this->data['Poll']['students']) ? print(implode(',', $this->data['Poll']['students']))  : ''; ?>];
                        if (selected_submenu.indexOf(parseInt(submenu.User.id)) != -1)
                        {
                            is_Selected = "selected=selected";
                        }
                        $('#PollStudents').append('<option value="' + submenu.User.id + '"' + is_Selected + '>' + submenu.User.name + '</option>');
                    }
                    document.getElementById('PollStudents').disabled = false;

                    //                    $('#PollStudents').chosen({no_results_text: "<?php __("No results matched") ?>",width:'415px;'});
                    $("#PollStudents").trigger("liszt:updated");

                } else {
                    document.getElementById('PollStudents').disabled = false;
                    //                    $('#PollStudents').chosen({no_results_text: "<?php __("No results matched") ?>"});
                    $("#PollStudents").trigger("liszt:updated");

                }

                return false;
            }

        });
    }
</script>

