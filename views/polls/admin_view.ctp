
<div class="module">
    <h2><span><?php  __('Poll');?></span></h2>
    <div class="module-body">
        <dl><?php $i = 0; $class = ' class="altrow"';?>
            		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $poll['Poll']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $poll['Poll']['name']; ?>
			&nbsp;
		</dd>
        </dl>


        <div class="actions">
            <h3><?php __('Actions'); ?></h3>
            <ul>
                		<li><?php echo $this->Html->link(__('Edit Poll', true), array('action' => 'edit', $poll['Poll']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Poll', true), array('action' => 'delete', $poll['Poll']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $poll['Poll']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Polls', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Poll', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Poll Questions', true), array('controller' => 'poll_questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Poll Question', true), array('controller' => 'poll_questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Poll Answers', true), array('controller' => 'poll_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Poll Answer', true), array('controller' => 'poll_answers', 'action' => 'add')); ?> </li>
            </ul>
        </div>
                    <div class="related">
                <h3><?php __('Related Poll Questions');?></h3>
                <?php if (!empty($poll['PollQuestion'])):?>
                <table cellpadding = "0" cellspacing = "0">
                    <tr>
                        		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Type'); ?></th>
		<th><?php __('Poll Id'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
                        <th class="actions"><?php __('Actions');?></th>
                    </tr>
                    	<?php
		$i = 0;
		foreach ($poll['PollQuestion'] as $pollQuestion):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $pollQuestion['id'];?></td>
			<td><?php echo $pollQuestion['name'];?></td>
			<td><?php echo $pollQuestion['type'];?></td>
			<td><?php echo $pollQuestion['poll_id'];?></td>
			<td><?php echo $pollQuestion['created'];?></td>
			<td><?php echo $pollQuestion['updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'poll_questions', 'action' => 'view', $pollQuestion['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'poll_questions', 'action' => 'edit', $pollQuestion['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'poll_questions', 'action' => 'delete', $pollQuestion['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $pollQuestion['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
                </table>
                <?php endif; ?>

                <div class="actions">
                    <ul>
                        <li><?php echo $this->Html->link(__('New Poll Question', true), array('controller' => 'poll_questions', 'action' => 'add'));?> </li>
                    </ul>
                </div>
            </div>
                    <div class="related">
                <h3><?php __('Related Poll Answers');?></h3>
                <?php if (!empty($poll['PollAnswer'])):?>
                <table cellpadding = "0" cellspacing = "0">
                    <tr>
                        		<th><?php __('Id'); ?></th>
		<th><?php __('Fekra User Id'); ?></th>
		<th><?php __('Poll Id'); ?></th>
		<th><?php __('Answer'); ?></th>
		<th><?php __('Poll Question Id'); ?></th>
                        <th class="actions"><?php __('Actions');?></th>
                    </tr>
                    	<?php
		$i = 0;
		foreach ($poll['PollAnswer'] as $pollAnswer):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $pollAnswer['id'];?></td>
			<td><?php echo $pollAnswer['fekra_user_id'];?></td>
			<td><?php echo $pollAnswer['poll_id'];?></td>
			<td><?php echo $pollAnswer['answer'];?></td>
			<td><?php echo $pollAnswer['poll_question_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'poll_answers', 'action' => 'view', $pollAnswer['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'poll_answers', 'action' => 'edit', $pollAnswer['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'poll_answers', 'action' => 'delete', $pollAnswer['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $pollAnswer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
                </table>
                <?php endif; ?>

                <div class="actions">
                    <ul>
                        <li><?php echo $this->Html->link(__('New Poll Answer', true), array('controller' => 'poll_answers', 'action' => 'add'));?> </li>
                    </ul>
                </div>
            </div>
            </div>
</div>