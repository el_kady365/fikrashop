<?php if (isset($poll)) { ?>
    <hr />
    <h4><?php echo __('Or Answer questions'); ?></h4>


    <div class="poll-questions">

        <?php if (!empty($poll['PollQuestion'])) { ?>

            <input type="hidden" name="data[Poll][poll_id]" value="<?php echo $poll['Poll']['id'] ?>">

            <?php
            foreach ($poll['PollQuestion'] as $question) {
                if ($question['type'] == 1) {
                    $class = "radio-inline";
                } elseif ($question['type'] == 2) {
                    $class = "checkbox-inline";
                } elseif ($question['type'] == 3) {
                    $class = "textarea";
                }
                $input = '';
                if ($question['type'] != 4) {
                    if (in_array($question['type'], array(1, 2))) {
                        if (!empty($question['PollQuestionOption'])) {

                            foreach ($question['PollQuestionOption'] as $option) {


                                if ($question['type'] == 1) {
                                    $check = '';
                                    if (!empty($this->data['PollQuestion']['question_' . $question['id']])) {
                                        if ($this->data['PollQuestion']['question_' . $question['id']] == $option['name']) {
                                            $check = 'checked="checked"';
                                        }
                                    }
                                    $input.= '
                                            <label class="' . $class . '">
                                            <input type = "radio" value = "' . $option['name'] . '" name = "data[PollQuestion][question_' . $question['id'] . ']" ' . $check . ' />
             ' . $option['name'] . '</label> ';
                                } elseif ($question['type'] == 2) {
                                    $check = '';
                                    if (!empty($this->data['PollQuestion']['question_' . $question['id']]) && is_array($this->data['PollQuestion']['question_' . $question['id']])) {
                                        foreach ($this->data['PollQuestion']['question_' . $question['id']] as $answer) {

                                            if ($answer == $option['name']) {
                                                $check = 'checked="checked"';
                                            }
                                        }
                                    }

                                    $input.= '
                                            <label class="' . $class . '">
                                            <input type = "checkbox" value = "' . $option['name'] . '" name = "data[PollQuestion][question_' . $question['id'] . '][]" ' . $check . '  />
             ' . $option['name'] . '</label> ';
                                }
                            }
                        }
                    } elseif ($question['type'] == 3) {
                        $value = '';
                        if (!empty($this->data['PollQuestion']['question_' . $question['id']])) {
                            $value = $this->data['PollQuestion']['question_' . $question['id']];
                        }
                        $input = '<textarea name = "data[PollQuestion][question_' . $question['id'] . ']"  >' . $value . '</textarea>';
                    }
                    if ($question['type'] == 3) {
                        ?>
                        <div class="input <?php echo $class; ?> control-group">
                            <label><?php echo $question['name']; ?></label>
                            <?php echo $input; ?>
                        </div>

                        <?php
                    } else {
                        ?>
                        <div >
                            <label><?php echo $question['name']; ?></label>
                            <?php echo $input; ?>      
                        </div>
                    <?php
                    }
                    if (isset($errors['question_' . $question['id']])) {
                        echo '<div class="error-message" style="clear:both;">' . $errors['question_' . $question['id']] . '</div>';
                    }
                    ?>
                </div>        
                <?php
            } else {
                ?>
                <h3><?php echo $question['name']; ?></h3>
                <?php
            }
        }
        ?>


    <?php } ?>
    </div>
    <?php
}?>