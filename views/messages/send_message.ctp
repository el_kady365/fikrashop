<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Send Message') ?></strong></h3>


    <?php
    echo $form->create('Message', array('type' => 'file', 'url' => array('action' => 'send_message', $this->data['Message']['receiver_id']), 'class' => 'form default-forms font-a'));
    if (!empty($this->data['Message']['fekra_id'])) {
        echo $form->hidden('fekra_id');
    }
    echo $form->input('id');
    echo $form->input('receiver_id', array('type' => 'hidden'));
    echo $form->input('subject');
    echo $form->input('body', array('label' => __('Message', true)));
    ?>

    <div id="resources">
        <div class="qualification_set block_set url-div" style="display: none;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                <tr>
                    <td valign="top">
                        <?php
                        echo $form->input('MessageAttachment.0.file', array('type' => 'file', 'div' => array('class' => 'input file '),
                            'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                        ?>
                    </td>	

                </tr>
            </table>
            <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
            <div class="clear"></div>
        </div>
        <div  id="relatedQualifications" class="items-block">
            <h4><?php __('Attachments') ?></h4>
            <?php if (!empty($this->data['MessageAttachment'])): ?>
                <?php
                foreach ($this->data['MessageAttachment'] as $i => $MessageAttachment):

                    $additional_class = '';
                    if ($i > 0) {
                        $additional_class = 'url-div';
                    }
                    ?>
                    <div class="qualification_set block_set url-div">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                            <tr>
                                <td valign="top">
                                    <?php
                                    echo $form->input("MessageAttachment.$i.id", array('class' => 'INPUT', 'value' => isset($MessageAttachment['id']) ? $MessageAttachment['id'] : ''));
                                    echo $form->input("MessageAttachment.$i.file", array('type' => 'file', 'div' => array('class' => 'input file '),
                                        'between' => $this->element('front_file_element', array('info' => isset($MessageAttachment['file_info']) ? $MessageAttachment['file_info'] : $attachmentInfo, 'field' => 'file'))));
                                    ?>
                                </td>	

                            </tr>
                        </table>
                        <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                        <div class="clear"></div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="qualification_set block_set url-div">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                        <tr>
                            <td valign="top">
                                <?php
                                echo $form->input('MessageAttachment.0.file', array('type' => 'file', 'div' => array('class' => 'input file'),
                                    'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                                ?>
                            </td>	

                        </tr>
                    </table>
                    <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                    <div class="clear"></div>
                </div>
            <?php endif; ?>
        </div>
        <a href="javascript:appendQualificationUploader();" class="btn btn-primary AddQualification AddItem"><i class="fa fa-plus"></i> <?php __('Add Attachment') ?></a>
    </div>
    <div class="input submit">
        <button type="submit"><span class="btn btn-default"><?php __('Send') ?></span></button>
    </div>
    <?php echo $form->end(); ?>
</div>

<?php echo $this->element('user/user_leftmenu'); ?>
<script type="text/javascript">

    $(document).on('click', '.delete-section2', function() {
        if (confirm('Are you sure?')) {
            container_id = $(this).parents('.items-block').attr('id');
            console.log(container_id);
            if ($('#' + container_id).find('.block_set').length <= 1) {
                $(this).parent('.block_set').find('input').val('');
            } else {
                $(this).parent('.block_set').remove();
            }
        }
        return false;
    });
    function appendQualificationUploader()
    {
        if ($('#relatedQualifications').is(':visible')) {
            if ($('.qualification_set').length <= 5) {
                $('#relatedQualifications').append('<div class="qualification_set block_set url-div">' + $('.qualification_set:first').html().replace(/\[0\]/g, '[' + $('.qualification_set').length + ']').replace(/MessageAttachment0/g, 'MessageAttachment' + $('.qualification_set').length) + '</div>');
                $('#relatedQualifications .qualification_set:last input').val('');
            } else {
                alert('<?php __('Sorry, only 5 attachments are allowed') ?>');
            }

        }
        else {
            $('#relatedQualifications').show();
        }
    }
</script>
