<?php echo $this->element('user/user_leftmenu'); ?>

<div class="content-box">
    <div class="message-view">
        <div class="row message-head">
            <div class="col-md-9">
                <h3 class="font-a sub-heading"><?php echo $message['Message']['subject'] ?></h3>
                <p class="sent-date"><?php echo date('d/m/Y', strtotime($message['Message']['created'])) ?></p>
            </div>
            <div class="col-md-3"> 

                <div class="btn-group pull-right">
                    <a href="<?php echo Router::url(array('action' => 'delete', $message['Message']['id'])) ?>" class="delete btn btn-default"><i class="fa fa-trash-o fa-lg"></i></a>
                    <a href="#reply" class="btn btn btn-default"><i class="fa fa-reply-all fa-lg"></i></a> 

                </div>            

            </div>
        </div>
        <div class="row  message-body">
            <div class="col-md-12">
                <p>
                    <?php echo nl2br($message['Message']['body']) ?>
                </p>
            </div>
        </div>
        <?php if (!empty($message['MessageAttachment'])) { ?>
            <div class="row  message-body">
                <h4 class="font-a sub-heading"><i class="fa fa-file"></i> <?php __('Attachments') ?></h4>
                <?php
                foreach ($message['MessageAttachment'] as $attach) {
                    ?>
                    <p><a target="_blank" href="<?php echo Router::url($attach['file_info']['full_path']); ?>"><i class="fa fa-file-text-o"></i> <?php echo $attach['file_info']['basename'] ?></a></p>
                <?php }
                ?>
            </div>
        <?php } ?>


        <div class="row message-reply" id="reply">
            <h3 class="font-a sub-heading"><?php __('Reply this messages') ?></h3>
            <?php echo $session->flash(); ?>
            <div class="col-md-12 ">
                <?php
                echo $form->create('Message', array('url' => array('action' => 'reply', $message['Message']['id']), 'type' => 'file'));
                echo $form->input('body', array('label' => __('Message', true)));
                ?>
                <div id="resources">
                    <div class="qualification_set block_set url-div" style="display: none;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                            <tr>
                                <td valign="top">
                                    <?php
                                    echo $form->input('MessageAttachment.0.file', array('type' => 'file', 'div' => array('class' => 'input file '),
                                        'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                                    ?>
                                </td>	

                            </tr>
                        </table>
                        <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                        <div class="clear"></div>
                    </div>
                    <div  id="relatedQualifications" class="items-block">
                        <h4><?php __('Attachments') ?></h4>
                        <?php if (!empty($this->data['MessageAttachment'])): ?>
                            <?php
                            foreach ($this->data['MessageAttachment'] as $i => $MessageAttachment):

                                $additional_class = '';
                                if ($i > 0) {
                                    $additional_class = 'url-div';
                                }
                                ?>
                                <div class="qualification_set block_set url-div">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                                        <tr>
                                            <td valign="top">
                                                <?php
                                                echo $form->input("MessageAttachment.$i.id", array('class' => 'INPUT', 'value' => isset($MessageAttachment['id']) ? $MessageAttachment['id'] : ''));
                                                echo $form->input("MessageAttachment.$i.file", array('type' => 'file', 'div' => array('class' => 'input file '),
                                                    'between' => $this->element('front_file_element', array('info' => isset($MessageAttachment['file_info']) ? $MessageAttachment['file_info'] : $attachmentInfo, 'field' => 'file'))));
                                                ?>
                                            </td>	

                                        </tr>
                                    </table>
                                    <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                                    <div class="clear"></div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="qualification_set block_set url-div">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                                    <tr>
                                        <td valign="top">
                                            <?php
                                            echo $form->input('MessageAttachment.0.file', array('type' => 'file', 'div' => array('class' => 'input file'),
                                                'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                                            ?>
                                        </td>	

                                    </tr>
                                </table>
                                <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                                <div class="clear"></div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <a href="javascript:appendQualificationUploader();" class="btn btn-primary AddQualification AddItem"><i class="fa fa-plus"></i> <?php __('Add Attachment') ?></a>
                </div>
                <br />
                <div class="input submit">
                    <button type="submit"><span class="btn btn-default"><?php __('Reply') ?></span></button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
echo $html->css('jquery-impromptu.min');
echo $javascript->link('jquery-impromptu.min');
?>
<script>
    $('.delete').click(function() {
        hrf = $(this).get(0).href;
        $this = $(this);
        var txt = '<?php __('Are you sure you want to remove this message?') ?>';

        $.prompt(txt, {
            buttons: {'<?php __('Delete') ?>': true, '<?php __('Cancel') ?>': false},
            close: function(e, v, m, f) {

                if (v) {

                    //Here is where you would do an ajax post to remove the user
                    //also you might want to print out true/false from your .php
                    //file and verify it has been removed before removing from the 
                    //html.  if false dont remove, $promt() the error.
                    self.location = hrf;

                }
                else {
                }

            }
        });
        return false;
    });
</script>
<script type="text/javascript">

    $(document).on('click', '.delete-section2', function() {
        if (confirm('Are you sure?')) {
            container_id = $(this).parents('.items-block').attr('id');
            console.log(container_id);
            if ($('#' + container_id).find('.block_set').length <= 1) {
                $(this).parent('.block_set').find('input').val('');
            } else {
                $(this).parent('.block_set').remove();
            }
        }
        return false;
    });
    function appendQualificationUploader()
    {
        if ($('#relatedQualifications').is(':visible')) {
            if ($('.qualification_set').length <= 5) {
                $('#relatedQualifications').append('<div class="qualification_set block_set url-div">' + $('.qualification_set:first').html().replace(/\[0\]/g, '[' + $('.qualification_set').length + ']').replace(/MessageAttachment0/g, 'MessageAttachment' + $('.qualification_set').length) + '</div>');
                $('#relatedQualifications .qualification_set:last input').val('');
            } else {
                alert('<?php __('Sorry, only 5 attachments are allowed') ?>');
            }

        }
        else {
            $('#relatedQualifications').show();
        }
    }
</script>
