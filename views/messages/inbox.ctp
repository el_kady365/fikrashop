<?php echo $this->element('user/user_leftmenu'); ?>

<div class="content-box">
    <div class="messages">
        <h3 class="font-a sub-heading"><strong><?php echo $h1 ?></strong></h3>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#Inbox" data-toggle="tab"><i class="fa fa-download"></i> <?php __('Inbox') ?></a></li>
            <li><a href="#Sent" data-toggle="tab"><i class="fa fa-location-arrow"></i> <?php __('Sent') ?></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="Inbox">
                <h4 class="font-a sub-heading"><strong><?php __('Inbox') ?></strong></h4>
                <?php echo $this->element('messages', array('messages' => $inbox, 'inbox_i' => 1)) ?>
            </div>
            <div class="tab-pane" id="Sent">
                <h4 class="font-a sub-heading"><strong> <?php __('Sent') ?></strong></h4>
                <?php // echo $this->Session->flash('email'); ?>
                <?php echo $this->element('messages', array('messages' => $sent)) ?>
            </div>
        </div>      
    </div>      
</div>      
<?php
echo $html->css('jquery-impromptu.min');
echo $javascript->link('jquery-impromptu.min');
?>
<script>
    $('.delete').click(function() {
        hrf = $(this).get(0).href;
        $this = $(this);
        var txt = '<?php __('Are you sure you want to remove this message?') ?>';

        $.prompt(txt, {
            buttons: {'<?php __('Delete') ?>': true, '<?php __('Cancel') ?>': false},
            close: function(e, v, m, f) {

                if (v) {

                    //Here is where you would do an ajax post to remove the user
                    //also you might want to print out true/false from your .php
                    //file and verify it has been removed before removing from the 
                    //html.  if false dont remove, $promt() the error.
                    self.location = hrf;

                }
                else {
                }

            }
        });
        return false;
    });
</script>
