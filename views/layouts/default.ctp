<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title_for_layout; ?></title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <?php
        echo $html->css(array('screen', 'app', 'fontawesome/css/font-awesome-min'));
        if ($lang != 'en') {
            echo $html->css(array($lang));
        }
        ?>
        <?php echo $this->Html->script(array('bootstrap', 'bootstrap-filestyle', 'flipbox', 'modernizr.2.5.3.min', 'jquery.flexslider')); ?>

        <?php if (isset($og_title)) { ?>
            <meta content="<?php echo $og_title ?>" property="og:title"/>
        <?php } ?>
        <meta content="website" property="og:type"/>
        <?php if (isset($og_description)) { ?>
            <meta content="<?php echo strip_tags($og_description) ?>" property="og:description" />
        <?php } ?>
    </head>
    <body>
        <?php
        if (!$user) {
            echo $this->element('user/registration_form');
            echo $this->element('user/login_form');
            echo $this->element('user/forget_form');
        } else {

            echo $this->element('user/skillform');
        }
        ?>
        <?php
        /* @var $html HtmlHelper */
        $languages = Configure::read('Config.languages');

        foreach ($languages as $key => $lan) {
            if ($key != $lang) {
                $langesUrl[$lan] = Router::url(array('controller' => '/', 'language' => $key));
            }
        }
        ?>

        <div class="header ">
            <div class="wrap clearfix">
                <div class="logo"><a href="<?php echo Router::url(array('controller' => '/')) ?>"><img src="<?php echo Router::url('/css/img/fikrashop_logo.png') ?>" alt="" title="" /></a></div>

                <div class="top-bar font-a">

                    <?php
                    if (!$user) {
                        ?>
                        <ul class="nav navbar-nav">
                            <li><a href="#" data-reveal-id="login"><?php __('Login') ?></a></li>
                            <li><span class="nav-sep">|</span></li>
                            <li><a href="#" data-reveal-id="register"><?php __('Register') ?></a></li>
                        </ul>
                    <?php } else { ?>
                        <ul class="nav navbar-nav ">
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" role="button" href="#" id="drop1">
                                    <?php if (!empty($user['image']['path'])) { ?>
                                        <img class="top-avatar" src="<?php echo Router::url($user['image']['path']) ?>" alt="" title="" />
                                    <?php } else { ?>
                                        <img class="top-avatar" src="<?php echo Router::url('/css/img/anonymous.jpg') ?>" alt="" title="" />
                                    <?php } ?>
                                    <?php echo $user['first_name'] ?> <b class="caret"></b>
                                </a>

                                <ul aria-labelledby="drop1" role="menu" class="dropdown-menu">
                                    <li role="presentation"><a tabindex="-1" role="menuitem" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'dashboard')) ?>"><?php __('Dashboard') ?></a></li>
                                    <li role="presentation"><a tabindex="-1" role="menuitem" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>"><?php __('My Profile') ?></a></li>
                                    <li role="presentation"><a tabindex="-1" role="menuitem" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')) ?>"><?php __('Logout') ?></a></li>
                                </ul>
                            </li>
                        </ul>
                    <?php } ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" role="button" href="#" id="drop2">
                                <?php __('Langauge') ?> <b class="caret"></b>
                            </a>

                            <ul aria-labelledby="drop2" role="menu" class="dropdown-menu">
                                <?php
                                /* @var $html HtmlHelper */
                                $languages = Configure::read('Config.languages');

                                foreach ($languages as $key => $lan) {
                                    if ($key != $lang) {
                                        $langesUrl[$lan] = Router::url(array('controller' => '/', 'language' => $key));
                                    }
                                }

                                foreach ($langesUrl as $key2 => $url) {
                                    ?>
                                    <li role="presentation"><a tabindex="-1" role="menuitem" href="<?php echo $url ?>"><?php __($key2); ?></a></li>

                                    <?php
                                    unset($langesUrl[$key2]);
                                    break;
                                }
                                ?>

                            </ul>
                        </li>
                    </ul>
                </div>

                <div class = "social-bar">
                    <ul>
                        <?php if (!empty($config['facebook'])) {
                            ?>
                            <li><a target="_blank" href="<?php echo $config['facebook'] ?>"><img src="<?php echo Router::url('/css/img/social/facebook.png') ?>" alt="" /></a></li>
                        <?php }if (!empty($config['google'])) { ?>
                            <li><a target="_blank" href="<?php echo $config['google'] ?>"><img src="<?php echo Router::url('/css/img/social/google.png') ?>" alt="" /></a></li>
                        <?php }if (!empty($config['linkedin'])) { ?>
                            <li><a target="_blank" href="<?php echo $config['linkedin'] ?>"><img src="<?php echo Router::url('/css/img/social/linked.png') ?>" alt="" /></a></li>
                        <?php }if (!empty($config['twitter'])) { ?>
                            <li><a target="_blank" href="<?php echo $config['twitter'] ?>"><img src="<?php echo Router::url('/css/img/social/twitter.png') ?>" alt="" /></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- header -->

        <?php
        if (!empty($top_menus)) {
            ?>
            <div class="wrap">
                <div class="top-nav font-a">
                    <ul>

                        <?php
                        foreach ($top_menus as $page) {
                            if ($page['Page']['type'] == 1) {
                                $url = Router::url(array('controller' => 'pages', 'action' => 'view', $page['Page']['permalink'], 'language' => $lang));
                            } else {
                                $url = Router::url('/' . $lang . '/' . $page['Page']['url']);
                            }
                            ?>

                            <li>
                                <a title="<?php echo $page['Page']['title'] ?>" href="<?php echo $url; ?>"><?php echo $page['Page']['title'] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php
        }
        ?>

        <!-- nav -->
        <?php if (isset($banners) && !empty($banners)) { ?>


            <div class="banners">
                <div class="wrap">
                    <div class="home-banners flexslider">
                        <ul class="slides">
                            <?php foreach ($banners as $banner) { ?>
                                <li>
                                    <a href="<?php echo $banner['Banner']['url'] ?>">
                                        <img src="<?php echo Router::url($banner['Banner']['image']['path']) ?>" alt="" title="" /></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>




        <?php } if ($this->action != "home") { ?>
            <div class="heading">
                <div class="wrap">
                    <h1 class="font-a"><?php __($h1) ?></h1>
                    <?php if (isset($sub_heading)) { ?>
                        <h4 class="font-a"><?php __($sub_heading) ?></h4>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <div class="main">
            <?php if ($this->action != "home") { ?>
                <div class="wrap">
                    <div class="content">
                        <!-- banners -->
                        <?php echo $session->flash(); ?>
                        <?php echo $content_for_layout; ?>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="wrap"><?php echo $session->flash(); ?></div>
                <?php
                echo $content_for_layout;
            }
            ?>
        </div>

        <div class="footer">
            <div class="wrap">
                <div class="footer-link contact-box">
                    <?php echo $footer_snippet; ?>
                </div>
                <div class="footer-link social-box">
                    <h3 class="font-a"><?php __('Follow us on <span class="highlight">social</span>') ?><span class="line-divider"></span></h3>
                    <p><?php __('Follow us on social media') ?></p>
                    <div class="social-ico clearfix">
                        <?php if (!empty($config['facebook'])) {
                            ?>
                            <a class="fb-ico" href="<?php echo $config['facebook'] ?>"></a>
                        <?php }if (!empty($config['google'])) { ?>
                            <a class="plus-ico" href="<?php echo $config['google'] ?>"></a>
                        <?php }if (!empty($config['linkedin'])) { ?>
                            <a class="in-ico" href="<?php echo $config['linkedin'] ?>"></a>
                        <?php }if (!empty($config['twitter'])) { ?>
                            <a class="twitter-ico" href="<?php echo $config['twitter'] ?>"></a>
                        <?php } ?>


                    </div>
                </div>
                <!-- / footer-link -->
                <div class="footer-link newsletters-box">
                    <h3 class="font-a"><?php __('Getting <span class="highlight">Informed</span>') ?><span class="line-divider"></span></h3>
                    <p><?php __('Subscribe for news and updates') ?></p>
                    <div class="newsletters-form">
                        <form method="post" id="NewsletterForm" action="<?php echo Router::url(array('controller' => 'newsletters', 'action' => 'add')); ?>">
                            <div class="input text left">
                                <input name="data[Newsletter][email]" type="text" value="" placeholder="mail@gmail.com" />
                            </div>
                            <div class="input button left">
                                <button type="submit" class="font-a"><?php __('Join') ?></button>
                            </div>
                            <div class="clear"></div>
                        </form>
                        <div id="subscription-form-message"></div>
                    </div>
                </div>
                <!-- / footer-link -->
                <div class="clear"></div>
            </div>
        </div>




        <?php echo $this->element('sql_dump'); ?>

        <?php
        echo $html->script(array('dropdown', 'caroufredsel', 'reveal'));
        echo $scripts_for_layout;
        ?>
        <script type="text/javascript">
            $(function () {
                if (self.location.hash == '#login') {
                    $('#login').reveal();
                }
                $('#NewsletterForm').submit(function () {
                    var post_data = $(this).serialize();
                    var url = $(this).attr('action');
                    $('#subscription-form-message').html('');
                    $('#subscription-form-message').addClass('form-loader');
                    $('#subscription-form-message').html('Please wait');
                    $.ajax({
                        cache: false,
                        url: url,
                        type: "POST",
                        dataType: "json",
                        data: post_data,
                        success: function (data) {
                            console.log(data);
                            $('#subscription-form-message').removeClass('form-loader');
                            $('#subscription-form-message').removeClass('validate-email-succ');
                            $('#subscription-form-message').removeClass('validate-email-error');
                            if (data.success) {
                                $('#subscription-form-message').html(data['message']);
                                $('#subscription-form-message').addClass('validate-email-succ');
                                $('#NewsletterForm')[0].reset();
                            } else {
                                $('#subscription-form-message').html(data['error']['email']);
                                $('#subscription-form-message').addClass('validate-email-error');
                            }
                        }

                    });
                    return false;
                });
            });
        </script>
        <script type="text/javascript">

            $(window).load(function () {
                $('.home-banners').flexslider({animation: "slide", directionNav: true, smoothHeight: true, controlNav: false});
//                $('.partners-slider').flexslider({animation: "slide", directionNav: false, smoothHeight: true, controlNav: false});

            });

        </script>


    </body>
</html>
