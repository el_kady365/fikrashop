<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <?php echo $html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>

        <?php
        echo $html->css(array('admin/stylesheets'));
        echo $javascript->link(array('admin/jquery-1.7.2.min.js', 'admin/jquery-ui-1.8.24.custom.min', 'admin/cakebootstrap'));
        echo $scripts_for_layout;
        ?>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/jquery/jquery.mousewheel.min.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/cookie/jquery.cookies.2.2.0.min.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/bootstrap.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/uniform/uniform.js') ?>'></script>


        <script type='text/javascript' src='<?php echo Router::url('/js/admin/maskedinput/jquery.maskedinput-1.3.min.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/validation/languages/jquery.validationEngine-en.js') ?>' charset='utf-8'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/validation/jquery.validationEngine.js') ?>' charset='utf-8'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/mcustomscrollbar/jquery.mCustomScrollbar.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/animatedprogressbar/animated_progressbar.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/qtip/jquery.qtip-1.0.0-rc3.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/select2/select2.min.js') ?>'></script>



        <script type='text/javascript' src='<?php echo Router::url('/js/admin/dataTables/jquery.dataTables.min.js') ?>'></script>    
        <script src="<?php echo Router::url('/js/admin/dataTables/jquery.dataTables.columnFilter.js') ?>" type="text/javascript"></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/fancybox/jquery.fancybox.pack.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/cookies.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/actions.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/plugins.js') ?>'></script>

    </head>
    <body>
        <div class="header">   
            <ul class="header_menu">
                <li class="list_icon">
                    <a href="#">&nbsp;</a>
                </li>
            </ul>    
        </div>
        <?php echo $this->element('side_menu');
        ?>
        <div class="content">
            <div class="breadLine">
                <ul class="breadcrumb">
                    <?php
                    /* @var $html HtmlHelper */
                    $html->addCrumb($config['site_name'], '/', array('target' => '_blank'));
                    if (!isset($crumbs['prefix']))
                        $html->addCrumb(Inflector::humanize(__($this->params['prefix'], true)), "/{$this->params['prefix']}");
                    else if (!empty($crumbs['prefix'][0]))
                        $html->addCrumb($crumbs['prefix'][0], "/{$crumbs['prefix'][1]}");

                    if (!isset($crumbs['controller']))
                        $html->addCrumb($titleAlias, array('controller' => $this->params['controller'], 'action' => 'index'));
                    else if (!empty($crumbs['controller'][0]))
                        $html->addCrumb($crumbs['controller'][0], $crumbs['controller'][1] ? "/{$crumbs['controller'][1]}" : null);

                    if (!isset($crumbs['action'])) {
                        $prefix = $this->params['prefix'];
                        $action = substr($this->action, strlen("{$this->params['prefix']}_"));
                        if ($action != 'index') {
                            $html->addCrumb(Inflector::humanize($action));
                        }
                    } else if (!empty($crumbs['action'][0]))
                        $html->addCrumb($crumbs['action'][0], $crumbs['action'][1] ? "/{$crumbs['action'][1]}" : null);


                    if (isset($crumbs['more'])) {
                        foreach ($crumbs['more'] as $crumb)
                            $html->addCrumb($crumb[0], $crumb[1] ? "/{$crumb[1]}" : null);
                    }


                    echo $html->getCrumbs(' > ');
                    ?>
                </ul>
            </div>

<!--                <li><a href="#">Simple Admin</a> <span class="divider">></span></li>                
                <li class="active">Dashboard</li>-->
            <div class="workplace">
                <?php
                echo $session->flash();
                echo $content_for_layout;
                ?>
            </div>
        </div>

        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>