<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Snippet']['id'])) {

                    __('Edit Snippet');
                } else {

                    __('Add Snippet');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('Snippet', array('type' => 'file')); ?>
        <div class="block-fluid">
            <?php
            echo $form->input('id');
            echo $form->input('key');
            echo $form->input('name');
            echo $fck->load('Snippet', 'content');
            echo $form->input('lang', array('label' => __('Language', true), 'options' => $languages));
            echo $form->submit('Submit', array('class' => 'submit-green'));
            ?>
        </div>
        <?php echo $form->end(); ?>
    </div>
</div>

