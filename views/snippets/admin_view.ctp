
<div class="module">
    <h2><span><?php  __('Snippet');?></span></h2>
    <div class="module-body">
        <dl><?php $i = 0; $class = ' class="altrow"';?>
            		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $snippet['Snippet']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Key'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $snippet['Snippet']['key']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $snippet['Snippet']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $snippet['Snippet']['content']; ?>
			&nbsp;
		</dd>
        </dl>


        <div class="actions">
            <h3><?php __('Actions'); ?></h3>
            <ul>
                		<li><?php echo $this->Html->link(__('Edit Snippet', true), array('action' => 'edit', $snippet['Snippet']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Snippet', true), array('action' => 'delete', $snippet['Snippet']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $snippet['Snippet']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Snippets', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Snippet', true), array('action' => 'add')); ?> </li>
            </ul>
        </div>
            </div>
</div>