<?php /* @var $html HtmlHelper */ ?>
<div class="box news_sections countries">
    <div class="head">
        <h2><a href="#" title=""><?php __('Search results') ?></a></h2>	
    </div>
    <div class="content">
        <?php
        if (!empty($results)) {
            ?>
            <div class="arena">
                <ul class="teasers_mod01">
                    <?php
                    foreach ($results as $result) {
                        $type = $result['Search']['type'];
                        $controller = Inflector::pluralize($type);
                        if (!in_array($type, array('image', 'video'))) {
                            $class = '';
                        } else {
                            $class = 'class="iframe"';
                        }
                        if ($type == 'video') {
                            $url = array('controller' => $controller, 'action' => 'view_video', $result['Search']['id']);
                        } elseif ($type == 'image') {
                            $url = '/img/uploads/' . $result['Search']['image'];
                        } else {
                            $url = array('controller' => $controller, 'action' => 'view', $result['Search']['permalink']);
                        }
                        ?>
                        <li>
                            <a href="<?php echo Router::url($url); ?>" <?php echo $class; ?> >
                                <img src="<?php echo get_resized_image_url($result['Search']['image'], 186, 193, false, false, false) ?>"  alt="<?php echo $result['Search'][$lang . '_title'] ?>" />
                                <span><?php echo $result['Search'][$lang . '_title']; ?></span>

                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        <?php } else {
            ?>
            <div class="note"><?php __('No Results'); ?> </div>   
        <? } ?> 

    </div>
</div>



