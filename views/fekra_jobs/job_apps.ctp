<?php echo $this->element('user/user_leftmenu'); ?>
<div class="content-box">
    <div class="fekra-head">


        <h3 class="font-a sub-heading"><strong><?php echo $sub_title ?></strong></h3>
        <div class="clear"></div>
    </div>
    <table class="table">
        <tr>
            <th><?php echo __('Job title', true) ?></th>
            <th><?php echo __('Hired', true) ?></th>
            <th><?php echo __('Actions', true) ?></th>
        </tr>

        <?php
        if (!empty($job_apps)) {

            foreach ($job_apps as $job) {
                ?>
                <tr>

                    <td><a href="<?php echo Router::url(array('action' => 'view', $job['FekraJob']['id'], slug($job['FekraJob']['title']))) ?>"><?php echo $job['FekraJob']['title'] ?></a></td>
                    <td>
                        <?php if ($job['FekraJobApplication']['status'] == 1) { ?>
                            <i class="fa fa-check"></i>
                        <?php } ?>
                    </td>

                    <td> 
                        <a href="<?php echo Router::url(array('action' => 'view_app', $job['FekraJobApplication']['id'])); ?>" class="btn btn-default btn-sm"><span aria-hidden="true" class="fa fa-pencil "></span> <?php __('View') ?></a>
                        <?php if ($job['FekraJobApplication']['status'] == 0) { ?>
                            <a href="<?php echo Router::url(array('action' => 'unapply', $job['FekraJobApplication']['id'])) ?>" class="delete btn btn-success btn-primary"><i class="fa fa-times"></i> <?php __('Unapply'); ?></a>
                            <?php
                        }
                        ?>
                        

                    </td>
                </tr>
                <?php
            }
            if ($paginator->numbers()) {
                ?>
                <div class="paging">
                    <ul >
                        <?php
                        if ($paginator->hasNext()) {
                            echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                        if ($paginator->hasPrev()) {
                            echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        ?>

                    </ul>
                </div>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="3">
                    <div id="flashMessage" class="info"><?php __('There aren\'t any job applications') ?></div>
                </td>
            </tr>
        <?php } ?>

    </table>
</div>
<?php
echo $html->css('jquery-impromptu.min');
echo $javascript->link('jquery-impromptu.min');
?>
<script>
    $('.delete').click(function() {
        hrf = $(this).get(0).href;
        $this = $(this);
        var txt = '<?php __('Are you sure you want to unapply from this job?') ?>';

        $.prompt(txt, {
            buttons: {'<?php __('Unapply') ?>': true, '<?php __('Cancel') ?>': false},
            close: function(e, v, m, f) {

                if (v) {

                    //Here is where you would do an ajax post to remove the user
                    //also you might want to print out true/false from your .php
                    //file and verify it has been removed before removing from the 
                    //html.  if false dont remove, $promt() the error.
                    self.location = hrf;

                }
                else {
                }

            }
        });
        return false;
    });
</script>