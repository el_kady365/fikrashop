<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Apply') ?></strong></h3>

    <div class="fekra-box font-a"> 
        <div class="fekra-box-content">
            <h4><?php echo $job['FekraJob']['title'] ?></h4>
            <h6 class="shape"><strong><?php __('Price') ?>:</strong> <?php echo format_price($job['FekraJob']['price']) ?>  </h6>
            <p><?php echo $text->truncate(strip_tags($job['FekraJob']['description'], 'br'), 250); ?> </p>
            <a target="_blank" class="btn btn-success btn-sm" href="<?php echo Router::url(array('action' => 'view', $job['FekraJob']['id'], slug($job['FekraJob']['title']))) ?>"><?php __('View Job') ?></a>
        </div>
    </div>
    <?php
    echo $form->create('FekraJobApplication', array('url' => array('controller' => 'fekra_jobs', 'action' => 'apply', $job['FekraJob']['id']), 'class' => 'form default-forms font-a'));
    ?>

    <div class="panel panel-default">

        <div class="panel-body">

            <?php
            echo $form->input('id');
            echo $form->input('price', array('label' => __('Paid to you', true)));
            ?>
            <div class="input text">
                <label><?php __('Fikra charges') ?></label>
                <div>
                    <p class="form-control-static" id="charges"><?php echo format_price(0); ?></p>
                </div>
            </div>
            <?php
            echo $form->input('total', array('label' => __('total payment', true)));
            echo $form->input('description', array('label' => __('Cover letter', true)));
            ?>
        </div>
    </div>

    <div class="input submit">
        <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-check-circle"></i> <?php __('Apply to this job') ?> </button>
    </div>
    <?php echo $form->end(); ?>
</div>

<?php echo $this->element('user/user_leftmenu'); ?>


<script>
    var $ratio;
    $(function() {
        $('#FekraJobApplicationPrice').bind('keyup change', function() {

            $charged = parseFloat($(this).val());
            if (!isNaN($charged)) {
                $total = getTotal($charged);
                $('#charges').text('$' + ($total - $charged).toFixed(2));
                $('#FekraJobApplicationTotal').val($total.toFixed(2));
                $(this).parent().find('div.error-message').remove();
            } else {
                if ($(this).parent().find('div.error-message').length != 0) {
                    $(this).parent().find('div.error-message').text('<?php __('Enter a valid number') ?>');
                } else {
                    jQuery('<div/>', {
                        id: 'foo',
                        class: 'error-message',
                        text: '<?php __('Enter a valid number') ?>'
                    }).insertAfter($('#FekraPhaseTotal'));
                }
            }
        });
        $('#FekraJobApplicationTotal').bind('keyup change', function() {

            $total = parseFloat($(this).val());

            if (!isNaN($total)) {
                $charged = getCharged($total);
                $('#charges').text('$' + ($total - $charged).toFixed(2));
                $('#FekraJobApplicationPrice').val($charged.toFixed(2));
                $(this).parent().find('div.error-message').remove();
            } else {
                if ($(this).parent().find('div.error-message').length != 0) {
                    $(this).parent().find('div.error-message').text('<?php __('Enter a valid number') ?>');
                } else {
                    jQuery('<div/>', {
                        id: 'foo',
                        class: 'error-message',
                        text: '<?php __('Enter a valid number') ?>'
                    }).insertAfter($('#FekraPhaseCharged'));
                }
            }
        });

    });

    function getTotal($total) {

        $rate = ($total * 1.039) + .3;
        return parseFloat($rate.toFixed(2));
    }
    function getCharged($total) {

        $rate = (($total - 0.3) * 0.96246390760346);
//        console.log($total);
        return parseFloat($rate.toFixed(2));
    }



</script>

