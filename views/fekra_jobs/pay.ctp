<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Pay installment') ?></strong></h3>

    <div class="fekra-box font-a"> 
        <div class="fekra-box-content">
            <h4><?php echo $job_app['User']['first_name'] . ' ' . $job_app['User']['last_name'] ?></h4>
            <h6 class="shape"><strong><?php __('Paid to developer') ?>:</strong> <?php echo format_price($job_app['FekraJobApplication']['price']) ?>  </h6>
            <h6 class="shape"><strong><?php __('Total Payments') ?>:</strong> <?php echo format_price($total_payments) ?>  </h6>

            <a target="_blank" class="btn btn-success btn-sm" href="<?php echo Router::url(array('action' => 'view_app', $job_app['FekraJobApplication']['id'])); ?>"><?php __('View Application') ?></a>
        </div>
    </div>
    <?php
    echo $form->create('FekraJobApplication', array('url' => array('controller' => 'fekra_jobs', 'action' => 'pay', $job_app['FekraJob']['id']), 'class' => 'form default-forms font-a'));
    ?>

    <div class="panel panel-default">

        <div class="panel-body">

            <?php
            echo $form->input('id');
            echo $form->input('charged', array('label' => __('Paid to developer', true)));
            ?>
            <div class="input text">
                <label><?php __('Fikra charges') ?></label>
                <div>
                    <p class="form-control-static" id="charges"><?php echo format_price(0); ?></p>
                </div>
            </div>
            <?php
            echo $form->input('total', array('label' => __('total payment', true)));
            ?>

        </div>
    </div>

    <div class="input submit">
        <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-check-circle"></i> <?php __('Pay') ?> </button>
    </div>
    <?php echo $form->end(); ?>
</div>

<?php echo $this->element('user/user_leftmenu'); ?>


<script>
    var $ratio;
    $(function() {
        $('#FekraJobApplicationTotal').bind('keyup change', function() {

            $total = parseFloat($(this).val());
            if (!isNaN($total)) {
                $charged = getCharged($total);
                $('#charges').text('$' + ($total - $charged).toFixed(2));
                $('#FekraJobApplicationCharged').val($charged.toFixed(2));
                $(this).parent().find('div.error-message').remove();
            } else {
                if ($(this).parent().find('div.error-message').length != 0) {
                    $(this).parent().find('div.error-message').text('<?php __('Enter a valid number') ?>');
                } else {
                    $('#charges').text('$0.00');
                    jQuery('<div/>', {
                        id: 'foo',
                        class: 'error-message',
                        text: '<?php __('Enter a valid number') ?>'
                    }).insertAfter($('#FekraPhaseTotal'));
                }
            }
        });
        $('#FekraJobApplicationCharged').bind('keyup change', function() {

            $charged = parseFloat($(this).val());

            if (!isNaN($charged)) {
                $total = getTotal($charged);
                $('#charges').text('$' + ($total - $charged).toFixed(2));
                $('#FekraJobApplicationTotal').val($total.toFixed(2));
                $(this).parent().find('div.error-message').remove();
            } else {
                if ($(this).parent().find('div.error-message').length != 0) {
                    $(this).parent().find('div.error-message').text('<?php __('Enter a valid number') ?>');
                } else {
                    $('#charges').text('$0.00');

                    jQuery('<div/>', {
                        id: 'foo',
                        class: 'error-message',
                        text: '<?php __('Enter a valid number') ?>'
                    }).insertAfter($('#FekraPhaseCharged'));
                }
            }
        });

    });

    function getTotal($total) {
        $rate = ($total * 1.039) + .3;
        return parseFloat($rate.toFixed(2));
    }
    function getCharged($total) {
        $rate = (($total - 0.3) * 0.96246390760346);
        return parseFloat($rate.toFixed(2));
    }



</script>

