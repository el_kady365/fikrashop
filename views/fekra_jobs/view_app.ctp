<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('View Application') ?></strong></h3>



    <div class="panel panel-default">

        <div class="panel-heading"><?php __('Developer') ?>:</div>
        <div class="panel-body">
            <p>
                <a target="_blank" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($job_app['User']['id']))) ?>">
                    <?php echo $job_app['User']['first_name'] . ' ' . $job_app['User']['last_name'] ?>
                </a>
            </p>
        </div>
        <div class="panel-heading"><?php __('Paid to developer') ?>:</div>
        <div class="panel-body">
            <p>
                <?php echo format_price($job_app['FekraJobApplication']['price']) ?>
            </p>
        </div>

        <div class="panel-heading"><?php __('Total Amount') ?>:</div>
        <div class="panel-body">
            <p>
                <?php echo format_price($job_app['FekraJobApplication']['total']) ?>
            </p>
        </div>

        <div class="panel-heading"><?php __('Cover letter') ?>:</div>
        <div class="panel-body">
            <p>
                <?php echo nl2br($job_app['FekraJobApplication']['description']) ?>
            </p>
        </div>



    </div>

    <div class="input submit">
        <?php if (empty($job_app['FekraJob']['status']) && $user['id'] != $job_app['FekraJobApplication']['user_id']) { ?>
            <a href="<?php echo Router::url(array('action' => 'hire', $job_app['FekraJobApplication']['id'])) ?>" class="btn btn-success btn-lg"> <i class="fa fa-check-circle"></i> <?php __('Hire') ?> </a>
        <?php } ?>
        <a href="<?php echo Router::url(array('action' => 'view', $job_app['FekraJobApplication']['fekra_job_id'])) ?>" class="btn btn-default btn-lg"> <i class="fa fa-caret-left "></i> <?php __('Go to Job') ?> </a>
    </div>
    <?php echo $form->end(); ?>
</div>

<?php echo $this->element('user/user_leftmenu'); ?>