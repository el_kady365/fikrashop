<div class="content-box">
    <div class="fekra-head">
        <?php if (isset($category)) { ?>
            <h3 class="font-a sub-heading"><strong><?php echo $category['Category'][$lang . '_title']; ?> </strong> <span class="shape">(<?php echo count($jobs) ?>)</span></h3>
        <?php } else { ?>
            <h3 class="font-a sub-heading"><strong><?php __('Search results') ?> </strong> <span class="shape">(<?php echo count($jobs) ?>)</span></h3>
        <?php }if (isset($filters)) { ?>
            <div class="quick-filters  font-a">
                <ul>
                    <?php foreach ($filters as $key => $filter) { ?>
                        <li><a href="#"><?php echo $filter['text'] ?> <span rel="<?php echo $key ?>">x</span></a></li>
                    <?php } ?>

                </ul>
            </div>
        <?php } ?>      
        <div class="clear"></div>
    </div>
    <?php echo $this->element('jobs', array('jobs' => $jobs)) ?>
</div>
<?php echo $this->element('jobs_search'); ?>
<?php if (isset($filters)) { ?>
    <script>
        $filters =<?php echo $javascript->object($filters) ?>;
        //        console.log($filters);
        $new_filters = new Object();
        $(function() {

            $('.quick-filters a span').click(function() {
                $rel = $(this).attr('rel');

                for (i in $filters) {
                    //                    console.log($filters[i]);
                    if (i != $rel) {
                        $new_filters[i] = $filters[i].val;
                    }
                }
                self.location = '<?php echo Router::url('') ?>?' + ($.param($new_filters));
                return false;
            });
        });
    </script>
<?php } ?>