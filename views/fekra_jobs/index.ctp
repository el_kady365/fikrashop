<div class="content-box">


    <?php if (!empty($categories)) { ?>

        <div class="featured-carousel inner">

            <ul>
                <?php
                foreach ($categories as $category) {
                    if (!empty($category['Category']['image'])) {
                        $img = Router::url($category['Category']['image']['path']);
                    } else {
                        $img = Router::url('/css/img/fekra-thumb.png');
                    }
                    ?>
                    <li>
                        <a href="<?php echo Router::url(array('action' => 'jobs', $category['Category']['id'], slug($category['Category'][$lang . '_title']))); ?>">
                            <img src="<?php echo $img ?>" alt="" />
                        </a>
                        <span class="font-a">
                            <a href="<?php echo Router::url(array('action' => 'jobs', $category['Category']['id'], slug($category['Category'][$lang . '_title']))); ?>">
                                <?php echo $category['Category'][$lang . '_title'] ?>
                                (<?php echo $category['Category']['jobs'] ?>)
                            </a></span>
                    </li>
                <?php } ?>
            </ul>


        </div>
    <?php } else { ?>
        <div id="flashMessage" class="info"><?php __('There aren\'t any ideas') ?></div>
    <?php } ?>
</div>



<?php // echo $this->element('search_box'); ?>
<?php echo $this->element('jobs_search'); ?>