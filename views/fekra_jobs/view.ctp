<?php
App::import('Model', 'UserRating');
$edit = $cuser['User']['id'] == $user['id'];
$hired = false;
if (!empty($job_apps)) {
    foreach ($job_apps as $app) {
        if ($app['FekraJobApplication']['status'] == 1) {
            $hired = $app;
        }
    }
}
?>
<div class = "content">
    <div class = "fekra-head">
        <div class = "pull-right">
            <?php if (empty($job_app) && !$edit && empty($job['FekraJob']['status'])) {
                ?>
                <a href="<?php echo Router::url(array('action' => 'apply', $job['FekraJob']['id'])) ?>" class="btn btn-large btn-success right"> <i class="fa fa-briefcase"></i> <?php __('Apply to this job') ?></a>
            <?php } ?>
            <?php if ($edit && !in_array($job['FekraJob']['status'], array(1, 2))) { ?>
                <a href="<?php echo Router::url(array('action' => 'edit', $job['FekraJob']['id'])) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> <?php __('Edit job'); ?></a>

                <?php
            } if ($edit) {
                if ($job['FekraJob']['status'] == 1) {
                    ?>
                    <a href="<?php echo Router::url(array('action' => 'pay', $job['FekraJob']['id'])) ?>" class="btn btn-success btn-primary"><i class="fa fa-dollar"></i> <?php __('Pay installment'); ?></a>
                    <?php
                }
                if (in_array($job['FekraJob']['status'], array(1, 2)) && empty($user_rating)) {
                    ?>
                    <a href="<?php echo Router::url(array('action' => 'rate_developer', $hired['FekraJobApplication']['user_id'], $job['FekraJob']['id'])) ?>" class="btn btn-success btn-primary"><i class="fa fa-star"></i> <?php __('Rate developer'); ?></a>
                    <?php
                }
            }
            ?>

        </div>

        <h3 class="font-a sub-heading">
            <strong><?php echo $job['FekraJob']['title'] ?></strong>
        </h3>
        <h5 class="font-a sub-heading">
            <i class="fa fa fa-lightbulb-o"></i> <strong><a target="_blank" href="<?php echo $html->url(array('controller' => 'fekras', 'action' => 'view', $job['Fekra']['id'], slug($job['Fekra']['title']))) ?>"><?php echo $job['Fekra']['title'] ?></a></strong>
        </h5>
        <div class="row">
            <div class="col-md-6">
                <i class="fa fa-clock-o"></i> <strong><?php __('Date posted') ?>: </strong><?php echo $time->relativeTime($job['FekraJob']['created']) ?>
                <br>
                <i class="fa fa-calendar"></i> <strong><?php __('Delivery date') ?>: </strong><?php echo date('d M Y', strtotime($job['FekraJob']['delivery_date'])) ?>
            </div>
            <div class="col-md-6"><i class="fa fa-usd"></i> <strong><?php __('Price') ?>: </strong><?php echo format_price($job['FekraJob']['price']); ?></div>
            <?php
//            debug($hired);
            if (!empty($hired)) {
                ?>
                <div class = "col-md-6"><i class = "fa fa-check"></i> <strong><?php __('Hired') ?>: </strong>
                    <a href="<?php echo $html->url(array('controller' => 'users', 'action' => 'view', base64_encode($hired['User']['id']))) ?>"><?php echo $hired['User']['first_name'] . ' ' . $hired['User']['last_name'] ?></a></div>
            <?php } ?>
        </div>
        <h6 class="font-a shape"><a href="<?php echo $html->url(array('controller' => 'users', 'action' => 'view', base64_encode($cuser['User']['id']))) ?>"><?php __('Created by') ?> : <strong class="highlight-b"><?php echo $cuser['User']['first_name'] . ' ' . $cuser['User']['last_name'] ?></strong></a></h6>
        <div class="clear"></div>
    </div>
    <!-- / fekra-head -->


    <!-- / fekra-intro -->

    <div class="fekra-tabs">

        <div class="tabs-header font-a">
            <h4 class="well well-sm"><?php __('Job Description') ?></h4>

            <?php echo nl2br($job['FekraJob']['description']); ?>
            <div class="clear"></div>
            <?php
//            debug($job['FekraJob']['skills']);
            if (!empty($job['FekraJob']['skills'])) {
                ?>
                <h5><strong><?php __('Skills') ?>:</strong></h5>

                <?php
                foreach ($job['FekraJob']['skills'] as $skill) {
                    ?>
                    <button class="btn btn-sm btn-primary"><?php echo Skill::get_skill($skill); ?></button>  
                    <?php
                }
            }
            if (!empty($job['FekraJobAttachment'])) {
                ?>
                <h5><strong><?php __('Attachments') ?>:</strong></h5>

                <?php
                foreach ($job['FekraJobAttachment'] as $attach) {
                    ?>
                    <p><a target="_blank" href="<?php echo Router::url($attach['file_info']['full_path']); ?>"><i class="fa fa-file-text-o"></i> <?php echo $attach['file_info']['basename'] ?></a></p>
                    <?php
                }
            }
            ?>
            <div class="clear"></div>
            <br />
            <?php if (!empty($job_apps)) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="acollapse collapsed" data-toggle="collapse"  href="#collapseOne">
                                <i class="fa fa-plus-square"></i> <?php __('Applicants') ?> (<?php echo count($job_apps); ?>)
                            </a>
                        </h3>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <tr> 

                                    <th><?php __('Name') ?></th>
                                    <th><?php __('Date applied') ?></th>
                                    <?php if ($edit) { ?>
                                        <th><?php __('Amount') ?></th>
                                        <th><?php __('Rating') ?></th>
                                        <th><?php __('Actions') ?></th>
                                    <?php } ?>
                                </tr>

                                <?php foreach ($job_apps as $app) {
                                    ?>
                                    <tr> 

                                        <td><a target="_blank" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($app['FekraJobApplication']['user_id']))); ?>"><?php echo $app['User']['first_name'] . ' ' . $app['User']['last_name'] ?></a></td>
                                        <td><?php echo $time->relativeTime($app['FekraJobApplication']['created']) ?></td>
                                        <?php if ($edit) { ?>
                                            <td><?php echo format_price($app['FekraJobApplication']['total']); ?></td>
                                            <td>
                                                <div data-average="<?php echo UserRating::get_rating($app['FekraJobApplication']['user_id']); ?>"  class="Rating"></div>
                                                (<?php echo UserRating::get_rating($app['FekraJobApplication']['user_id']) . ' ' . __('of 5', true); ?>)
                                            </td>
                                            <td>
                                                <a href="<?php echo Router::url(array('action' => 'view_app', $app['FekraJobApplication']['id'])); ?>" class="btn btn-default btn-sm"><span aria-hidden="true" class="fa fa-search "></span> <?php __('View') ?></a>
                                                <?php if (!in_array($job['FekraJob']['status'], array(1, 2))) { ?>
                                                    <a href="<?php echo Router::url(array('action' => 'hire', $app['FekraJobApplication']['id'])); ?>" class="btn btn-success btn-sm"><span aria-hidden="true" class="fa fa-check "></span> <?php __('Hire') ?></a>
                                                <?php } ?>
                                            </td>

                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </table>

                        </div>
                    </div>
                </div>

            <?php } ?>

        </div>
    </div>
</div>
<?php
echo $html->css(array('jRating.jquery'));
echo $html->script(array('jRating.jquery'));
?>
<script>
    $(function () {
        $('.acollapse').click(function () {
            if ($(this).hasClass('collapsed')) {
                $(this).find('i').removeClass('fa-plus-square').addClass('fa-minus-square');
            } else {
                $(this).find('i').removeClass('fa-minus-square').addClass('fa-plus-square');

            }
        });
        $(".Rating").jRating({
            length: 5,
            bigStarsPath: '<?php echo Router::url('/css/img/rating/stars.png') ?>',
            step: 1,
            rateMax: 5,
            showRateInfo: true,
            sendRequest: false,
            isDisabled: true
        });
    });
</script>



