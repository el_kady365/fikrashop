<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Rate developer') ?></strong></h3>
    <?php
    echo $form->create('UserRating', array('url' => array('controller' => 'fekra_jobs', 'action' => 'rate_developer', $user_id, $job,$transaction), 'class' => 'form default-forms font-a'));
    ?>
    <div class="panel panel-default">

        <div class="panel-body">
            <?php
            echo $form->input('rating', array('class' => 'span6', 'empty' => __('Select Rating', true), 'options' => UserRating::$ratings));
            echo $form->input('description', array('class' => 'span6', 'type' => 'textarea', 'label' => __('Comment', true)));
            ?>
        </div>
    </div>
    <div class="input submit">
        <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-star-half-empty"></i> <?php __('Rate') ?> </button>
    </div>
    <?php echo $form->end(); ?>
</div>
<?php echo $this->element('user/user_leftmenu'); ?>