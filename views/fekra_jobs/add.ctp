<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('Add job') ?></strong></h3>


    <?php echo $form->create('FekraJob', array('type' => 'file', 'class' => 'form default-forms font-a')); ?>
    <div class="panel panel-default">

        <div class="panel-body">
            <?php
            echo $form->input('id');
            echo $form->input('title');
            if (!empty($this->data['FekraJob']['fekra_id'])) {
                echo $form->hidden('fekra_id');
            } else {
                echo $form->input('fekra_id', array('empty' => __('Select one', true), 'label' => __('Idea', true)));
            }

            echo $form->input('description');
            echo $form->input('skills', array('multiple' => true, 'style' => 'width:300px'));
            echo $form->input('price');
            echo $form->input('delivery_date', array('type' => 'text', 'class' => 'hasDate'));
            echo $form->input('published', array('div' => array('class' => 'checkbox')));
            ?>
            <div id="resources">
                <div class="qualification_set block_set url-div" style="display: none;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                        <tr>
                            <td valign="top">
                                <?php
                                echo $form->input('FekraJobAttachment.0.file', array('type' => 'file', 'div' => array('class' => 'input file '),
                                    'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                                ?>
                            </td>	

                        </tr>
                    </table>
                    <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                    <div class="clear"></div>
                </div>
                <div  id="relatedQualifications" class="items-block">
                    <h4><?php __('Attachments') ?></h4>
                    <?php if (!empty($this->data['FekraJobAttachment'])): ?>
                        <?php
                        foreach ($this->data['FekraJobAttachment'] as $i => $FekraJobAttachment):

                            $additional_class = '';
                            if ($i > 0) {
                                $additional_class = 'url-div';
                            }
                            ?>
                            <div class="qualification_set block_set url-div">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                                    <tr>
                                        <td valign="top">
                                            <?php
                                            echo $form->input("FekraJobAttachment.$i.id", array('class' => 'INPUT', 'value' => isset($FekraJobAttachment['id']) ? $FekraJobAttachment['id'] : ''));
                                            echo $form->input("FekraJobAttachment.$i.file", array('type' => 'file', 'div' => array('class' => 'input file '),
                                                'between' => $this->element('front_file_element', array('info' => isset($FekraJobAttachment['file_info']) ? $FekraJobAttachment['file_info'] : $attachmentInfo, 'field' => 'file'))));
                                            ?>
                                        </td>	

                                    </tr>
                                </table>
                                <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                                <div class="clear"></div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="qualification_set block_set url-div">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="FormsTable">
                                <tr>
                                    <td valign="top">
                                        <?php
                                        echo $form->input('FekraJobAttachment.0.file', array('type' => 'file', 'div' => array('class' => 'input file'),
                                            'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                                        ?>
                                    </td>	

                                </tr>
                            </table>
                            <a href="#" class='delete-section2'> <i class="fa fa-trash-o fa-lg"></i></a>
                            <div class="clear"></div>
                        </div>
                    <?php endif; ?>
                </div>
                <a href="javascript:appendQualificationUploader();" class="btn btn-primary AddQualification AddItem"><i class="fa fa-plus"></i> <?php __('Add Attachment') ?></a>
            </div>
            <div class="input submit">
                <button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-check-circle"></i> <?php __('Save') ?> </button>
            </div>
        </div>
    </div>
    <?php echo $form->end(); ?>

</div>
<?php echo $this->element('user/user_leftmenu'); ?>

<?php
echo $html->css(array('jquery-ui-1.9.2.custom.min', 'select2'));
echo $javascript->link(array('jquery-ui-1.9.2.custom.min', 'select2'));
?>
<script>
    $(document).ready(function() {
        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
            changeYear: true, minDate: new Date()});
        $('#FekraJobSkills').select2({
            placeholder: "<?php echo __('Select Skill', true) ?>",
        });

    });
    $('.hasDate').on('focus', function() {
        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
            changeYear: true, minDate: new Date()});
    })
</script>
<script type="text/javascript">

    $(document).on('click', '.delete-section2', function() {
        if (confirm('Are you sure?')) {
            container_id = $(this).parents('.items-block').attr('id');
            console.log(container_id);
            if ($('#' + container_id).find('.block_set').length <= 1) {
                $(this).parent('.block_set').find('input').val('');
            } else {
                $(this).parent('.block_set').remove();
            }
        }
        return false;
    });
    function appendQualificationUploader()
    {
        if ($('#relatedQualifications').is(':visible')) {
            if ($('.qualification_set').length <= 5) {
                $('#relatedQualifications').append('<div class="qualification_set block_set url-div">' + $('.qualification_set:first').html().replace(/\[0\]/g, '[' + $('.qualification_set').length + ']').replace(/FekraJobAttachment0/g, 'FekraJobAttachment' + $('.qualification_set').length) + '</div>');
                $('#relatedQualifications .qualification_set:last input').val('');
            } else {
                alert('<?php __('Sorry, only 5 attachments are allowed') ?>');
            }

        }
        else {
            $('#relatedQualifications').show();
        }
    }
</script>
