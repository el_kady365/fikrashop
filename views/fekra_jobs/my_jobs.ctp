<?php echo $this->element('user/user_leftmenu'); ?>
<div class="content-box">
    <div class="fekra-head">
        <a class="btn btn-success right" href="<?php echo Router::url(array('action' => 'add')) ?>"><i class="fa fa-plus-square"></i> <?php __('Add new job') ?></a>

        <h3 class="font-a sub-heading"><strong><?php echo $sub_title ?></strong></h3>
        <div class="clear"></div>
    </div>
    <table class="table">
        <tr>
            <th><?php echo __('Id', true) ?></th>
            <th><?php echo __('Job title', true) ?></th>
            <th><?php echo __('Idea', true) ?></th>
            <th><?php echo __('Actions', true) ?></th>
        </tr>

        <?php
        if (!empty($jobs)) {

            foreach ($jobs as $job) {
                ?>
                <tr>
                    <td><a href="<?php echo Router::url(array('action' => 'view', $job['FekraJob']['id'], slug($job['FekraJob']['title']))) ?>"><?php echo $job['FekraJob']['id'] ?></a></td>
                    <td><a href="<?php echo Router::url(array('action' => 'view', $job['FekraJob']['id'], slug($job['FekraJob']['title']))) ?>"><?php echo $job['FekraJob']['title'] ?></a></td>
                    <td><a href="<?php echo Router::url(array('controller' => 'fekras', 'action' => 'view', $job['Fekra']['id'], slug($job['Fekra']['title']))) ?>"><?php echo $job['Fekra']['title'] ?></a></td>
                    <td> 
                        <?php if ($job['FekraJob']['status'] == 1) { ?>
                            <a href="<?php echo Router::url(array('action' => 'pay', $job['FekraJob']['id'])) ?>" class="btn btn-success btn-primary"><i class="fa fa-check-square"></i> <?php __('Pay Installment'); ?></a>
                            <?php
                        }
                        if (!in_array($job['FekraJob']['status'], array(1, 2))) {
                            ?>
                            <a href="<?php echo Router::url(array('action' => 'edit', $job['FekraJob']['id'])); ?>" class="btn btn-default btn-sm"><span aria-hidden="true" class="fa fa-pencil "></span> <?php __('Edit') ?></a>
                            <a href="<?php echo Router::url(array('action' => 'delete', $job['FekraJob']['id'])); ?>" class="btn btn-danger btn-sm delete"><span aria-hidden="true" class="fa fa-trash-o"></span> <?php __('Delete') ?></a>        
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
            if ($paginator->numbers()) {
                ?>
                <div class="paging">
                    <ul >
                        <?php
                        if ($paginator->hasNext()) {
                            echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                        if ($paginator->hasPrev()) {
                            echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                        }
                        ?>

                    </ul>
                </div>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="4">
                    <div id="flashMessage" class="info"><?php __('There aren\'t any jobs') ?></div>
                </td>
            </tr>
        <?php } ?>

    </table>
</div>
<?php
echo $html->css('jquery-impromptu.min');
echo $javascript->link('jquery-impromptu.min');
?>
<script>
    $('.delete').click(function() {
        hrf = $(this).get(0).href;
        $this = $(this);
        var txt = '<?php __('Are you sure you want to remove this job?') ?>';

        $.prompt(txt, {
            buttons: {'<?php __('Delete') ?>': true, '<?php __('Cancel') ?>': false},
            close: function(e, v, m, f) {

                if (v) {

                    //Here is where you would do an ajax post to remove the user
                    //also you might want to print out true/false from your .php
                    //file and verify it has been removed before removing from the 
                    //html.  if false dont remove, $promt() the error.
                    self.location = hrf;

                }
                else {
                }

            }
        });
        return false;
    });
</script>