<div class="content-box">
    <div class="fekra-head">
        <?php if (isset($edit)) { ?>
            <a class="btn btn-success right" href="<?php echo Router::url(array('action' => 'add')) ?>"><?php __('Add new idea') ?></a>
        <?php } ?>
        <h3 class="font-a sub-heading"><strong><?php echo $sub_title ?></strong>
        </h3>
        <div class="clear"></div>
    </div>
    <?php echo $this->element('ideas', array('fekras' => $fekras)) ?>
</div>
<?php echo $this->element('user/user_leftmenu'); ?>