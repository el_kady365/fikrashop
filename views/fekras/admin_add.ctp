<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Fekra']['id'])) {

                    __('Edit Fekra');
                } else {

                    __('Add Fekra');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('Fekra', array('type' => 'file')); ?>
        <div class="block-fluid tabs ui-tabs ui-widget ui-widget-content ui-corner-all">
            <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
                    <a href="#Brief" data-toggle="tab">
                        <span class="fa fa-search" aria-hidden="true"></span> <?php __('Idea Brief') ?>
                    </a>
                </li>
                <li class="ui-state-default ui-corner-top">
                    <a href="#Images" data-toggle="tab"><i class="fa fa-th-list"></i> <?php __('Phases') ?></a>
                </li>
                <li class="ui-state-default ui-corner-top"><a href="#Videos" data-toggle="tab"><span class="fa fa-video-camera"></span> <?php __('Videos') ?></a></li>
                <li class="ui-state-default ui-corner-top"><a href="#Business" data-toggle="tab"><span class="fa fa-bookmark" aria-hidden="true"></span> <?php __('Business Plan') ?></a></li>
            </ul>
            <div id="Brief" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
                <?php
                echo $form->input('id');
                echo $form->input('category_id');
                echo $form->input('title');
                echo $form->input('description');
                echo $form->input('end_date', array('type' => 'text', 'class' => 'hasDate'));
                echo $form->input('price');
                echo $form->input('image', array('type' => 'file',
                    'between' => $this->element('image_element', array('info' => !empty($this->data['Fekra']['image']) ? $this->data['Fekra']['image'] : '', 'field' => 'image'))));
                echo $form->input('published');
                echo $form->input('featured');
                echo $form->submit('Submit', array('class' => 'submit-green'));
                ?>
            </div>

            <div id="Images" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
                <div class="Authors" >
                    <?php
                    $section_count = 0;
//                debug($this->data['AssignmentFile']);
                    if (!empty($this->data['FekraPhase'])) {
                        foreach ($this->data['FekraPhase'] as $j => $image) {
                            //debug($level);
                            ?>

                            <div id='row<?php echo $j ?>' class="url-div">
                                <?php
                                echo $form->input('FekraPhase.' . $j . '.id');
                                echo $form->input('FekraPhase.' . $j . '.title');
                                echo $form->input('FekraPhase.' . $j . '.description');
                                echo $form->input('FekraPhase.' . $j . '.percentage');
                                echo $form->input('FekraPhase.' . $j . '.price');
                                ?>
                                &nbsp;
                                &nbsp<a href = '#' class = "delete-section2  btn-default btn-small" onClick = 'removeFormField("#row<?php echo $j ?>");
                                                return false;'> <i class="fa fa-trash-o"></i> </a>
                            </div>
                            <?php
                            $section_count++;
                        }
                    } else {
                        ?>
                        <div id='row0' class="url-div">
                            <?php
//                echo $form->input('Author.0.id');

                            echo $form->input('FekraPhase.' . 0 . '.title');
                            echo $form->input('FekraPhase.' . 0 . '.description');
                            echo $form->input('FekraPhase.' . 0 . '.percentage');
                            echo $form->input('FekraPhase.' . 0 . '.price');
                            ?>
                        </div>
                    <?php } ?>

                </div>
                <a href="#" class="add-image btn btn-default btn-small ">
                    <span class="st-ico icon-plus"></span> <?php __('Add Phase') ?>
                </a>
            </div>

            <div id="Videos" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" >
                <div class="Authors">
                    <?php
                    $section_count2 = 0;
//                debug($this->data['AssignmentFile']);
                    if ($this->data['FekraVideo']) {
                        foreach ($this->data['FekraVideo'] as $i => $video) {
                            ?>

                            <div id='row<?php echo $i ?>' class="url-div">
                                <?php
                                echo $form->input('FekraVideo.' . $i . '.id');
                                echo $form->input('FekraVideo.' . $i . '.type', array('options' => $types, 'empty' => __('Choose Type', true)));
                                echo $form->input('FekraVideo.' . $i . '.video', array('div' => array('class' => 'hide input text video'), 'label' => __('Youtube Link', true)));
                                echo $form->input('FekraVideo.' . $i . '.file', array('type' => 'file', 'div' => array('class' => 'input file hide'),
                                    'between' => $this->element('front_file_element', array('info' => isset($video['file_info']) ? $video['file_info'] : '', 'field' => 'file'))));
                                ?>
                                &nbsp;
                                &nbsp<a href = '#' class = "delete-section2  btn-default btn-small" onClick = 'removeFormField("#row<?php echo $i ?>");
                                                return false;'> <i class="fa fa-trash-o"></i> </a>
                            </div>
                            <?php
                            $section_count2++;
                        }
                    } else {
                        ?>
                        <div id='row0' class="url-div">
                            <?php
//                echo $form->input('Author.0.id');
                            echo $form->input('FekraVideo.' . 0 . '.type', array('options' => $types, 'empty' => __('Choose Type', true)));
                            echo $form->input('FekraVideo.' . 0 . '.video', array('div' => array('class' => 'input text hide video'), 'label' => __('Youtube Link', true)));
                            echo $form->input('FekraVideo.' . 0 . '.file', array('type' => 'file', 'div' => array('class' => 'input file hide'), 'between' => $this->element('file_element', array('info' => $info, 'field' => 'file'))));
                            ?>
                        </div>
                    <?php } ?>

                </div>
                <a href="#" class="add-video btn btn-default btn-small ">
                    <span class="st-ico icon-plus"></span> <?php __('Add Video') ?>
                </a>
            </div>
            <div id="Business" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
                <?php
                echo $form->input('file', array('type' => 'file', 'label' => __('Upload bussiness plan', true),
                    'between' => $this->element('file_element', array('info' => !empty($this->data['Fekra']['file_info']) ? $this->data['Fekra']['file_info'] : $binfo, 'field' => 'file'))));
                ?>

            </div>
            <?php echo $form->end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
                                    $(document).ready(function() {
                                        $('.hasDate').datepicker({
                                            "dateFormat": 'yy-mm-dd',
                                            changeMonth: true,
                                            changeYear: true,
                                            //            isRTL: true
                                        });

                                        $(".hide").hide();
//                                 change_status($(this));
                                        $('select[id$="Type"]').each(function() {
                                            change_status($(this));
                                        });
                                        $('.ui-state-default a').click(function() {
                                            $href = $(this).attr('href');
                                            $($href).find('.uploader').show();
                                        });


                                    });
                                    $(document).on('change', 'select[id$="Type"]', function() {
                                        change_status($(this));
                                    });
                                    var section_count = '<?php echo $section_count ?>';
                                    var section_count2 = '<?php echo $section_count2 ?>';
                                    $('.add-image').on('click', function() {

                                        section_count++;
                                        x = '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.title', array('div' => array('class' => 'form-row control-group clearfix')))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.description', array('div' => array('class' => 'form-row control-group clearfix')))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.percentage', array('div' => array('class' => 'form-row control-group clearfix')))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.price', array('div' => array('class' => 'form-row control-group clearfix')))) ?>';
                                        x += '<a href="#" class=" delete-section2 btn-default btn-small" onClick=\'removeFormField("#row0"); return false;\' > <i class="fa fa-trash-o"></i> </a>';
                                        y = x.replace(/FekraPhase0/g, 'FekraPhase' + section_count);
                                        y = y.replace(/row0/g, 'row' + section_count);
                                        y = y.replace(/\[FekraPhase]\[0\]/g, '[FekraPhase][' + section_count + ']');
                                        $(this).parent().find('.Authors').append('<div class="url-div" id="row' + section_count + '">' + y + '</div>');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('textarea').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').attr('checked', false);

                                        return false;
                                    });
                                    $('.add-video').on('click', function() {

                                        section_count2++;
                                        x = '<?php echo $javascript->escapeString($form->input('FekraVideo.' . 0 . '.type', array('options' => $types, 'div' => array('class' => 'form-row control-group clearfix'), 'empty' => __('Choose Type', true)))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraVideo.' . 0 . '.video', array('div' => array('class' => 'form-row control-group clearfix hide video'), 'label' => __('Youtube Link', true)))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraVideo.' . 0 . '.file', array('type' => 'file', 'div' => array('class' => 'form-row control-group clearfix hide file'), 'between' => $this->element('file_element', array('info' => $info, 'field' => 'file'))))) ?>';
                                        x += '<a href="#" class=" delete-section2 btn-default btn-small" onClick=\'removeFormField("#row0"); return false;\' > <i class="fa fa-trash-o"></i> </a>';
                                        y = x.replace(/FekraVideo0/g, 'FekraVideo' + section_count2);
                                        y = y.replace(/row0/g, 'row' + section_count2);
                                        y = y.replace(/\[FekraVideo]\[0\]/g, '[FekraVideo][' + section_count2 + ']');
                                        $(this).parent().find('.Authors').append('<div class="url-div" id="row' + section_count2 + '">' + y + '</div>');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('textarea').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').attr('checked', false);
                                        $(".url-div:last .hide").hide();
                                        return false;
                                    });
                                    function removeFormField(id) {
                                        $(id).remove();
                                    }


                                    function change_status($type) {
                                        var ttype = $type.val();
                                        if (ttype == '1')
                                        {
                                            $type.closest('.url-div').find('.video').removeClass('hide').slideDown();
                                            $type.closest('.url-div').find('.file').removeClass('hide').slideUp();
                                        }
                                        if (ttype == '2')
                                        {
                                            $type.closest('.url-div').find('.video').removeClass('hide').slideUp();
                                            $type.closest('.url-div').find('.file').removeClass('hide').slideDown();
                                        }

                                    }

</script>

