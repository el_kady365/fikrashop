<div class="content-box">
    <?php if (isset($category)) { ?>
        <a href="<?php echo Router::url(array('action' => 'ideas', $category['Category']['id'], slug($category['Category'][$lang . '_title']))); ?>">
            <h3 class="font-a sub-heading"><strong><?php echo $category['Category'][$lang . '_title']; ?> </strong> <span class="shape">(<?php echo count($fekras) ?>)</span></h3>
        </a>
    <?php } else { ?>
        <h3 class="font-a sub-heading"><strong><?php __('Search results') ?> </strong> <span class="shape">(<?php echo count($fekras) ?>)</span></h3>
    <?php }if (isset($filters)) { ?>
        <div class="quick-filters  font-a">
            <ul>
                <?php foreach ($filters as $key => $filter) { ?>
                    <li><a href="#"><?php echo $filter['text'] ?> <span rel="<?php echo $key ?>">x</span></a></li>
                <?php } ?>

            </ul>
        </div>
    <?php } ?>


    <?php
    if (!empty($fekras)) {
        ?>
        <div class="featured-carousel inner">

            <ul>
                <?php
                foreach ($fekras as $fekra) {
                    if (!empty($fekra['Fekra']['image'])) {
                        $img = Router::url($fekra['Fekra']['image']['thumb1']);
                    } else {
                        $img = Router::url('/css/img/fekra-thumb.png');
                    }
//                debug($fekra);
                    ?>


                    <li>
                        <a href="<?php echo Router::url(array('action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']))); ?>" >
                            <img src="<?php echo $img ?>" alt="" />
                        </a>
                        <span class="font-a">
                            <a href="<?php echo Router::url(array('action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']))); ?>" >
                                <?php echo $fekra['Fekra']['title'] ?>
                            </a>
                        </span>
                    </li>




                    <?php
                }
                ?>
            </ul>
        </div>

        <?php
        $paginator->options(array('url' => array_merge($this->passedArgs, array('?' => ltrim(strstr($_SERVER['QUERY_STRING'], '&'), '&')))));
        if ($paginator->numbers()) {
            ?>
            <div class="paging">
                <ul >
                    <?php
                    if ($paginator->hasNext()) {
                        echo $paginator->next(__('Next', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                    }
                    echo $paginator->numbers(array('separator' => '', 'tag' => 'li'));
                    if ($paginator->hasPrev()) {
                        echo $paginator->prev(__('Previous', true), array('tag' => 'li'), null, array('class' => 'disabled'));
                    }
                    ?>

                </ul>
            </div>
            <?php
        }
    } else {
        ?>
        <div id="flashMessage" class="info"><?php __('There aren\'t any ideas') ?></div>
    <?php } ?>
</div>

<?php echo $this->element('search_box'); ?>
<?php if (isset($filters)) { ?>
    <script>
        $filters =<?php echo $javascript->object($filters) ?>;
        //        console.log($filters);
        $new_filters = new Object();
        $(function() {

            $('.quick-filters a span').click(function() {
                $rel = $(this).attr('rel');

                for (i in $filters) {
                    //                    console.log($filters[i]);
                    if (i != $rel) {
                        $new_filters[i] = $filters[i].val;
                    }
                }
                self.location = '<?php echo Router::url('') ?>?' + ($.param($new_filters));
                return false;
            });
        });
    </script>
<?php } ?>