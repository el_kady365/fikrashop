<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php __('View idea phase') ?></strong></h3>



    <div class="panel panel-default">

        <div class="panel-heading"><?php __('Title') ?>:</div>
        <div class="panel-body">
            <p>               
                <?php echo $phase['FekraPhase']['title'] ?> 
            </p>
        </div>
        <?php if (!empty($phase['FekraPhase']['description'])) { ?>
            <div class="panel-heading"><?php __('Description') ?>:</div>
            <div class="panel-body">
                <p>               
                    <?php echo $phase['FekraPhase']['description'] ?> 
                </p>
            </div>
        <?php } ?>
        <?php if (!empty($phase['FekraPhase']['file'])) { ?>
            <div class="panel-heading"><?php __('Scope of work') ?>:</div>
            <div class="panel-body">
                <p>               
                    <a  class="btn btn-default" target="_blank" href="<?php echo Router::url($phase['FekraPhase']['file_info']['full_path']); ?>"><i class="fa fa-download"></i> <?php echo __('Download', true) ?> </a>
                </p>
            </div>
        <?php } ?>

        <div class="panel-heading"><?php __('price') ?>:</div>
        <div class="panel-body">
            <p>
                <?php echo format_price($phase['FekraPhase']['price']) ?>
            </p>
        </div>





    </div>

    <div class="input submit">

        <a href="<?php echo Router::url(array('action' => 'invest', $phase['Fekra']['id'])) ?>" class="btn btn-default btn-lg"> <i class="fa fa-caret-left "></i> <?php __('Back') ?> </a>
    </div>
    <?php echo $form->end(); ?>
</div>

<?php echo $this->element('user/user_leftmenu'); ?>
