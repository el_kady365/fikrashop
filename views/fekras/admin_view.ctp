
<div class="module">
    <h2><span><?php  __('Fekra');?></span></h2>
    <div class="module-body">
        <dl><?php $i = 0; $class = ' class="altrow"';?>
            		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Category'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($fekra['Category']['title'], array('controller' => 'categories', 'action' => 'view', $fekra['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Progress'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['progress']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Image'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['image']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $fekra['Fekra']['updated']; ?>
			&nbsp;
		</dd>
        </dl>


        <div class="actions">
            <h3><?php __('Actions'); ?></h3>
            <ul>
                		<li><?php echo $this->Html->link(__('Edit Fekra', true), array('action' => 'edit', $fekra['Fekra']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Fekra', true), array('action' => 'delete', $fekra['Fekra']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $fekra['Fekra']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fekras', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fekra', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fekra Users', true), array('controller' => 'fekra_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fekra User', true), array('controller' => 'fekra_users', 'action' => 'add')); ?> </li>
            </ul>
        </div>
                    <div class="related">
                <h3><?php __('Related Fekra Users');?></h3>
                <?php if (!empty($fekra['FekraUser'])):?>
                <table cellpadding = "0" cellspacing = "0">
                    <tr>
                        		<th><?php __('Id'); ?></th>
		<th><?php __('Fekra Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th><?php __('Type'); ?></th>
                        <th class="actions"><?php __('Actions');?></th>
                    </tr>
                    	<?php
		$i = 0;
		foreach ($fekra['FekraUser'] as $fekraUser):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $fekraUser['id'];?></td>
			<td><?php echo $fekraUser['fekra_id'];?></td>
			<td><?php echo $fekraUser['user_id'];?></td>
			<td><?php echo $fekraUser['type'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'fekra_users', 'action' => 'view', $fekraUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'fekra_users', 'action' => 'edit', $fekraUser['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'fekra_users', 'action' => 'delete', $fekraUser['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $fekraUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
                </table>
                <?php endif; ?>

                <div class="actions">
                    <ul>
                        <li><?php echo $this->Html->link(__('New Fekra User', true), array('controller' => 'fekra_users', 'action' => 'add'));?> </li>
                    </ul>
                </div>
            </div>
            </div>
</div>