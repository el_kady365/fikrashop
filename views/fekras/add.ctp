<?php
/* @var $html HtmlHelper */
?>


<div class="content-box">
    <h3 class="font-a sub-heading"><strong><?php echo $h1 ?></strong></h3>
    <?php echo $form->create('Fekra', array('id' => 'FekraForm', 'class' => 'form default-forms', 'type' => 'file')); ?>
    <div>
        <div class="tabs-header font-a">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#Brief" data-toggle="tab">
                        <span class="fa fa-search" aria-hidden="true"></span> <?php __('Idea Brief') ?>
                    </a>
                </li>
                <li><a href="#Images" data-toggle="tab"><i class="fa fa-th-list"></i> <?php __('Phases') ?></a></li>
                <li><a href="#Videos" data-toggle="tab"><span class="fa fa-video-camera"></span> <?php __('Videos') ?></a></li>
                <li><a href="#Business" data-toggle="tab"><span class="fa fa-bookmark" aria-hidden="true"></span> <?php __('Business Plan') ?></a></li>
            </ul>
        </div>

        <div class="tab-content">
            <div id="Brief" class="tab-pane active" >
                <?php
                echo $form->input('id');
                echo $form->input('category_id', array('empty' => __('Choose category', true)));
                echo $form->input('title');
                echo $form->input('description', array('class' => 'summernote'));
                //echo $fck->load('Fekra', 'description', '', 'Basic');
                echo $form->input('price');
                echo $form->input('image', array('type' => 'file',
                    'between' => $this->element('front_image_element', array('info' => !empty($this->data['Fekra']['image']) ? $this->data['Fekra']['image'] : '', 'field' => 'image'))));
                echo $form->input('end_date', array('type' => 'text', 'class' => 'hasDate'));
                echo $form->input('published');
                ?> 

            </div>

            <div id="Images" class="tab-pane">
                <div class="Authors" >
                    <?php
                    $section_count = 0;
//                debug($this->data['AssignmentFile']);
                    if (!empty($this->data['FekraPhase'])) {
                        foreach ($this->data['FekraPhase'] as $j => $image) {
                            //debug($level);
                            ?>

                            <div id='row<?php echo $j ?>' class="url-div">
                                <?php
                                echo $form->input('FekraPhase.' . $j . '.id');
                                echo $form->input('FekraPhase.' . $j . '.title');
                                echo $form->input('FekraPhase.' . $j . '.description');
                                echo $form->input('FekraPhase.' . $j . '.file', array('type' => 'file', 'label' => __('Scope of work', true),
                                    'between' => $this->element('front_file_element', array('info' => isset($image['file_info']) ? $image['file_info'] : $attachmentInfo, 'field' => 'file'))));
                                echo $form->input('FekraPhase.' . $j . '.percentage');
                                echo $form->input('FekraPhase.' . $j . '.price');
                                ?>
                                &nbsp;
                                &nbsp<a href = '#' class = "delete-section2  btn-default btn-small" onClick = 'removeFormField("#row<?php echo $j ?>");
                                                return false;'> <i class="fa fa-trash-o"></i> </a>
                            </div>
                            <?php
                            $section_count++;
                        }
                    } else {
                        ?>
                        <div id='row0' class="url-div">
                            <?php
//                echo $form->input('Author.0.id');

                            echo $form->input('FekraPhase.' . 0 . '.title');
                            echo $form->input('FekraPhase.' . 0 . '.description');
                            echo $form->input('FekraPhase.' . 0 . '.file', array('type' => 'file', 'label' => __('Scope of work', true),
                                'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file'))));
                            echo $form->input('FekraPhase.' . 0 . '.percentage');
                            echo $form->input('FekraPhase.' . 0 . '.price');
                            ?>
                        </div>
                    <?php } ?>

                </div>
                <a href="#" class="add-image btn btn-default btn-small ">
                    <span class="fa fa-plus"></span> <?php __('Add Phase') ?>
                </a>
            </div>

            <div id="Videos" class="tab-pane" >
                <div class="Authors">
                    <?php
                    $section_count2 = 0;
//                debug($this->data['AssignmentFile']);
                    if ($this->data['FekraVideo']) {
                        foreach ($this->data['FekraVideo'] as $i => $video) {
                            ?>

                            <div id='row<?php echo $i ?>' class="url-div">
                                <?php
                                echo $form->input('FekraVideo.' . $i . '.id');
                                echo $form->input('FekraVideo.' . $i . '.type', array('options' => $types, 'empty' => __('Choose Type', true)));
                                echo $form->input('FekraVideo.' . $i . '.video', array('div' => array('class' => 'hide input text video'), 'label' => __('Youtube Link', true)));
                                echo $form->input('FekraVideo.' . $i . '.file', array('type' => 'file', 'div' => array('class' => 'input file hide'),
                                    'between' => $this->element('front_file_element', array('info' => isset($video['file_info']) ? $video['file_info'] : '', 'field' => 'file'))));
                                ?>
                                &nbsp;
                                &nbsp<a href = '#' class = "delete-section2  btn-default btn-small" onClick = 'removeFormField("#row<?php echo $i ?>");
                                                return false;'> <i class="fa fa-trash-o"></i> </a>
                            </div>
                            <?php
                            $section_count2++;
                        }
                    } else {
                        ?>
                        <div id='row0' class="url-div">
                            <?php
//                echo $form->input('Author.0.id');
                            echo $form->input('FekraVideo.' . 0 . '.type', array('options' => $types, 'empty' => __('Choose Type', true)));
                            echo $form->input('FekraVideo.' . 0 . '.video', array('div' => array('class' => 'input text hide video'), 'label' => __('Youtube Link', true)));
                            echo $form->input('FekraVideo.' . 0 . '.file', array('type' => 'file', 'div' => array('class' => 'input file hide'), 'between' => $this->element('file_element', array('info' => $info, 'field' => 'file'))));
                            ?>
                        </div>
                    <?php } ?>

                </div>
                <a href="#" class="add-video btn btn-default btn-small ">
                    <span class="fa fa-plus"></span> <?php __('Add Video') ?>
                </a>
            </div>
            <div id="Business" class="tab-pane">
                <?php
                echo $form->input('file', array('type' => 'file', 'label' => __('Upload bussiness plan', true),
                    'between' => $this->element('front_file_element', array('info' => !empty($this->data['Fekra']['file_info']) ? $this->data['Fekra']['file_info'] : $binfo, 'field' => 'file'))));
                ?>
                <div id="POLL">

                </div>
            </div>
        </div>
    </div>


    <div class="input submit">
        <button class="btn btn-success btn-lg" type="submit"> <i class="fa fa-check-circle"></i> Save </button>
    </div>

    <?php echo $form->end(); ?>



</div>

<?php
echo $html->css(array('jquery-ui-1.9.2.custom.min', 'summernote'));
echo $javascript->link(array('jquery-ui-1.9.2.custom.min', 'summernote'), false);
?>
<?php echo $this->element('user/user_leftmenu'); ?>
<?php echo $html->scriptStart(array('inline' => false)); ?>

//<script>
                                    $(function() {
                                        $('.summernote').summernote({
                                            height: 300,
                                            focus: true
                                        });

                                        $('#FekraForm').submit(function() {
                                            var valid = true;
                                            $('#Images input').each(function() {
                                                if ($(this).attr('type')!='hidden' && $(this).attr('type')!='file' && $(this).val() == '') {
                                                    valid = false;
                                                }
                                            });
                                            if (valid == false) {
                                                alert('<?php __('Please complete phases data') ?>');
                                                $('.nav-tabs li a[href="#Images"]').click();
                                                return false;
                                            } else if ($('#Images input[type!="hidden"]').length == 0) {
                                                alert('<?php __('Please, add at least one phase') ?>');
                                                $('.nav-tabs li a[href="#Images"]').click();
                                                return false;
                                            }

                                        });

                                        $(".hide").hide();
                                        getpoll($('#FekraCategoryId').val());




//                                 change_status($(this));
                                        $('select[id$="Type"]').each(function() {
                                            change_status($(this));
                                        });

                                        $('#FekraCategoryId').change(function() {
                                            getpoll($(this).val());
                                        });

                                    });
                                    function getpoll(id) {
                                        if (id != '') {

                                            var fekra_user = '<?php isset($fekra_user) ? print($fekra_user)  : print(""); ?>';
                                            $.ajax({
                                                cache: false,
                                                url: '<?php echo Router::url(array('controller' => 'polls', 'action' => 'solve')) ?>/' + id + '/' + fekra_user,
                                                type: 'POST',
                                                dataType: 'html',
                                                success: function(data) {
                                                    $('#POLL').html(data);
                                                }
                                            });
                                        }
                                    }

                                    $(document).on('change', 'select[id$="Type"]', function() {
                                        change_status($(this));
                                    });
                                    var section_count = '<?php echo $section_count ?>';
                                    var section_count2 = '<?php echo $section_count2 ?>';
                                    $('.add-image').on('click', function() {

                                        section_count++;
                                        x = '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.title')) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.description')) ?>';
                                        x += '<?php
echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.file', array('type' => 'file', 'label' => __('Scope of work', true),
            'between' => $this->element('front_file_element', array('info' => $attachmentInfo, 'field' => 'file')))))
?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.percentage')) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraPhase.' . 0 . '.price')) ?>';
                                        x += '<a href="#" class=" delete-section2 btn-default btn-small" onClick=\'removeFormField("#row0"); return false;\' > <i class="fa fa-trash-o"></i> </a>';
                                        y = x.replace(/FekraPhase0/g, 'FekraPhase' + section_count);
                                        y = y.replace(/row0/g, 'row' + section_count);
                                        y = y.replace(/\[FekraPhase]\[0\]/g, '[FekraPhase][' + section_count + ']');
                                        $(this).parent().find('.Authors').append('<div class="url-div" id="row' + section_count + '">' + y + '</div>');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('textarea').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').attr('checked', false);
                                        return false;
                                    });
                                    $('.add-video').on('click', function() {

                                        section_count2++;
                                        x = '<?php echo $javascript->escapeString($form->input('FekraVideo.' . 0 . '.type', array('options' => $types, 'empty' => __('Choose Type', true)))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraVideo.' . 0 . '.video', array('div' => array('class' => 'input text hide video'), 'label' => __('Youtube Link', true)))) ?>';
                                        x += '<?php echo $javascript->escapeString($form->input('FekraVideo.' . 0 . '.file', array('type' => 'file', 'div' => array('class' => 'input file  hide file'), 'between' => $this->element('file_element', array('info' => $info, 'field' => 'file'))))) ?>';
                                        x += '<a href="#" class=" delete-section2 btn-default btn-small" onClick=\'removeFormField("#row0"); return false;\' > <i class="fa fa-trash-o"></i> </a>';
                                        y = x.replace(/FekraVideo0/g, 'FekraVideo' + section_count2);
                                        y = y.replace(/row0/g, 'row' + section_count2);
                                        y = y.replace(/\[FekraVideo]\[0\]/g, '[FekraVideo][' + section_count2 + ']');
                                        $(this).parent().find('.Authors').append('<div class="url-div" id="row' + section_count2 + '">' + y + '</div>');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('textarea').val('');
                                        $(this).parent().find('.Authors').find('.url-div:last').find('input').attr('checked', false);
                                        $(".url-div:last .hide").hide();
                                        return false;
                                    });
                                    function removeFormField(id) {
                                        $(id).remove();
                                    }


                                    function change_status($type) {
                                        var ttype = $type.val();
                                        if (ttype == '1')
                                        {
                                            $type.closest('.url-div').find('.video').removeClass('hide').slideDown();
                                            $type.closest('.url-div').find('.file').removeClass('hide').slideUp();
                                        }
                                        if (ttype == '2')
                                        {
                                            $type.closest('.url-div').find('.video').removeClass('hide').slideUp();
                                            $type.closest('.url-div').find('.file').removeClass('hide').slideDown();
                                        }
                                    }


                                    $(document).ready(function() {

                                        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
                                            changeYear: true});
                                    });
                                    $('.hasDate').on('focus', function() {
                                        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
                                            changeYear: true});
                                    })

//</script>

<?php echo $html->scriptEnd(); ?>
