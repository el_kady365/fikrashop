<div id="flashMessage" class="flash-hide">

</div>

<div class="content">
    <div class="fekra-head"> 
        <div class="btn-group pull-right">
            <a href="<?php echo Router::url(array('action' => 'invest', $fekra['Fekra']['id'])) ?>" class="btn btn-success right"><i class="fa fa-money"></i> <?php __('Invest on this project') ?></a> 
            <a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'send_message', base64_encode($fekra['User']['id']), '?' => array('idea' => $fekra['Fekra']['id']))); ?>" class="btn btn-default right"><i class="fa fa-envelope"></i> <?php __('Contact fekra creator') ?></a>
        </div>     


        <div class="rating right" data-average="<?php echo Fekra::get_rating($fekra['Fekra']['id']); ?>" data-id="<?php echo $fekra['Fekra']['id'] ?>" id="fekraRating"></div>
        <h3 class="font-a sub-heading">
            <strong><?php echo $fekra['Fekra']['title'] ?></strong>
            <span class="shape user-place">
                <i class="place-ico"></i>
                <?php echo User::get_country($fekra['User']['country_code'], $lang) ?> <?php echo!empty($fekra['User']['city']) ? ',' . $fekra['User']['city'] : '' ?>  </span>
        </h3>
        <h6 class="font-a shape"><a href="<?php echo $html->url(array('controller' => 'users', 'action' => 'view', base64_encode($fekra['User']['id']))) ?>"><?php __('Created by') ?> : <strong class="highlight-b"><?php echo $fekra['User']['first_name'] . ' ' . $fekra['User']['last_name'] ?></strong></a></h6>
        <div class="clear"></div>
    </div>
    <!-- / fekra-head -->

    <div class="fekra-intro">
        <div class="featured-img">
            <?php if (!empty($fekra['Fekra']['image'])) { ?>
                <img src="<?php echo Router::url($fekra['Fekra']['image']['thumb2']) ?>" alt="" title="" />
            <?php } else { ?>
                <img src="<?php echo Router::url('/css/img/fekra-thumb.png') ?>" alt="" title="" />
            <?php } ?>


        </div>
        <div class="fekra-stat">
            <h4 class="font-a"><?php echo format_price(Fekra::get_funds($fekra['Fekra']['id'])); ?> <span class="shape"><?php __('Funded') ?></span></h4>
            <?php if (!empty($fekra['Fekra']['price'])) { ?>
                <h4 class="font-a highlight-b">$<?php echo $fekra['Fekra']['price'] ?> 
                    <span class="shape"><?php __('goal') ?></span>
                </h4>
            <?php } ?>
            <h4 class="font-a"><?php echo $fekra['Fekra']['end_date'] ?><span class="shape"><?php __('Completion Date') ?></span></h4>
            <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                <a class="addthis_button_facebook"></a>
                <a class="addthis_button_twitter"></a>
                <a class="addthis_button_linkedin"></a>
                <a class="addthis_button_google_plusone_share"></a>
                <a class="addthis_button_email"></a>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <!-- / fekra-intro -->

    <div class="fekra-tabs">
        <?php if (isset($messages)) { ?>
            <h3 class="right">
                <a href="<?php echo Router::url(array('action' => 'edit', $fekra['Fekra']['id'])) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> <?php __('Edit Idea'); ?></a>
            </h3>
            <div class="clear"></div>
        <?php } ?>
        <div class="tabs-header font-a">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#Brief" data-toggle="tab"><i class="fa fa-file-text"></i> <?php __('Idea Brief') ?> </a>
                </li>
                <?php
                if (!empty($user)) {
                    if (!empty($videos)) {
                        ?>
                        <li>
                            <a href="#Videos"  data-toggle="tab">
                                <i class="fa fa-video-camera"></i> <?php __('Video Tour') ?>
                            </a>
                        </li>
                    <?php } if (!empty($fekra['Fekra']['file']) || !empty($poll_answer)) { ?>
                        <li><a href="#Business"  data-toggle="tab"><i class="fa fa-calendar"></i>  <?php __('Business Plan') ?></a></li>
                    <?php } ?>
                    <li><a href="#Team"  data-toggle="tab"><i class="fa fa-users"></i> <?php __('Team') ?></a></li>
                    <li><a href="#Comments"  data-toggle="tab"><i class="fa fa-comments"></i> <?php __('Discussions') ?></a></li>
                    <li><a href="#BordRating"  data-toggle="tab"><i class="fa fa-bar-chart-o"></i> <?php __('Board Rating') ?></a></li>
                    <?php if (isset($messages)) { ?>
                        <li><a href="#Messages"  data-toggle="tab"><i class="fa fa-envelope-o"></i> <?php __('Messages') ?></a></li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="Brief">
                <?php echo $fekra['Fekra']['description'] ?> 
            </div>
            <?php
            if (!empty($user)) {
                if (!empty($videos)) {
                    ?>
                    <div class="tab-pane" id="Videos">
                        <?php foreach ($videos as $video) { ?>
                            <?php
                            if ($video['FekraVideo']['type'] == 2) {
                                echo $this->element('video_player', array('file' => Router::url($video['FekraVideo']['file_info']['full_path'])));
                            } else {
                                echo parse_video_url($video['FekraVideo']['video'], 'embed', 550, 310);
                            }
                            ?>
                            <br/>
                        <?php } ?>

                    </div>
                <?php } if (!empty($fekra['Fekra']['file']) || !empty($poll_answer)) { ?>
                    <div class="tab-pane" id="Business">
                        <?php
                        if (!empty($fekra['Fekra']['file'])) {
                            ?>
                            <a class="btn btn-default" href="<?php echo Router::url(array('action' => 'download_bussiness_plan', $fekra['Fekra']['id'])); ?>"><i class="fa fa-download"></i> <?php __('Download Bussiness Plan') ?></a>
                            <?php
                        }
                        if (!empty($poll_answer)) {
                            foreach ($poll_answer as $answer) {
                                ?>
                                <li>
                                    <h5><?php echo $answer['PollQuestion']['name']; ?></h5>
                                    <p><?php echo $answer['PollAnswer']['answer']; ?></p>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </div>
                <?php } ?>
                <div class="tab-pane" id="Team">
                    <?php
                    if (!empty($team)) {
                        ?>
                        <table cellpadding="" cellspacing="" class="table" >
                            <tr>
                                <th>
                                    <?php __('Name') ?>
                                </th>
                                <th>
                                    <?php __('Roll') ?>
                                </th>
                            </tr>
                            <?php foreach ($team as $tea) { ?>
                                <tr>
                                    <td>
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($tea['User']['id']))) ?>"><?php echo $tea['User']['first_name'] . ' ' . $tea['User']['last_name'] ?></a>
                                    </td>
                                    <td>
                                        <?php
                                        if (!empty($tea['FekraUser']['type'])) {
                                            echo $roles[$tea['FekraUser']['type']];
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <?php
                    }
                    ?>
                </div>
                <div class="tab-pane" id="Comments">
                    <?php echo $session->flash(); ?>
                    <?php if (!empty($comments)) { ?>
                        <div class="comments">
                            <?php
                            foreach ($comments as $comment) {
                                ?>
                                <div class = "media comment-item">

                                    <div class = "quick-actions right">
                                        <?php if (($user['id'] == $fekra['FekraUser']['user_id']) && $fekra['FekraUser']['type'] == 1 && !$comment['FekraComment']['approved']) { ?>
                                            <a title="<?php __('Approve') ?>" href = "<?php echo Router::url('/fekra_comments/approve/' . $comment['FekraComment']['id']) ?>" class = "skill"><span class = "fa fa-check fa-lg" aria-hidden = "true"></span></a>
                                            <?php
                                        }
                                        if (($user['id'] == $fekra['FekraUser']['user_id']) || ($comment['FekraComment']['user_id'] == $user['id'])) {
                                            ?>
                                            <a title="<?php __('Delete') ?>" href = "<?php echo Router::url('/fekra_comments/delete/' . $comment['FekraComment']['id']) ?>" class = "delete"><span class = "fa fa-trash-o fa-lg" aria-hidden = "true"></span></a>
                                        <?php } ?>
                                    </div>

                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($comment['User']['id']))) ?>" class="pull-left">
                                        <?php if (!empty($comment['User']['image']['path'])) { ?>
                                            <img width="120" height="120" src="<?php echo Router::url($comment['User']['image']['path']) ?>" class="media-object pull-right">
                                        <?php } else { ?>
                                            <img width="120" height="120" src="<?php echo Router::url('/css/img/anonymous.jpg') ?>" class="media-object pull-right">
                                        <?php } ?>
                                    </a>
                                    <div class="media-body">
                                        <?php if (!empty($comment['User'])) { ?>
                                            <h4 class="media-heading">
                                                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($comment['User']['id']))) ?>">
                                                    <?php echo $comment['User']['first_name'] . ' ' . $comment['User']['last_name'] ?>
                                                </a>
                                            </h4>

                                            <p><?php echo date('d F Y', strtotime($comment['FekraComment']['created'])) ?> </p>
                                        <?php } ?>
                                        <p><?php echo $comment['FekraComment']['content'] ?></p>    
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    <?php } ?>
                    <h3 id="reply" class="font-a form-section"><?php __('Add Comment') ?></h3>
                    <?php echo $form->create('FekraComment', array('class' => 'form default-forms', 'type' => 'file', 'url' => array('controller' => 'fekra_comments', 'action' => 'add'))); ?>
                    <?php
                    echo $form->hidden('fekra_id', array('value' => $fekra['Fekra']['id']));

                    echo $form->input('content', array('class' => 'span6', 'label' => __('Comment', true)));
                    ?>
                    <div class="input submit">
                        <button type="submit"><span class="btn btn-default"><?php __('Add comment') ?></span></button>
                    </div>
                    <?php echo $form->end(); ?>
                </div>
                <div class="tab-pane" id="BordRating">
                    <?php echo $session->flash('rate'); ?>
                    <?php if (!empty($boardratings)) { ?>
                        <div class="comments">
                            <?php
                            foreach ($boardratings as $rating) {
                                ?>
                                <div class = "media comment-item">
                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($comment['User']['id']))) ?>" class="pull-left">
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($comment['User']['id']))) ?>" class="pull-left">
                                            <?php if (!empty($rating['User']['image']['path'])) { ?>
                                                <img width="120" height="120" src="<?php echo Router::url($rating['User']['image']['path']) ?>" class="media-object pull-right">
                                            <?php } else { ?>
                                                <img width="120" height="120" src="<?php echo Router::url('/css/img/anonymous.jpg') ?>" class="media-object pull-right">
                                            <?php } ?>
                                        </a>
                                        <div class="media-body">
                                            <?php if (!empty($rating['User'])) { ?>
                                                <h4 class="media-heading">
                                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($rating['User']['id']))) ?>">
                                                        <?php echo $rating['User']['first_name'] . ' ' . $rating['User']['last_name'] ?>
                                                    </a>
                                                </h4>
                                                <p><div data-average="<?php echo $rating['BoardRating']['rating']; ?>" data-id="<?php echo $fekra['Fekra']['id'] ?>" id="BoardRating"></div></p>
                                                <p><?php echo date('d F Y', strtotime($rating['BoardRating']['created'])) ?>  </p>
                                            <?php } ?>
                                            <p><?php echo $rating['BoardRating']['description'] ?></p>    
                                        </div>
                                </div>
                            <?php } ?>

                        </div>
                    <?php } else {
                        ?>
                        <div id="flashMessage" class="info"><?php __('There are not any Board Ratings!'); ?></div>
                    <?php } if ($user['evaluation_board']) { ?>

                        <h3 id="reply" class="font-a form-section"><?php __('Rate This Idea') ?></h3>
                        <?php echo $form->create('BoardRating', array('class' => 'form default-forms', 'type' => 'file', 'url' => array('controller' => 'fekras', 'action' => 'add_board_rating'))); ?>
                        <?php
                        echo $form->hidden('fekra_id', array('value' => $fekra['Fekra']['id']));

                        echo $form->input('rating', array('class' => 'span6', 'empty' => __('Select Rating', true)));
                        echo $form->input('description', array('class' => 'span6', 'label' => __('Comment', true)));
                        ?>
                        <div class="input submit">
                            <button type="submit"><span class="btn btn-default"><?php __('Rate it') ?></span></button>
                        </div>
                        <?php echo $form->end(); ?>
                    <?php } ?>
                </div>
                <?php if (isset($messages)) { ?>
                    <div class="tab-pane" id="Messages">
                        <?php
                        if (!empty($messages)) {
                            foreach ($messages as $message) {
                                ?>
                                <div class="row message-row">
                                    <div class="col-md-1"><span aria-hidden="true" class="fa fa-envelope-o fa-2x"></span></div>
                                    <div class="col-md-2">
                                        <p><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', base64_encode($message['Receiver']['id']))) ?>" title="<?php echo $message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'] ?>"><?php echo $text->truncate($message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'], 12) ?></p>
                                        <p class="sent-date"><?php echo date('d/m/Y', strtotime($message['Message']['created'])) ?></p>
                                    </div>
                                    <div class="col-md-9">
                                        <p><a href="<?php echo Router::url(array('controller' => 'messages', 'action' => 'read', $message['Message']['id'])) ?>"><strong><?php echo $message['Message']['subject'] ?></strong></a></p>
                                        <p class="shape"><?php echo $text->truncate($message['Message']['body']) ?></p>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="Notemessage" id="flashMessage"><?php __('There are not any messages right now') ?></div>
                        <?php } ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <!-- / fekra-tabs -->

    <div class="clear"></div>
    <!-- AddThis Button BEGIN -->

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
    <!-- AddThis Button END -->
    <?php
    echo $html->css(array('jquery-impromptu.min', 'jRating.jquery'));
    echo $html->script(array('flexslider', 'jquery-impromptu.min', 'jRating.jquery'));
    ?>
    <script>
        $(window).load(function() {
            $('.flex').flexslider({
                animation: "slide",
                controlNav: false,
                directionNav: false,
            });
        });

        $(function() {
            $('.flash-hide').hide();
            if (self.location.hash != '') {
                href = self.location.hash;
                $('a[href="' + href + '"]').click();
            }
        });

        $("#fekraRating").jRating({
            length: 5,
            bigStarsPath: '<?php echo Router::url('/css/img/rating/stars.png') ?>',
            step: 1,
            rateMax: 5,
            showRateInfo: false,
            phpPath: '<?php echo Router::url('/fekras/add_rating') ?>',
            onSuccess: function(data) {
                $('.flash-hide').fadeIn();
                $('#flashMessage').removeClass().addClass('flash-hide success');
                $('#flashMessage').html(data.message);
                setTimeout(function() {
                    $('.flash-hide').fadeOut();
                }, 3000)
            },
            onError: function(data) {
                $('.flash-hide').fadeIn();
                $('#flashMessage').removeClass().addClass('flash-hide info');
                $('#flashMessage').html(data.message);
                setTimeout(function() {
                    $('.flash-hide').fadeOut();
                }, 3000);
            }
        });
        $("#BoardRating").jRating({
            length: 5,
            bigStarsPath: '<?php echo Router::url('/css/img/rating/stars.png') ?>',
            step: 1,
            rateMax: 5,
            sendRequest: false,
            isDisabled: true
        });

        $('.delete').click(function() {
            hrf = $(this).get(0).href;
            $this = $(this);
            var txt = '<?php __('Are you sure you want to remove this comment?') ?>';

            $.prompt(txt, {
                buttons: {'<?php __('Delete') ?>': true, '<?php __('Cancel') ?>': false},
                close: function(e, v, m, f) {

                    if (v) {

                        //Here is where you would do an ajax post to remove the user
                        //also you might want to print out true/false from your .php
                        //file and verify it has been removed before removing from the 
                        //html.  if false dont remove, $promt() the error.
                        $.ajax({
                            url: hrf,
                            type: 'POST',
                            dataType: 'json',
                            success: function(data) {
                                if (data.status == '1') {
                                    $this.closest('.comment-item').hide('slow', function() {
                                        $this.closest('.comment-item').remove();
                                        $.prompt(data.message);
                                    });
                                } else {
                                    $.prompt(data.message);
                                }
                            }
                        })

                    }
                    else {
                    }

                }
            });
            return false;
        });
    </script>
