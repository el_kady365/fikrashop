<?php echo $this->element('user/user_leftmenu'); ?>

<div class="content-box">
    <div class="messages">
        <h3 class="font-a sub-heading"><strong><?php echo $subtitle ?></strong></h3>
        <table class="table">
            <tr>
                <th><?php echo __('Title', true) ?></th>
                <th><?php echo __('Goal', true) ?></th>
                <th><?php echo __('Descritpion', true) ?></th>
                <th><?php echo __('Your Payments', true) ?></th>
                <th><?php echo __('Actions', true) ?></th>
            </tr>

            <?php
            if (!empty($phases)) {
                foreach ($phases as $phase) {
                    ?>
                    <tr>
                        <td><?php echo $phase['FekraPhase']['title'] ?></td>
                        <td><?php echo format_price($phase['FekraPhase']['price']) ?></td>
                        <td width="150"><?php echo $text->truncate($phase['FekraPhase']['description'], 50) ?></td>
                        <td><?php echo format_price($phase['FekraPhase']['payments']) ?></td>
                        <td>
                            <a href="<?php echo Router::url(array('action' => 'view_phase', $phase['FekraPhase']['id'])); ?>" class="btn btn-primary btn-sm"><span aria-hidden="true" class="fa fa-money "></span> <?php __('View') ?></a>  
                            <a href="<?php echo Router::url(array('action' => 'checkout', $phase['FekraPhase']['id'])); ?>" class="btn btn-success btn-sm"><span aria-hidden="true" class="fa fa-money "></span> <?php __('Invest') ?></a>  
                        </td> 
                    </tr>

                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">
                        <div class="Notemessage" id="flashMessage"><?php __('There are not any phases right now') ?></div>
                    </td>
                </tr>
            <?php } ?>
        </table>      
    </div>      
</div>      

