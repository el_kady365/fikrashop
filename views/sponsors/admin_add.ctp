<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php
                if (!empty($this->data['Sponsor']['id'])) {

                    __('Edit Sponsor');
                } else {

                    __('Add Sponsor');
                }
                ?>
            </h1>
            <div class="clear"></div>
        </div>



<?php echo $form->create('Sponsor', array('type' => 'file')); ?>
        <div class="block-fluid">
        <?php
        echo $form->input('id');
        echo $form->input('title');
        echo $form->input('image', array('type' => 'file',
            'between' => $this->element('image_element', array('info' => !empty($this->data['Sponsor']['image']) ? $this->data['Sponsor']['image'] : $info, 'field' => 'image'))));
        echo $form->input('url');
        echo $form->input('active');
        echo $form->input('display_order');
        echo $form->submit('Submit', array('class' => 'submit-green'));
        ?>
        </div>
            <?php echo $form->end(); ?>
    </div>
</div>

