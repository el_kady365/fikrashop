<h2>
    <span>
        <?php
        if (!empty($this->data['Country']['id'])) {

            __('Edit Country');
        } else {

            __('Add Country');
        }
        ?>    </span>
</h2>
 

<?php echo $form->create('Country', array('type' => 'file')); ?>
<div class="module-body">
<?php
echo $form->input('id');
echo $form->input('code');

echo $form->input('ar_name');
echo $form->input('en_name');
echo $form->input('fa_name');
echo $form->input('category_id');
echo $form->submit('Submit', array('class' => 'submit-green'));
?>

    <?php echo $form->end(); ?>

    <div class="actions">
        <h3><?php __('Actions'); ?></h3>
        <ul>

            <li><?php echo $this->Html->link(__('List Countries', true), array('action' => 'index')); ?></li>
        </ul>
    </div>
</div>