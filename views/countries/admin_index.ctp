<?
/* @var $html Html */
?>

<h2><span><?php __('Countries'); ?></span></h2>
<div class="module-table-body">
    <form action="<?php echo Router::url(array("action" => "do_operation")) ?>" id="forn" method="post">



        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><input type="checkbox" id="check_all" /></th>
                <th><?php echo $paginator->sort('id'); ?></th>
                <th><?php echo $paginator->sort('ar_name'); ?></th>
                <th class="actions"><?php __('Actions'); ?></th>
            </tr>
            <?php
            $i = 0;
            foreach ($countries as $country):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }
                ?>
                <tr<?php echo $class; ?>>
                    <td>
                        <input  type="checkbox" name="chk[]" value="<?php echo $country['Country']['id']; ?>" />
                    </td>
                    <td>
                        <?php echo $country['Country']['id']; ?>
                    </td>
                    <td>
                        <?php echo $country['Country']['ar_name']; ?>
                    </td>
                    <td class="actions">
                        <?php echo $html->link(__('Edit', true), array('action' => 'edit', $country['Country']['id']), array('class' => 'Edit')); ?>
                        <?php echo $html->link(__('Delete', true), array('action' => 'delete', $country['Country']['id']), array('class' => 'Delete'), sprintf(__('Are you sure you want to delete # %s?', true), $country['Country']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>


        <div class="pagination">
            <?php
            if (!empty($this->params['url']['cat_id'])) {
                $paginator->options(array('url' => array('?' => array('cat_id' => $this->params['url']['cat_id']))));
            }

            if ($paginator->numbers()) {

                if ($paginator->hasNext()) {
                    ?> 

                    <div class="button">
                        <!--<span>--> 

                        <?php echo $paginator->next(__('Next', true), array(), null, array('class' => 'disabled')); ?>
                                                    <!--<img height="9" width="12" alt="Previous" src="<?php // echo Router::url()      ?>">-->
                        <!--</span>-->
                    </div>
                <?php } ?>            
                <div class="numbers"> 
                    <?php echo $paginator->numbers(array('separator' => ' | ')); ?>
                </div>

                <?php if ($paginator->hasPrev()) { ?>           
                    <div class="button" >
                        <!--<span>-->
                            <!--<img height="9" width="12" alt="Next"  src="<?php echo Router::url("/img/admin/arrow-000-small.gif") ?>" />-->
                        <?php echo $paginator->prev(__('Previous', true), array(), null, array('class' => 'disabled')); ?>
                        <!--</span>-->
                    </div>
                <?php } ?>            		<?php } ?>        </div>

        <div class="table-apply">
            <div>
                <span><?php echo __("Choose Operation") ?></span> 
                <select class="input-medium" id="acts" name="operation" style="width:auto;">
                    <option value="">
                        <?php echo __("Choose Operation") ?></option>
                    <option value="delete"><?php echo __("Delete") ?></option>
                </select>
            </div>

        </div>
    </form>
    <script type="text/javascript">
        $("#check_all").live('click',function(){
            if($(this).prop('checked')==true)
            {
                $('input[name="chk[]"]').prop('checked',true);
            }else{
                $('input[name="chk[]"]').prop('checked',false);
            }
	
        });
        $(document).ready(function(){
            $("#acts").change(function(){
                action=$(this).val();
                if(action!="")
                {
                    if($('input[name="chk[]"]:checked').length==0)
                    {
                        alert("<?php echo __('You must choose on element at least'); ?>");
                        $(this).val('');
                    }else{
			
                        del=confirm("<?php echo __('Are you sure you want to perform this process?'); ?>");
                        if(del)
                        {
                            $('#forn').submit();
                            $(this).val('');
                        }else{
                            $(this).val('');
                        }
                    }
	
                }
            });
        });
    </script>
</div>


