<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php

            	if(!empty($this->data['UserSkill']['id'])){

            		__('Edit User Skill');

                	}else{

                		__('Add User Skill');

                	}
            ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('UserSkill',array('type'=>'file'));?>
        <div class="block-fluid">
            	<?php
		echo $form->input('id');
		echo $form->input('name');
		echo $form->input('level');
		echo $form->input('years_of_exp');
		echo $form->input('user_id');
echo 	$form->submit('Submit', array('class' => 'submit-green'));
	?>
        </div>
        <?php echo $form->end();?>
    </div>
</div>

