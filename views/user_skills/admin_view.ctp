
<div class="module">
    <h2><span><?php  __('User Skill');?></span></h2>
    <div class="module-body">
        <dl><?php $i = 0; $class = ' class="altrow"';?>
            		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userSkill['UserSkill']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userSkill['UserSkill']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Level'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userSkill['UserSkill']['level']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Years Of Exp'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $userSkill['UserSkill']['years_of_exp']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($userSkill['User']['id'], array('controller' => 'users', 'action' => 'view', $userSkill['User']['id'])); ?>
			&nbsp;
		</dd>
        </dl>


        <div class="actions">
            <h3><?php __('Actions'); ?></h3>
            <ul>
                		<li><?php echo $this->Html->link(__('Edit User Skill', true), array('action' => 'edit', $userSkill['UserSkill']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete User Skill', true), array('action' => 'delete', $userSkill['UserSkill']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $userSkill['UserSkill']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List User Skills', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Skill', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
            </ul>
        </div>
            </div>
</div>