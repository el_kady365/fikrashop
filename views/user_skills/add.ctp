<h3 class="font-a">
    <strong> <?php
        if (!empty($this->data['UserSkill']['id'])) {
            __('Edit Skills');
        } else {
            __('Add New Skill');
        }
        ?>
    </strong>
</h3>
<div class="message"></div>
<?php echo $form->create('UserSkill', array('class' => 'fonta', 'id' => 'skillForm')) ?>
<?php echo $form->input('id'); ?>
<div class="input text">     
    <?php echo $form->input('name', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'SkillName', 'placeholder' => __('Skill name', true))); ?>
</div>
<div class="input select">
    <?php echo $form->input('level', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'SkillLevel', 'placeholder' => __('Level', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('years_of_exp', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'YearsOfExp', 'placeholder' => __('Years Of Experience', true))); ?>
</div>

<div class="input submit">
    <button type="submit"><span class="btn btn-default"><?php __('Save') ?></span></button>
</div>                   
<?php echo $form->end(); ?>


<?php
echo $html->script(array('jquery.validate'));
?>
<script type="text/javascript">
    $(function() {
        var v = jQuery("#skillForm").validate({
            messages: {
                'data[UserSkill][name]': {
                    required: '<?php __('Required') ?>',
                },
                'data[UserSkill][level]': {
                    required: '<?php __('Required') ?>'
                },
                'data[UserSkill][years_of_exp]': {
                    required: '<?php __('Required') ?>'
                }
            },
            errorClass: "error-message",
            errorElement: "div",
            errorPlacement: function(error, element) {
                if (element.get(0).type != "checkbox") {
                    error.insertAfter(element);
                } else {
                    element.parent().append(error);
                }
            },
            submitHandler: function(form) {
                $('div.message').html('').hide();
                $.ajax({
                    url: $(form).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: $(form).serialize(),
                    success: function(data) {

                        if (data.status == '1') {
                            $('div.message').html('<div id="flashMessage" class="success">' + data.message + '</div>').fadeIn();
                            setTimeout(function() {
                                location.reload()
                            }, '300');
                        } else {
                            $('div#flashMessage').addClass('fail');
                            $('div.message').html('<div id="flashMessage" class="fail">' + data.message + '</div>').fadeIn();
                        }
                    }
                })
            }
        });

    });
</script>






