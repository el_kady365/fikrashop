<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <?php echo $html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>

        <?php
        echo $html->css(array('admin/stylesheets'));
        echo $javascript->link(array('admin/jquery-1.7.2.min.js', 'admin/jquery-ui-1.8.24.custom.min', 'admin/cakebootstrap'));
        ?>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/jquery/jquery.mousewheel.min.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/cookie/jquery.cookies.2.2.0.min.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/bootstrap.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/uniform/uniform.js') ?>'></script>


        <script type='text/javascript' src='<?php echo Router::url('/js/admin/maskedinput/jquery.maskedinput-1.3.min.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/validation/languages/jquery.validationEngine-en.js') ?>' charset='utf-8'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/validation/jquery.validationEngine.js') ?>' charset='utf-8'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/mcustomscrollbar/jquery.mCustomScrollbar.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/animatedprogressbar/animated_progressbar.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/qtip/jquery.qtip-1.0.0-rc3.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/select2/select2.min.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/dataTables/jquery.dataTables.min.js') ?>'></script>    
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/fancybox/jquery.fancybox.pack.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/cookies.js') ?>'></script>
        <script type='text/javascript' src='<?php echo Router::url('/js/admin/actions.js') ?>'></script>

        <script type='text/javascript' src='<?php echo Router::url('/js/admin/plugins.js') ?>'></script>
        <?php // echo $scripts_for_layout; ?>
    </head>
    <body>
        <div class="loginBox">        
            <div class="loginHead">
                <h1> <?php __("Login") ?></h1>
            </div>
            <?php echo $form->create('Admin', array('action' => 'login')); ?>
            <?php echo $session->flash(); ?>
            <?php echo $form->input('username'); ?>
            <?php echo $form->input('password'); ?>
            <!--        <div class="control-group" style="margin-bottom: 5px;">                
                        <label class="checkbox"><input type="checkbox"> Remember me</label>                                                
                    </div>-->
            <?php echo $form->submit('Submit') ?>
            <?php echo $form->end(); ?>        

        </div>  

        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>