<h3 class="font-a">
    <strong> <?php
        if (!empty($this->data['UserEducation']['id'])) {
            __('Edit Education');
        } else {
            __('Add New Education');
        }
        ?>
    </strong></h3>
<div class="message"></div>

<?php echo $form->create('UserEducation', array('class' => 'fonta', 'id' => 'skillForm')) ?>
<?php echo $form->input('id'); ?>
<div class="input text">     
    <?php echo $form->input('organization', array('div' => false, 'label' => false, 'class' => 'required', 'id' => 'SkillName', 'placeholder' => __('School name', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('certification', array('div' => false, 'label' => false, 'placeholder' => __('Degree', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('major', array('type' => 'text', 'div' => false, 'label' => false, 'placeholder' => __('Field of Study', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('grade', array('type' => 'text', 'div' => false, 'label' => false, 'placeholder' => __('Grade', true))); ?>
</div>
<div class="input text">
    <?php echo $form->input('graduation_date', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'required hasDate', 'placeholder' => __('Graduation date', true))); ?>
</div>



<div class="input submit">
    <button type="submit"><span class="btn btn-default"><?php __('Save') ?></span></button>
</div>                   
<?php echo $form->end(); ?>


<?php
echo $html->script(array('jquery.validate'));
?>
<?php
echo $html->css('jquery-ui-1.9.2.custom.min');
echo $javascript->link('jquery-ui-1.9.2.custom.min');
?>
<script type="text/javascript">
    $(document).ready(function() {        
        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
            changeYear: true});
        
    });

    $('.hasDate').on('focus', function() {
        $('.hasDate').datepicker({"dateFormat": 'yy-mm-dd', changeMonth: true,
            changeYear: true});
    })

</script>
<script type="text/javascript">
    $(function() {
        
        var v = jQuery("#skillForm").validate({
           
            messages: {
                'data[UserEducation][organization]': {
                    required: '<?php __('Required') ?>',
                },
                'data[UserEducation][graduation_date]': {
                    required: '<?php __('Required') ?>'
                }
            },
            errorClass: "error-message",
            errorElement: "div",
            errorPlacement: function(error, element) {
                if (element.get(0).type != "checkbox") {
                    error.insertAfter(element);
                } else {
                    element.parent().append(error);
                }
            },
            submitHandler: function(form) {
                $('div.message').html('').hide();
                $.ajax({
                    url: $(form).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: $(form).serialize(),
                    success: function(data) {

                        if (data.status == '1') {
                            $('div.message').html('<div id="flashMessage" class="success">' + data.message + '</div>').fadeIn();
                            setTimeout(function() {
                                location.reload()
                            }, '300');
                        } else {
                            $('div#flashMessage').addClass('fail');
                            $('div.message').html('<div id="flashMessage" class="fail">' + data.message + '</div>').fadeIn();
                        }
                    }
                })
            }
        });
    });
</script>






