<div class="row-fluid">

    <div class="span12">
        <div class="head">
            <h1>
                <?php

            	if(!empty($this->data['UserEducation']['id'])){

            		__('Edit User Education');

                	}else{

                		__('Add User Education');

                	}
            ?>
            </h1>
            <div class="clear"></div>
        </div>



        <?php echo $form->create('UserEducation',array('type'=>'file'));?>
        <div class="block-fluid">
            	<?php
		echo $form->input('id');
		echo $form->input('organization');
		echo $form->input('certification');
		echo $form->input('major');
		echo $form->input('graduation_date');
		echo $form->input('grade');
		echo $form->input('country');
		echo $form->input('city');
		echo $form->input('user_id');
echo 	$form->submit('Submit', array('class' => 'submit-green'));
	?>
        </div>
        <?php echo $form->end();?>
    </div>
</div>

