<?php
class FekraJobAttachmentsController extends AppController {

var $name = 'FekraJobAttachments';
/**
* @var FekraJobAttachment*/
var $FekraJobAttachment;
    
function admin_index() {
$this->FekraJobAttachment->recursive = 0;
$this->set('fekraJobAttachments', $this->FekraJobAttachment->find('all'));
}

function admin_view($id = null) {
if (!$id) {
    $this->setFlash(__('Invalid fekra job attachment', true));
    $this->redirect(array('action' => 'index'));
}
$this->set('fekraJobAttachment', $this->FekraJobAttachment->read(null, $id));
}

function admin_add() {
if (!empty($this->data)) {
$this->FekraJobAttachment->create();
if ($this->FekraJobAttachment->save($this->data)) {
    $this->setFlash(__('The fekra job attachment has been saved', true),'alert alert-success');
    $this->redirect(array('action' => 'index'));
} else {
    $this->setFlash(__('The fekra job attachment could not be saved. Please, try again.', true),'alert alert-error');
}
}
		$fekraJobs = $this->FekraJobAttachment->FekraJob->find('list');
		$this->set(compact('fekraJobs'));
}

function admin_edit($id = null) {
if (!$id && empty($this->data)) {
    $this->setFlash(__('Invalid fekra job attachment', true),'alert alert-error');
    $this->redirect(array('action' => 'index'));
}
if (!empty($this->data)) {
if ($this->FekraJobAttachment->save($this->data)) {
    $this->setFlash(__('The fekra job attachment has been saved', true),'alert alert-success');
    $this->redirect(array('action' => 'index'));
} else {
    $this->setFlash(__('The fekra job attachment could not be saved. Please, try again.', true),'alert alert-error');
}
}
if (empty($this->data)) {
$this->data = $this->FekraJobAttachment->read(null, $id);
}
		$fekraJobs = $this->FekraJobAttachment->FekraJob->find('list');
		$this->set(compact('fekraJobs'));
$this->render('admin_add');
}

function admin_delete($id = null) {
if (!$id) {
    $this->setFlash(__('Invalid id for fekra job attachment', true),'alert alert-error');
    $this->redirect(array('action'=>'index'));
}
if ($this->FekraJobAttachment->delete($id)) {
    $this->setFlash(__('Fekra job attachment deleted', true),'alert alert-success');
    $this->redirect(array('action'=>'index'));
}
    $this->setFlash(__('Fekra job attachment was not deleted', true),'alert alert-error');
$this->redirect(array('action' => 'index'));
}

function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->FekraJobAttachment->deleteAll(array('FekraJobAttachment.id' => $ids))) {
                $this->setFlash(__('Fekra job attachment deleted successfully',true), 'alert alert-success');
            } else {
                $this->setFlash(__('Fekra job attachment can not be deleted',true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }
}
