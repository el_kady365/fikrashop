<?php

App::import('Vendor', 'Paypal', array('file' => 'paypalfunctions.php'));

class ComplainsController extends AppController {

    var $name = 'Complains';

    /**
     * @var Complain */
    var $Complain;

    function admin_index() {
        $this->Complain->recursive = 2;
        $this->set('complains', $this->Complain->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid complain', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->Complain->recursive = 2;
        $this->set('complain', $this->Complain->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Complain->create();
            if ($this->Complain->save($this->data)) {
                $this->setFlash(__('The complain has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The complain could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $transactions = $this->Complain->Transaction->find('list');
        $this->set(compact('transactions'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid complain', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Complain->save($this->data)) {
                $this->setFlash(__('The complain has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The complain could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Complain->read(null, $id);
        }
        $transactions = $this->Complain->Transaction->find('list');
        $this->set(compact('transactions'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for complain', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Complain->delete($id)) {
            $this->setFlash(__('Complain deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Complain was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Complain->deleteAll(array('Complain.id' => $ids))) {
                $this->setFlash(__('Complain deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Complain can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function admin_complete($id) {
        $complain = $this->Complain->read(null, $id);

        $this->Complain->Transaction->id = $complain['Complain']['transaction_id'];
        if ($this->Complain->Transaction->saveField('status', 2)) {

            $this->Complain->id = $id;
            $this->Complain->saveField('status', 1);
            $this->setFlash(__('this transaction has been completed successfully', true), 'success alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
    }

    function admin_refund() {
        $complain = $this->Complain->read(null, $id);
        $this->Complain->id = $id;
        if ($this->Complain->saveField('status', 2)) {
            $resArray = Refund($complain['Transaction']['transaction_id']);
            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $this->Complain->Transaction->id = $complain['Complain']['transaction_id'];
                if ($this->Complain->Transaction->save(array('Transaction' => array('status' => 3, 'transaction_id' => $resArray['REFUNDTRANSACTIONID'])))) {
                    $this->setFlash(__('This transaction has been refunded successfully', true), 'success alert alert-success');
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
    }

}
