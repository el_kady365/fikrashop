<?php

class CategoriesController extends AppController {

    var $name = 'Categories';

    /**
     * @var Category */
    var $Category;

    function admin_index() {
        $this->Category->recursive = 0;
        $this->set('categories', $this->Category->find('all'));
//        $this->get_languages();
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid category', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('category', $this->Category->read(null, $id));
    }

    function admin_add() {
        $this->loadModel('Poll');
        if (!empty($this->data)) {
            $this->Category->create();
            if ($this->Category->save($this->data)) {
                $this->setFlash(__('The category has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The category could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->set('polls', $this->Poll->find('list'));
        $this->set('info', $this->Category->getImageSettings());
//        $this->get_languages();
    }

    function admin_edit($id = null) {
        $this->loadModel('Poll');
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid category', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Category->save($this->data)) {
                $this->setFlash(__('The category has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The category could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Category->read(null, $id);
        }
//        $this->get_languages();
        $this->set('polls', $this->Poll->find('list'));
        $this->set('info', $this->Category->getImageSettings());
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for category', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Category->delete($id)) {
            $this->setFlash(__('Category deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Category was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Category->deleteAll(array('Category.id' => $ids))) {
                $this->setFlash(__('Category deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Category can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
