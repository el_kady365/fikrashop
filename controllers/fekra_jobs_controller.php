<?php

App::import('Vendor', 'Paypal', array('file' => 'paypalfunctions.php'));
App::import('Model', 'Skill');

class FekraJobsController extends AppController {

    var $name = 'FekraJobs';

    /**
     * @var FekraJob */
    var $FekraJob;
    var $helpers = array('Time');
    var $components = array('Email');

    function add($fekra_id = false) {
        $user = $this->authenticate();
        if (!empty($this->data)) {
            $this->FekraJob->create();
            $this->data['FekraJob']['status'] = 0;
            if ($this->FekraJob->save($this->data)) {
                $this->FekraJob->addJobAttachments($this->data);
                $this->set('user_data', $user);
                $this->_send_message($user['email'], $this->config['site_name'] . "<{$this->config['admin_send_mail_from']}>", __("Your job application is added and ready for bidding", true), 'add_job');
                $this->setFlash('Job has been added successfully', 'success alert alert-success');
                $this->redirect(array('action' => 'user_jobs'));
            } else {
                $this->setFlash('The Job could not be saved. Please, try again.');
            }
        }
        if ($fekra_id) {
            $this->data['FekraJob']['fekra_id'] = $fekra_id;
        }
        $this->pageTitle = __('Add Job', true);
        $this->set('h1', __('Jobs', true));
        $this->loadModel('FekraUser');
        $this->loadModel('Skill');
        $fekras = $this->FekraUser->find('list', array('conditions' => array('FekraUser.user_id' => $user['id']), 'fields' => array('FekraUser.fekra_id')));
        $this->set('fekras', $this->FekraJob->Fekra->find('list', array('conditions' => array('Fekra.id' => array_values($fekras), 'Fekra.published' => 1))));
        $this->set('skills', $this->Skill->find('list', array('conditions' => array('Skill.active' => 1))));
        $this->set('attachmentInfo', $this->FekraJob->FekraJobAttachment->getFileSettings());
    }

    function edit($id = false) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid Job', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        $user = $this->authenticate();
        if (!empty($this->data)) {
            $this->FekraJob->create();
            if ($this->FekraJob->save($this->data)) {
                $this->FekraJob->addJobAttachments($this->data);
                $this->setFlash('Job has been added successfully', 'success alert alert-success');
                $this->redirect(array('action' => 'user_jobs'));
            } else {
                $this->setFlash('The Job could not be saved. Please, try again.');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->FekraJob->read(null, $id);
        }
        $this->loadModel('Skill');
        $this->set('skills', $this->Skill->find('list', array('conditions' => array('Skill.active' => 1))));
        $this->pageTitle = __('Edit Job', true);
        $this->set('h1', __('Jobs', true));
        $this->set('attachmentInfo', $this->FekraJob->FekraJobAttachment->getFileSettings());
        $this->render('add');
    }

    function index() {
        $this->pageTitle = $sub_title = __('Jobs', true);
        $h1 = __('Jobs', true);
        $this->loadModel('Category');
        $categories = $this->Category->find('all', array('conditions' => array('Category.active' => 1)));
        $listCategories = array();

        if (!empty($categories)) {
            foreach ($categories as &$category) {
                $listCategories[$category['Category']['id']] = $category['Category'][$this->lang . '_title'];
                $fekras = $this->FekraJob->Fekra->find('list', array('conditions' => array('Fekra.category_id' => $category['Category']['id'], 'Fekra.published' => 1)));
                $category['Category']['jobs'] = $this->FekraJob->find('count', array('conditions' => array('FekraJob.fekra_id' => array_keys($fekras))));
            }
        }
        $priceRanges = array();
        for ($i = 500; $i <= 10000; $i+=500) {
            $priceRanges[$i] = $i;
        }
        $this->pageTitle = $h1 = __('Jobs', true);

        $this->set(compact('categories', 'h1', 'listCategories', 'priceRanges'));
        $this->loadModel('Skill');
        $this->set('skills', $this->Skill->find('list', array('conditions' => array('Skill.active' => 1))));
        $jobs = $this->paginate('FekraJob', array('FekraJob.published' => 1));
        $this->set(compact('jobs', 'h1', 'sub_title'));
    }

    function jobs($category_id = false) {
        $conditions = array();
        $conditions['FekraJob.published'] = 1;
        $this->loadModel('Category');
        $h1 = __('Jobs', true);
        if ($category_id) {
            $fekras = $this->FekraJob->Fekra->find('list', array('conditions' => array('Fekra.category_id' => $category_id, 'Fekra.published' => 1)));
            $conditions['FekraJob.fekra_id'] = array_keys($fekras);
            $category = $this->Category->read(null, $category_id);
            $this->pageTitle = $sub_title = $category['Category'][$this->lang . '_title'];
        } else {
            $this->pageTitle = $sub_title = __('Search results', true);
        }
        //$this->get_countries();

        $params = $this->params['url'];
//        debug($params);
        $filters = array();
        if (!empty($params['category_id'])) {
            $fekras = $this->FekraJob->Fekra->find('list', array('conditions' => array('Fekra.category_id' => $params['category_id'], 'Fekra.published' => 1)));
            $conditions['FekraJob.fekra_id'] = array_keys($fekras);
            $this->Category->recursive = -1;
            $fcategory = $this->Category->read(null, $params['category_id']);
            $filters['category_id'] = array('val' => $params['category_id'], 'text' => $fcategory['Category'][$this->lang . '_title']);
        }
        if (!empty($params['keyword'])) {
            $conditions[] = "MATCH (FekraJob.title,FekraJob.description) AGAINST ('{$params['keyword']}')";
            App::import('Helper', 'Text');
            $text = new TextHelper();
            $filters['keyword'] = array('val' => $params['keyword'], 'text' => $text->truncate($params['keyword'], 15));
        }

        if (!empty($params['price'])) {
//                $conditions["Fekra.price >= "] = $price[0];
            $conditions["FekraJob.price <= "] = $params['price'];
            $filters['price'] = array('val' => $params['price'], 'text' => __('Up to', true) . " $" . $params['price']);
        }
        $this->loadModel('Skill');

        if (!empty($params['skills'])) {
            $skills = $this->Skill->find('list', array('conditions' => array('Skill.active' => 1, 'Skill.id' => $params['skills'])));
            $conditions[] = "FekraJob.skills IN (" . implode(',', $params['skills']) . ")";
            $filters['skills'] = array('val' => $params['skills'], 'text' => implode(', ', array_values($skills)));
        }


        if (!empty($filters)) {
            $this->set('filters', $filters);
        }
        $priceRanges = array();
        for ($i = 500; $i <= 10000; $i+=500) {
            $priceRanges[$i] = $i;
        }


        $this->paginate = array('limit' => 10, 'order' => 'FekraJob.created desc');
        $jobs = $this->paginate('FekraJob', $conditions);

        $this->set('skills', $this->Skill->find('list', array('conditions' => array('Skill.active' => 1))));

        $listCategories = $this->Category->find('list', array('conditions' => array('Category.active' => 1), 'fields' => array('id', $this->lang . '_title')));
        $this->set(compact('h1', 'category', 'jobs_count', 'jobs', 'listCategories', 'priceRanges', 'sub_title'));
    }

    function user_jobs($id = false) {
        $user = $this->authenticate();
        $this->loadModel('FekraUser');
        $this->FekraUser->recursive = 1;
        $this->paginate = array('limit' => 10);

        if ($id) {
            $user_id = $id;
            $this->loadModel('User');
            $this->User->recursive = 0;
            $found_user = $this->User->find('first', array('conditions' => array('User.id' => $id)));
            $this->pageTitle = $sub_title = __('Jobs', true) . ' ' . __('by', true) . ' ' . $found_user['User']['first_name'];
            $h1 = __('Jobs', true);
            $fekras = $this->FekraUser->find('list', array('conditions' => array('FekraUser.user_id' => $found_user['User']['id']), 'fields' => array('FekraUser.fekra_id')));

            $jobs = $this->paginate('FekraJob', array('FekraJob.fekra_id' => array_values($fekras)));
            $this->set(compact('jobs', 'h1', 'sub_title'));
        } else {
            $user_id = $user['id'];
            $this->set('edit', 1);
            $this->pageTitle = $sub_title = __('My Jobs', true);
            $h1 = __('Jobs', true);
            $fekras = $this->FekraUser->find('list', array('conditions' => array('FekraUser.type' => 1, 'FekraUser.user_id' => $user['id']), 'fields' => array('FekraUser.fekra_id')));
            $this->paginate = array('limit' => 10, 'order' => 'FekraJob.created desc');
            $jobs = $this->paginate('FekraJob', array('FekraJob.fekra_id' => array_values($fekras)));
            $this->set(compact('jobs', 'h1', 'sub_title'));
            $this->render('my_jobs');
        }
    }

    function job_apps() {
        $user = $this->authenticate();
        $this->loadModel('FekraJobApplication');
        $this->paginate['order'] = 'FekraJobApplication.created desc';
        $job_apps = $this->paginate('FekraJobApplication', array('FekraJobApplication.user_id' => $user['id']));
        $this->set('job_apps', $job_apps);
        $h1 = __('Jobs', true);
        $sub_title = __('Job Applications', true);
        $this->set(compact('h1', 'sub_title'));
    }

    function view($id = false) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid Job', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        $user = $this->is_user();
        $job = $this->FekraJob->find('first', array('conditions' => array('FekraJob.id' => $id)));
        $this->set('og_description', $job['FekraJob']['description']);
        $this->loadModel('FekraUser');
        $cuser = $this->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $job['FekraJob']['fekra_id'], 'FekraUser.type' => 1)));
        $this->pageTitle = $job['FekraJob']['title'];

        $this->loadModel('FekraJobApplication');
        $job_app = $this->FekraJobApplication->find('first', array('conditions' => array('FekraJobApplication.fekra_job_id' => $id, 'FekraJobApplication.user_id' => $user['id'])));
        $job_apps = $this->FekraJobApplication->find('all', array('conditions' => array('FekraJobApplication.fekra_job_id' => $id)));

        $this->loadModel('UserRating');
        $user_rating = $this->UserRating->find('first', array('conditions' => array('UserRating.fekra_job_id' => $id, 'UserRating.creator_id' => $user['id'])));

        $h1 = __('Jobs', true);
        $this->set(compact('job_app', 'h1', 'job', 'cuser', 'job_apps', 'user_rating'));
    }

    function apply($job_id = false) {
        if (!$job_id && empty($this->data)) {
            $this->setFlash(__('Invalid Job', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        $user = $this->authenticate();
        $this->loadModel('FekraUser');
        $this->loadModel('FekraJobApplication');
        $job = $this->FekraJob->find('first', array('conditions' => array('FekraJob.id' => $job_id)));
        $job_app = $this->FekraJobApplication->find('first', array('conditions' => array('FekraJobApplication.fekra_job_id' => $job_id, 'FekraJobApplication.user_id' => $user['id'])));
        $cuser = $this->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $job['FekraJob']['fekra_id'], 'FekraUser.type' => 1)));
        if (!empty($job_app)) {
            $this->setFlash(__('You have already applied to this job', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'view', $job_id, slug($job['FekraJob']['title'])));
        }
        if (!empty($this->data)) {
            $this->FekraJobApplication->create();
            $this->data['FekraJobApplication']['user_id'] = $user['id'];
            $this->data['FekraJobApplication']['fekra_job_id'] = $job_id;
            if ($this->FekraJobApplication->save($this->data)) {
                $jobapp = $this->FekraJobApplication->read(null, $this->FekraJobApplication->id);
                $this->set('user', $cuser);
                $this->set('job_app', $jobapp);

                $this->Email->to = $cuser['User']['email'];
                $name = 'FikraShop';
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'apply';
                $this->Email->from = $name . '<noreply@fikrashop.com>';
                $this->Email->subject = $this->config['site_name'] . ': ' . __('You have received new bid on the job') . ': ' . $job['FekraJob']['title'];
                $this->Email->send();

                $this->setFlash('Your Application has been saved successfully', 'success alert alert-success');
                $this->redirect(array('action' => 'view', $job_id, slug($job['FekraJob']['title'])));
            } else {
                $this->setFlash('Your Application could not be saved. Please, try again.');
            }
        }
        $h1 = __('Jobs', true);
        $this->pageTitle = __('Apply to job', true);
        $this->set('h1', $h1);
        $this->set('job', $job);
    }

    function view_app($app_id = false) {
        if (!$app_id && empty($this->data)) {
            $this->setFlash(__('Invalid Application', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        $user = $this->authenticate();
        $this->loadModel('FekraJobApplication');
        $job_app = $this->FekraJobApplication->find('first', array('conditions' => array('FekraJobApplication.id' => $app_id)));
        $this->loadModel('FekraUser');
        $cuser = $this->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $job_app['FekraJob']['fekra_id'], 'FekraUser.type' => 1)));
        if (empty($cuser) && $user['id'] != $job_app['FekraJobApplication']['user_id']) {
            $this->setFlash(__('You are not allowed to view this page', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        $h1 = __('Jobs', true);
        $this->pageTitle = __('View Job Application', true);
        $this->set(compact('job_app', 'h1'));
    }

    function unapply($app_id = false) {
        if (!$app_id && empty($this->data)) {
            $this->setFlash(__('Invalid Application', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        $this->loadModel('FekraJobApplication');
        if ($this->FekraJobApplication->delete($app_id)) {
            $this->setFlash(__('You have unapplied from this job successfully', true), 'success alert alert-success');
            $this->redirect(array('action' => 'job_apps'));
        } else {
            $this->setFlash(__('something went wrong', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'job_apps'));
        }
    }

    function hire($app_id) {
        $user = $this->authenticate();
        $this->loadModel('FekraJobApplication');
        $job_app = $this->FekraJobApplication->find('first', array('conditions' => array('FekraJobApplication.id' => $app_id)));
        $this->loadModel('FekraUser');
        $cuser = $this->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $job_app['FekraJob']['fekra_id'], 'FekraUser.user_id' => $user['id'], 'FekraUser.type' => 1)));
        if (empty($cuser)) {
            $this->setFlash(__('You are not allowed to view this page', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        $this->FekraJobApplication->create();
        $this->FekraJobApplication->id = $app_id;
        if ($this->FekraJobApplication->saveField('status', 1)) {
            $this->set('user', $job_app['User']);
            $this->set('jobapp', $job_app);

            $this->Email->to = $job_app['User']['email'];
            $name = 'FikraShop';
            $this->Email->sendAs = 'html';
            $this->Email->layout = 'contact';
            $this->Email->template = 'hire';
            $this->Email->from = $name . '<noreply@fikrashop.com>';
            $this->Email->subject = $this->config['site_name'] . ': ' . __('Congratulations new job is awarded to you', true) . ' ' . $job_app['FekraJob']['title'];
            $this->Email->send();

            $this->FekraJob->create();
            $this->FekraJob->id = $job_app['FekraJobApplication']['fekra_job_id'];
            $this->FekraJob->saveField('status', 1);
            $this->setFlash(__('This Applicant is hired successfully', true), 'success alert alert-success');
            $this->redirect(array('action' => 'view', $job_app['FekraJob']['id'], slug($job_app['FekraJob']['title'])));
        }
    }

    function pay($job_id) {

        $user = $this->authenticate();
        $this->loadModel('FekraJobApplication');
        $this->loadModel('FekraUser');
        $this->loadModel('Transaction');
        $job_app = $this->FekraJobApplication->find('first', array('conditions' => array('FekraJobApplication.fekra_job_id' => $job_id, 'FekraJobApplication.status' => 1)));
        $cuser = $this->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $job_app['FekraJob']['fekra_id'], 'FekraUser.user_id' => $user['id'], 'FekraUser.type' => 1)));
        $total_payments = FekraJob::getTotalPayments($job_id, $job_app['User']['id']);
        $limit = $job_app['FekraJobApplication']['price'] - $total_payments;
        if (empty($cuser)) {
            $this->setFlash(__('You are not allowed to view this page', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }
        if (isset($_GET['success']) && !empty($_GET['success'])) {

            $transdata = GetShippingDetails($_GET['token']);
            $ack = strtoupper($transdata["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $resArray = DoExpressCheckout($transdata['TOKEN'], $transdata['PAYERID'], $transdata['AMT'], CURRENCY_CODE);
//                debug($resArray);exit;
                $datat['Transaction']['fekra_job_id'] = $job_id;
                $datat['Transaction']['sender_id'] = $user['id'];
                $datat['Transaction']['receiver_id'] = $job_app['User']['id'];
                $datat['Transaction']['actual'] = getCharged($transdata['AMT']);
                $datat['Transaction']['total'] = $transdata['AMT'];
                $datat['Transaction']['first_name'] = $cuser['User']['first_name'];
                $datat['Transaction']['last_name'] = $cuser['User']['last_name'];
                $datat['Transaction']['email'] = $cuser['User']['email'];
                $datat['Transaction']['status'] = 5;
                $datat['Transaction']['transaction_id'] = $resArray['PAYMENTINFO_0_TRANSACTIONID'];
                $this->Transaction->create();
                if ($this->Transaction->save($datat)) {
                    $transaction = $this->Transaction->read(null, $this->Transaction->id);
                    $this->set('transaction', $transaction);
                    $this->_send_message($transaction['Sender']['email'], $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>', __('Thank you for sending the payment',true), 'pay_installment_sender');
                    $this->_send_message($transaction['Receiver']['email'], $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>', __('Payment is received against the job',true).' '.$transaction['FekraJob']['title'], 'pay_installment_receiver');




                    $this->setFlash(__('The transaction has been saved successfully', true), 'success alert alert-success');
                    $totalAfterPayment = FekraJob::getTotalPayments($job_id, $job_app['User']['id']);
                    if ($totalAfterPayment == $job_app['FekraJobApplication']['price']) {
                        $this->FekraJob->create();
                        $this->FekraJob->id = $job_id;
                        $this->FekraJob->saveField('status', 2);
                        $this->redirect(array('action' => 'rate_developer', $job_app['User']['id'], $job_id, true));
                    } else {
                        $this->redirect(array('controller' => 'transactions', 'action' => 'index'));
                    }
                }
            }
        }


        if (!empty($this->data)) {
            $datat = array();
            $this->FekraJobApplication->validate = array();
            $this->FekraJobApplication->validate['total'] = array(
                array('rule' => 'numeric', 'message' => __('Enter a valid number', true), 'allowEmpty' => false),
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array(
                    'rule' => array('comparison', '>', 0),
                    'message' => __('Enter a valid number', true)
                ),
            );
            $this->FekraJobApplication->validate['charged'] = array(
                array('rule' => array('checkvalid', $limit), 'message' => __('Total payment to developer must less than or equal', true) . ' ' . format_price($limit)),
                array('rule' => 'numeric', 'message' => __('Enter a valid number', true)),
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array(
                    'rule' => array('comparison', '>', 0),
                    'message' => __('Enter a valid number', true)
                ),
            );


            $this->FekraJobApplication->set($this->data);
            if ($this->FekraJobApplication->validates()) {
                $tid = $this->Transaction->id;
                $cancelURL = Router::url(array('action' => 'pay', $job_id), true);
                $returnURL = Router::url(array('action' => 'pay', $job_id, '?' => array('success' => 1)), true);
                $itemName = 'Paying Installment to ' . $job_app['FekraJob']['title'];
                $resArray = SetExpressCheckout($this->data['FekraJobApplication']['total'], CURRENCY_CODE, 'Sale', $returnURL, $cancelURL, $itemName);
                $ack = strtoupper($resArray["ACK"]);
                if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                    $token = urldecode($resArray["TOKEN"]);
                    RedirectToPayPal($token);
                }
            } else {
                $this->setFlash(__('Please complete paying information, and try again', true));
            }
        }
        $h1 = __('Jobs', true);
        $this->pageTitle = __('Pay installment', true);
        $this->set(compact('job_app', 'h1', 'total_payments'));
    }

    function delete($id = false) {
        if (!$id) {
            $this->setFlash(__('Invalid id for Job', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'user_jobs'));
        }
        if ($this->FekraJob->delete($id)) {
            $this->setFlash(__('Job deleted', true), 'success alert alert-success');
            $this->redirect(array('action' => 'user_jobs'));
        }
        $this->setFlash(__('Job can not be deleted', true), 'fail alert alert-error');
        $this->redirect(array('action' => 'user_ideas'));
    }

    function rate_developer($user_id = false, $job = false, $transaction = false) {
        $this->loadModel('UserRating');
        $user = $this->authenticate();
        if (!$user_id) {
            $this->setFlash(__('Invalid id for User', true), 'fail alert alert-error');
            $this->redirect(array('controller' => '/'));
        }

        $ratings = $this->UserRating->find('count', array('conditions' => array('UserRating.creator_id' => $user['id'], 'UserRating.user_id' => $user_id, 'UserRating.fekra_job_id' => $job)));
        if ($ratings) {
            $this->setFlash(__('You have already rated this developer', true), 'info alert alert-warning');
            $this->redirect(array('action' => 'view', $job));
        }
        if (!empty($this->data)) {
            $this->data['UserRating']['creator_id'] = $user['id'];
            $this->data['UserRating']['user_id'] = $user_id;
            $this->data['UserRating']['fekra_job_id'] = $job;
            $this->UserRating->create();
            if ($this->UserRating->save($this->data)) {
                $this->setFlash(__('Thank you for your rating', true), 'success alert alert-success');
                if ($transaction) {
                    $this->redirect(array('controller' => 'transactions', 'action' => 'index'));
                }
                $this->redirect(array('action' => 'view', $job));
            } else {
                $this->setFlash(__('Please complete fields and try again', true), 'fail alert alert-error');
            }
        }
        $this->set('user_id', $user_id);
        $this->set('job', $job);
        $this->set('transaction', $transaction);
        $h1 = __('Users', true);
        $this->pageTitle = __('Rate developer', true);
        $this->set(compact('h1'));
    }

}
