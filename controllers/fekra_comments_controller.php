<?php

class FekraCommentsController extends AppController {

    var $name = 'FekraComments';

    /**
     * @var FekraComment */
    var $FekraComment;
    var $components = array('RequestHandler');

    function admin_index() {
        $this->FekraComment->recursive = 0;
        $this->set('fekraComments', $this->FekraComment->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid fekra comment', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('fekraComment', $this->FekraComment->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->FekraComment->create();
            if ($this->FekraComment->save($this->data)) {
                $this->setFlash(__('The fekra comment has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The fekra comment could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $fekras = $this->FekraComment->Fekra->find('list');
        $users = $this->FekraComment->User->find('list');
        $this->set(compact('fekras', 'users'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid fekra comment', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->FekraComment->save($this->data)) {
                $this->setFlash(__('The fekra comment has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The fekra comment could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->FekraComment->read(null, $id);
        }
        $fekras = $this->FekraComment->Fekra->find('list');
        $users = $this->FekraComment->User->find('list');
        $this->set(compact('fekras', 'users'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for fekra comment', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FekraComment->delete($id)) {
            $this->setFlash(__('Fekra comment deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Fekra comment was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->FekraComment->deleteAll(array('FekraComment.id' => $ids))) {
                $this->setFlash(__('Fekra comment deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Fekra comment can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function add() {
        $this->loadModel('FekraUser');
        $user = $this->authenticate();
        if (!empty($this->data)) {
            $fekra = $this->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $this->data['FekraComment']['fekra_id'], 'FekraUser.type' => 1)));
            $this->data['FekraComment']['user_id'] = $user['id'];

            if ($fekra['FekraUser']['user_id'] == $user['id']) {
                $this->data['FekraComment']['approved'] = 1;
            }

            if (!empty($this->data['FekraComment']['content'])) {

                $this->FekraComment->create();
                if ($this->FekraComment->save($this->data, false)) {
                    if (isset($this->data['FekraComment']['approved']) && $this->data['FekraComment']['approved'] == 1) {
                        $this->setFlash(__('The comment has been saved', true), 'success alert alert-success');
                    } else {
                        $this->set('fekra', $fekra);
                        $this->set('comment', $this->FekraComment->read(null, $id));
                        if ($this->_send_message($fekra['User']['email'], $this->config['site_name'] . '<noreply@fikrashop.com>', $this->config['site_name'] . ': ' . __('You have received a new comment on your idea', true) . ' ' . $fekra['Fekra']['title'], 'new_comment')) {
                            $this->setFlash(__('The comment is waiting to be approved from idea creator', true), 'success alert alert-success');
                            $this->redirect(array('controller' => 'fekras', 'action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']), '#' => 'Comments'));
                        }
                    }
                }
            } else {
                $this->setFlash(__('The comment could not be saved. Please, try again.', true), 'fail alert alert-error');
                $this->redirect(array('controller' => 'fekras', 'action' => 'view', $fekra['Fekra']['id'], slug($fekra['Fekra']['title']), '#' => 'Comments'));
            }
        }
    }

    function delete($id = null) {
        $return = array();
        $comment = $this->FekraComment->read(null, $id);
        if ($this->RequestHandler->isAjax()) {
            if ($this->FekraComment->delete($id)) {
                $return['message'] = __('The Comment deleted successfully', true);
                $return['status'] = 1;
            } else {
                $return['message'] = __('The Comment cannot be deleted', true);
                $return['status'] = 0;
            }

            echo json_encode($return);
            exit();
        } else {
            if ($this->FekraComment->delete($id)) {
                $this->setFlash(__('The Comment deleted successfully', true), 'success alert alert-success');
                $this->redirect(array('controller' => 'fekras', 'action' => 'view', $comment['Fekra']['id'], slug($comment['Fekra']['title']), '#' => 'Comments'));
            } else {
                $this->setFlash(__('The Comment cannot be deleted', true), 'fail alert alert-error');
                $this->redirect(array('controller' => 'fekras', 'action' => 'view', $comment['Fekra']['id'], slug($comment['Fekra']['title']), '#' => 'Comments'));
            }
        }
    }

    function approve($id = false) {
        $this->FekraComment->id = $id;
        $comment = $this->FekraComment->read(null, $id);
        if ($this->FekraComment->saveField('approved', 1)) {
            $this->setFlash(__('The comment has been approved successfully', true), 'success alert alert-success');
            $this->redirect(array('controller' => 'fekras', 'action' => 'view', $comment['Fekra']['id'], slug($comment['Fekra']['title']), '#' => 'Comments'));
        } else {
            $this->setFlash(__('The comment could not be saved. Please, try again.', true), 'fail alert alert-error');
            $this->redirect(array('controller' => 'fekras', 'action' => 'view', $comment['Fekra']['id'], slug($comment['Fekra']['title']), '#' => 'Comments'));
        }
    }

}
