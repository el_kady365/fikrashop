<?php

class UserEducationsController extends AppController {

    var $name = 'UserEducations';

    /**
     * @var UserEducation */
    var $UserEducation;

    function admin_index() {
        $this->UserEducation->recursive = 0;
        $this->set('userEducations', $this->UserEducation->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid user education', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('userEducation', $this->UserEducation->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->UserEducation->create();
            if ($this->UserEducation->save($this->data)) {
                $this->setFlash(__('The user education has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user education could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $users = $this->UserEducation->User->find('list');
        $this->set(compact('users'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid user education', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->UserEducation->save($this->data)) {
                $this->setFlash(__('The user education has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user education could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->UserEducation->read(null, $id);
        }
        $users = $this->UserEducation->User->find('list');
        $this->set(compact('users'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for user education', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->UserEducation->delete($id)) {
            $this->setFlash(__('User education deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('User education was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->UserEducation->deleteAll(array('UserEducation.id' => $ids))) {
                $this->setFlash(__('User education deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('User education can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }
    
    function add() {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            if (!empty($this->data)) {
                $this->UserEducation->create();
                $this->data['UserEducation']['user_id'] = $user['id'];
                if ($this->UserEducation->save($this->data)) {
                    $return['message'] = __('Your Education has been added successfully', true);
                    $return['status'] = 1;
                } else {

                    $return['message'] = __('The Education could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            }
           
        }
    }

    function edit($id = false) {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            $this->UserEducation->recursive = -1;
            if (!empty($this->data)) {
                $this->UserEducation->create();
                $this->data['UserEducation']['user_id'] = $user['id'];
                if ($this->UserEducation->save($this->data)) {
                    $return['message'] = __('Your Education has been modified successfully', true);
                    $return['status'] = 1;
                } else {
                    $return['message'] = __('The Education could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            } else {
                $this->data = $this->UserEducation->read(null, $id);
            }
            $this->set('levels', $this->UserEducation->levels);
            $this->render('add');
        }
    }
    function delete($id = null) {
         $return = array();
        if ($this->UserEducation->delete($id)) {
            $return['message'] = __('Your education deleted successfully', true);
            $return['status'] = 1;
        } else {
            $return['message'] = __('Your education cannot be deleted', true);
            $return['status'] = 0;
        }
        echo json_encode($return);
        exit();
    }

}
