<?php

class PollsController extends AppController {

    var $name = 'Polls';

    /**
     * @var Poll */
    var $Poll;

    function admin_index() {
        $this->Poll->recursive = 0;
        $this->set('polls', $this->paginate('Poll'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid poll', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('poll', $this->Poll->read(null, $id));
    }

    function admin_add($course_id = false) {

        if (!empty($this->data)) {
            $this->Poll->create();
            if ($this->Poll->save($this->data)) {
                $this->setFlash(__('The Poll has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The poll could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }

        $this->set('course_id', $course_id);
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid poll', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        $poll = $this->Poll->read(null, $id);

        if (!empty($this->data)) {
            if ($this->Poll->save($this->data)) {
                $this->setFlash(__('The poll has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The poll could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $poll;
        }

        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for poll', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        $poll = $this->Poll->read(null, $id);
        if ($this->Poll->delete($id)) {
            $this->setFlash(__('Poll deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Poll was not deleted', true), 'alert alert-error');
        $this->redirect($this->referer());
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['form']['operation'];
        if ($operation == 'delete') {
            if ($this->Poll->deleteAll(array('Poll.id' => $ids))) {
                $this->setFlash(__('Poll deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Poll can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['form']['operation'];
        if ($operation == 'delete') {
            if ($this->Poll->deleteAll(array('Poll.id' => $ids))) {
                $this->setFlash(__('Poll deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Poll can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function solve($category_id = false, $fekra_user = false) {
        $this->layout = false;
        $this->loadModel('Category');
        $this->Poll->recursive = 2;
        $category = $this->Category->read(null, $category_id);
        if (!empty($category['Category']['poll_id'])) {
            $id = $category['Category']['poll_id'];
            $poll = $this->Poll->findById($id);

            $errors = array();
//        debug($_POST);exit;
            $questions = $this->Poll->PollQuestion->find('all', array('conditions' => array('PollQuestion.poll_id' => $id)));
            if (!empty($this->data)) {
//            debug($this->data);


                if (!empty($questions)) {
                    foreach ($questions as $ques) {
                        if (empty($this->data['PollQuestion']['question_' . $ques['PollQuestion']['id']])) {
                            $errors['question_' . $ques['PollQuestion']['id']] = __('Required', true);
                        }
                    }
                }

                if (empty($errors)) {
                    $this->Poll->PollAnswer->deleteAll(array('PollAnswer.user_id' => $user['User']['id'], 'PollAnswer.poll_id' => $id));
                    foreach ($this->data['PollQuestion'] as $key => $question) {
                        $params = explode('_', $key);
                        $answer_data['poll_id'] = $this->data['Poll']['poll_id'];
                        $answer_data['user_id'] = $user['User']['id'];
                        $answer_data['poll_question_id'] = $params[1];
                        if (is_array($question)) {
                            $answer_data['answer'] = implode(',', $question);
                        } else {
                            $answer_data['answer'] = $question;
                        }


                        $question_answer_save['PollAnswer'] = $answer_data;
                        $this->Poll->PollAnswer->create();
                        $this->Poll->PollAnswer->save($question_answer_save, false);
                    }
                    $this->setFlash('You have successfully answer the poll', 'alert alert-success');
                    $this->redirect(array('action' => 'index', $poll['Poll']['course_id'], '?' => array('level' => $poll['Poll']['level_id'])));
                } else {
                    $this->setFlash('Please answer all questions', 'alert alert-error');
                }
            }
            if (empty($this->data) && !empty($fekra_user)) {
                foreach ($questions as $ques) {
                    $ans = $this->Poll->PollAnswer->find('first', array('conditions' => array('PollAnswer.fekra_user_id' => $fekra_user, 'PollAnswer.poll_question_id' => $ques['PollQuestion']['id'])));
//                    debug($ans);
                    $this->data['PollQuestion']['question_' . $ques['PollQuestion']['id']] = $ans['PollAnswer']['answer'];
                }
            }





            $this->set('poll', $poll);
            $this->set('errors', $errors);
        }
    }

}

