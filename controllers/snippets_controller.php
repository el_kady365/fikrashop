<?php

class SnippetsController extends AppController {

    var $name = 'Snippets';

    /**
     * @var Snippet */
    var $Snippet;

    function admin_index() {
        $this->Snippet->recursive = 0;
        $this->set('snippets', $this->Snippet->find('all'));
         $this->get_languages();
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid snippet', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('snippet', $this->Snippet->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Snippet->create();
            if ($this->Snippet->save($this->data)) {
                $this->setFlash(__('The snippet has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The snippet could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->get_languages();
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid snippet', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Snippet->save($this->data)) {
                $this->setFlash(__('The snippet has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The snippet could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Snippet->read(null, $id);
        }
        $this->get_languages();
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for snippet', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Snippet->delete($id)) {
            $this->setFlash(__('Snippet deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Snippet was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Snippet->deleteAll(array('Snippet.id' => $ids))) {
                $this->setFlash(__('Snippet deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Snippet can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
