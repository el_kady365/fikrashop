<?php
class MessageAttachmentsController extends AppController {

var $name = 'MessageAttachments';
/**
* @var MessageAttachment*/
var $MessageAttachment;
    
function admin_index() {
$this->MessageAttachment->recursive = 0;
$this->set('messageAttachments', $this->MessageAttachment->find('all'));
}

function admin_view($id = null) {
if (!$id) {
    $this->setFlash(__('Invalid message attachment', true));
    $this->redirect(array('action' => 'index'));
}
$this->set('messageAttachment', $this->MessageAttachment->read(null, $id));
}

function admin_add() {
if (!empty($this->data)) {
$this->MessageAttachment->create();
if ($this->MessageAttachment->save($this->data)) {
    $this->setFlash(__('The message attachment has been saved', true),'alert alert-success');
    $this->redirect(array('action' => 'index'));
} else {
    $this->setFlash(__('The message attachment could not be saved. Please, try again.', true),'alert alert-error');
}
}
		$messages = $this->MessageAttachment->Message->find('list');
		$this->set(compact('messages'));
}

function admin_edit($id = null) {
if (!$id && empty($this->data)) {
    $this->setFlash(__('Invalid message attachment', true),'alert alert-error');
    $this->redirect(array('action' => 'index'));
}
if (!empty($this->data)) {
if ($this->MessageAttachment->save($this->data)) {
    $this->setFlash(__('The message attachment has been saved', true),'alert alert-success');
    $this->redirect(array('action' => 'index'));
} else {
    $this->setFlash(__('The message attachment could not be saved. Please, try again.', true),'alert alert-error');
}
}
if (empty($this->data)) {
$this->data = $this->MessageAttachment->read(null, $id);
}
		$messages = $this->MessageAttachment->Message->find('list');
		$this->set(compact('messages'));
$this->render('admin_add');
}

function admin_delete($id = null) {
if (!$id) {
    $this->setFlash(__('Invalid id for message attachment', true),'alert alert-error');
    $this->redirect(array('action'=>'index'));
}
if ($this->MessageAttachment->delete($id)) {
    $this->setFlash(__('Message attachment deleted', true),'alert alert-success');
    $this->redirect(array('action'=>'index'));
}
    $this->setFlash(__('Message attachment was not deleted', true),'alert alert-error');
$this->redirect(array('action' => 'index'));
}

function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->MessageAttachment->deleteAll(array('MessageAttachment.id' => $ids))) {
                $this->setFlash(__('Message attachment deleted successfully',true), 'alert alert-success');
            } else {
                $this->setFlash(__('Message attachment can not be deleted',true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }
}
