<?php

class UserExperiencesController extends AppController {

    var $name = 'UserExperiences';

    /**
     * @var UserExperience */
    var $UserExperience;

    function admin_index() {
        $this->UserExperience->recursive = 0;
        $this->set('userExperiences', $this->UserExperience->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid user experience', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('userExperience', $this->UserExperience->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->UserExperience->create();
            if ($this->UserExperience->save($this->data)) {
                $this->setFlash(__('The user experience has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user experience could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $users = $this->UserExperience->User->find('list');
        $this->set(compact('users'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid user experience', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->UserExperience->save($this->data)) {
                $this->setFlash(__('The user experience has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user experience could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->UserExperience->read(null, $id);
        }
        $users = $this->UserExperience->User->find('list');
        $this->set(compact('users'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for user experience', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->UserExperience->delete($id)) {
            $this->setFlash(__('User experience deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('User experience was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->UserExperience->deleteAll(array('UserExperience.id' => $ids))) {
                $this->setFlash(__('User experience deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('User experience can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function add() {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            if (!empty($this->data)) {
                $this->UserExperience->create();
                $this->data['UserExperience']['user_id'] = $user['id'];
                if ($this->UserExperience->save($this->data)) {
                    $return['message'] = __('Your Experience has been added successfully', true);
                    $return['status'] = 1;
                } else {
                    $return['message'] = __('Your Experience could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            }
            $this->set('levels', $this->UserExperience->levels);
        }
    }

    function edit($id = false) {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            $this->UserExperience->recursive = -1;
            if (!empty($this->data)) {
                $this->UserExperience->create();
                $this->data['UserExperience']['user_id'] = $user['id'];
                if ($this->UserExperience->save($this->data)) {
                    $return['message'] = __('Your Experience has been modified successfully', true);
                    $return['status'] = 1;
                } else {
                    $return['message'] = __('Your Experience could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            } else {
                $this->data = $this->UserExperience->read(null, $id);
            }
            $this->set('levels', $this->UserExperience->levels);
            $this->render('add');
        }
    }
    
    function delete($id = null) {
       $return = array();
        if ($this->UserExperience->delete($id)) {
            $return['message'] = __('Your experience deleted successfully', true);
            $return['status'] = 1;
        } else {
            $return['message'] = __('Your experience cannot be deleted', true);
            $return['status'] = 0;
        }
        echo json_encode($return);
        exit();
    }

}
