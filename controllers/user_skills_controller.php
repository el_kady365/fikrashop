<?php

class UserSkillsController extends AppController {

    var $name = 'UserSkills';

    /**
     * @var UserSkill */
    var $UserSkill;

    function admin_index() {
        $this->UserSkill->recursive = 0;
        $this->set('userSkills', $this->UserSkill->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid user skill', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('userSkill', $this->UserSkill->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->UserSkill->create();
            if ($this->UserSkill->save($this->data)) {
                $this->setFlash(__('The user skill has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user skill could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $users = $this->UserSkill->User->find('list');
        $this->set(compact('users'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid user skill', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->UserSkill->save($this->data)) {
                $this->setFlash(__('The user skill has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user skill could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->UserSkill->read(null, $id);
        }
        $users = $this->UserSkill->User->find('list');
        $this->set(compact('users'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for user skill', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->UserSkill->delete($id)) {
            $this->setFlash(__('User skill deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('User skill was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->UserSkill->deleteAll(array('UserSkill.id' => $ids))) {
                $this->setFlash(__('User skill deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('User skill can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function add() {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            if (!empty($this->data)) {
                $this->UserSkill->create();
                $this->data['UserSkill']['user_id'] = $user['id'];
                if ($this->UserSkill->save($this->data)) {
                    $this->loadModel('Skill');
                    $name=$this->data['UserSkill']['name'];
                    $skill_check2 = $this->Skill->find('first', array('conditions' => array('LOWER(Skill.title) = \'' . low($name) . '\'')));
                    if (empty($skill_check2)) {
                        $skilldata2 = array();
                        $skilldata2['Skill']['title'] = ucwords($name);
                        $skilldata2['Skill']['active'] = 1;
                        $this->Skill->create();
                        $this->Skill->save($skilldata2, false);
                    }

                    $return['message'] = __('Your skill has been added successfully', true);
                    $return['status'] = 1;
                } else {

                    $return['message'] = __('The user skill could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            }
            $this->set('levels', $this->UserSkill->levels);
        }
    }

    function edit($id = false) {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            $this->UserSkill->recursive = -1;
            if (!empty($this->data)) {
                $this->UserSkill->create();
                $this->data['UserSkill']['user_id'] = $user['id'];
                if ($this->UserSkill->save($this->data)) {
                    $return['message'] = __('Your skill has been modified successfully', true);
                    $return['status'] = 1;
                } else {
                    $return['message'] = __('The user skill could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            } else {
                $this->data = $this->UserSkill->read(null, $id);
            }
            $this->set('levels', $this->UserSkill->levels);
            $this->render('add');
        }
    }

    function delete($id = null) {
        $return = array();
        if ($this->UserSkill->delete($id)) {
            $return['message'] = __('Your skill deleted successfully', true);
            $return['status'] = 1;
        } else {
            $return['message'] = __('Your skill cannot be deleted', true);
            $return['status'] = 0;
        }
        echo json_encode($return);
        exit();
    }

}
