<?php

class CountriesController extends AppController {

    var $name = 'Countries';

    /**
     * @var Country */
    var $Country;

    function admin_index() {
        
        $this->Country->recursive = 0;
        $conditions = array();
        if (isset($_GET['cat_id']) && $_GET['cat_id'] != '') {
            $conditions[] = array('Country.category_id' => $_GET['cat_id']);
        }
        $this->set('countries', $this->paginate('Country', $conditions));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid country', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('country', $this->Country->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Country->create();
            if ($this->Country->save($this->data)) {
                $this->setFlash(__('The country has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The country could not be saved. Please, try again.', true), 'fail');
            }
        }
      $this->set('categories', $this->Country->Category->find('list', array('conditions' => array('Category.active' => 1, 'Category.type' => 3), 'fields' => 'id,ar_title')));  
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid country', true), 'fail');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Country->save($this->data)) {
                $this->setFlash(__('The country has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The country could not be saved. Please, try again.', true), 'fail');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Country->read(null, $id);
        }
        $this->set('categories', $this->Country->Category->find('list', array('conditions' => array('Category.active' => 1, 'Category.type' => 3), 'fields' => 'id,ar_title')));  
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for country', true), 'fail');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Country->delete($id)) {
            $this->setFlash(__('Country deleted', true), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Country was not deleted', true), 'fail');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['form']['operation'];
        if ($operation == 'delete') {
            if ($this->Country->deleteAll(array('Country.id' => $ids))) {
                $this->setFlash(__('Country deleted successfully', true), 'success');
            } else {
                $this->setFlash(__('Country can not be deleted', true), 'fail');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
