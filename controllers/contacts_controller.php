<?php

class ContactsController extends AppController {

    var $name = 'Contacts';

    /**
     * @var Contact */
    var $Contact;
    var $components = array('Email');

    function admin_index() {
        $this->Contact->recursive = 0;
        $this->set('contacts', $this->Contact->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid contact', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('contact', $this->Contact->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Contact->create();
            if ($this->Contact->save($this->data)) {
                $this->setFlash(__('The contact has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The contact could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid contact', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Contact->save($this->data)) {
                $this->setFlash(__('The contact has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The contact could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Contact->read(null, $id);
        }
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for contact', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Contact->delete($id)) {
            $this->setFlash(__('Contact deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Contact was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Contact->deleteAll(array('Contact.id' => $ids))) {
                $this->setFlash(__('Contact deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Contact can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function contactus() {

        $this->pageTitle = $h1 = __('Contact us', true);
        $sub_heading = __('Contact us any time', true);
        $subject = "{$this->config['site_name']}: New Contact us";
        $this->set('coordinates', $this->config['coordinates']);
        $this->set('h1', $h1);
        $this->set('sub_heading', $sub_heading);
        $to = $this->config['admin_email'];
        $snippet = $this->get_snippets('contact-us');

        $this->set('sitename', $this->config['site_name']);
        $this->set('snippet', $snippet);

        if (!empty($this->data)) {
            $this->Contact->create();
            if ($this->Contact->save($this->data)) {
                $this->set('data', $this->data['Contact']);
                $this->Email->to = $to;
                $name = $this->data['Contact']['name'];
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'contact';
                $this->Email->from = $name . '<' . $this->data['Contact']['email'] . '>';
                $this->Email->subject = $subject;


                if ($this->Email->send()) {
                    $this->data = array();
                    $this->setFlash(__('Your message has been sent.', true), 'success');
                } else {
                    $this->setFlash(__('can\'t send email', true), 'fail');
                }
            } else {
                // debug($this->Contact->validationErrors);
                $this->setFlash(__('Your message could not be sent, please check for input errors and try again.', true), 'fail');
            }
        }
    }

}
