<?php

class PollQuestionsController extends AppController {

    var $name = 'PollQuestions';

    /**
     * @var PollQuestion */
    var $PollQuestion;

    function admin_index($poll_id) {

        $this->PollQuestion->recursive = 0;
        $this->set('pollQuestions', $this->paginate('PollQuestion', array('PollQuestion.poll_id' => $poll_id)));
        $this->set('poll_id', $poll_id);
        $this->set('types', $this->PollQuestion->types);
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid poll question', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('pollQuestion', $this->PollQuestion->read(null, $id));
    }

    function admin_add($poll_id = false) {

        if (!empty($this->data)) {
            $this->PollQuestion->create();
            if ($this->PollQuestion->save($this->data)) {
                $this->PollQuestion->addOptions($this->data);
                $this->setFlash(__('The poll question has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index', $this->data['PollQuestion']['poll_id']));
            } else {
                $this->setFlash(__('The poll question could not be saved. Please, try again.', true), 'fail');
            }
        }
        if (empty($this->data)) {
            $this->data['PollQuestion']['poll_id'] = $poll_id;
        }
        $this->set('types', $this->PollQuestion->types);
        $this->set('poll_id', $poll_id);
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid poll question', true), 'fail');
            $this->redirect(array('action' => 'index'));
        }
        $poll_question = $this->PollQuestion->read(null, $id);


        if (!empty($this->data)) {
           
            $this->PollQuestion->addOptions($this->data);
            if ($this->PollQuestion->save($this->data)) {
                $this->setFlash(__('The poll question has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index', $this->data['PollQuestion']['poll_id']));
            } else {
                $this->setFlash(__('The poll question could not be saved. Please, try again.', true), 'fail');
            }
        }
        if (empty($this->data)) {
            $this->data = $poll_question;
        }
        $this->set('types', $this->PollQuestion->types);


        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for poll question', true), 'fail');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->PollQuestion->delete($id)) {
            $this->setFlash(__('Poll question deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Poll question was not deleted', true), 'fail');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['form']['operation'];
        if ($operation == 'delete') {
            if ($this->PollQuestion->deleteAll(array('PollQuestion.id' => $ids))) {
                $this->setFlash(__('Poll question deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Poll question can not be deleted', true), 'fail');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
