<?php

class NewslettersController extends AppController {

    var $name = 'Newsletters';

    /**
     * @var Newsletter
     */
    var $Newsletter;
    var $helpers = array('Html', 'Form');
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Newsletter->recursive = 0;
        $this->set('newsletters', $this->paginate());
    }

    function add() {
        $this->layout = $this->autoRender = false;
        Configure::write('debug', 0);
        $return = array();
        if (!empty($this->data)) {
            $this->Newsletter->create();
            if ($this->Newsletter->save($this->data)) {
                $return['message'] = __('Your Email has been saved', true);
                $return['success'] = 1;
            } else {
                $return['error']['email'] = $this->Newsletter->validationErrors['email'];
            }
        }

        echo json_encode($return);
        exit;
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid Newsletter', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('newsletter', $this->Newsletter->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Newsletter->create();
            if ($this->Newsletter->save($this->data)) {
                $this->setFlash(__('The Newsletter has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The Newsletter could not be saved. Please, try again.', true), 'fail');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid Newsletter', true), 'fail');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Newsletter->save($this->data)) {
                $this->setFlash(__('The Newsletter has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The Newsletter could not be saved. Please, try again.', true), 'fail');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Newsletter->read(null, $id);
        }
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for Newsletter', true), 'fail');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Newsletter->del($id)) {
            $this->setFlash(__('Newsletter deleted', true), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('The Newsletter could not be deleted. Please, try again.', true), 'fail');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['form']['operation'];
        if ($operation == 'delete') {
            if ($this->Newsletter->deleteAll(array('Newsletter.id' => $ids))) {
                $this->setFlash('Newsletter deleted successfully', 'success');
            } else {
                $this->setFlash('Newsletter can not be deleted', 'fail');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function admin_export() {
        // Stop Cake from displaying action's execution time
        Configure::write('debug', 0);
        // Find fields needed without recursing through associated models
        $this->layout = 'default';
        $data = $this->Newsletter->find(
                'all', array(
            'fields' => array('id', 'email'),
            'order' => "Newsletter.id ASC",
        ));
        // Define column headers for CSV file, in same array format as the data itself
        $headers = array(
            'Newsletter' => array(
                'id' => 'ID',
                'created' => 'Email',
            )
        );
        // Add headers to start of data array
        array_unshift($data, $headers);
        // Make the data available to the view (and the resulting CSV file)
        $this->set(compact('data'));
    }

}

?>