<?php

class PagesController extends AppController {

    var $name = 'Pages';

    /**
     * @var Page
     */
    var $Page;
//    var $uses = array('Page', 'Category');
    var $helpers = array('Html', 'Form', 'Text');

    function admin_index() {
        $this->Page->recursive = 0;
        $conditions = array();

        if (isset($_GET['main']) && $_GET['main'] != '') {
            $conditions[] = array('Page.add_to_mainmenu' => $_GET['main']);
        }

        $this->paginate = array('order' => array('Page.display_order' => 'asc'));
        $this->set('pages', $this->Page->find('all', array('conditions' => $conditions)));
        $this->get_languages();
    }

    function admin_dashboard() {
        
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid Page', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('page', $this->Page->read(null, $id));
    }

    function admin_add($id = false) {
        if (!empty($this->data)) {
            $this->Page->create();
            if ($this->Page->save($this->data)) {
                $this->setFlash(__('The Page has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The Page could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->get_languages();

        $this->set('types', $this->Page->types);
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid Page', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Page->save($this->data)) {
                $this->setFlash(__('The Page has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The Page could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Page->read(null, $id);
        }
        $this->set('types', $this->Page->types);

        $this->get_languages();
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for Page', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Page->delete($id)) {
            $this->setFlash(__('Page deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('The Page could not be deleted. Please, try again.', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Page->deleteAll(array('Page.id' => $ids))) {
                $this->setFlash('Page deleted successfully', 'alert alert-success');
            } else {
                $this->setFlash('Page can not be deleted', 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function home() {
        $this->set('meta_keywords', $this->config['home_keywords']);
        $this->set('meta_description', $this->config['home_description']);

        $this->loadModel('Category');
        $this->loadModel('Sponsor');
        $this->loadModel('Fekra');
        $this->loadModel('Banner');

        $this->set('categories', $this->Category->find('list', array('contain' => false, 'conditions' => array('Category.active' => 1), 'fields' => array($this->lang . '_title'), 'order' => 'Category.display_order asc', 'recursive' => -1)));
        $this->set('banners', $this->Banner->find('all', array('conditions' => array('Banner.active' => 1), 'order' => 'Banner.id asc', 'recursive' => -1)));
        $this->set('sponsors', $this->Sponsor->find('all', array('conditions' => array('Sponsor.active' => 1), 'order' => 'Sponsor.display_order asc', 'recursive' => -1)));
        $this->set('featured_ideas', $this->Fekra->find('all', array('conditions' => array('Fekra.published' => 1,'Fekra.featured'=>1), 'order' => 'Fekra.created desc', 'recursive' => -1)));
        $snippet = $this->get_snippets('home-snippet', $this->lang);
        $this->set('snippet', $snippet);

        $this->pageTitle = __('Home', true);
    }

    function check_paragraph() {
        App::import('Vendor', 'check_errors', array('file' => 'check_errors.php'));

        if (!empty($this->data)) {
            $data = array('key' => '123456', 'data' => $this->data['Paragraph']['content']);
            $postText = http_build_query($data, '', '&');
            $checker = new check_errors();
            $errors = $checker->get_errors($postText);
//            debug($errors);
            $this->setFlash('Many errors has found');
            if (!$errors) {
                $this->setFlash('There are not any errors', 'alert alert-success');
                $errors = array();
            }
            $this->set('errorsww', $errors);
        }
    }

    function view($permalink = false) {
        if (!$permalink) {
            $this->setFlash('Invalid Page');
            $this->redirect('/');
        }

        $page = $this->Page->find('first', array('conditions' => array('Page.permalink' => $permalink, 'Page.lang' => $this->lang)));

        $this->set('meta_keywords', $page['Page']['keywords']);
        $this->set('meta_description', strip_tags($page['Page']['description']));

        $this->pageTitle = $h1 = $page['Page']['title'];
        $this->set('h1', $h1);
        $this->set('sub_heading', $page['Page']['sub_heading']);
        $this->set('page', $page);
    }

    function messages() {
        $this->pageTitle = $h1 = __('Messages', true);
        $this->set(compact('h1'));
    }

}

?>