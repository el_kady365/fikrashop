<?php

class SearchesController extends AppController {

    var $name = 'Searches';

    /**
     * @var Search
     */
    var $Search;
    var $helpers = array('Html', 'Form');

    function index() {

        if (!isset($_GET['q']) || strpos($_SERVER['REQUEST_URI'], '%27')) {
            $this->redirect('/');
        }

        $q = $_GET['q'];
        $conditions = array();

        $conditions[] = array('Or' => array('Search.title like' => "%{$q}%", 'Search.description like' => "%{$q}%"));

        $results = $this->Search->find('all', array('conditions' => $conditions));
        

        $this->set(compact('q', 'results'));
        $this->pageTitle = __('Search results', true);
        
    }

}

?>