<?php
class SkillsController extends AppController {

var $name = 'Skills';
/**
* @var Skill*/
var $Skill;
    
function admin_index() {
$this->Skill->recursive = 0;
$this->set('skills', $this->Skill->find('all'));
}

function admin_view($id = null) {
if (!$id) {
    $this->setFlash(__('Invalid skill', true));
    $this->redirect(array('action' => 'index'));
}
$this->set('skill', $this->Skill->read(null, $id));
}

function admin_add() {
if (!empty($this->data)) {
$this->Skill->create();
if ($this->Skill->save($this->data)) {
    $this->setFlash(__('The skill has been saved', true),'alert alert-success');
    $this->redirect(array('action' => 'index'));
} else {
    $this->setFlash(__('The skill could not be saved. Please, try again.', true),'alert alert-error');
}
}
}

function admin_edit($id = null) {
if (!$id && empty($this->data)) {
    $this->setFlash(__('Invalid skill', true),'alert alert-error');
    $this->redirect(array('action' => 'index'));
}
if (!empty($this->data)) {
if ($this->Skill->save($this->data)) {
    $this->setFlash(__('The skill has been saved', true),'alert alert-success');
    $this->redirect(array('action' => 'index'));
} else {
    $this->setFlash(__('The skill could not be saved. Please, try again.', true),'alert alert-error');
}
}
if (empty($this->data)) {
$this->data = $this->Skill->read(null, $id);
}
$this->render('admin_add');
}

function admin_delete($id = null) {
if (!$id) {
    $this->setFlash(__('Invalid id for skill', true),'alert alert-error');
    $this->redirect(array('action'=>'index'));
}
if ($this->Skill->delete($id)) {
    $this->setFlash(__('Skill deleted', true),'alert alert-success');
    $this->redirect(array('action'=>'index'));
}
    $this->setFlash(__('Skill was not deleted', true),'alert alert-error');
$this->redirect(array('action' => 'index'));
}

function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Skill->deleteAll(array('Skill.id' => $ids))) {
                $this->setFlash(__('Skill deleted successfully',true), 'alert alert-success');
            } else {
                $this->setFlash(__('Skill can not be deleted',true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }
}
