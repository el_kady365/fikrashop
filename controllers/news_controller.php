<?php

class NewsController extends AppController {

    var $name = 'News';

    /**
     * @var News */
    var $News;

    function admin_index() {
        $this->News->recursive = 0;
        $this->set('news', $this->News->find('all', array('order' => 'News.creation_date desc')));
        $this->get_languages();
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid news', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('news', $this->News->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->News->create();
            if ($this->News->save($this->data)) {
                $this->setFlash(__('The news has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The news could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->get_languages();
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid news', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->News->save($this->data)) {
                $this->setFlash(__('The news has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The news could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->News->read(null, $id);
        }
        $this->get_languages();
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for news', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->News->delete($id)) {
            $this->setFlash(__('News deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('News was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->News->deleteAll(array('News.id' => $ids))) {
                $this->setFlash(__('News deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('News can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function index() {
        $this->paginate = array('order' => 'News.creation_date desc', 'limit' => 12);
        $news = $this->paginate('News', array('News.active' => 1, 'News.lang' => $this->lang));
        $this->set('news', $news);
        $this->set('h1', 'News');
        $this->set('sub_heading', 'See our latest news');
        $this->pageTitle = __('News', true);
    }

    function view($permalink = false) {
        $news = $this->News->find('first', array('conditions' => array('News.permalink' => $permalink)));
        $this->set('news', $news);
        $this->set('h1', 'News');
        $this->set('sub_heading', 'See our latest news');
        $this->pageTitle = $news['News']['title'];
    }

}
