<?php

class UsersController extends AppController {

    var $name = 'Users';

    /**
     * @var User */
    var $User;
    var $components = array('Email');

    function admin_index() {
        $this->User->recursive = 0;

        $this->set('users', $this->paginate('User'));
    }

    function admin_loginas($id = false) {
        $user = $this->User->find('first', array('conditions' => array('User.id' => $id), 'contain' => false));
        $this->Session->write('user', $user['User']);
        $this->redirect('/users/dashboard');
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__(sprintf(__('Invalid %s', true), __('user', true)), true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    function admin_add() {

        if (!empty($this->data)) {
            $this->User->create();
            $this->data['User']['confirmed'] = 1;
            if ($this->User->save($this->data)) {

                $this->setFlash(sprintf(__('The %s has been saved', true), __('user', true)), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(sprintf(__('The %s could not be saved. Please, try again', true), __('user', true)));
            }
        }
        $this->loadModel('Country');
        $countries = $this->Country->find('list', array('fields' => array('Country.code', 'Country.en_name')));
        $this->set('countries', $countries);
    }

    function admin_edit($id = null) {

        if (!$id && empty($this->data)) {
            $this->setFlash(sprintf(__('Invalid %s.', 'user', true)));
            $this->redirect(array('action' => 'index'));
        }
        $user = $this->User->read(null, $id);
        if (empty($user)) {
            $this->setFlash(__('User not found', true));
            $this->redirect(array("action" => "index"));
        }

        if (!empty($this->data)) {
            if ($this->User->save($this->data)) {
                $this->setFlash(sprintf(__('The %s  has been saved', true), __('user', true)), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(sprintf(__('The %s could not be saved. Please, try again', true), __('user', true)));
            }
        }
        if (empty($this->data)) {
            $user['User']['password'] = null;
            $this->data = $user;
        }
        $this->loadModel('Country');
        $countries = $this->Country->find('list', array('fields' => array('Country.code', 'Country.en_name')));
        $this->set('countries', $countries);
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for User', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->User->delete($id)) {
            $this->setFlash(__('User deleted', true), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('The User could not be deleted. Please, try again.', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->User->deleteAll(array('User.id' => $ids))) {
                $this->setFlash(__('Users deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Users can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function admin_confirm($id = false) {
        if (!$id) {
            $this->setFlash(__('Invalid id for User', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        $this->User->create();
        $this->User->id = $id;
        if ($this->User->saveField('confirmed', '1')) {
            $user = $this->User->read(null, $id);
            $user_data = $user['User'];
            $this->set('user_data', $user_data);
            $this->Email->to = $user['User']['email'];
            $name = $user['User']['username'];
            $subject = "{$this->config['site_name']}: Your account has been confirmed";
            $this->Email->sendAs = 'html';
            $this->Email->layout = 'contact';
            $this->Email->template = 'confirm';
            $this->Email->from = $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>';
            $this->Email->subject = $subject;
            $this->Email->send();

            $this->setFlash('The User has confirmed successfully', 'success');
            $this->redirect(array('action' => 'index'));
        }
    }

    function board() {
        $boards = $this->User->find('all', array('conditions' => array('User.evaluation_board' => 1), 'contain' => 'UserExperience'));
        $this->pageTitle = $h1 = __('Fikra Board', true);
        $this->set(compact('boards', 'h1'));
    }

    function register($provider = false) {
        if ($this->is_user()) {
            $this->redirect('/');
        }


        if (!empty($this->data) && !$provider) {
            $this->User->create();
            $name = preg_match_all('/\w+/', $this->data['User']['full_name'], $matches);

            $this->data['User']['first_name'] = array_shift($matches[0]);
            $this->data['User']['last_name'] = implode(' ', $matches[0]);
            $this->data['User']['confirmed'] = 1;
            $return = array();
            if ($this->User->save($this->data)) {
                $id = $this->User->getLastInsertID();
                $this->User->contain();
                $user = $this->User->read(null, $id);
                $this->Session->write('user', $user['User']);
                $this->set('user_data', $user);
                $this->_send_message($user['User']['email'], $this->config['site_name'] . "<{$this->config['admin_send_mail_from']}>", __("Registration Complete", true), 'registration');
                $return['message'] = __('You have registered successfully', true);
                $return['status'] = 1;
                $previous_page = $this->Session->read('LOGINREDIRECT');
                if (empty($previous_page)) {
                    $url = Router::url(array('action' => 'dashboard'), true);
                } else {
                    $this->Session->delete('LOGINREDIRECT');
                    $url = $previous_page;
                }

                $return['url'] = $url;
            } else {
                $return['message'] = __('You could not register. Please, try again.', true);
                $return['status'] = 0;
            }
            echo json_encode($return);
            exit();
        } elseif ($provider) {
            App::import('vendor', 'Authunticate', true, array(), 'social_networks/Authenticate.php', false);
            $authenticate = new Authenticate();
            $data = $authenticate->getUserProfile($provider);

            $user_check = $this->User->find('first', array('conditions' => array('User.email' => $data['email'])));

            if (!empty($user_check)) {
//                debug($user_check['User']);exit;
                $this->Session->write('user', $user_check['User']);
                $previous_page = $this->Session->read('LOGINREDIRECT');
                if (empty($previous_page)) {
                    $this->redirect(array('action' => 'dashboard'));
                } else {
                    $this->Session->delete('LOGINREDIRECT');
                    header("location:http://" . $_SERVER["HTTP_HOST"] . $previous_page);
                }
                exit;
            }
            if (!$data) {
                $this->setFlash(__("You could not register. Please, try again.", true), "fail alert alert-error");
                $this->redirect(array('controller' => '/'));
            }
            $user_data = array();
            $user_data['User']['first_name'] = $data['firstName'];
            $user_data['User']['last_name'] = $data['lastName'];
            $user_data['User']['email'] = $data['email'];
            if (isset($data['gender'])) {
                if ($data['gender'] == 'male') {
                    $user_data['User']['gender'] = '1';
                } else {
                    $user_data['User']['gender'] = '2';
                }
            }
            if (isset($data['phone'])) {
                if (isset($data['phone']['home'])) {
                    $user_data['User']['telephone'] = $data['phone']['home'];
                } elseif (isset($data['phone']['mobile'])) {
                    $user_data['User']['mobile'] = $data['phone']['mobile'];
                }
            }
            if ($provider == "facebook") {
                $user_data['User']['facebook_url'] = $data['profileURL'];
            } else {
                $user_data['User']['linkedin_url'] = $data['profileURL'];
            }
            if (!empty($data['description'])) {
                $user_data['User']['summary'] = $data['description'];
            }
            if (!empty($data['photoURL'])) {
                $image = file_get_contents($data['photoURL']);
                $file_name = 'image_' . $data['identifier'] . '.jpg';
                file_put_contents(WWW_ROOT . 'img' . DS . 'uploads' . DS . $file_name, $image);
                $user_data['User']['image'] = $file_name;
            }

            if (isset($data['country_code'])) {
                $user_data['User']['country_code'] = $data['country_code'];
            }

//            debug($user_data);exit;

            $this->User->create();
            if ($this->User->save($user_data, false)) {
                $id = $this->User->getLastInsertID();

                if (!empty($data)) {
                    if (!empty($data['skills']->values)) {
                        $this->loadModel('Skill');
                        $skills = (array) $data['skills']->{'values'};
                        foreach ($skills as $skill) {
                            $name = $skill->{'skill'}->{'name'};
                            $skill_check = $this->User->UserSkill->find('first', array('conditions' => array('LOWER(UserSkill.name) = \'' . low($name) . '\'', 'UserSkill.user_id' => $user['id'])));
                            $skill_check2 = $this->Skill->find('first', array('conditions' => array('LOWER(Skill.title) = \'' . low($name) . '\'')));

                            if (empty($skill_check)) {
                                $skilldata = array();
                                $skilldata['UserSkill']['name'] = $name;
                                $skilldata['UserSkill']['user_id'] = $id;
                                $this->User->UserSkill->create();
                                $this->User->UserSkill->save($skilldata, false);
                            }
                            if (empty($skill_check2)) {
                                $skilldata2 = array();
                                $skilldata2['Skill']['title'] = ucwords($name);
                                $skilldata2['Skill']['active'] = 1;
                                $this->Skill->create();
                                $this->Skill->save($skilldata2, false);
                            }
                        }
                    }
                    if (!empty($data['languages']->values)) {
                        $languages = (array) $data['languages']->{'values'};
                        foreach ($languages as $language) {
                            $name = $language->{'language'}->{'name'};
                            $lang_check = $this->User->UserLanguage->find('first', array('conditions' => array('LOWER(UserLanguage.name) = \'' . low($name) . '\'', 'UserLanguage.user_id' => $user['id'])));
                            if (empty($lang_check)) {
                                $skilldata = array();
                                $skilldata['UserLanguage']['name'] = $name;
                                $skilldata['UserLanguage']['user_id'] = $id;
                                $this->User->UserLanguage->create();
                                $this->User->UserLanguage->save($skilldata, false);
                            }
                        }
                    }

                    if (!empty($data['educations']->values)) {
                        $educations = (array) $data['educations']->{'values'};
                        foreach ($educations as $education) {
                            $name = $education->{'schoolName'};
                            $edu_check = $this->User->UserEducation->find('first', array('conditions' => array('LOWER(UserEducation.organization) = \'' . low($name) . '\'', 'UserEducation.user_id' => $user['id'])));
                            if (empty($edu_check)) {
                                $skilldata = array();
                                $skilldata['UserEducation']['organization'] = $name;
                                if (isset($education->{'degree'})) {
                                    $skilldata['UserEducation']['certification'] = $education->{'degree'};
                                }
                                $skilldata['UserEducation']['graduation_date'] = $education->{'endDate'}->{'year'} . '-1-1';
                                if (isset($education->{'fieldOfStudy'})) {
                                    $skilldata['UserEducation']['major'] = $education->{'fieldOfStudy'};
                                }
                                $skilldata['UserEducation']['user_id'] = $id;
                                $this->User->UserEducation->create();
                                $this->User->UserEducation->save($skilldata, false);
                            }
                        }
                    }
                    if (!empty($data['positions']->values)) {
                        $positions = (array) $data['positions']->{'values'};
                        foreach ($positions as $pos) {

                            $name = $pos->{'company'}->{'name'};

                            $exp_check = $this->User->UserExperience->find('first', array('conditions' => array('LOWER(UserExperience.company_name) = \'' . low($name) . '\'', 'UserExperience.user_id' => $user['id'])));

                            if (empty($exp_check)) {
                                $expdata = array();
                                $expdata['UserExperience']['company_name'] = $name;
                                $expdata['UserExperience']['till_now'] = (int) $pos->{'isCurrent'};
                                if (!empty($pos->{'summary'})) {
                                    $expdata['UserExperience']['job_description'] = $pos->{'summary'};
                                }
                                $expdata['UserExperience']['name'] = $pos->{'title'};
                                if (isset($pos->{'startDate'})) {
                                    $month = isset($pos->{'startDate'}->{'month'}) ? $pos->{'startDate'}->{'month'} : '1';
                                    $expdata['UserExperience']['start_date'] = date('Y-m-1', strtotime($pos->{'startDate'}->{'year'} . '-' . $month . '-' . '1'));
                                }
                                if (isset($pos->{'endDate'})) {
                                    $month = isset($pos->{'endDate'}->{'month'}) ? $pos->{'endDate'}->{'month'} : '1';
                                    $expdata['UserExperience']['end_date'] = date('Y-m-1', strtotime($pos->{'endDate'}->{'year'} . '-' . $month . '-' . '1'));
                                }
                                $expdata['UserExperience']['user_id'] = $id;
                                $this->User->UserExperience->create();
                                $this->User->UserExperience->save($expdata, false);
                            }
                        }
                    }
                }


                $user = $this->User->read(null, $id);
                $this->Session->write('user', $user['User']);
                $this->set('user_data', $user);
                $this->_send_message($user['User']['email'], $this->config['site_name'] . "<{$this->config['admin_send_mail_from']}>", __("Registration Complete", true), 'registration');
                $this->setFlash(__('You have registered successfully', true), 'success alert alert-success');
                $this->redirect(array('action' => 'profile'));
            }
        }
        $this->pageTitle = __('Register', true);
    }

    function dashboard() {
        $user = $this->authenticate();
        $this->pageTitle = $h1 = __("Dashboard", true);
        $data = $this->User->read(null, $user['id']);
        $this->set('data', $data);
        $this->set('edit', 1);
        $this->set('levels', $this->User->UserSkill->levels);
        $this->set('h1', $h1);
    }

    function get_linkedin_data() {
        $user = $this->authenticate();
        App::import('vendor', 'Authunticate', true, array(), 'social_networks/Authenticate.php', false);
        $authenticate = new Authenticate(array(
            'linkedin' => array(
                'api_key' => '77f0zppk3wklrh',
                'api_secret' => 'C0akfg7IEVnx0RW9',
                'redirect_uri' => "http://fikrashop.com/test/users/get_linkedin_data",
                'scope' => 'r_fullprofile r_emailaddress r_contactinfo'
            )
                )
        );
        $data = $authenticate->getUserProfile('linkedin');
//        debug($data);
//        exit;
        if (!empty($data)) {
            if (!empty($data['skills']->values)) {
                $skills = (array) $data['skills']->{'values'};
                foreach ($skills as $skill) {
                    $name = $skill->{'skill'}->{'name'};
                    $skill_check = $this->User->UserSkill->find('first', array('conditions' => array('LOWER(UserSkill.name) = \'' . low($name) . '\'', 'UserSkill.user_id' => $user['id'])));

                    if (empty($skill_check)) {
                        $skilldata = array();
                        $skilldata['UserSkill']['name'] = $name;
                        $skilldata['UserSkill']['user_id'] = $user['id'];
                        $this->User->UserSkill->create();
                        $this->User->UserSkill->save($skilldata, false);
                    }
                }
            }
            if (!empty($data['languages']->values)) {
                $languages = (array) $data['languages']->{'values'};
                foreach ($languages as $language) {
                    $name = $language->{'language'}->{'name'};
                    $lang_check = $this->User->UserLanguage->find('first', array('conditions' => array('LOWER(UserLanguage.name) = \'' . low($name) . '\'', 'UserLanguage.user_id' => $user['id'])));
                    if (empty($lang_check)) {
                        $skilldata = array();
                        $skilldata['UserLanguage']['name'] = $name;
                        $skilldata['UserLanguage']['user_id'] = $user['id'];
                        $this->User->UserLanguage->create();
                        $this->User->UserLanguage->save($skilldata, false);
                    }
                }
            }

            if (!empty($data['educations']->values)) {
                $educations = (array) $data['educations']->{'values'};
                foreach ($educations as $education) {
                    $name = $education->{'schoolName'};
                    $edu_check = $this->User->UserEducation->find('first', array('conditions' => array('LOWER(UserEducation.organization) = \'' . low($name) . '\'', 'UserEducation.user_id' => $user['id'])));
                    if (empty($edu_check)) {
                        $skilldata = array();
                        $skilldata['UserEducation']['organization'] = $name;
                        if (isset($education->{'degree'})) {
                            $skilldata['UserEducation']['certification'] = $education->{'degree'};
                        }
                        $skilldata['UserEducation']['graduation_date'] = $education->{'endDate'}->{'year'} . '-1-1';
                        if (isset($education->{'fieldOfStudy'})) {
                            $skilldata['UserEducation']['major'] = $education->{'fieldOfStudy'};
                        }
                        $skilldata['UserEducation']['user_id'] = $user['id'];
                        $this->User->UserEducation->create();
                        $this->User->UserEducation->save($skilldata, false);
                    }
                }
            }
            if (!empty($data['positions']->values)) {
                $positions = (array) $data['positions']->{'values'};
                foreach ($positions as $pos) {

                    $name = $pos->{'company'}->{'name'};

                    $exp_check = $this->User->UserExperience->find('first', array('conditions' => array('LOWER(UserExperience.company_name) = \'' . low($name) . '\'', 'UserExperience.user_id' => $user['id'])));

                    if (empty($exp_check)) {
                        $expdata = array();
                        $expdata['UserExperience']['company_name'] = $name;
                        $expdata['UserExperience']['till_now'] = (int) $pos->{'isCurrent'};
                        if (!empty($pos->{'summary'})) {
                            $expdata['UserExperience']['job_description'] = $pos->{'summary'};
                        }
                        $expdata['UserExperience']['name'] = $pos->{'title'};
                        if (isset($pos->{'startDate'})) {
                            $month = isset($pos->{'startDate'}->{'month'}) ? $pos->{'startDate'}->{'month'} : '1';
                            $expdata['UserExperience']['start_date'] = date('Y-m-1', strtotime($pos->{'startDate'}->{'year'} . '-' . $month . '-' . '1'));
                        }
                        if (isset($pos->{'endDate'})) {
                            $month = isset($pos->{'endDate'}->{'month'}) ? $pos->{'endDate'}->{'month'} : '1';
                            $expdata['UserExperience']['end_date'] = date('Y-m-1', strtotime($pos->{'endDate'}->{'year'} . '-' . $month . '-' . '1'));
                        }
                        $expdata['UserExperience']['user_id'] = $user['id'];
                        $this->User->UserExperience->create();
                        $this->User->UserExperience->save($expdata, false);
                    }
                }
            }
            $user_data = array();
            $user_data['User']['id'] = $user['id'];
            if (!empty($data['photoURL'])) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $data['photoURL']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $image = curl_exec($ch);
                curl_close($ch);
//                debug($image);exit;
                $file_name = 'image_' . $data['identifier'] . '.jpg';
                $path = $this->User->getFullFolder('image') . $file_name;
                if (file_exists($path)) {
                    unlink($path);
                }
//                $fp = fopen($path, 'x');
//                fwrite($fp, $image);
//                fclose($fp);
                $old_path = $this->User->getFullFolder('image') . $user['image']['basename'];
                if (!empty($user['image']) && $old_path != $path) {
                    unlink($old_path);
                }
                file_put_contents($this->User->getFullFolder('image') . $file_name, $image);
//                
                $user_data['User']['image'] = $file_name;
            }

            $user_data['User']['linkedin_url'] = $data['profileURL'];
            $user_data['User']['summary'] = $data['description'];
            if ($this->User->save($user_data)) {
                $user = $this->User->read(null, $this->User->id);
                $this->Session->write('user', $user['User']);
            }
        }
//        exit;
        $this->setFlash(__("Your Linkedin profile data has been imported successfully", true), "success alert alert-success");
        $this->redirect(array('action' => 'dashboard'));
    }

    function view($seeds = false) {
        $user = $this->authenticate();

        if ($seeds) {
            $id = base64_decode($seeds);


            $data = $this->User->read(null, $id);

            $this->pageTitle = $h1 = $data['User']['first_name'] . ' ' . $data['User']['last_name'];


            if (isset($data['UserExperience'][0])) {
                $this->set('sub_heading', $data['UserExperience'][0]['name'] . ' @ ' . $data['UserExperience'][0]['company_name']);
            }
            $this->set(compact('data', 'h1'));
            if ($user['id'] == $data['User']['id']) {
                $this->set('edit', 1);
            }


            $this->set('levels', $this->User->UserSkill->levels);
            $this->render('dashboard');
        } else {
            $this->redirect(array('controller' => '/'));
        }
    }

    function profile() {
        $user = $this->is_user();
        if (!$this->is_user()) {
            $this->setFlash('You must login first', "fail alert alert-error");
            $this->redirect('/#login');
        }
        if (!empty($this->data)) {
            if (!empty($this->data['User']['password']) && !$user['confirmed']) {
                $this->data['User']['confirmed'] = 1;
            }

            if ($this->User->save($this->data)) {
                $user = $this->User->read(null, $this->User->id);
                $this->Session->write('user', $user['User']);
                $this->setFlash(__("Your profile has been saved successfully", true), "success alert alert-success");
                $this->redirect(array('controller' => 'users', 'action' => 'profile'));
            } else {
                $this->setFlash(__("You profile could not be saved", true), "fail alert alert-error");
            }
        } else {

            unset($user['password']);
            $this->data['User'] = $user;
        }
//        debug($this->User->find('first',array('conditions'=>array('User.id'=>2))));

        $this->loadModel('Country');
        $this->loadModel('Category');

        $this->set('countries', $this->Country->find('list', array('fields' => array('code', $this->lang . '_name'), 'recusive' => -1)));
        $this->set('categories', $this->Category->find('list', array('fields' => array('id', $this->lang . '_title'), 'recusive' => -1)));
        $this->pageTitle = $h1 = __("My Profile", true);
        $this->set('h1', $h1);
    }

    function login() {
//        if ($this->is_user()) {
//            $this->redirect(array('action' => 'dashboard'));
//        }
        Configure::write('debug', 0);

        $this->layout = $this->autoRender = false;


        if (!empty($this->data)) {

            $user = $this->User->find('first', array('conditions' => array('User.email' => $this->data['User']['email'], 'User.password' => hashPassword($this->data['User']['password'])), 'contain' => false));
            $return = array();
            $return['pass'] = hashPassword($this->data['User']['password']);
            if ($user) {

                $this->Session->write('user', $user['User']);
//$this->setFlash(__("You have logined successfully", true), 'success');

                $return['message'] = __('You have logined successfully', true);
                $return['status'] = 1;
                $previous_page = $this->Session->read('LOGINREDIRECT');
                if (empty($previous_page)) {
                    $url = Router::url(array('action' => 'dashboard'), true);
                } else {
                    $url = $previous_page;
                    $this->Session->delete('LOGINREDIRECT');
                }
                $return['url'] = $url;

//$this->redirect(array('action' => 'dashboard'));
            } else {
//                $this->setFlash("Invalid email or password");
                $return['message'] = __('Invalid email or password', true);
                $return['status'] = 0;
            }
            echo json_encode($return);
            exit();
        }
        $this->pageTitle = __("Login", true);
    }

    function confirm($confirm_code = false) {

        if (!$confirm_code) {
            $this->setFlash('Invalid security code', 'alert alert-error');
            $this->redirect('/');
        }

        $seeds = explode('-', base64_decode($confirm_code));
        $id = $seeds[0];
        $email = $seeds[1];
        $user = $this->User->read(null, $id);
        if ($user['User']['email'] != $email) {
            $this->setFlash('Invalid security code', 'alert alert-error');
            $this->redirect('/');
        } else {
            $this->User->create();
            $this->User->id = $id;
            if ($this->User->saveField('confirmed', '1')) {
                $this->setFlash('Your account has been actived successfully', 'success');
                $this->redirect('/');
            }
        }
    }

    function change_password() {

        $user = $this->authenticate();

        if (!empty($this->data)) {
            $user = $this->User->change_password($this->data, $user['id']);
            if (!empty($user)) {
                $this->Session->write('user', $user['User']);
                $this->setFlash(__("Your password has been changed successfully", true), "success alert alert-success");
                $this->redirect(array('action' => 'dashboard'));
            } else {
                $this->setFlash(__("Could not change you password", true), "fail alert alert-error");
            }
        }
        $this->pageTitle = __("Change Password", true);
        $h1 = __("Users", true);
        $this->set(compact('h1'));
    }

    function forget_password() {
        Configure::write('debug', 0);
        $this->layout = $this->autoRender = false;

        if (!empty($this->data)) {
            $user = $this->User->Forget($this->data);
            if (!empty($user)) {
                $hash = base64_encode($user['User']['id'] . "#" . $user['User']['email']);
                $this->set('hash', $hash);
                $this->Email->to = $user['User']['email'];
                $this->set('user_data', $user);
                $subject = "{$this->config['site_name']}: " . __('Reset your password', true);
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'reset';
                $this->Email->from = $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>';
                $this->Email->subject = $subject;
                $this->Email->send();

                $return['message'] = __('A message has been sent to confirm reset your passowrd', true);
                $return['status'] = 1;
            } else {
                $return['message'] = __('Could not reset your password', true);
                $return['status'] = 0;
            }
            echo json_encode($return);
            exit();
        }
    }

    function reset_password($hash_data = false) {
        $this->layout = false;
        if (!$hash_data) {
            $this->setFlash(__('Wrong Data', true));
            $this->redirect('/');
        } else {

            $seeds = base64_decode($hash_data);

            $parts = explode('#', $seeds);

            $id = $parts[0];
            $user = $this->User->find('first', array('conditions' => array('User.id' => $id), 'recursive' => -1));


            $new_passowrd = substr(md5(rand()), 0, 6);
            if (!empty($user)) {
                $this->set('new_passowrd', $new_passowrd);
                $this->set('user_data', $user);
                $this->User->create();
                $this->User->id = $user['User']['id'];
                if ($this->User->saveField('password', $new_passowrd)) {
                    $this->Email->to = $user['User']['email'];
                    $subject = "{$this->config['site_name']}: You have received a new password ";
                    $this->Email->sendAs = 'html';
                    $this->Email->layout = 'contact';
                    $this->Email->template = 'newpassword';
                    $this->Email->from = $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>';
                    $this->Email->subject = $subject;
                    $this->data = array();

                    if ($this->Email->send()) {
                        $this->setFlash(__("Your new password has been sent to your email", true), "success alert alert-success");
                        $this->redirect('/');
                    } else {
                        $this->setFlash(__("Failed to send your new password. Please try again", true), "fail alert alert-error");
                    }
                }
            } else {
                $this->setFlash(__('Wrong Data', true));
                $this->redirect('/');
            }
        }
    }

    function logout() {
        $this->Session->delete('user');
        $this->redirect('/');
    }

    function check_input() {
        Configure::write('debug', 0);
        $this->layout = $this->autoRender = false;


        $username = isset($_GET['data']['User']['username']) ? $_GET['data']['User']['username'] : '';
        $email = isset($_GET['data']['User']['email']) ? $_GET['data']['User']['email'] : '';
        if (isset($username) && !empty($username)) {
            $check = $this->User->find('count', array('conditions' => array('User.username' => $username)));
        } elseif (isset($email) && !empty($email)) {
            $check = $this->User->find('count', array('conditions' => array('User.email' => $email)));
        }
        if (!empty($check)) {
            $output = false;
        } else {
            $output = true;
        }
        echo json_encode($output);
//$this->User->find('count',array)
    }

    function summary() {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            if (!empty($this->data)) {
                $this->User->create();
                $this->User->id = $user['id'];
                if ($this->User->saveField('summary', $this->data['User']['summary'])) {
                    $user = $this->User->read(null, $user['id']);
                    $this->Session->write('user', $user['User']);
                    $return['message'] = __('Your summary has been saved', true);
                    $return['status'] = 1;
                } else {
                    $return['message'] = __('Your summary could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            } else {
                $this->data["User"]['summary'] = $user['summary'];
            }
        }
    }

    function get_ideas_for_rating($board_id = false) {
        $user = $this->is_user();
        $this->layout = false;
        $board_user = $this->User->read(null, $board_id);
        $this->loadModel('FekraUser');
        if (!empty($user)) {
            if (!empty($this->data)) {
                if (!empty($this->data['User']['ideas'])) {
                    $this->set('board_user', $board_user);
                    $this->set('selected_fekras', $this->FekraUser->Fekra->find('list', array('conditions' => array('Fekra.id' => array_values($this->data['User']['ideas']), 'Fekra.published' => 1))));
                    $from = $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>';
                    $this->set('sender', $user);
                    if ($this->_send_message($board_user['User']['email'], $from, $this->config['site_name'] . ': ' . __('You have received a request to rate an idea(s)', true), 'rate_idea')) {
                        $return['message'] = __('Your request sent to board member successfully', true);
                        $return['status'] = 1;
                    } else {
                        $return['message'] = __('unfortunatly you request has not been sent', true);
                        $return['status'] = 0;
                    }
                } else {
                    $return['message'] = __('Please select at least one idea for rating', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            }
            $fekras = $this->FekraUser->find('list', array('conditions' => array('FekraUser.user_id' => $user['id']), 'fields' => array('FekraUser.fekra_id')));
            $this->set('fekras', $this->FekraUser->Fekra->find('list', array('conditions' => array('Fekra.id' => array_values($fekras), 'Fekra.published' => 1))));
            $this->set('board_id', $board_id);
        }
    }

    function get_jobs_by_skills() {
        $this->loadModel('FekraJob');
        $this->loadModel('Skill');
        $this->autoRender = false;
        $users = $this->User->find('all', array('conditions' => array('send_jobs' => 1), 'contain' => false, 'recursive' => -1, 'callbacks' => false));
        if (!empty($users)) {
            foreach ($users as $user) {
                $user_skills = $this->User->UserSkill->find('all', array('conditions' => array('UserSkill.user_id' => $user['User']['id'])));
                $return_jobs = array();
                if (!empty($user_skills)) {
                    foreach ($user_skills as $user_skill) {
                        $skills = $this->Skill->find('list', array('conditions' => array('lower(Skill.title) Like \'%' . strtolower($user_skill['UserSkill']['name']) . '%\'')));
                        foreach ($skills as $key => $skill) {
                            $skill = strtolower($skill);
                            $conditions = array();
                            $conditions['FekraJob.published'] = 1;
                            $conditions['FekraJob.created >='] = date('Y-m-d H:i:s', strtotime('yesterday'));
                            $conditions['or'] = array("concat(',',FekraJob.skills,',') LIKE '%,{$key},%'", "concat(' ',FekraJob.title,' ') LIKE '% {$skill} %'", "concat(' ',FekraJob.description,' ') LIKE '% {$skill} %'");
                            $job = $this->FekraJob->find('first', array('conditions' => $conditions, 'recursive' => -1));
                            if (!empty($job)) {
                                $return_jobs[$job['FekraJob']['id']] = $job['FekraJob'];
                            }
                        }
                    }
                }
//            debug($return_jobs);

                if (!empty($return_jobs)) {
                    $this->set(compact('return_jobs', 'user'));
                    $this->Email->delivery = 'smtp';
                    $this->_send_message($user['User']['email'], $this->config['site_name'] . '<noreply@fikrashop.com>', $this->config['site_name'] . ': ' . __('Jobs that match your skills', true), 'daily_jobs');
                }
            }
        }
        exit;
    }

    function get_ideas_by_categories() {
        $this->loadModel('Category');
        $this->loadModel('Fekra');
        $this->autoRender = false;
        $users = $this->User->find('all', array('conditions' => array("User.categories is not null or User.categories!=''"), 'contain' => false, 'recursive' => -1, 'callbacks' => false));
        if (!empty($users)) {
            foreach ($users as $user) {
                $return_ideas = array();
                if (!empty($user['User']['categories'])) {
                    $conditions = array();
                    $conditions['Fekra.published'] = 1;
                    $conditions['Fekra.category_id'] = $user['User']['categories'];
                    $conditions['Fekra.created >='] = date('Y-m-d H:i:s', strtotime('yesterday'));
                    $fekras = $this->Fekra->find('all', array('conditions' => $conditions, 'recursive' => -1));

                    if (!empty($fekras)) {
                        $this->set(compact('fekras', 'user'));
                        $this->Email->delivery = 'smtp';
                        $this->_send_message($user['User']['email'], $this->config['site_name'] . '<noreply@fikrashop.com>', $this->config['site_name'] . ': ' . __('Ideas that match your chosen categories', true), 'daily_ideas');
                    }
                }
            }
        }
        exit;
    }

}
