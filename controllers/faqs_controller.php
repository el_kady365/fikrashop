<?php

class FaqsController extends AppController {

    var $name = 'Faqs';

    /**
     * @var Faq */
    var $Faq;

    function admin_index() {
        $this->Faq->recursive = 0;
        $this->set('faqs', $this->Faq->find('all'));
        $this->get_languages();
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid faq', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('faq', $this->Faq->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Faq->create();
            if ($this->Faq->save($this->data)) {
                $this->setFlash(__('The faq has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The faq could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->get_languages();
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid faq', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Faq->save($this->data)) {
                $this->setFlash(__('The faq has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The faq could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Faq->read(null, $id);
        }
        $this->get_languages();
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for faq', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Faq->delete($id)) {
            $this->setFlash(__('Faq deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Faq was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Faq->deleteAll(array('Faq.id' => $ids))) {
                $this->setFlash(__('Faq deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Faq can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function index() {

        $faqs = $this->Faq->find('all', array('conditions' => array('Faq.active' => 1, 'Faq.lang' => $this->lang), 'order' => 'Faq.display_order asc'));
        $this->set('faqs', $faqs);
        $this->pageTitle = $h1 = __('FAQs', true);
        $this->set('h1', $h1);
        $this->set('sub_heading', 'We can help you');
    }

}
