<?php

App::import('Vendor', 'Paypal', array('file' => 'paypalfunctions.php'));

class TransactionsController extends AppController {

    var $name = 'Transactions';

    /**
     * @var Transaction */
    var $components = array('RequestHandler', 'Email');
    var $Transaction;

    function index() {
        $user = $this->authenticate();
        $this->loadModel('User');
        $find_user = $this->User->find('first', array('conditions' => array('User.id' => $user['id'])));

        $allowed_statuses = array(1, 2, 6, 8);
        $this->paginate = array('order' => 'Transaction.created desc');
        $sent = $this->paginate('Transaction', array('Transaction.sender_id' => $user['id'], 'Transaction.status !=' => 0));
        $received = $this->paginate('Transaction', array('Transaction.receiver_id' => $user['id'], 'Transaction.status' => $allowed_statuses));
        $withdrawals = $this->paginate('Transaction', array('Transaction.receiver_id' => $user['id'], 'Transaction.status' => 4));
        $h1 = $this->pageTitle = $sub_title = __('Transactions', true);
        $this->set(compact('h1', 'sub_title', 'find_user'));
        $this->set('sent', $sent);
        $this->set('received', $received);
        $this->set('withdrawals', $withdrawals);
        $this->set('statuses', $this->Transaction->statuses);
    }

    function refund($id = false) {
        $user = $this->authenticate();
        $this->autoRender = FALSE;
        if (!$id) {
            $this->setFlash(__('invalid Transaction', true));
            $this->redirect(array('action' => 'index'));
        }
        $transaction = $this->Transaction->read(null, $id);
        if ($transaction['Transaction']['status'] == 5) {
            $resArray = Refund($transaction['Transaction']['transaction_id']);
            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $this->Transaction->create();
                $this->Transaction->id = $id;
                if ($this->Transaction->save(array('Transaction' => array('status' => 3, 'transaction_id' => $resArray['REFUNDTRANSACTIONID'])))) {
                    $this->setFlash(__('Your Transaction has been refunded successfully', true), 'success alert alert-success');
                    $this->redirect(array('action' => 'index'));
                }
            }
        } else {
            $this->setFlash(__('Invalid transaction status', true));
            $this->redirect(array('action' => 'index'));
        }
    }

    function complete($id = false) {
        $user = $this->authenticate();
        $this->autoRender = FALSE;
        if (!$id) {
            $this->setFlash(__('invalid Transaction', true));
            $this->redirect(array('action' => 'index'));
        }
        $transaction = $this->Transaction->read(null, $id);

        if (isset($_GET['token']) && !empty($_GET['token'])) {
            $resArray = GetShippingDetails($_GET['token']);
            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $resArray = DoExpressCheckout($resArray['TOKEN'], $resArray['PAYERID'], $resArray['AMT'], CURRENCY_CODE);
//                debug($resArray);exit;
                $this->Transaction->id = $id;
                if ($this->Transaction->save(array('Transaction' => array('status' => 5, 'transaction_id' => $resArray['PAYMENTINFO_0_TRANSACTIONID'])))) {
                    $this->setFlash(__('The transaction has been saved successfully', true), 'success alert alert-success');
                    $this->redirect(array('controller' => 'transactions', 'action' => 'index'));
                }
            }
        }

        if ($transaction['Transaction']['status'] == 0) {
            $tid = $this->Transaction->id;
            $cancelURL = Router::url(array('action' => 'transactions', 'action' => 'index'), true);
            $returnURL = Router::url(array('action' => 'complete', $id), true);
            if (!empty($transaction['FekraPhase']['id'])) {
                $itemName = 'Investment in idea ' . $transaction['Fekra']['title'];
            } elseif (!empty($transaction['FekraPhase']['id'])) {
                $itemName = 'Job in idea ' . $transaction['Fekra']['title'];
            }
            $resArray = SetExpressCheckout($transaction['Transaction']['total'], CURRENCY_CODE, 'Sale', $returnURL, $cancelURL, $itemName);
            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $token = urldecode($resArray["TOKEN"]);
                RedirectToPayPal($token);
            }
        } else {
            $this->setFlash(__('Invalid transaction status', true));
            $this->redirect(array('action' => 'index'));
        }
    }

    function get_paid($id) {
        $this->autoRender = false;
        $user = $this->authenticate();
        $transaction = $this->Transaction->find('first', array('conditions' => array('Transaction.id' => $id, 'Transaction.receiver_id' => $user['id'])));


        if (!empty($transaction)) {

            $postData = array("cancelUrl" => Router::url(array('action' => 'index'), true),
                "returnUrl" => Router::url(array('action' => 'index'), true),
                "senderEmail" => 'el_kady365-facilitator@yahoo.com',
                "receiverList.receiver(0).email" => $transaction['Receiver']['email'],
                "receiverList.receiver(0).amount" => $transaction['Transaction']['actual'],
                "currencyCode" => "USD",
                "feesPayer" => "EACHRECEIVER",
                "memo" => "Withdraw Transaction" . $transaction['Transaction']['id']);

            $response = makeAdaptivePayment($postData);
            if (strtoupper($response['responseEnvelope.ack']) == 'SUCCESS') {
                $this->Transaction->create();
                $this->Transaction->id = $id;
                $this->Transaction->saveField('status', 6);
                $this->Transaction->create();
                if ($this->Transaction->save(array('Transaction' => array('total' => $transaction['Transaction']['actual'], 'actual' => $transaction['Transaction']['actual'], 'receiver_id' => $user['id'], 'fekra_phase_id' => $transaction['Transaction']['fekra_phase_id'], 'fekra_job_id' => $transaction['Transaction']['fekra_job_id'], 'status' => 4)))) {
                    $this->setFlash('You have withdraw this amount successfully', 'success alert alert-success');
                    $this->redirect(array('action' => 'index'));
                }
            }
        }




        $h1 = __('Transactions', true);
        $this->pageTitle = __('Get paid', true);
        $this->set(compact('h1', 'sub_title', 'find_user'));
    }

    function get_paid2() {
        $user = $this->authenticate();
        $this->loadModel('User');
        $find_user = $this->User->find('first', array('conditions' => array('User.id' => $user['id'])));
        if ($find_user['User']['completed_payments'] == '0') {
            $this->setFlash(__("You don't have any completed payments to be withdrawn", true));
            $this->redirect(array('action' => 'index'));
        }

        if (!empty($this->data)) {
            if ($this->data['Transaction']['total'] > $find_user['User']['completed_payments']) {
                $this->Transaction->validationErrors['total'] = __('Invalid value', true);
            } else {
                $postData = array("cancelUrl" => Router::url(array('action' => 'get_paid'), true),
                    "returnUrl" => Router::url(array('action' => 'index'), true),
                    "senderEmail" => 'el_kady365-facilitator@yahoo.com',
                    "receiverList.receiver(0).email" => $find_user['User']['email'],
                    "receiverList.receiver(0).amount" => $this->data['Transaction']['total'],
                    "currencyCode" => "USD",
                    "feesPayer" => "EACHRECEIVER",
                    "memo" => "Withdraw ");

                $response = makeAdaptivePayment($postData);
                if (strtoupper($response['responseEnvelope.ack']) == 'SUCCESS') {
                    $this->Transaction->create();
                    if ($this->Transaction->save(array('Transaction' => array('total' => $this->data['Transaction']['total'], 'actual' => $this->data['Transaction']['total'], 'receiver_id' => $user['id'], 'status' => 4)))) {
                        $this->setFlash('You have withdraw this amount successfully', 'success alert alert-success');
                        $this->redirect(array('action' => 'index'));
                    }
                }
            }
        }



        $h1 = __('Transactions', true);
        $this->pageTitle = __('Get paid', true);
        $this->set(compact('h1', 'sub_title', 'find_user'));
    }

    function set_pending() {
        $this->autoRender = false;
        $date = date('Y-m-d h:i:s', strtotime('+10 minutes'));
        $conditions = array();
        $conditions[] = array('Transaction.status' => 5);
        $conditions[] = array("Transaction.created >= " => $date);

        $transactions = $this->Transaction->find('all', array('conditions' => $conditions));
//        debug($transactions);
//        exit();

        foreach ($transactions as $transaction) {
            $this->Transaction->id = $transaction['Transaction']['id'];
            if ($this->Transaction->save(array('Transaction' => array('status' => 1)))) {
                $this->set('data', $transaction);
                $this->Email->to = $transaction['Sender']['email'];
                $name = 'FikraShop';
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'set_pending';
                $this->Email->from = $name . '<noreply@fikrashop.com>';
                $this->Email->subject = $this->config['site_name'] . ': Action Required';
                $this->Email->send();
            }
        }
    }

    function set_as_complete($id) {
        $user = $this->authenticate();
        $this->autoRender = false;
        $transaction = $this->Transaction->find('first', array('conditions' => array('Transaction.sender_id' => $user['id'], 'Transaction.id' => $id)));
        if (empty($transaction)) {
            $this->setFlash('You are not allowed to set this transaction as complete', 'fail alert alert-danger');
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Transaction->id = $id;
            if ($this->Transaction->save(array('Transaction' => array('status' => 2)))) {
                $this->set('transaction', $this->Transaction->read(null, $id));
                if (!empty($transaction['Transaction']['fekra_phase_id'])) {
                    $subject = __('Payment for idea', true) . ' ' . $transaction['Fekra']['title'] . ' ' . __('is ready for release', true);
                } else {
                    $subject = __('Payment for job', true) . ' ' . $transaction['FekraJob']['title'] . ' ' . __('is ready for release', true);
                }
                $this->_send_message($transaction['Receiver']['email'], $this->config['site_name'] . '<' . $this->config['admin_send_mail_from'] . '>', $subject, 'complete_transaction');
                $this->setFlash('The transaction set as completed successfully', 'success alert alert-success');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    function complain($transaction_id, $type) {
        $user = $this->authenticate();
        $transaction = $this->Transaction->find('first', array('conditions' => array('Transaction.id' => $transaction_id)));
        $this->loadModel('Complain');
        if (!empty($this->data)) {
            $this->Complain->create();
            if ($this->Complain->save($this->data)) {
                $this->Transaction->create();
                $this->Transaction->id = $this->data['Complain']['transaction_id'];
                if ($type == 1) {
                    $this->Transaction->saveField('status', 7);
                } else {
                    $this->Transaction->saveField('status', 8);
                }
                $this->Complain->recursive = 2;
                $complain = $this->Complain->read(null, $this->Complain->id);
                $this->set('data', $complain);
                $this->Email->to = $this->config['admin_email'];
                $name = 'FikraShop';
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'complain';
                $this->Email->from = $name . '<noreply@fikrashop.com>';

                $subject = $this->config['site_name'] . ': You have received a new complain';

                $this->Email->subject = $subject;
                $this->Email->send();
                $this->setFlash('Your complain request sent successfully', 'success alert alert-success');
                $this->redirect(array('action' => 'index'));
            }
        }
        if (empty($this->data)) {
            $this->data['Complain']['transaction_id'] = $transaction_id;
            $this->data['Complain']['type'] = $type;
        }
        $h1 = __('Transactions', true);
        $this->pageTitle = __('Complain', true);
        $this->set(compact('h1'));
    }

    function remove_transactions() {
        $this->autoRender = false;
        $date = date('Y-m-d h:i:s', strtotime('+10 minutes'));
        $this->Transaction->deleteAll(array('Transaction.status' => 0, "Transaction.created >= " => $date));
    }

}

?>
