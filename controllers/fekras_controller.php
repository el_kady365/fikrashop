<?php

App::import('Vendor', 'Paypal', array('file' => 'paypalfunctions.php'));

class FekrasController extends AppController {

    var $name = 'Fekras';

    /**
     * @var Fekra */
    var $Fekra;
    var $components = array('RequestHandler');

    function admin_index() {
        $this->Fekra->recursive = 0;
        $this->set('fekras', $this->Fekra->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid fekra', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('fekra', $this->Fekra->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Fekra->create();
            if ($this->Fekra->save($this->data)) {
                $this->setFlash(__('The idea has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The idea could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $categories = $this->Fekra->Category->find('list', array('fields' => array($this->lang . '_title')));
        $types = array(1 => __('Youtube', true), __('File', true));
        $this->set('info', $this->Fekra->FekraVideo->getFileSettings());
        $this->set('binfo', $this->Fekra->getFileSettings());
        $this->set(compact('categories', 'types'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid fekra', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Fekra->save($this->data)) {
                $this->Fekra->addIdeaAssets($this->data);
                $this->setFlash(__('The idea has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The idea could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Fekra->read(null, $id);
        }
        $categories = $this->Fekra->Category->find('list', array('fields' => array($this->lang . '_title')));
        $types = array(1 => __('Youtube', true), __('File', true));
        $this->set('info', $this->Fekra->FekraVideo->getFileSettings());
        $this->set('binfo', $this->Fekra->getFileSettings());
        $this->set(compact('categories', 'types'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for fekra', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Fekra->delete($id)) {
            $this->setFlash(__('Fekra deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Idea can not be deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Fekra->deleteAll(array('Fekra.id' => $ids))) {
                $this->setFlash(__('Idea deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Idea can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function user_ideas($id = false) {
        $user = $this->authenticate();
        $this->Fekra->FekraUser->recursive = 1;
        $this->paginate = array('limit' => 10);

        if ($id) {
            $user_id = $id;
            $this->loadModel('User');
            $found_user = $this->User->find('first', array('conditions' => array('User.id' => $id)));
            $this->pageTitle = $sub_title = __('Ideas', true) . ' ' . __('by', true) . ' ' . $found_user['User']['first_name'];
            $h1 = __('Ideas', true);
            $fekras = $this->paginate('FekraUser', array('FekraUser.user_id' => $user_id));
            $this->set(compact('fekras', 'h1', 'sub_title'));
        } else {
            $user_id = $user['id'];
            $this->set('edit', 1);
            $this->pageTitle = $sub_title = __('My Ideas', true);
            $h1 = __('Ideas', true);
            $fekras = $this->paginate('FekraUser', array('FekraUser.user_id' => $user_id));
            if (!empty($fekras)) {
                $this->loadModel('Transaction');
                $transactions = $this->Transaction->find('all', array('conditions' => array('Transaction.status' => array(2, 6), "(Transaction.fekra_phase_id is not null or Transaction.fekra_phase_id !='')")));
                foreach ($fekras as &$fekra) {
                    if (!empty($transactions)) {
                        foreach ($transactions as $trans) {
                            if ($fekra['Fekra']['id'] == $trans['Fekra']['id']) {
                                $fekra['Fekra']['allow_editing'] = 0;
                            } else {
                                $fekra['Fekra']['allow_editing'] = 1;
                            }
                        }
                    }
                }
            }
            $this->set(compact('fekras', 'h1', 'sub_title'));
            $this->render('my_ideas');
        }
    }

    function index() {
        $categories = $this->Fekra->Category->find('all', array('conditions' => array('Category.active' => 1)));
        $listCategories = array();

        if (!empty($categories)) {
            foreach ($categories as &$category) {
                $listCategories[$category['Category']['id']] = $category['Category'][$this->lang . '_title'];
                $category['Category']['fekras'] = $this->Fekra->FekraUser->find('count', array('conditions' => array('Fekra.category_id' => $category['Category']['id'], 'Fekra.published' => 1, 'FekraUser.type' => 1)));
            }
        }
        $priceRanges = array();
        for ($i = 500; $i <= 10000; $i+=500) {
            $priceRanges[$i] = $i;
        }
        $this->pageTitle = $h1 = __('Idea Categories', true);
        $this->get_countries();
        $this->set(compact('categories', 'h1', 'listCategories', 'priceRanges'));
    }

    function ideas($category_id = false) {
        $conditions = array();
        $conditions['FekraUser.type'] = 1;
        $conditions['Fekra.published'] = 1;
        if ($category_id) {
            $conditions['Fekra.category_id'] = $category_id;
            $category = $this->Fekra->Category->read(null, $category_id);
            $this->pageTitle = $h1 = $category['Category'][$this->lang . '_title'];
        } else {
            $this->pageTitle = $h1 = __('Search results', true);
        }
        $this->get_countries();

        $params = $this->params['url'];
        $filters = array();
        if (!empty($params['category_id'])) {
            $conditions['Fekra.category_id'] = $params['category_id'];
            $this->Fekra->Category->recursive = -1;
            $fcategory = $this->Fekra->Category->read(null, $params['category_id']);
            $filters['category_id'] = array('val' => $params['category_id'], 'text' => $fcategory['Category'][$this->lang . '_title']);
        }
        if (!empty($params['keyword'])) {
            $conditions[] = "MATCH (Fekra.title,Fekra.description) AGAINST ('{$params['keyword']}')";
            App::import('Helper', 'Text');
            $text = new TextHelper();
            $filters['keyword'] = array('val' => $params['keyword'], 'text' => $text->truncate($params['keyword'], 15));
        }

        if (!empty($params['price'])) {
//                $conditions["Fekra.price >= "] = $price[0];
            $conditions["Fekra.price <= "] = $params['price'];
            $filters['price'] = array('val' => $params['price'], 'text' => __('Up to', true) . " $" . $params['price']);
        }

        if (!empty($params['country'])) {
            $conditions['User.country_code'] = $params['country'];
            $this->Country->recursive = -1;
            $country = $this->get_countries($params['country']);
            $filters['country'] = array('val' => $params['country'], 'text' => $country);
        }
        if (!empty($filters)) {
            $this->set('filters', $filters);
        }
        $priceRanges = array();
        for ($i = 500; $i <= 10000; $i+=500) {
            $priceRanges[$i] = $i;
        }
        $this->paginate = array('limit' => 10);
        $fekras = $this->paginate('FekraUser', $conditions);

        $listCategories = $this->Fekra->Category->find('list', array('conditions' => array('Category.active' => 1), 'fields' => array('id', $this->lang . '_title')));
        $this->set(compact('h1', 'category', 'fekras', 'listCategories', 'priceRanges'));
    }

    function view($id = false, $permalink = false) {
        $this->loadModel('PollAnswer');
        $this->loadModel('FekraRating');
        $this->loadModel('BoardRating');
        $user = $this->is_user();

        $fekra = $this->Fekra->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $id, 'FekraUser.type' => 1)));
        $this->set('og_description', $fekra['Fekra']['description']);
        $team = $this->Fekra->FekraUser->find('all', array('conditions' => array('FekraUser.fekra_id' => $id), 'recursive' => 0));
        $videos = $this->Fekra->FekraVideo->find('all', array('conditions' => array('FekraVideo.fekra_id' => $id)));
        $comments_condition = array('FekraComment.fekra_id' => $id);
        if ($user['id'] != $fekra['FekraUser']['user_id']) {
            $comments_condition[] = array('FekraComment.approved' => 1);
        } else {
            $this->loadModel('Message');
            $this->set('messages', $this->Message->find('all', array('conditions' => array('Message.fekra_id' => $id))));
        }
        $comments = $this->Fekra->FekraComment->find('all', array('conditions' => $comments_condition, 'order' => 'FekraComment.created desc'));
        $boardratings = $this->BoardRating->find('all', array('conditions' => array('BoardRating.fekra_id' => $id), 'order' => 'BoardRating.created desc'));
        $poll_answer = $this->PollAnswer->find('all', array('conditions' => array('PollAnswer.fekra_user_id' => $fekra['FekraUser']['id'])));

        $roles = $this->Fekra->FekraUser->roles;
        $ratings = $this->BoardRating->ratings;
        $this->pageTitle = $h1 = $fekra['Fekra']['title'];
        $this->set(compact('fekra', 'h1', 'videos', 'poll_answer', 'business_plan', 'team', 'roles', 'comments', 'ratings', 'boardratings'));
    }

    function invest($id) {
        $user = $this->authenticate();
        $this->loadModel('Transaction');
        $phases = $this->Fekra->FekraPhase->find('all', array('conditions' => array('FekraPhase.fekra_id' => $id)));
        $this->Fekra->recursive = -1;
        $fekra = $this->Fekra->read(null, $id);
        $this->Transaction->recursive = -1;
        $transactions = $this->Transaction->find('all', array('conditions' => array('Transaction.status' => array(2, 6)), 'fields' => array('Transaction.total', 'Transaction.sender_id', 'Transaction.fekra_phase_id')));
        if (!empty($phases)) {

            foreach ($phases as &$phase) {
                $total = 0;

                if (!empty($transactions)) {
                    foreach ($transactions as $trans) {
                        if ($trans['Transaction']['fekra_phase_id'] == $phase['FekraPhase']['id'] && $trans['Transaction']['sender_id'] == $user['id'])
                            $total += $trans['Transaction']['total'];
                    }
                }
                $phase['FekraPhase']['payments'] = $total;
            }
        }

        $this->pageTitle = __('Invest in idea', true);
        $subtitle = __('Invest in idea', true) . ' ' . $fekra['Fekra']['title'];
        $h1 = __('Ideas', true);
        $this->set(compact('phases', 'h1', 'subtitle'));
    }

    function checkout($id) {
        $user = $this->authenticate();
        $this->loadModel('Transaction');
        $fekra = $this->Fekra->FekraPhase->read(null, $id);
//        debug($fekra);exit;
        $fekrau = $this->Fekra->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $fekra['Fekra']['id'], 'FekraUser.type' => 1)));
        if (isset($_GET['success']) && !empty($_GET['success'])) {
            $resArray = GetShippingDetails($_GET['token']);
            $ack = strtoupper($resArray["ACK"]);
            if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                $resArray = DoExpressCheckout($resArray['TOKEN'], $resArray['PAYERID'], $resArray['AMT'], CURRENCY_CODE);
//                debug($resArray);exit;
                $this->Transaction->id = $_GET['success'];
                if ($this->Transaction->save(array('Transaction' => array('status' => 5, 'transaction_id' => $resArray['PAYMENTINFO_0_TRANSACTIONID'])))) {
                    $transaction = $this->Transaction->read(null, $_GET['success']);
                    $this->set('transaction', $transaction);

                    $this->Email->to = $fekrau['User']['email'];
                    $name = 'FikraShop';
                    $this->Email->sendAs = 'html';
                    $this->Email->layout = 'contact';
                    $this->Email->template = 'invest';
                    $this->Email->from = $name . '<noreply@fikrashop.com>';
                    $this->Email->subject = $this->config['site_name'] . ': New investment to your idea' . $fekra['Fekra']['title'];
                    $this->Email->send();




                    $this->setFlash(__('The transaction has been saved successfully', true), 'success alert alert-success');
                    $this->redirect(array('controller' => 'transactions', 'action' => 'index'));
                }
            }
        }

        if (!empty($this->data)) {
            $this->Fekra->FekraPhase->validate['total'] = array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'numeric', 'message' => __('Enter a valid number', true)),
            );
            $this->Fekra->FekraPhase->validate['charged'] = array(
                array('rule' => 'notempty', 'message' => __('Required', true)),
                array('rule' => 'numeric', 'message' => __('Enter a valid number', true)),
            );
            $this->Fekra->FekraPhase->set($this->data);
            if ($this->Fekra->FekraPhase->validates()) {
                $datat['Transaction']['fekra_phase_id'] = $id;
                $datat['Transaction']['sender_id'] = $user['id'];
                $datat['Transaction']['receiver_id'] = $fekrau['FekraUser']['user_id'];
                $datat['Transaction']['actual'] = $this->data['FekraPhase']['charged'];
                $datat['Transaction']['total'] = $this->data['FekraPhase']['total'];
                $datat['Transaction']['first_name'] = $this->data['FekraPhase']['first_name'];
                $datat['Transaction']['last_name'] = $this->data['FekraPhase']['last_name'];
                $datat['Transaction']['email'] = $this->data['FekraPhase']['email'];
                $datat['Transaction']['status'] = 0;
                $this->Transaction->create();
                if ($this->Transaction->save($datat)) {
                    $tid = $this->Transaction->id;
                    $cancelURL = Router::url(array('action' => 'invest', $fekra['Fekra']['id']), true);
                    $returnURL = Router::url(array('action' => 'checkout', $id, '?' => array('success' => $tid)), true);
                    $itemName = 'Investment in idea ' . $fekra['Fekra']['title'];
                    $resArray = SetExpressCheckout($this->data['FekraPhase']['total'], CURRENCY_CODE, 'Sale', $returnURL, $cancelURL, $itemName);
                    $ack = strtoupper($resArray["ACK"]);
                    if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                        $token = urldecode($resArray["TOKEN"]);
                        RedirectToPayPal($token);
                    }
                }
            } else {
                $this->setFlash(__('Please complete investment information, and try again', true));
            }
        }

        if (empty($this->data)) {
            $this->data['FekraPhase'] = $user;
            $this->data['FekraPhase']['id'] = $id;
        }
        $this->pageTitle = __('Ideas', true) . ' - ' . __('Checkout', true);
        $h1 = __('Ideas', true);

        $this->set(compact('h1', 'fekra'));
    }

    function download_bussiness_plan($id = false) {
        $business_plan = $this->Fekra->read(null, $id);
        $pos = strrpos($business_plan ['Fekra']['file'], '.');
        if ($pos !== false) {
            $ext = low(substr($business_plan['Fekra']['file'], $pos + 1));
        }
        if (!empty($business_plan['Fekra']['file'])) {

            $file = ("{$business_plan['Fekra']['title']} Business plan.{$ext}" );
            $file_name = WWW_ROOT . 'img' . DS . 'uploads' . DS . $business_plan['Fekra']['file'];
        } else {
            echo 'not found';
        }
        header("Content-Disposition: attachment; filename=\"{$file}\"");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file_name));
        flush(); // this doesn't really matter.

        $fp = fopen($file_name, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }

        fclose($fp);
        die();
    }

    function add_rating() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $user = $this->is_user();
        if ($this->RequestHandler->isAjax()) {
            $this->loadModel('FekraRating');
            $fekra = $this->Fekra->FekraUser->find('count', array('conditions' => array('FekraUser.user_id' => $user['id'], 'FekraUser.fekra_id' => $_POST['idBox'], 'FekraUser.type' => 1)));
            if (!$fekra) {
                $count = $this->FekraRating->find('count', array('conditions' => array('FekraRating.user_id' => $user['id'], 'FekraRating.fekra_id' => $_POST['idBox'])));
                if (!$count) {
                    $this->FekraRating->create();
                    $this->FekraRating->save(array('FekraRating' => array('user_id' => $user['id'], 'fekra_id' => $_POST['idBox'], 'rating' => $_POST['rate'])));
                    $data['success'] = 1;
                    $data ['message'] = __('Thank you for your rating', true);
                } else {
                    $data['error'] = 1;
                    $data['message'] = __('You have already rate this idea', true);
                }
            } else {
                $data['error'] = 1;
                $data['message'] = __('You can not rate your idea', true);
            }
            echo json_encode($data);
            exit;
        }
    }

    function add_board_rating() {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $user = $this->is_user();

        $this->loadModel('BoardRating');
        $count = $this->BoardRating->find('first', array('conditions' => array('BoardRating.user_id' => $user['id'], 'BoardRating.fekra_id' => $this->data['BoardRating']['fekra_id'])));

        if (!empty($this->data['BoardRating']['rating'])) {
            if (!empty($count)) {
                $this->data['BoardRating']['id'] = $count['BoardRating']['id'];
            } else {
                $this->BoardRating->create();
            }
            $this->data['BoardRating']['user_id'] = $user['id'];

            if ($this->BoardRating->save($this->data)) {
                $this->setFlash(__('Thank you for your rating', true), 'success', 'rate');
                $this->redirect(array('action' => 'view', $this->data['BoardRating']['fekra_id'], '#' => 'BordRating'));
            } else {
                $this->setFlash(__('Sorry, Your rating has not been saved', true), 'alert alert-error fail', 'rate');
                $this->redirect(array('action' => 'view', $this->data['BoardRating']['fekra_id'], '#' => 'BordRating'));
            }
        } else {
            $this->setFlash(__('Please, Choose Rating', true), 'alert alert-error fail', 'rate');
            $this->redirect(array('action' => 'view', $this->data['BoardRating']['fekra_id'],
                '#' => 'BordRating'));
        }
    }

    function add() {
        $user = $this->authenticate();

        if (!empty($this->data)) {
            $this->Fekra->create();
            if ($this->Fekra->save($this->data)) {
                $id = $this->Fekra->getLastInsertID();
                $this->Fekra->FekraUser->create();
                $this->Fekra->FekraUser->save(array('FekraUser' => array('user_id' => $user['id'], 'fekra_id' => $id, 'type' => 1)));
                $fekra_user = $this->Fekra->FekraUser->getLastInsertID();

                $this->Fekra->addIdeaAssets($this->data);
                $this->Fekra->addPollAnswers($this->data, $fekra_user);
                $this->setFlash(__('The idea has been saved', true), 'success');
                $this->redirect(array('action' => 'user_ideas'));
            } else {
                $this->setFlash(__('The idea could not be saved. Please, try again.', true), 'alert alert-error fail');
            }
        } $this->pageTitle = $h1 = __('Add Idea', true);
        $categories = $this->Fekra->Category->find('list', array('fields' => array($this->lang . '_title')));
        $types = array(1 => __('Youtube', true), __('File', true));
        $this->set('info', $this->Fekra->FekraVideo->getFileSettings());
        $this->set('binfo', $this->Fekra->getFileSettings());
        $this->set('attachmentInfo', $this->Fekra->FekraPhase->getFileSettings());
        $this->set(compact('categories', 'types', 'h1'));
    }

    function edit($id = false) {

        $user = $this->authenticate();
        $fekra_user = $this->Fekra->FekraUser->find('first', array('conditions' => array('FekraUser.fekra_id' => $id, 'FekraUser.user_id' => $user['id'])));
        if (!$id || empty($fekra_user)) {
            $this->setFlash('Invalid Idea', 'alert alert-error fail');
            $this->redirect(array('action' => 'user_ideas'));
        }

        if (!empty($this->data)) {
            $this->Fekra->create();
            if ($this->Fekra->save($this->data)) {
                $this->Fekra->addIdeaAssets($this->data);
                $this->Fekra->addPollAnswers($this->data, $fekra_user['FekraUser']['id']);
                $this->setFlash(__('The idea has been saved', true), 'success');
                $this->redirect(array('action' => 'user_ideas'));
            } else {
                $this->setFlash(__('The idea could not be saved. Please, try again.', true), 'alert alert-error fail');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Fekra->read(null, $id);
        }
        $this->pageTitle = $h1 = __('Edit Idea', true);
        $categories = $this->Fekra->Category->find('list', array('fields' => array($this->lang . '_title')));
        $types = array(1 => __('Youtube', true), __('File', true));
        $this->set('info', $this->Fekra->FekraVideo->getFileSettings());
        $this->set('binfo', $this->Fekra->getFileSettings());
        $this->set('fekra_user', $fekra_user['FekraUser']['id']);
        $this->set(compact('categories', 'types', 'h1'));
        $this->render('add');
    }

    function delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for idea', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'user_ideas'));
        }
        if ($this->Fekra->delete($id)) {
            $this->setFlash(__('Idea deleted', true), 'success alert alert-success');
            $this->redirect(array('action' => 'user_ideas'));
        } $this->setFlash(__('Idea can not be deleted', true), ' fail alert alert-error');
        $this->redirect(array('action' => 'user_ideas'));
    }

    function view_phase($id) {
        $user = $this->authenticate();
        $this->loadModel('FekraPhase');
        $phase = $this->FekraPhase->find('first', array('conditions' => array('FekraPhase.id' => $id)));

        $h1 = __('Ideas', true);
        $this->pageTitle = __('View Idea Phase', true);
        $this->set(compact('phase', 'h1'));
    }

}
