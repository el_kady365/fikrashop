<?php

class BannersController extends AppController {

    var $name = 'Banners';

    /**
     * @var Banner
     */
    var $Banner;
    var $helpers = array('Html', 'Form');

    function admin_index() {
        $this->Banner->recursive = 0;
        $this->set('banners', $this->paginate());
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid Banner', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('banner', $this->Banner->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Banner->create();
            if ($this->Banner->save($this->data)) {
                $this->setFlash(__('The Banner has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The Banner could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->set('info', $this->Banner->getImageSettings());
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid Banner', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Banner->save($this->data)) {
                $this->setFlash(__('The Banner has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The Banner could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Banner->read(null, $id);
        }
        $this->set('info', $this->Banner->getImageSettings());
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for Banner', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Banner->delete($id)) {
            $this->setFlash(__('Banner deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('The Banner could not be deleted. Please, try again.', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['form']['operation'];
        if ($operation == 'delete') {
            if ($this->Banner->deleteAll(array('Banner.id' => $ids), true, true)) {
                $this->setFlash('Banner deleted successfully', 'alert alert-success');
            } else {
                $this->setFlash('Banner can not be deleted', 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}

?>
