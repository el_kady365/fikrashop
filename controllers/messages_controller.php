<?php

class MessagesController extends AppController {

    var $name = 'Messages';

    /**
     * @var Message */
    var $Message;
    var $components = array('Email');

    function index() {
        $this->Message->recursive = 0;
        $this->set('messages', $this->Message->find('all'));
    }

    function read($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid message', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->Message->create();
        $this->Message->id = $id;
        $this->Message->saveField('read', 1);
        $message = $this->Message->read(null, $id);
        $this->pageTitle = __('View message', true) . ' - ' . $message['Message']['subject'];
        $this->set('attachmentInfo', $this->Message->MessageAttachment->getFileSettings());
        $h1 = __('Messages', true);
        $this->set(compact('message', 'h1'));
    }

    function reply($id) {
        $user = $this->authenticate();
        $cmessage = $this->Message->read(null, $id);
        $receiver_id = $cmessage['Message']['sender_id'];
        if ($cmessage['Message']['sender_id'] == $user['id']) {
            $receiver_id = $cmessage['Message']['receiver_id'];
        }
        $subject = strstr($cmessage['Message']['subject'], __('Re:', true)) ? $cmessage['Message']['subject'] : __('Re:', true) . ' ' . $cmessage['Message']['subject'];
        if (!empty($this->data)) {
            if (!empty($this->data)) {
                $this->data['Message']['sender_id'] = $user['id'];
                $this->data['Message']['receiver_id'] = $receiver_id;
                $this->data['Message']['subject'] = $subject;
                $this->data['Message']['read'] = 0;
                $this->data['Message']['fekra_id'] = $cmessage['Message']['fekra_id'];
                $this->Message->create();
                if ($this->Message->save($this->data)) {
                    $this->Message->addMessageAttachments($this->data);


                    $message = $this->Message->read(null, $this->Message->id);
                    $this->set('data', $message);
                    $this->Email->to = $message['Receiver']['email'];
                    $name = $message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'];
                    $this->Email->sendAs = 'html';
                    $this->Email->layout = 'contact';
                    $this->Email->template = 'sent';
                    $this->Email->attachments = $this->Message->get_attachments($this->Message->id);
                    $this->Email->from = $name . '<noreply@fikrashop.com>';
                    $this->Email->subject = $subject;
                    if ($this->Email->send()) {
                        $this->data = array();
                        $this->setFlash(__('Your message has been sent.', true), 'success');
                    } else {
                        $this->setFlash(__("can't send email", true), 'fail');
                    }
//                $this->setFlash(__('The message has been sent', true), 'success alert alert-success');
                    $this->redirect(array('action' => 'read', $cmessage['Message']['id']));
                } else {
                    $this->setFlash(__('The message could not be sent. Please, try again.', true), 'fail alert alert-error');
                    $this->redirect(array('action' => 'read', $cmessage['Message']['id']));
                }
            }
        }
    }

    function send_message($user_id = false) {
        $user = $this->authenticate();
        if (!$user_id) {
            $this->setFlash(__('Invalid User', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'inbox'));
        }
        if (isset($_GET['idea']) && !empty($_GET['idea'])) {
            $fekra_id = $_GET['idea'];
        }

        $user_id = base64_decode($user_id);
        if (!empty($this->data)) {
            $this->data['Message']['sender_id'] = $user['id'];
            $this->data['Message']['read'] = 0;
            $this->Message->create();
            if ($this->Message->save($this->data)) {
                $this->Message->addMessageAttachments($this->data);

                $message = $this->Message->read(null, $this->Message->id);
                $this->set('data', $message);
                $this->Email->to = $message['Receiver']['email'];
                $name = $message['Receiver']['first_name'] . ' ' . $message['Receiver']['last_name'];
                $this->Email->sendAs = 'html';
                $this->Email->layout = 'contact';
                $this->Email->template = 'sent';
                $this->Email->attachments = $this->Message->get_attachments($this->Message->id);
//                $this->Email->delivery = 'debug';
                $this->Email->from = $name . '<noreply@fikrashop.com>';
                $this->Email->subject = __('You received a new message from', true) . $message['Sender']['first_name'];
                if ($this->Email->send()) {
                    $this->data = array();
                    $this->setFlash(__('Your message has been sent.', true), 'success');
                } else {
                    $this->setFlash(__("can't send email", true), 'fail');
                }
//                $this->setFlash(__('The message has been sent', true), 'success alert alert-success');
                $this->redirect(array('action' => 'inbox'));
            } else {
                $this->setFlash(__('The message could not be sent. Please, try again.', true), 'fail alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data['Message']['receiver_id'] = $user_id;
            $this->data['Message']['fekra_id'] = $fekra_id;
        }

        $this->pageTitle = __('Messages', true);
        $h1 = __('Send Message', true);
        $this->set('attachmentInfo', $this->Message->MessageAttachment->getFileSettings());
        $this->set(compact('h1'));
    }

    function inbox() {
        $user = $this->authenticate();
        $receiver_conditions = array('Message.receiver_id' => $user['id'], '(Message.receiver_deleted is Null Or Message.receiver_deleted=0)');
        $sender_conditions = array('Message.sender_id' => $user['id'], '(Message.sender_deleted is Null Or Message.sender_deleted=0)');
        $this->paginate = array('limit' => 10, 'order' => 'Message.created desc');
        $sent = $this->paginate('Message', $sender_conditions);
        $inbox = $this->paginate('Message', $receiver_conditions);
        $this->pageTitle = $h1 = __('Messages', true);
        $this->set(compact('inbox', 'h1', 'sent'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for message', true), 'fail alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        $message = $this->Message->read(null, $id);
        $user = $this->authenticate();
        if ($message['Message']['receiver_id'] == $user['id']) {
            if ($message['Message']['sender_deleted'] == 1) {
                $this->Message->delete($id);
            } else {
                $this->Message->create();
                $this->Message->id = $id;
                $this->Message->saveField('receiver_deleted', 1);
            }
        } elseif ($message['Message']['sender_id'] == $user['id']) {
            if ($message['Message']['receiver_deleted'] == 1) {
                $this->Message->delete($id);
            } else {
                $this->Message->create();
                $this->Message->id = $id;
                $this->Message->saveField('sender_deleted', 1);
            }
        }
        $this->setFlash(__('Message deleted', true), 'success alert alert-success');
        $this->redirect(array('action' => 'inbox'));
    }

    function do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Message->deleteAll(array('Message.id' => $ids))) {
                $this->setFlash(__('Message deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Message can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
