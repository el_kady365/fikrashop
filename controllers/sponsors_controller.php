<?php

class SponsorsController extends AppController {

    var $name = 'Sponsors';

    /**
     * @var Sponsor */
    var $Sponsor;

    function admin_index() {
        $this->Sponsor->recursive = 0;
        $this->set('sponsors', $this->Sponsor->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid sponsor', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('sponsor', $this->Sponsor->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Sponsor->create();
            if ($this->Sponsor->save($this->data)) {
                $this->setFlash(__('The sponsor has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The sponsor could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $this->set('info',  $this->Sponsor->getImageSettings());
        
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid sponsor', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Sponsor->save($this->data)) {
                $this->setFlash(__('The sponsor has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The sponsor could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Sponsor->read(null, $id);
        }
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for sponsor', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Sponsor->delete($id)) {
            $this->setFlash(__('Sponsor deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('Sponsor was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->Sponsor->deleteAll(array('Sponsor.id' => $ids))) {
                $this->setFlash(__('Sponsor deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('Sponsor can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
