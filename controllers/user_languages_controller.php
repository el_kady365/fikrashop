<?php

class UserLanguagesController extends AppController {

    var $name = 'UserLanguages';

    /**
     * @var UserLanguage */
    var $UserLanguage;

    function admin_index() {
        $this->UserLanguage->recursive = 0;
        $this->set('userLanguages', $this->UserLanguage->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid user language', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('userLanguage', $this->UserLanguage->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->UserLanguage->create();
            if ($this->UserLanguage->save($this->data)) {
                $this->setFlash(__('The user language has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user language could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        $users = $this->UserLanguage->User->find('list');
        $this->set(compact('users'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->setFlash(__('Invalid user language', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->UserLanguage->save($this->data)) {
                $this->setFlash(__('The user language has been saved', true), 'alert alert-success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash(__('The user language could not be saved. Please, try again.', true), 'alert alert-error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->UserLanguage->read(null, $id);
        }
        $users = $this->UserLanguage->User->find('list');
        $this->set(compact('users'));
        $this->render('admin_add');
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->setFlash(__('Invalid id for user language', true), 'alert alert-error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->UserLanguage->delete($id)) {
            $this->setFlash(__('User language deleted', true), 'alert alert-success');
            $this->redirect(array('action' => 'index'));
        }
        $this->setFlash(__('User language was not deleted', true), 'alert alert-error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_do_operation() {
        $ids = $this->params['form']['chk'];
        $operation = $this->params['url']['action'];
        if ($operation == 'delete') {
            if ($this->UserLanguage->deleteAll(array('UserLanguage.id' => $ids))) {
                $this->setFlash(__('User language deleted successfully', true), 'alert alert-success');
            } else {
                $this->setFlash(__('User language can not be deleted', true), 'alert alert-error');
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    function add() {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            if (!empty($this->data)) {
                $this->UserLanguage->create();
                $this->data['UserLanguage']['user_id'] = $user['id'];
                if ($this->UserLanguage->save($this->data)) {
                    $return['message'] = __('Your Language has been added successfully', true);
                    $return['status'] = 1;
                } else {

                    $return['message'] = __('The Language could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            }
            $this->set('levels', $this->UserLanguage->levels);
        }
    }

    function edit($id = false) {
        Configure::write('debug', 0);
        $this->layout = false;
        $user = $this->is_user();
        if ($user) {
            $this->UserLanguage->recursive = -1;
            if (!empty($this->data)) {
                $this->UserLanguage->create();
                $this->data['UserLanguage']['user_id'] = $user['id'];
                if ($this->UserLanguage->save($this->data)) {
                    $return['message'] = __('Your Language has been modified successfully', true);
                    $return['status'] = 1;
                } else {
                    $return['message'] = __('The Language could not be saved. Please, try again.', true);
                    $return['status'] = 0;
                }
                echo json_encode($return);
                exit();
            } else {
                $this->data = $this->UserLanguage->read(null, $id);
            }
            $this->set('levels', $this->UserLanguage->levels);
            $this->render('add');
        }
    }
    function delete($id = null) {
         $return = array();
        if ($this->UserLanguage->delete($id)) {
            $return['message'] = __('Your language deleted successfully', true);
            $return['status'] = 1;
        } else {
            $return['message'] = __('Your language cannot be deleted', true);
            $return['status'] = 0;
        }
        echo json_encode($return);
        exit();
    }

}
